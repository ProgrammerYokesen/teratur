<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',function () {
//   return Redirect::to(env('APP_URL'));
// });

// tokopedia api route
Route::get('/order-tokped', 'tokopediaController@orderTokped')->name('orderTokped');
Route::get('/tracking/order/status', 'tokopediaController@orderStatus')->name('orderStatus');
Route::get('/chat-tokped', 'tokopediaController@chat')->name('chat');
// Route::get('/chat-tokped1', 'tokopediaController@chatTokped1')->name('chatTokped1');
Route::get('admin/cetak/label/{kode}', 'tokopediaController@cetakLabel')->name('cetakLabel');
Route::get('/auto-accept-order', 'tokopediaController@AutoAcceptOrder')->name('AutoAcceptOrder');
Route::get('/getProducts', 'tokopediaController@getProducts')->name('getProducts');
Route::get('/sku', 'tokopediaController@productSku')->name('productSku');
Route::get('/campaign', 'tokopediaController@campaign')->name('campaign');
Route::get('/label/{id}', 'tokopediaController@label')->name('label');
Route::get('/tokped-label/1', 'tokopediaController@tokpedLabel')->name('tokpedLabel');
Route::get('/tokped-label/2', 'tokopediaController@tokpedLabel2')->name('tokpedLabel2');
Route::get('/discount-tokped', 'tokopediaController@discount')->name('discount');
Route::get('/tokped-cek', 'tokopediaController@cek')->name('discount');
Route::get('/tokped-update-stock', 'tokopediaController@updateStock')->name('discount');
// webhooks
Route::post('/v1/chat/notification', 'tokopediaController@chatWebhooks')->name('chatWebhooks');
Route::get('/cek-single-product', 'tokopediaController@cekSingleProduct')->name('cekSingleProduct');
Route::get('/isi-label', 'tokopediaController@isiLabel')->name('isiLabel');

// blibli
Route::get('/get-order-blibli', 'blibliController@getOrder')->name('getOrderBlibli');
Route::get('/getBlibliProduct', 'blibliController@getBlibliProduct')->name('getBlibliProduct');
Route::get('/get-order-status', 'blibliController@orderStatusBlibli')->name('orderStatusBlibli');
Route::get('/tes-sku', 'blibliController@tesSku')->name('tesSku');
Route::get('/blibli-voucher', 'blibliController@sellerVoucher')->name('sellerVoucher');
Route::get('/blibli-discount', 'blibliController@sellerDiscount')->name('sellerDiscount');
Route::get('/blibli-promo', 'blibliController@blibliDiscount')->name('blibliDiscount');
Route::get('/blibli-check-discount', 'blibliController@getBlibliDiscount')->name('getBlibliDiscount');
Route::get('/blibli-check', 'blibliController@cekBlibli')->name('cekBlibli');

// shopee
Route::get('/create-auth/{id}', 'shopeeController@createAuth')->name('createAuth');
Route::get('/chat-create-auth/{id}', 'shopeeController@createChatAuth')->name('createChatAuth');
Route::get('/get-order', 'shopeeController@getShopeeOrder')->name('getShopeeOrder');
// Route::get('/get-chat', 'shopeeController@shopeeChat')->name('shopeeChat');
Route::get('/afterRedirect', 'shopeeController@afterRedirect')->name('afterRedirect');
Route::get('/afterRedirectChat', 'shopeeController@afterRedirectChat')->name('afterRedirectChat');
Route::get('/shopeeAutoAccepOrder', 'shopeeController@shopeeAutoAccepOrder')->name('shopeeAutoAccepOrder');
// Route::get('/tes', 'shopeeController@tes')->name('tes');
Route::get('/get-products', 'shopeeController@getShopeeProducts')->name('getShopeeProducts');
Route::post('/shopee/chat/notification', 'shopeeController@chatWebhooks')->name('chatWebhooks');
Route::post('/shopee/webhooks', 'shopeeController@shopeeWebhooks')->name('shopeeWebhooks');
Route::post('/shopee/tes', 'shopeeController@getLabelShopee')->name('getLabelShopee');
Route::get('/cek-shopee', 'shopeeController@cekShopee')->name('cekShopee');
Route::get('/cek-tracking-number', 'shopeeController@cekTrackingNumber')->name('cekTrackingNumber');
Route::get('/shopeeLabel', 'shopeeController@shopeeLabel')->name('shopeeLabel');

Route::get('/shopee-product', 'shopeeController@oneProduct')->name('oneProduct');
Route::get('/shopee-voucher', 'shopeeController@voucher')->name('voucher');
Route::get('/shopee-addon', 'shopeeController@addon')->name('addon');
Route::get('/shopee-discount', 'shopeeController@discount')->name('discount');
Route::get('/shopee-getDiscountStatistik', 'shopeeController@getDiscountStatistik')->name('getDiscountStatistik');
Route::get('/shopee-cekorderan', 'shopeeController@cekorderan')->name('cekorderan');
Route::get('/shopee-refreshToken', 'shopeeController@shopeeRefreshToken')->name('shopeeRefreshToken');
Route::get('/shopee-refreshChatToken', 'shopeeController@shopeeRefreshChatToken')->name('shopeeRefreshChatToken');
Route::get('/teratur-update-shopee', 'shopeeController@TeraturShopee')->name('TeraturShopee');
// JDID
Route::get('/get-auth/{id}', 'JdidController@getAuth')->name('getAuth');
Route::get('/jdid-redirect', 'JdidController@jdidRedirect')->name('jdidRedirect');
Route::get('/jdid-getorder', 'JdidController@jdidOrder')->name('jdidOrder');
Route::get('/jdid-getproduct', 'JdidController@getProductJDID')->name('jdidgetProduct');
Route::get('/jdid-label', 'JdidController@jdidLabel')->name('jdidLabel');
Route::get('/jdid-cek', 'JdidController@cekJdid')->name('cekJdid');
Route::get('/jdid-update-orderstatus', 'JdidController@cekJdidOrderStatus')->name('cekJdidOrderStatus');

// route process
// Route::get('/admin/balas-page/{id}/{platform}', 'ProcessController@balasPage')->name('balasPage');
// Route::post('/admin/balas/{id}/{platform}', 'ProcessController@balas')->name('balas');
Route::get('/admin/balas/{id}', 'PageController@balasPage')->name('balasPage');
Route::post('submit-balas/', 'ProcessController@balasChat')->name('balasChat');
Route::get('/admin/products', 'PageController@products')->name('products');
Route::get('/admin/cek-invoice/{id}', 'ProcessController@cekInvoice')->name('cekInvoice');
Route::get('/admin/proses-pengemasan/{id}', 'ProcessController@prosesPengemasan')->name('prosesPengemasan');
Route::get('/admin/edit-invoice/{id}', 'ProcessController@editInvoice')->name('editInvoice');
Route::get('/admin/logistic/{id}', 'ProcessController@logistic')->name('logistic');
Route::post('update-logistic', 'ProcessController@updateLogistic')->name('updatePLogistic');
Route::post('update-packing', 'ProcessController@updatePacking')->name('updatePacking');
Route::post('admin/search/order/{type}', 'ProcessController@searchOrder')->name('searchOrder');
Route::post('/admin/submit/edit-invoice/{id}', 'ProcessController@submitEditInvoice')->name('submitEditInvoice');
Route::get('/admin/print/{path}', 'ProcessController@print')->name('print');
Route::get('admin/filter-date', 'ProcessController@filterDate')->name('filterDate');
Route::get('/update-order/{id}', 'ProcessController@updateOrderCekInvoice')->name('updateOrderCekInvoice');
Route::get('/update/order/{station}', 'ProcessController@updateOrder')->name('updateOrder');
Route::get('/pdf-sticker/{id}', 'ProcessController@pdfSticker')->name('pdfSticker');
Route::get('/filter-marketplace/{marketplace}', 'ProcessController@filterMarketplace')->name('filterMarketplace');
Route::get('/update-product/{store}/{marketplace}', 'ProcessController@updateProductPerStore')->name('updateProductPerStore');
Route::get('/proses-shopee-blibli/{bookinganId}', 'ProcessController@prosesShopeeBlibli')->name('prosesShopeeBlibli');

// statistic
Route::get('/product-statistic', 'StatisticController@productStatistic')->name('productStatistic');
Route::get('/topcity-totalorder-statistic', 'StatisticController@topCityBasedOnTotalOrder')->name('topCityBasedOnTotalOrder');
Route::get('/marketplace-totalorder-statistic', 'StatisticController@totalOrderByMarketplace')->name('totalOrderByMarketplace');
Route::get('/topBuyer-totalorder-statistic', 'StatisticController@topBuyer')->name('topBuyer');
Route::get('/cancellation-rate-statistic', 'StatisticController@cancellationRate')->name('cancellationRate');
Route::get('/tokopedia-demographic-statistic', 'StatisticController@demographicTokped')->name('demographicTokped');
Route::get('/iterationc-statistic', 'StatisticController@iteration')->name('iteration');

// SHD routes
Route::get('/sinde-login', 'SindeHomeDeliveryController@sindeLogin')->name('sindeLogin');
Route::get('/sinde-list-order', 'SindeHomeDeliveryController@getListOrder')->name('getListOrder');
Route::get('/sinde-get-products', 'SindeHomeDeliveryController@getProducts')->name('getProducts');
Route::get('/tes-sql', 'SindeHomeDeliveryController@tesSql')->name('tesSql');
Route::get('/tes-raw-sql', 'SindeHomeDeliveryController@ranked')->name('ranked');

// chat
Route::get('admin/chat/{toko?}', 'ProcessController@chatToko')->name('chatToko');
// Route::get('admin/chat/', 'ProcessController@chat')->name('chat');

// Dashboard
Route::get('admin/dashboard/', 'DashboardController@dashboardPage')->name('dashboardPage');
Route::get('admin/dashboard/history/', 'DashboardController@historyPage')->name('historyPage');

// route station
Route::get('/admin/pack/{id}', 'PageController@pack')->name('pack');
Route::get('/admin/drop', 'PageController@drop')->name('drop');
Route::get('/admin/done', 'PageController@done')->name('done');
Route::post('/admin/done-detail', 'PageController@detailDone')->name('detailDone');
Route::get('/admin/pick', 'PageController@pick')->name('pick');
Route::get('/update/order/{station}/{bookinganId}/{count}', 'ProcessController@updateOrder')->name('updateOrder');

Route::get('/total', 'ProcessController@totalOrder')->name('totalOrder');
// Route::get('/order/station-satu', 'ProcessController@orderStationSatu')->name('orderStationSatu');

// Route::get('/station/pack', 'PageController@pack')->name('packPage');
// Route::get('/station/drop', 'PageController@drop')->name('dropPage');
// Route::get('/station/pick-up', 'PageController@pick')->name('pickPage');
// Route::get('/stations', 'PageController@station')->name('stationPage');

// harbolnas
Route::post('/harbolnas', 'HarbolnasController@getData')->name('getData');
// Route::get('/invoice-harbolnas/{id}', 'HarbolnasController@invoiceHarbolnas')->name('invoiceHarbolnas');
Route::get('/invoice-harbolnas', 'HarbolnasController@invoiceHarbolnas')->name('invoiceHarbolnas');
Route::get('/getDataHarbolnas', 'csbController@getDataHarbolnas')->name('getDataHarbolnas');
Route::get('/getOrderHarbolnas1111', 'HarbolnasController@orderHarbolnas')->name('orderHarbolnas');
Route::get('/getOrderHarbolnas1212', 'HarbolnasController@harbolnas1212')->name('harbolnas1212');
Route::get('/change-station', 'HarbolnasController@changeStation')->name('changeStation');

// distribution route
Route::get('admin/distribute/maker/step1','DistributionController@distribute_step_1')->name('distributeStep1');
Route::get('admin/distribute/maker/step2','DistributionController@distribute_step_2')->name('distributeStep2');
Route::post('admin/distribute/maker/step3','DistributionController@distribute_step_3')->name('distributeStep3');
Route::get('admin/distribute/maker/cancel','DistributionController@distribute_cancel')->name('distributeCancel');
Route::get('admin/distribute/maker/submit','DistributionController@distribute_submit')->name('distributeSubmit');
Route::get('admin/distribute/terima/{id}','DistributionController@distribute_terima')->name('distributeTerima');
Route::post('admin/distribute/verikasi/{id}','DistributionController@distribute_verifikasi')->name('distributeVerifikasi');

Route::get('admin/stok/opname','DistributionController@stockOpname')->name('viewStockOpname');
Route::post('admin/stok/opname/doing/{id}','DistributionController@stockOpnameDoing')->name('stockOpnameDoing');

Route::get('/admin/products/paket/cari/{id}','DistributionController@productList')->name('productPaketCariProduct');
Route::get('/admin/detail-conversion/{id}','DistributionController@convertionList')->name('convertionList');

Route::get('products/update/{id}','DistributionController@productEdit')->name('productEdit');
Route::get('products/delete/{id}','DistributionController@productDelete')->name('productDelete');
Route::post('products/updating/{id}','DistributionController@productUpdating')->name('productUpdating');
Route::post('products-assembly/{id}','DistributionController@productAssembly')->name('productAssembly');
Route::get('product/image-display/{id}/{gbr}','DistributionController@imageDelete')->name('productImgDelete');
Route::post('product/add-display/{id}','DistributionController@displayAdd')->name('productAddDisplay');
Route::get('products/delete/{id}','DistributionController@productDelete')->name('productDelete');
Route::get('product/image-detail/{id}/{gbr}','DistributionController@detailDelete')->name('productDtlDelete');
Route::post('product/add-detail/{id}','DistributionController@detailAdd')->name('productAddDetail');
Route::get('admin/products76/add-product','DistributionController@productUpload')->name('productUpload');
Route::post('products/save','DistributionController@productUploadSave')->name('productUploadSave');
Route::post('products/paket/save','DistributionController@productPaketUpload')->name('productPaketUploadSave');
Route::get('products/paket/remove/{id}/{rm}','DistributionController@productPaketKurang')->name('productPaketKurang');
Route::get('tes2','DistributionController@tes2')->name('tes2');
Route::get('/admin/delete-product/{index}','DistributionController@deleteProduct')->name('deleteProduct');
// Route::get('admin/induk_produk/add-product','AdminIndukProdukController@productUpload')->name('productUpload');

// edit products
Route::get('admin/list_products/edit-products/{id}','PageController@editProductPage')->name('editProductPage');
Route::get('admin/induk-produk/{toko}','ProcessController@cariIndukProduk')->name('cariIndukProduk');
Route::post('admin/edit-produk/{id}','ProcessController@editProduk')->name('editProduk');
Route::get('admin/detail-induk-edit/{id}','ProcessController@detailIndukEdit')->name('detailIndukEdit');

Route::get('update-penjualan','DistributionController@updatePenjualan')->name('updatePenjualan');



// create assembly
Route::get('product/assembly/create','DistributionController@assemblyCreate')->name('productCreateAssembly');
Route::get('product/assembly/update/{id}','DistributionController@assemblyUpdate')->name('productUpdateAssembly');
Route::post('products/paket/add/{id}','DistributionController@productPaketTambah')->name('productPaketTambah');

Route::get('penjualan-dummy/{kode}/{qty}/{toko}','ProcessController@penjualanDummy')->name('penjualanDummy');

// penjualan offline
Route::get('orderan/maker/step1','ProcessController@createOrderanStep1')->name('createOrderanStep1');
Route::post('orderan/making/step1','ProcessController@creatingOrderStep1')->name('creatingOrderStep1');
Route::get('orderan/maker/{id}/step2','ProcessController@createOrderanStep2')->name('createOrderanStep2');
Route::post('orderan/making/{id}/step2','ProcessController@creatingOrderStep2')->name('creatingOrderStep2');
Route::get('orderan/maker/{id}/submit','ProcessController@orderan_submit')->name('orderanSubmit');
Route::get('orderan/maker/cancel','ProcessController@orderan_cancel')->name('orderanCancel');
Route::get('orderan/substract/{id}','ProcessController@substractOrderan')->name('substractOrderan');

Route::get('orderan-stock-awal','ProcessController@createStockAwal')->name('createStockAwal');

// warisan
Route::get('hash-key','WarisanController@createHashKey')->name('createHashKey');
Route::get('warisan-getProducts','WarisanController@getProduct')->name('getProduct');
Route::get('warisan-invoice/{orderId}','WarisanController@createInvoiceWarisan')->name('createInvoiceWarisan');
Route::get('change-invoice','WarisanController@changeInvoice')->name('changeInvoice');

// label
Route::get('send-label','LabelController@SendImage')->name('SendImage');
Route::get('fix-label','LabelController@fixLabel')->name('fixLabel');


Route::get('double-product','ProcessController@doubleProduct')->name('doubleProduct');
Route::get('update-stock-product','ProcessController@updateBarang')->name('updateBarang');
Route::get('update-toko','ProcessController@changeToko')->name('changeToko');

Route::get('new-tokped-label-flow','LabelController@tesNewFlowTokpedLabel')->name('tesNewFlowTokpedLabel');

// lab
Route::get('lab-queue/{id}','LabController@insertQueueTime')->name('insertQueueTime');
Route::get('lab-time','LabController@testTime')->name('testTime');
