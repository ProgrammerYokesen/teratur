<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-order/{startTime}','csbController@getOrder');
Route::get('get-order-stw','csbController@getOrderStw');
Route::get('get-order/bookingan/{startTime}','csbController@getOrderBookingan');

// warisan
Route::get('warisan-insert-client','WarisanController@warisanInsertClient');
Route::get('warisan-searchproducts','WarisanController@searchProductsWarisan')->name('searchProductsWarisan');
Route::get('warisan-getproducts','WarisanController@getProductsWarisan')->name('getProductsWarisan');
Route::post('warisan-createproducts','WarisanController@createProductToTeratur')->name('createProductToTeratur');
Route::post('warisan-postorders','WarisanController@getOrderWarisan')->name('getOrderWarisan');
