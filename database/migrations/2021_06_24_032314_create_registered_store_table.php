<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisteredStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_store', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('store_name')->nullable();
            $table->integer('data_toko')->nullable();
            $table->integer('shop_id')->nullable();
            $table->string('access_token')->nullable();
            $table->dateTime('access_token_created')->nullable();
            $table->integer('expired_in_second')->nullable();
            $table->string('refresh_token')->nullable();
            $table->string('seller_api_key')->nullable();
            $table->string('link_toko')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_store');
    }
}
