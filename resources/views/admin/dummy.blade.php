<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <style>
        .container {
            max-width: 1320px;
            margin-right: auto;
            margin-left: auto;
            width: 100%;
            position: relative;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            vertical-align: top;
            border-color: #bbbbbb;
            border: 1px solid #bbbbbb;
            caption-side: bottom;
            border-collapse: collapse;
            border-radius: 5px
        }

        .table>tbody {
            vertical-align: inherit;
        }

        tbody,
        td,
        tfoot,
        th,
        thead,
        tr {
            border-color: inherit;
            border-style: solid;
            border-width: 1px;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #dedede;
            color: black;
        }

        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }

        .table-no {
            border: none !important;
            text-align: left;
        }

        .table-no tbody,
        .table-no td,
        .table-no tfoot,
        .table-no th,
        .table-no thead,
        .table-no tr {
            border-style: none;
        }

    </style>

</head>

<body>
    <section class="invoice container">
        <div class="row page-header">
            <div class="col-4">
                <img src="https://teratur.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
            </div>
            <div class="col-4">
            </div>
            <div class="col-4">
            </div>
        </div>

        <table class="table table-no" style="margin-top: 3rem">
            <thead>
                <tr>
                    <td><b> Dari </b></td>
                    <td><b> To </b></td>
                    <td><b> Detail Transaksi </b></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <address>
                            <p><strong>Warisan Perdagangan Nusantara</strong></p>
                            <p>
                                Ruko Crystal 8 no 18 <br>
                                Alam Sutera, Tangerang Selatan<br>
                                Email: info@warisangajahmada.com
                            </p>
                        </address>
                    </td>

                    <td>
                        <address>
                            <p><strong>Nama pemesan</strong></p>
                            <p><strong>Warisan Perdagangan Nusantara</strong><br>
                                Ruko Crystal 8 no 18 <br>
                                Alam Sutera, Tangerang Selatan<br>
                                Email: info@warisangajahmada.com</p>
                            <p><strong>Phone:</strong> 082222222231</p>
                        </address>
                    </td>

                    <td>
                        <p><b>No Invoice : INV/921/213/555 </b> </p>
                        <p> Payment : PAID <br>
                            Platform : Harbolnas <br>
                            Kurir : ID Express</p>
                    </td>
                </tr>
            </tbody>
        </table>

        {{-- <div class="row invoice-info">
            <div class="col-4 invoice-col">
                Dari
                <p>
                <address>
                    <p><strong>Warisan Perdagangan Nusantara</strong></p>
                    <p>
                        Ruko Crystal 8 no 18 <br>
                        Alam Sutera, Tangerang Selatan<br>
                        Email: info@warisangajahmada.com
                    </p>
                </address>
                </p>
            </div>
            <div class="col-4 invoice-col">
                To
                <address>
                    <p><strong>Nama pemesan</strong></p>
                    <p><strong>Warisan Perdagangan Nusantara</strong><br>
                        Ruko Crystal 8 no 18 <br>
                        Alam Sutera, Tangerang Selatan<br>
                        Email: info@warisangajahmada.com</p>
                    <p><strong>Phone:</strong> 082222222231</p>
                </address>
            </div>
            <div class="col-4 invoice-col">
                Detail Transaksi
                <p><b>No Invoice : INV/921/213/555 </b> </p>
                <p> Payment : PAID <br>
                    Platform : Harbolnas <br>
                    Kurir : ID Express</p>
            </div>
        </div> --}}
        <div class="row" style="margin-top: 3rem">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>

                        <td>1 Bundle Larutan Penyegar Cap Badak</td>
                        <td>1
                    </tr>
                    <tr>

                        <td>1 Bundle Larutan Penyegar Cap Badak</td>
                        <td>1
                    </tr>
                    <tr>

                        <td>1 Bundle Larutan Penyegar Cap Badak</td>
                        <td>1
                    </tr>
                    <tr>

                        <td>1 Bundle Larutan Penyegar Cap Badak</td>
                        <td>1
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
    <div id="editor"></div>
    <div class="clearfix"></div>

</body>

</html>
