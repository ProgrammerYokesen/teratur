@extends("crudbooster::admin_template")
@section("content")
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row page-header">
      <div class="col-xs-4">
          <img src="https://teratur.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
      </div>
      <div class="col-xs-4">
          <!--<img src="data:image/png;base64,{{DNS1D::getBarcodePNG($order[0]->bookinganId, 'UPCA' , 4, 100)}}" alt="barcode"  style="margin-top:15px;margin-left: auto;margin-right:auto;display: block;"/>-->
          <img src="data:image/png;base64,{{DNS2D::getBarcodePNG("https://teratur.warisangajahmada.com/admin/logistic/".$order[0]->bookinganId, 'QRCODE',7,7)}}" alt="barcode"  style="margin-top:15px;margin-left: auto;margin-right:auto;display: block;"/>
      </div>
      <div class="col-xs-4">
          @if($order[0]->platform == 'Shopee' && empty($order[0]->awbNo))
            <p class="tracking__number" style="font-size:18px;"><b>Tracking Number: -</b></p>
          <!--<img class="pull-right" src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">-->
          @elseif ($order[0]->platform == 'Shopee' && !empty($order[0]->awbNo))
            <p class="tracking__number" style="font-size:18px;"><b>Tracking Number: {{$order[0]->awbNo}}</b></p>
          @endif
      </div>
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dari

        <address>
          @if ($order[0]->toko == 223)
            <strong>Warisan Gajahmada</strong><br>
          @elseif($order[0]->toko == 234)
            <strong>Warisan Hayamwuruk</strong><br>
          @elseif($order[0]->toko == 243)
            <strong>BARDI BANTEN</strong><br>
          @else
            <strong>Warisan Perdagangan Nusantara</strong><br>

          @endif

          Ruko Crystal 8 no 18 <br>
          Alam Sutera, Tangerang Selatan<br>
          Email: info@warisangajahmada.com
          <br>
          <br>
          <label for="toko">Cabang/Depo</label>
          <br>
          <p>{{$toko->nama_ukm}}</p>
        </address>

      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        To
        <address>
          <p><strong>{{$order[0]->nama_pemesan}}</strong></p>
          <p>{!! nl2br(e($order[0]->alamat)) !!}</p>
          <p><strong>Phone:</strong> {{$order[0]->phone}}</p>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Detail Transaksi
          <p><b>No Invoice : {{$order[0]->kode}} </b></p>
          <p>Payment : PAID</p>
          <p>Platform : {{$order[0]->platform}}</p>
          <p>Kurir : {{$order[0]->kurir}}</p>
          <!--<h3 class="text-danger">{{$pesanan->status}}</h3>-->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Product</th>
            <th>Qty</th>
          </tr>
          </thead>
          <tbody>
            @foreach($order as $row)
          <tr>

            <td>{{$row->item}}</td>
            <td>{{$row->qty}} 
            <!--<sup>{{$row->productNote1}}</sup></td>-->
          </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <!--<span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>-->

        @if ($order[0]->status == "accept order" && (CRUDBooster::myPrivilegeID()== $toko->privillegeId || CRUDBooster::myPrivilegeID()== 2 || CRUDBooster::myPrivilegeID()== 1) || CRUDBooster::myPrivilegeID()== 42)
            @if($order[0]->platform == 'Shopee')
                <a href="#" type="button" class="btn btn-success pull-right proses_pengemasan" style="margin-right: 5px;" > Menunggu Tracking Number</a>
            @else
                <a href="{{route('prosesPengemasan',$order[0]->bookinganId)}}" type="button" class="btn btn-success pull-right proses_pengemasan" style="margin-right: 5px;" > Konfirmasi Pengemasan</a>
            @endif
          <a href="{{route('editInvoice',$order[0]->bookinganId)}}" type="button" class="btn btn-primary pull-right"style="margin-right: 5px;" > Edit</a>
        @endif

        <!--<a href="{{ url('/') }}/{{$order[0]->buktiBayar }}" target=_blank class="btn btn-primary"-->
        <!--    style="margin-left: 5px;">-->
        <!--    <i class="fa fa-download"></i> Invoice-->
        <!--</a>-->
      </div>
    </div>
  </section>
  <div id="editor"></div>

  <!-- /.content -->
  <div class="clearfix"></div>

<!-- /.content-wrapper -->

@endsection

@section('pageJs')
    @if($order[0]->platform == 'Shopee')
        <script>
            $('document').ready(function(){
               setInterval(function() {
                   let id = "<?php echo $id; ?>";
                   let endPoint = '{{route("prosesPengemasan",":bookinganid")}}'
                   endPoint = endPoint.replace(':bookinganid', id);
                   $.ajax({
                        url: "/update-order/" + id,
                        success: function(data) {
                            // console.log(data, data.trackingNumber)
                            if(data != 'tidak ada'){
                                console.log(data);
                                let text = `Tracking Number : ${data}`
                                $('.proses_pengemasan').attr('href', endPoint);
                                $('.proses_pengemasan').text('Konfirmasi Pengemasan');
                                $('.tracking__number').text(text);
                            }else{
                                console.log('tidak ada');
                            }
                        }
                    })
               },5000)
            })
        </script>
    @endif
@endsection
