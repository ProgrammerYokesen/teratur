@extends('master')

@section('content')
   <div class="container">
        <div class="card text-white bg-success mb-3">
            <div class="card-header">
                <h1>Station 1</h1>
            </div>
            <div class="card-body">
                <h3 class="card-title">Order</h3>
                <!--<p class="card-text" style='font-size:24px;'>Larutan Penyegar Cap Badak</p>-->
                <div class="form-group row mt-5">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Invoice</label>
                    <div class="col-sm-10">
                        <p style="padding:.375rem .75rem;" id="invoice">INV/20210723/MPL/1432397022</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Item</label>
                    <div class="col-sm-10">
                        <p style="padding:.375rem .75rem;" id="productName">Larutan Penyegar Cap Badak</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Qty</label>
                    <div class="col-sm-10">
                        <p style="padding:.375rem .75rem;" id="qty">10</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Kurir</label>
                    <div class="col-sm-10">
                        <p style="padding:.375rem .75rem;" id="kurir">Grab Express</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsPage')
    
@endsection
