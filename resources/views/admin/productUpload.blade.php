@extends("crudbooster::admin_template")

@section("content")
<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header">
        <h4>Add Products</h4>
      </div>
      <div class="box-body">
        <form action="{{route('productUploadSave')}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="Brand">Brand</label>
            <select class="form-control" name="Brand">
              @foreach($brands as $brand)
              <option value="{{$brand->id}}">{{$brand->nama_ukm}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="ProductName">Product Name</label>
            <input type="text" name="productName" class="form-control"  placeholder="Product Name" >
          </div>
          <div class="form-group">
            <label for="ProductName">client</label>
            <select class="form-control" name="client">
            @foreach($clients as $client)
              <option value="{{$client->id}}">{{$client->name}}</option>
             @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="ProductName">kode_item</label>
            <input type="text" name="kodeItem" class="form-control"  placeholder="kode item" >
          </div>
          <div class="form-group">
            <label for="Product Name">Product Description</label>
            <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" ></textarea>
          </div>
          <div class="form-group">
            <label for="Product Function">Product Type</label>
            <select class="form-control" name="ProductFunction">
              @foreach($functions as $function)
              <option value="{{$function->id}}">{{$function->nama_kategori}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="Product Name">Product Display (can attach more than one):</label>
            <br />
            <input type="file" class="form-control" name="photos[]" multiple required/>
          </div>

          <div class="form-group">
            <label for="Product Video">Product Detail  (can attach more than one)</label>
            <input type="file" class="form-control" name="videos[]" multiple required/>
          </div>

          <div class="form-group">
            <label for="Product Function">Product Status</label>
            <select class="form-control" name="productStatus">
              <option value="active">Active</option>
              <option value="inactive">InActive</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Harga Jual Retail</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">Rp</div>
                <input type="text" name="productPrice" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" required>
                <div class="input-group-addon">.00</div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="Product Name">Harga Website</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">Rp</div>
                <input type="text" name="productPrice1" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" required>
                <div class="input-group-addon">.00</div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="Product Name">Harga Reseller</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">Rp</div>
                <input type="text" name="productPrice2" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" required>
                <div class="input-group-addon">.00</div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="Product Name">Harga Pokok Penjualan</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon">Rp</div>
                <input type="text" name="productPrice3" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" required>
                <div class="input-group-addon">.00</div>
              </div>
            </div>
          </div>

          <div class="">
            <div class="row" style="align-items: center">

              <div class="col-md-3">
                <label for="Product Name">Nama Unit</label>
                <div class="form-group">
                    <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" placeholder="Carton" required>
                </div>
              </div>

              <div class="col-md-3">
                <label for="Product Name">Satuan Unit vs Satuan Terkecil</label>
                <div class="form-group">
                    <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" placeholder="0" required>
                </div>
              </div>

              <div class="col-md-3">
                <label for="Product Name">Stok Awal</label>
                <div class="form-group">
                    <input type="text" name="stockAwalUnit[]" id="nameUnitChange3" class="form-control" placeholder="0" required>
                </div>
              </div>

              <div class="col-md-2">
                <label for=""> </label>
                <div class="form-group">
                  <button type="button" class="btn btn-info" onclick="addInput()" style="margin-top: 3px; width: 100%">Tambah</button>
                </div>
              </div>

            </div>
          </div>

          <div id="moreInput"></div>


          <!-- <div class="form-group">
            <label for="Product Name">Satuan Unit vs Satuan Terkecil</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon" id="stockSatuanUnit1">Unit 1</div>
                <input type="number" name="stockConversionUnit1" class="form-control" placeholder="0" required>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="Product Name">Stock Awal</label>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon" id="stockNameUnit1">Unit 1</div>
                <input type="number" name="stockAwalUnit1" class="form-control" placeholder="0" required>
              </div>
            </div>
          </div> -->

          <div class="form-group">
            <label for="Product Name">Satuan Transaksi</label>
            <input type="text" name="productNote1" class="form-control">
            <!--<select class="form-control" name="productNote1" required>-->
              <!--<option value="1" id="nameUnit1">Unit 1</option>-->
              <!--<option value="2" id="nameUnit2">Unit 2</option>-->
              <!--<option value="3" id="nameUnit3">Unit 3</option>-->
            <!--</select>-->
          </div>

          <div class="form-group">
            <label for="Product Name">Minimum pembelian Per Unit</label>
            <select class="form-control" name="productNote2" required>
              @for ($i=1; $i < 11; $i++)
              <option value="{{$i}}">{{$i}}</option>
              @endfor
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product Weight</label>
            <input type="text" name="productNote3" class="form-control" placeholder="jangan pakai petik '" >
          </div>

          <div class="form-group">
            <label for="Product Name">Product Varian (Rasa)</label>
            <input type="text" name="productNote4" class="form-control" placeholder="jangan pakai petik '" >
          </div>

          <div class="form-group">
            <label for="Product Name">Product label BEST</label>
            <select class="form-control" name="productNote5" required>
              <option value="0">--Pilih--</option>
              <option value="1">Ya</option>
              <option value="2">Tidak</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product label HOT</label>
            <select class="form-control" name="productNote6" required>
              <option value="0">--Pilih--</option>
              <option value="1">Ya</option>
              <option value="2">Tidak</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product label NEW</label>
            <select class="form-control" name="productNote7" required>
              <option value="0">--Pilih--</option>
              <option value="1">Ya</option>
              <option value="2">Tidak</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product label OFFER THIS WEEK</label>
            <select class="form-control" name="productNote10" required>
              <option value="0" {{$product->productNote10 == "0" ? "selected" : ""}}>--Pilih--</option>
              <option value="1" {{$product->productNote10 == "1" ? "selected" : ""}}>Ya</option>
              <option value="2" {{$product->productNote10 == "2" ? "selected" : ""}}>Tidak</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product label DISCOUNT</label>
            <select class="form-control" name="productNote8" required>
              <option value="0">--Pilih--</option>
              <option value="1">Ya</option>
              <option value="2">Tidak</option>
            </select>
          </div>

          <div class="form-group">
            <label for="Product Name">Product Discount</label>
            <div class="input-group">
              <div class="input-group-addon">Diskon</div>
              <input type="text" name="productNote9" class="form-control" placeholder="00.00">
              <div class="input-group-addon">%</div>
            </div>
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" value="Simpan" />
          </div>
        </form>
      </div>
    </div>

  </div>
</div>
@endsection

@section('pageJs')
<script>
    function addInput() {
      $('#moreInput').append(`
        <div class="row" style="align-items: center">

          <div class="col-md-3">
            <label for="Product Name">Nama Unit</label>
            <div class="form-group">
                <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" placeholder="Carton" required>
            </div>
          </div>

          <div class="col-md-3">
            <label for="Product Name">Satuan Unit vs Satuan Terkecil</label>
            <div class="form-group">
                <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" placeholder="0" required>
            </div>
          </div>

          <div class="col-md-3">
            <label for="Product Name">Stok Awal</label>
            <div class="form-group">
                <input type="text" name="stockAwalUnit[]" id="nameUnitChange3" class="form-control" placeholder="0" required>
            </div>
          </div>

          <div class="col-md-2">
            <label for=""> </label>
            <div class="form-group">
              <button type="button" class="btn btn-danger" style="margin-top: 3px; width: 100%" onclick="hapus()" >Hapus</button>
            </div>
          </div>

        </div>
        `)
    }
    
    function hapus(){
        console.log('hapus');
    }
</script>
@endsection

@section('jsonpage')
<script type="text/javascript">
$(document).ready(function(){
  console.log('asdasd')
  $("#nameUnitChange1").change(function(){
    document.getElementById("nameUnit1").innerHTML = document.getElementById("nameUnitChange1").value;
    document.getElementById("nameUnit1").value = document.getElementById("nameUnitChange1").value;
    document.getElementById("stockSatuanUnit1").innerHTML = document.getElementById("nameUnitChange1").value;
    document.getElementById("stockNameUnit1").innerHTML = document.getElementById("nameUnitChange1").value;
  });
  $("#nameUnitChange2").change(function(){
    document.getElementById("nameUnit2").innerHTML = document.getElementById("nameUnitChange2").value;
    document.getElementById("nameUnit2").value = document.getElementById("nameUnitChange2").value;
    document.getElementById("stockSatuanUnit2").innerHTML = document.getElementById("nameUnitChange2").value;
    document.getElementById("stockNameUnit2").innerHTML = document.getElementById("nameUnitChange2").value;
  });
  $("#nameUnitChange3").change(function(){
    document.getElementById("nameUnit3").innerHTML = document.getElementById("nameUnitChange3").value;
    document.getElementById("nameUnit3").value = document.getElementById("nameUnitChange3").value;
    document.getElementById("stockSatuanUnit3").innerHTML = document.getElementById("nameUnitChange3").value;
    document.getElementById("stockNameUnit3").innerHTML = document.getElementById("nameUnitChange3").value;
  });
});
</script>

@endsection
