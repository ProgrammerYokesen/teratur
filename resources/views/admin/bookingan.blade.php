@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
        <!--<img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">-->
      </h2>
    </div>
  </div>
  <form class="form-group" action="{{route('creatingOrderStep1')}}" method="post" enctype="multipart/form-data">
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
        <br>
        <br>
        <br>
        <label for="toko">Cabang/Depo</label>
        <br>
        <select name="toko" required>
          <option value="">--Pilih Cabang/Depo--</option>
          @foreach($toko as $value)
            <option value="{{$value->id}}">{{$value->nama_ukm}}</option>
          @endforeach
        </select>
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <label for="nama">Nama Pembeli</label>
        <br>
        <input type="text" name="nama" required>
        <br>
        <label for="phone">Nomor Telepon Pembeli</label>
        <br>
        <input type="text" name="phone" required>
        <br>
        <label for="alamat">Alamat Pembeli</label>
        <br>
        <textarea name="alamat" rows="3" cols="25" required></textarea>
        <br>
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      Detail Transaksi
      <address>
        <label for="platform">Platform Transaksi</label>
        <br>
        <select name="platform" required>
          <option value="">--Pilih Platform--</option>
          <option value="Tokopedia">Tokopedia</option>
          <option value="Shopee">Shopee</option>
          <option value="BliBli">BliBli</option>
          <option value="Bukalapak">Bukalapak</option>
          <option value="Whatsapp">Whatsapp</option>
          <option value="Facebook/Instagram">Facebook/Instagram</option>
          <option value="Website">Website</option>
          <option value="Omaru">Omaru</option>
          <option value="Hadiah">Hadiah</option>
          <option value="Sample">Sample</option>
          <option value="Influencer">Influencer</option>
          <option value="JDID">JDID</option>
          <option value="Compliment">Compliment</option>
          <option value="Reseller">Reseller</option>
          <option value="Harbolnas">Harbolnas</option>
          <option value="Sinde Home Delivery">Sinde Home Delivery</option>
        </select>
        <br>
        <label for="noinvoice">Nomor Invoice</label>
        <br>
        <input type="text" name="noinvoice" required>
        <br>
        <label for="invoice">Upload Invoice</label>
        <br>
        <input type="file" name="invoice">
        <br>
        <label for="kurir">Tipe Kurir</label>
        <br>
        <select name="kurir" required>
          <option value="">--Pilih Platform--</option>
          <option value="" disabled>-----Drop-----</option>
          <option value="JNE">JNE</option>
          <option value="TIKI">TIKI</option>
          <option value="Pos Indonesia">Pos Indonesia</option>
          <option value="Wahana">Wahana</option>
          <option value="SiCepat-drop">SiCepat</option>
          <option value="J&T Express">J&T Express</option>
          <option value="Shopee Express">Shopee Express</option>
          <option value="" disabled>-----Pick Up-----</option>
          <option value="GoSend">Go-Send</option>
          <option value="GrabExpress">GrabExpress</option>
          <option value="Ninja Xpress">Ninja Xpress</option>
          <option value="AnterAja">AnterAja</option>
          <option value="SiCepat">SiCepat</option>
          <option value="Rex Kiriman Express">Rex Kiriman Express</option>
          <option value="Lion Parcel">Lion Parcel</option>
          <option value="ID Express">ID Express</option>
        </select>
      </address>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <input type="submit" class="btn btn-primary pull-right" value="SUBMIT DATA PEMBELI">
      <a href="/admin/bookinganWarisan" class="btn btn-danger pull-right" style="margin:0px 20px;">CANCEL</a>

    </div>
  </div>
  @csrf
  </form>
</section>
<div id="editor"></div>

<!-- /.content -->
<div class="clearfix"></div>

@endsection

@section('jslist')
@endsection

@section('jsonpage')

@endsection
