@extends("crudbooster::admin_template")

@section('content')
           
   <div class="container-fluid chat__wrapper">
      <div class="card">
        <div class="card-header">
          <div class="chat__head_pic">
            <div class="online__badge"></div>
          </div>
          <div class="chat__head">
            <h3>{{$finalData[0]->sender_name}}</h3>
            <p>Online</p>
          </div>
        </div>
        <div class="card-body">
           <!--Message Start Here -->
           
          <div class="chat__message" id="chat_box">
              @foreach($finalData as $key=>$value)
                @if($value->role == 'customer' )
                    @if(Carbon\Carbon::now()->format('Y-m-d') == $finalData[$key]->date && $finalData[$key]->date != $finalData[$key - 1]->date )
                        <div class="chat__date_splitter">
                          <p>TODAY, {{$value->time}}</p>
                        </div>
                    @elseif($finalData[$key]->date != $finalData[$key - 1]->date )
                        <div  class="chat__date_splitter">
                          <p>{{$value->date}}</p>
                        </div>
                    @endif
                    <div class="chat__sender">
                      <p>{{$value->msg}}<span class="chat__time">{{$value->time}}</span></p>
                    </div>
                @else
                    @if(Carbon\Carbon::now()->format('Y-m-d') == $finalData[$key]->date && $finalData[$key]->date != $finalData[$key - 1]->date )
                            <div class="chat__date_splitter">
                              <p>TODAY, {{$value->time}}</p>
                            </div>
                    @elseif($finalData[$key]->date != $finalData[$key - 1]->date )
                            <div  class="chat__date_splitter">
                              <p>{{$value->date}}</p>
                            </div>
                    @endif
                    <div class="chat__admin">
                        <span class='chat__admin'>{{$value->nama}}</span>
                      <p>{{$value->msg}}<span class="chat__time">{{$value->time}}</span></p>
                    </div>
                @endif
              @endforeach
          </div>
           <!--Input Message Start -->
          <div class="chat__input">
            <div class="input-group">
              <input
                type="text"
                class="form-control input_chat"
                placeholder="Pesan"
                aria-label="Recipient's username"
                aria-describedby="button-addon2"
              />
              <div class="input-group-btn">
                <button
                  class="btn btn-outline-secondary btn_chat"
                  type="button"
                  id="button-addon2"
                >
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M2.72113 2.05149L18.0756 9.61746C18.3233 9.73952 18.4252 10.0393 18.3031 10.287C18.2544 10.3858 18.1744 10.4658 18.0756 10.5145L2.72144 18.0803C2.47374 18.2023 2.17399 18.1005 2.05193 17.8528C1.99856 17.7445 1.98619 17.6205 2.0171 17.5038L3.9858 10.0701L2.01676 2.62789C1.94612 2.36093 2.10528 2.08726 2.37224 2.01663C2.48893 1.98576 2.61285 1.99814 2.72113 2.05149ZM3.26445 3.43403L4.87357 9.51612L4.93555 9.50412L5 9.5H12C12.2761 9.5 12.5 9.72386 12.5 10C12.5 10.2455 12.3231 10.4496 12.0899 10.4919L12 10.5H5C4.9686 10.5 4.93787 10.4971 4.90807 10.4916L3.26508 16.6976L16.7234 10.066L3.26445 3.43403Z"
                      fill="#242424"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </div>
           <!--Input Message End -->
        </div>
      </div>
    </div>
@endsection

@section('pageJs')
<script>
    $(window).load(function() {
    console.log('loaded')
      $("#chat_box").animate({ scrollTop: 9999 }, 4000);
    });
    $('document').ready(function(e){
        $('.input_chat').keypress(function (e) {
            if(e.which == 13){
                let id = "<?php echo $id; ?>";
                let platform = "<?php echo $platform; ?>";
                let lastMessageTime = "<?php echo $lastMessageTime; ?>";
                var d = new Date()
                let hour = d.getHours();
                let minutes = String(d.getMinutes()).padStart(2, "0");
                let seconds = String(d.getSeconds()).padStart(2,"0");
                let ampm = hour >= 12 ? 'pm' : 'am';
                let hours = hour % 12;
                let fullDate = d.getFullYear()+"-"+String(d.getMonth()+1).padStart(2,'0')+"-"+String(d.getDate()).padStart(2,'0')+" "+hour+":"+minutes+":"+seconds
                let onlyDate = d.getFullYear()+"-"+String(d.getMonth()+1).padStart(2,'0')+"-"+String(d.getDate()).padStart(2,'0')
                let message = $('.input_chat').val();
                let data = {fullDate,message, id, platform}
                // var urlBalas = window.location.origin+"/admin/submit-balas/";
                var urlBalas = "{{URL::to('/')}}";
                // console.log(token, urlBalas, lastMessageTime == onlyDate, hours, ampm, minutes);
                $.ajax({
                    type:'POST',
                    url:  "{{route('balasChat')}}",
                    data:data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    } 
                })
                // data:
                .done(function (data){
                    if(data == 'success'){
                        if(lastMessageTime == onlyDate){
                            $('.chat__message').append(`
                                <div class="chat__admin">
                                  <p>${message}<span class="chat__time">${hours}:${minutes} ${ampm}</span></p>
                                </div>
                            `)
                            
                        }else{
                             $('.chat__message').append(`
                             <div class="chat__date_splitter">
                              <p>TODAY, ${hours}:${minutes} ${ampm}</p>
                            </div>
                            <div class="chat__admin">
                              <p>${message}<span class="chat__time">${hours}:${minutes} ${ampm}</span></p>
                            </div>
                         `)
                        }
                        
                        $('.input_chat').val(' ');
                    }
                })
                .fail(function(jqXHR, ajaxOptions, thrownError)
                    {
                          alert('server not responding...');
                    });
            }
        })
        $('.btn_chat').on('click', function(){
            let message = $('.input_chat').val();
            console.log(message);
        })
    })
</script>
@endsection

