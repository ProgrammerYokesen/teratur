@extends("crudbooster::admin_template")
@section("content")
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header">
        <h4 class="text-header">Pengiriman product ke :</h4>
      </div>
      <div class="box-body">
        <form action="{{route('distributeStep2')}}" method="get">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="Brand">Depo</label>
            <select class="form-control" name="tokoId" required>
              <option value="">---Pilih Depo----</option>
              @foreach($toko as $brand)
                <option value="{{$brand->id}}">{{$brand->nama_ukm}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="ProductName">Nomor Purchace Order :</label>
            <input type="text" name="documentNumber" class="form-control"  placeholder="Tuliskan PO Lengkap" required>
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" value="Lanjutkan" />
          </div>
        </form>
      </div>
      <div class="box-footer">

      </div>
    </div>
  </div>
  <div class="col-md-6">


  </div>
</div>
@endsection

@section('jsonpage')
@endsection
