@extends("crudbooster::admin_template")

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="nav-tabs-custom" style="margin-top:20px;">
            <ul class="nav nav-tabs">
              <li class="{{ Request::url() == 'https://teratur.warisangajahmada.com/admin/chat/wgm'? 'active': '' }}"><a href="{{ route('chatToko', ['toko'=>'wgm']) }}">WGM</a></li>
              <li class="{{ Request::url() == 'https://teratur.warisangajahmada.com/admin/chat/chat'? 'active': '' }}"><a href="{{ route('chatToko', ['toko'=>'chat']) }}">Chat</a></li>
              <li class="{{ Request::url() == 'https://teratur.warisangajahmada.com/admin/chat/whw'? 'active': '' }}"><a href="{{ route('chatToko', ['toko'=>'whw']) }} ">WHW</a></li>
              <li class="{{ Request::url() == 'https://teratur.warisangajahmada.com/admin/chat/sriwijaya'? 'active': '' }}"><a href="{{ route('chatToko', ['toko'=>'sriwijaya']) }} ">Sriwijaya</a></li>
              <li class="{{ Request::url() == 'https://teratur.warisangajahmada.com/admin/chat/bardi'? 'active': ''  }}"><a href="{{ route('chatToko', ['toko'=>'bardi']) }} ">Bardi</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">ALL Chat</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Toko</th>
                                    <th>Platform</th>
                                    <th>Nama Pengirim</th>
                                    <th>Pesan</th>
                                    <th>Waktu</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($chatData as $data )
                                    
                                <tr>
                                    <td>{{ $data->nama_toko }}</td>
                                    <td>{{ $data->platform }}</td>
                                    <td>{{ $data->sender_name }}</td>
                                    <td> {{ $data->msg }}</td>
                                    <td> {{ $data->created_at }}</td>
                                    <td> <a href="{{route('balasPage', ['id'=>$data->id, 'platform'=>$data->platform])}}" class='btn btn-success accept'>Balas</a></td>
                                </tr>
            
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">
                                        {{ $chatData->links() }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@section('jsPage')

@endsection