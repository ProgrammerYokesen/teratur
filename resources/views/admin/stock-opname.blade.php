@extends("crudbooster::admin_template")

@section('otherAssets')
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section("content")
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header">
        <h4>Stock Opname</h4>
      </div>
      <div class="box-body">
        <table class="table table-bordered" id= 'opname_table'>
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Product</th>
                    <th>Owner</th>
                    <th>Gudang</th>
                    <th>Book Unit 1</th>
                    <th>Book Unit 2</th>
                    <th>Book Unit 3</th>
                    <th>Book Unit 4</th>
                    <th>Stock Opname Unit 1</th>
                    <th>Stock Opname Unit 2</th>
                    <th>Stock Opname Unit 3</th>
                    <th>Stock Opname Unit 4</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                @if (CRUDBooster::myPrivilegeId() > '8')
                                @foreach ($product as $n => $value)
                                    <?php
                                    $stock = DB::table('stock')
                                    ->where('stock.induk_produk_id', $value->id)
                                    
                                    ->orderby('stock.id', 'desc')
                                    ->first();
                                    $book = DB::table('stock_number')
                                    ->where('stock_number.stock_id', $stock->id)
                                    ->join('convertion', 'stock_number.convertion_id', 'convertion.id')
                                    ->orderby('convertion.stock_convertion_unit', 'desc')
                                    ->get();
                                    $gudang = DB::table('data_toko')->where('id', $value->brand_id)->first();
                                    ?>

                        <td>{{ $n + 1 }}</td>
                        <td>{{ $value->productName }}</td>
                        @if($value->client_id == 2)
                            <td>PT. Sinde Budi Sentosa</td>
                        @elseif($value->client_id == 1)
                            <td>PT. Yokesen Teknologi Indonesia</td>
                        @else
                            <td>-</td>
                        @endif
                        <td>{{$gudang->nama_ukm}}</td>
                        <td>{{ $book[0]->bookStockUnit_induk }} <sup>{{ $book[0]->nama_satuan }}</sup></td>
                        <td>{{ $book[1]->bookStockUnit_induk }} <sup>{{ $book[1]->nama_satuan }}</sup></td>
                        <td>{{ $book[2]->bookStockUnit_induk }} <sup>{{ $book[2]->nama_satuan }}</sup></td>
                        <td>{{ $book[3]->bookStockUnit_induk }} <sup>{{ $book[3]->nama_satuan }}</sup></td>
                        <form class="form-control" action="{{ route('stockOpnameDoing', $value->id) }}" method="post">
                            <td><input type="number" name="stock[]" placeholder="0" required></td>
                            <td><input type="number" name="stock[]" placeholder="0" required></td>
                            <td><input type="number" name="stock[]" placeholder="0" required></td>
                            <td><input type="number" name="stock[]" placeholder="0" required></td>
                            @csrf()
                            <td> <input type="submit" name="submit" value="Stock Opname"> </td>
                        </form>
                </tr>
                <tr>
                            @endforeach
                        @else
                            @foreach ($product as $n => $value)
                                <?php
                                 // dd($toko, $value->id, $value);
                                $stock = DB::table('stock')
                                ->where('stock.induk_produk_id', $value->id)
                                ->orderby('stock.id', 'desc')
                                ->first();
                                $book = DB::table('stock_number')
                                ->where('stock_number.stock_id', $stock->id)
                                ->join('convertion', 'stock_number.convertion_id', 'convertion.id')
                                ->orderby('convertion.stock_convertion_unit', 'desc')
                                ->get();
                                // dd($stock, $book);
                                $gudang = DB::table('data_toko')->where('id', $value->brand_id)->first();
                                ?>

                        <td>{{ $n + 1 }}</td>
                        <td>{{ $value->productName }}</td>
                        @if($value->client_id == 2)
                            <td>PT. Sinde Budi Sentosa</td>
                        @elseif($value->client_id == 1)
                            <td>PT. Yokesen Teknologi Indonesia</td>
                        @else
                            <td>-</td>
                        @endif
                        <td>{{$gudang->nama_ukm}}</td>
                        <td>{{ $book[0]->bookStockUnit_induk }} <sup>{{ $book[0]->nama_satuan }}</sup></td>
                        <td>{{ $book[1]->bookStockUnit_induk }} <sup>{{ $book[1]->nama_satuan }}</sup></td>
                        <td>{{ $book[2]->bookStockUnit_induk }} <sup>{{ $book[2]->nama_satuan }}</sup></td>
                        <td>{{ $book[3]->bookStockUnit_induk }} <sup>{{ $book[3]->nama_satuan }}</sup></td>
                        <form class="form-control" action="{{ route('stockOpnameDoing', $value->id) }}" method="post">
                            <td><input type="number" name="stock[]" placeholder="0" required></td>
                            <td><input type="number" name="stock[]" placeholder="0" ></td>
                            <td><input type="number" name="stock[]" placeholder="0" ></td>
                            <td><input type="number" name="stock[]" placeholder="0" ></td>
                            @csrf()
                            <td> <input type="submit" name="submit" value="Stock Opname"> </td>
                        </form>
                </tr>

                            @endforeach
                            @endif




            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection

@section('jslist')
@endsection

@section('pageJs')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datables.bootstrap5.js') }}"></script>
    <script>
        $(function() {
            $('#opname_table').DataTable();
        })
    </script>
@endsection
