@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
        <!--<img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">-->
      </h2>
    </div>
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <strong>{{Session::get('po.toko')->nama_ukm}}</strong><br>
        {{Session::get('po.toko')->alamat}}<br>
        {{Session::get('po.toko')->kota}}<br>

      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <b>Nomor PO : {{Session::get('po.documentNumber')}}</b><br>
      <br>
      <b>Tanggal :</b> {{date('d-M-Y')}}<br>
      <h3 class="text-danger">{{$pesanan->status}}</h3>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Product</th>
          <th>Qty</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if(Session::get('po.orderan'))
          @foreach(Session::get('po.orderan') as $n => $row)
        <tr>
          <td>{{$row['Product']->productName}}</td>
          <td>
              @foreach($row['data'] as $stock)
                <strong>{{$stock->qty}}</strong> <sup>{{$stock->satuan}}</sup>
              @endforeach
          </td>
          <td> <strong class="text-danger xbutton" id="xbutton-{{$n}}" style="cursor:pointer">X</strong> </td>
        </tr>
          @endforeach
        @endif
        <tr>
          <form class="form-group" action="{{route('distributeStep3')}}" method="post" id='formDistribusi'>
            <!--<button type="button" name="button" onclick="clickButton()">Klik Disini</button>-->
            <!--<button type="button" name="button" onclick="getData()">Get Data</button>-->
            <div id="newInput">
            </div>
              <td width="45%">
                    <select class="form-control select2-show-search" id="js-data-referal" name="Product" data-placeholder="Cari Product ..." ></select>
                  @csrf
              </td>
              <td width="45%">
                <div class="form-group">
                  <div class="input-group" id='inputElement'>

                  </div>

                </div>
              </td>



              <td width="10%">
                <input type="submit" id='submitBtn' name="submit" value="+ Submit Produk" class="form-control btn btn-success">
              </td>
          </form>
        </tr>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <!-- <a href="#" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
      <button onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
      <a href="{{route('distributeSubmit')}}" class="btn btn-primary pull-right">SUBMIT DELIVERY</a>
      <a href="{{route('distributeCancel')}}" class="btn btn-danger pull-right" style="margin:0px 20px;">CANCEL</a>

    </div>
  </div>
</section>
<div id="editor"></div>

<!-- /.content -->
<div class="clearfix"></div>

@endsection

@section('jslist')
  <script src="{{url('/')}}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
@endsection

@section('pageJs')
<script type="text/javascript">

function clickButton(){
  $('#newInput').append(`<input type="text" name="new[]" value="">`)
}
function getData(){
  var data = $("input[name='new[]']").map(function(){return $(this).val();}).get();;
  let Product = $('#js-data-referal').select2('data')[0].id;
        let qtyUnit = $("input[name='qtyUnit[]']").map(function(){return $(this).val();}).get();
  // console.log(data,Product, qtyUnit);
}

  $(function(){
    $('#js-data-referal').select2({
      placeholder: 'Cari...',
      ajax: {
        url: "/admin/products/paket/cari/{{Session::get('po.toko')->id}}",
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            console.log(data);
          return {
            results:  $.map(data, function (item) {

              return {
                text: item.productName,
                id: item.id
              }

            })
          };
        },
        cache: true
      }
    });
    $('#js-data-referal').on('select2:select', function (e) {
        let id = e.params.data.id;
        $.ajax({
            url: "/admin/detail-conversion/" + id,
            type: 'Get',
            success: function(data) {
                // let convertion = '';
                $.each(data, function(i, e) {
                    $('.input-group').append(`
                        <input type="number" name="qtyUnit[]" class="form-control qtyUnit" value="0" />
                        <div class="input-group-addon">.</div>
                    `
                    );
                })
                // <div class="input-group-addon">.</div>
                // $('#inputElement').html('');
                // $('#inputElement').append(<input type="number" name="qtyUnit[]" class="form-control qtyUnit" value="0" >);
            }
        })
    });

    $('#formDistribusi').on('submit', function(e){
        e.preventDefault();
        let Product = $('#js-data-referal').select2('data')[0].id;
        let qtyUnit = $("input[name='qtyUnit[]']").map(function(){return $(this).val();}).get();
        let data = {
            Product,
            qtyUnit
        }
        // console.log(Product, qtyUnit);
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: "{{route('distributeStep3')}}",
            data:data,
            success: function(data) {
                console.log(data, 'tes');
                location.reload();
            }
        })
    })
    $('.xbutton').on('click', function(){

        let id = this.id;
        let split = id.split('-')
        let index = split[1];

        // console.log(index);
        $.ajax({
            url: "/admin/delete-product/" + index,
            type: 'Get',
            success: function(data) {
              console.log(data)
              location.reload();
            }
        })

    })
  });

</script>
@endsection
