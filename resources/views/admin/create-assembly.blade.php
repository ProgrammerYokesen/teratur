@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4 class="text-header">Product Assembly</h4>
        </div>
        <div class="box-body">
          <form action="{{route('productPaketUploadSave')}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="Brand">Brand</label>
              <select class="form-control" name="Brand">
                @foreach($brands as $brand)
                  <option value="{{$brand->id}}">{{$brand->nama_ukm}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name</label>
              <input type="text" name="productName" class="form-control"  placeholder="Product Name" >
            </div>
            <div class="form-group">
              <label for="Product Code">Kode Item</label>
              <input type="text" name="kode_item" class="form-control"  placeholder="Product Name" >
            </div>
            <div class="form-group">
              <label for="Product Name">Product Description</label>
              <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" ></textarea>
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-block" value="Simpan" />
            </div>
          </form>
        </div>
        <div class="box-footer">

        </div>
      </div>
    </div>
    <div class="col-md-6">


    </div>
  </div>
@endsection

@section('jsonpage')
@endsection
