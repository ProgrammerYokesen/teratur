@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
        <!--<img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">-->
      </h2>
    </div>
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
        <br>
        <br>
        <br>
        <label for="toko">Cabang/Depo</label>
        <br>
        <strong>{{$toko->nama_ukm}}</strong>
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <p><strong>{{$pesanan->nama_pemesan}}</strong></p>
        <p>{!! nl2br(e($pesanan->alamat)) !!}</p>
        <p><strong>Phone:</strong> {{$pesanan->phone}}</p>
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      Detail Transaksi
        <p><b>No Invoice : <a href="{{url('/')}}/{{$pesanan->buktiBayar}}" download>{{$pesanan->kode}}</a> </b></p>
        <p>Payment : PAID</p>
        <p>Platform : {{$pesanan->platform}}</p>
        <p>Kurir : {{$pesanan->kurir}}</p>
        <h3 class="text-danger">{{$pesanan->status}}</h3>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Product</th>
          <th>Qty</th>
          <th>satuanUnit</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if(Session::get('po.orderan'))
          @foreach(Session::get('po.orderan') as $n => $row)
        <tr>
          <td>{{$row['Product']->productName}}</td>
          <td><strong>{{$row['qty']}}</strong> <sup>{{$row['satuanUnit']}}</sup></td>
          <td><a href="{{route('substractOrderan',$n)}}"><strong class="text-danger">X</strong></a></td>
        </tr>
          @endforeach
        @endif
        <tr>
          <form class="form-group" action="{{route('creatingOrderStep2',$id)}}" method="post">
          <td width="45%">
                <select class="form-control select2-show-search" id="js-data-referal" name="Product" data-placeholder="Cari Product ..." required></select>
              @csrf
          </td>
          <td width="20%">
            <div class="form-group">
              <div class="input-group">
                <input type="number" name="qty"  class="form-control" value="0" required>
                <!-- <input type="hidden" id="inputSatuanUnit" name="satuanUnit"> -->
              </div>
            </div>
          </td>

          <td width="20%">
            <div class="input-group">
              <select class="form-control selectNameUnit" name="nameUnitId">

              </select>
            </div>
          </td>
          <td width="10%">
            <input type="submit" name="submit" value="+ Submit Produk" class="form-control btn btn-success">
          </td>
          </form>
        </tr>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>
      <a href="{{route('orderanSubmit',$id)}}" class="btn btn-primary pull-right">SUBMIT ORDERAN</a>
      <a href="{{route('orderanCancel')}}" class="btn btn-danger pull-right" style="margin:0px 20px;">CANCEL</a>

    </div>
  </div>

</section>
<div id="editor"></div>

<!-- /.content -->
<div class="clearfix"></div>

@endsection

@section('jslist')
  <script src="{{url('/')}}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
@endsection

@section('pageJs')
<script type="text/javascript">
    var tokoId = "<?php echo $toko->id; ?>";
  $(function(){
    $('#js-data-referal').select2({
      placeholder: 'Cari...',
      ajax: {
        url: "/admin/products/paket/cari/" + tokoId,
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results:  $.map(data, function (item) {
              return {
                text: item.productName,
                id: item.id
              }

            })
          };
        },
        cache: true
      }
    });
    $('#js-data-referal').on('select2:select', function (e) {
        let id = e.params.data.id;
        $.ajax({
            url: "/admin/detail-conversion/" + id,
            type: 'Get',
            success: function(data) {
              console.log(data);
                // let convertion = '';
                $.each(data, function(i, e) {
                    $('.selectNameUnit').append(`
                        <option value=${e.id}>${e.nama_satuan}</option>
                    `
                    );
                })
            }
        })
    });
  });


</script>
@endsection
