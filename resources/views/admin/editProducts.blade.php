@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4 class="text-header">Edit Produk</h4>
        </div>
        <div class="box-body">
          <form action="{{route('editProduk', $product->id)}}" method="post" name="editProduct" id="editProduct">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="ProductName">Product Name</label>
              <input type="text" name="productName" class="form-control"  value="{{$name}}" >
            </div>
            <div class="form-group">
              <label for="ProductInitial">Initial</label>
              <input type="text" name="initial" class="form-control"  value={{$product->initial}} >
            </div>
            <div class="form-group">
              <label for="Product Code">Kode Item</label>
              <input type="text" name="kode_item" class="form-control"  value={{$product->kode_item}} >
            </div>
            <div class="form-group">
              <label for="Product sku">SKU</label>
              <input type="text" name="productSku" class="form-control"  value={{$product->sku}} >
            </div>
            <div class="form-group">
              <label for="Product price">Price</label>
              <input type="text" name="productPrice" class="form-control"  value={{$product->price}} >
            </div>
            <div class="form-group">
              <label for="jenis barang">Jenis Barang</label>
              <input type="text" name="jenis_barang" class="form-control"  value={{$product->jenis_barang}} >
            </div>
            <div class="form-group">
              <label for="indukProduk">Induk Produk</label>
              <select class="form-control select2-show-search" id="js-data-referal"  name="indukProduk" data-placeholder="Cari Induk Produk ..." value={{$product->induk_barang_id}}></select>
            </div>
            <div class="box-body" style="display:none;">
              <div class="row">
                <div class="col-md-4 image__edit">
                  <hr>
                </div>
              </div>

            </div>
            <div class="form-group">
              <label for="stockName">Stock Name Unit</label>
              <select class="form-control" name="stockName">
                @foreach($convert as $stockUnit)
                  <option value="{{$stockUnit->id}}">{{$stockUnit->nama_satuan}}</option>
                @endforeach

              </select>
              <!-- <input type="number" name="stockName" class="form-control"  value={{$product->stockNameUnit}} > -->
            </div>
            <div class="form-group">
              <label for="stockConvertion">Stok Conversion Unit</label>
              <input type="text" name="stockUnit" class="form-control"  value={{$product->stock_convertion_unit}} >
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-block" value="Simpan" />
            </div>
          </form>
        </div>
        <div class="box-footer">

        </div>
      </div>
    </div>
    <div class="col-md-6">


    </div>
  </div>
@endsection

@section('jslist')
  <script src="{{url('/')}}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
@endsection

@section('pageJs')
<script type="text/javascript">
  $(function(){
    $('#js-data-referal').select2({
      placeholder: 'Cari...',
      ajax: {
        url: '/admin/induk-produk/{{$toko}}',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results:  $.map(data, function (item) {
              return {
                text: item.productName,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });

    $('#js-data-referal').on('select2:select', function (e) {
        let id = e.params.data.id;
        $.ajax({
            url: "/admin/detail-induk-edit/" + id,
            type: 'Get',
            success: function(data) {
                $('.box-body').css("display","block")
                $('.image__edit').empty();
                $('.image__edit').append(`<p><img src="https://teratur.gudangin.id/${data.productImages}" alt="" width="100%"></p>`)
            }
        })
    });
  });
</script>
@endsection
