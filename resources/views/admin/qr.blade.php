<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

  </head>

  <body>
    @if($logistic_type == 'pickUp')
        <div class="col-xs-4 bottom">
            <img src="data:image/png;base64,{{DNS2D::getBarcodePNG("https://teratur.warisangajahmada.com/admin/logistic/".$order_sn, 'QRCODE',7,7)}}" alt="barcode"  style="margin-top:10px;margin-left: 20px;margin-right:auto;display: block;border:10px solid #0DCAF0"/>
        </div>
    @else
        <div class="col-xs-4 bottom">
            <img src="data:image/png;base64,{{DNS2D::getBarcodePNG("https://teratur.warisangajahmada.com/admin/logistic/".$order_sn, 'QRCODE',7,7)}}" alt="barcode"  style="margin-top:15px;margin-left: 20px;margin-right:auto;display: block;border:10px solid #FFC107"/>
        </div>
    @endif()
    
  </body>
</html>