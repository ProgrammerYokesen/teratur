@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Add Products</h4>
        </div>
        <div class="box-body">
          <form action="{{route('productUpdating',$product->id)}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="Brand">Brand ID</label>
              <select class="form-control" name="Brand">
                @foreach($brands as $brand)
                  <option value="{{$brand->id}}" {{$product->brand_id == $brand->id ? 'selected' : ''}}>{{$brand->nama_ukm}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name</label>
              <input type="text" name="productName" class="form-control"  placeholder="Product Name" value="{{$product->productName}}">
            </div>
            <div class="form-group">
              <label for="Product Name">Product Description</label>
              <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" >{{$product->productDescription}}</textarea>
            </div>
            <div class="form-group">
              <label for="Product Function">Product Function</label>
              <select class="form-control" name="ProductFunction">
                @foreach($functions as $function)
                  <option value="{{$function->id}}" {{$product->productFunction == $function->id ? 'selected' : ''}}>{{$function->nama_kategori}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="Product Function">Product Status</label>
              <select class="form-control" name="productStatus">
                <option value="active" {{$product->productStatus == 'active' ? 'selected' : ''}}>Active</option>
                <option value="inactive" {{$product->productStatus == 'inactive' ? 'selected' : ''}}>InActive</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Toko</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Website</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice1" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice1}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Reseller</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice2" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice2}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Bandara</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice3" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice3}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Satuan Pembelian</label>
              <select class="form-control" name="productNote1" required>
                <option value="Paket" {{$product->productNote1 == "Paket" ? "selected" : ""}}>Paket</option>
                <option value="Toples" {{$product->productNote1 == "Toples" ? "selected" : ""}}>Toples</option>
                <option value="Pcs" {{$product->productNote1 == "Pcs" ? "selected" : ""}}>Pcs</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Minimum pembelian</label>
              <select class="form-control" name="productNote2" required>
                @for ($i=1; $i < 11; $i++)
                  <option value="{{$i}}" {{$product->productNote2 == $i ? "selected" : ""}}>{{$i}}</option>
                @endfor
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Weight</label>
              <input type="text" name="productNote3" class="form-control" placeholder="jangan pakai petik '" value="{{$product->productNote3}}">
            </div>

            <div class="form-group">
              <label for="Product Name">Product Varian (Rasa)</label>
              <input type="text" name="productNote4" class="form-control" placeholder="jangan pakai petik '" value="{{$product->productNote4}}" readonly>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label BEST</label>
              <select class="form-control" name="productNote5" required>
                <option value="0" {{$product->productNote5 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote5 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote5 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label HOT</label>
              <select class="form-control" name="productNote6" required>
                <option value="0" {{$product->productNote6 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote6 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote6 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label NEW</label>
              <select class="form-control" name="productNote7" required>
                <option value="0" {{$product->productNote7 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote7 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote7 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label OFFER THIS WEEK</label>
              <select class="form-control" name="productNote10" required>
                <option value="0" {{$product->productNote10 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote10 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote10 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label DISCOUNT</label>
              <select class="form-control" name="productNote8" required>
                <option value="0" {{$product->productNote8 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote8 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote8 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Discount</label>
              <div class="input-group">
                <div class="input-group-addon">Diskon</div>
                <input type="text" name="productNote9" class="form-control" placeholder="00.00" value="{{$product->productNote9}}">
                <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-block" value="Simpan" />
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Isi Paket</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">Harga Modal</span>
                  <span class="info-box-number">{{number_format($harga,0,'.','.')}}</span>

                  <div class="progress">
                    <div class="progress-bar" style="width: 50%"></div>
                  </div>
                  <span class="progress-description">
                    Paket ini keterangannya adalah
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <ul class="users-list clearfix">
                <?php
                  $productPaket = unserialize($product->productNote4);
                ?>
                @if(!empty($productPaket))
                  @foreach ($productPaket as $no => $pilihan)
                    <?php
                      $productTerpilih = DB::table('products')->where('id',$pilihan)->first();
                      $imageProductTerpilih = unserialize($productTerpilih->productImages);
                    ?>
                    <li>
                      <img src="{{url('/')}}/{{$imageProductTerpilih[0]}}" alt="User Image">
                      <a href="{{route('productPaketKurang',[$product->id,$no])}}" class="text-danger"> <b>remove</b> </a>
                      <a class="users-list-name" href="#">{{$productTerpilih->productName}}</a>
                      <span class="users-list-date">{{number_format($productTerpilih->productPrice,0,'.','.')}}</span>
                    </li>
                  @endforeach
                @endif
              </ul>
              <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">

            </div>
            <!-- /.box-footer -->
          </div>
          <!--/.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>Tambah Product</h4>
            </div>
            <form class="form-group" action="{{route('productPaketTambah',$product->id)}}" method="post">
              <div class="box-body">
                <select class="form-control select2-show-search" id="js-data-referal"  name="newProductPacket" data-placeholder="Cari Product ..." required></select>
              </div>
              <div class="box-footer">
                <button class="btn btn-warning btn-block">Pilih</button>
              </div>
              @csrf
            </form>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto display</h4>

            </div>
            <div class="box-body">
              <?php
                $displays = unserialize($product->productImages);
              ?>
              <div class="row">
                @if($displays)
                  @foreach ($displays as $n => $display)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/{{$display}}" alt="" width="100%"></p>
                      <p><a href="{{route('productImgDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                      <hr>
                    </div>
                  @endforeach
                @endif
              </div>

            </div>
            <div class="box-footer">
              <form action="{{route('productAddDisplay',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Name">Product Display (can attach more than one):</label>
                  <br />
                  <input type="file" class="form-control" name="photos[]" multiple/>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto detail</h4>
            </div>
            <div class="box-body">
              <?php
                $details = unserialize($product->productVideos);
              ?>
              <div class="row">
                @if($details)
                  @foreach ($details as $n => $detail)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/uploads/{{$detail}}" alt="" height="100px"></p>
                      <p><a href="{{route('productDtlDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                    </div>
                  @endforeach
                @endif
              </div>

            </div>
            <div class="box-footer">
              <form action="{{route('productAddDetail',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Video">Product Detail  (can attach more than one)</label>
                  <input type="file" class="form-control" name="videos[]" multiple />
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('jslist')
  <script src="{{url('/')}}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
@endsection

@section('pageJs')
<script type="text/javascript">
  $(function(){
    $('#js-data-referal').select2({
      placeholder: 'Cari...',
      ajax: {
        url: '/admin/products/paket/cari/{{$product->brand_id}}',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results:  $.map(data, function (item) {
              return {
                text: item.productName,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
  });
</script>
@endsection
