@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img src="https://teratur.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
        <img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">
      </h2>
    </div>
  </div>
  <form class="form-group" action="{{route('submitEditInvoice', ['id'=>$order[0]->id])}}" method="post" enctype="multipart/form-data">
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
        <br>
        <br>
        <br>
        <label for="toko">Cabang/Depo</label>
        <br>
        <select name="toko" required>
          <option value="">--Pilih Cabang/Depo--</option>
          @foreach($toko as $value)
            @if($value->id == $order[0]->toko)
                <option value="{{$value->id}}" selected>{{$value->nama_ukm}}</option>
            @else
                <option value="{{$value->id}}">{{$value->nama_ukm}}</option>
            @endif
          @endforeach
        </select>
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <label for="nama">Nama Pembeli</label>
        <br>
        <input type="text" name="nama" value="{{$order[0]->nama_pemesan}}" required>
        <br>
        <label for="phone">Nomor Telepon Pembeli</label>
        <br>
        <input type="text" name="phone" required value="{{$order[0]->phone}}">
        <br>
        <label for="alamat">Alamat Pembeli</label>
        <br>
        <textarea name="alamat" rows="3" cols="25" required>{{$order[0]->alamat}}</textarea>
        <br>
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      Detail Transaksi
      <address>
        <label for="platform">Platform Transaksi</label>
        <br>
        <select name="platform" required>
          <option value="" disabled>--Pilih Platform--</option>
          <option value="Tokopedia" {{ $order[0]->platform == 'Tokopedia' ? 'selected' : '' }}>Tokopedia</option>
          <option value="Shopee" {{ $order[0]->platform == 'Shopee' ? 'selected' : '' }}>Shopee</option>
          <option value="Blibli" {{ $order[0]->platform == 'Blibli' ? 'selected' : '' }}>Blibli</option>
          <!--<option value="Bukalapak">Bukalapak</option>-->
          <!--<option value="Whatsapp">Whatsapp</option>-->
          <!--<option value="Facebook/Instagram">Facebook/Instagram</option>-->
          <!--<option value="Website">Website</option>-->
          <!--<option value="Omaru">Omaru</option>-->
          <!--<option value="Hadiah">Hadiah</option>-->
          <!--<option value="Sample">Sample</option>-->
          <!--<option value="Influencer">Influencer</option>-->
          <!--<option value="JDID">JDID</option>-->
          <!--<option value="Compliment">Compliment</option>-->
        </select>
        <br>
        <label for="noinvoice">Nomor Invoice</label>
        <br>
        <input type="text" name="noinvoice" required value="{{$order[0]->kode}}">
        <br>
        @if(empty($order[0]->buktiBayar))
            <label for="invoice">Upload Invoice</label>
            <br>
            <input type="file" name="invoice" required>
        @else
            <label for="invoice">Invoice</label>
            <br>
            <input type="text" name="invoice" required value="{{$order[0]->buktiBayar}}" required>
        @endif
        
        <br>
        <label for="kurir">Tipe Kurir</label>
        <br>
        <label for="platform">Platform Transaksi</label>
        <br>
        <select name="kurir" required>
          <option value="" disabled>--Pilih Kurir--</option>
          @if($order[0]->platform == 'Tokopedia')
            <option value="GoSend" {{ {{$order[0]->kurir}} == 'GoSend' ? 'selected' : '' }}>GoSend</option>
            <option value="SiCepat" {{ {{$order[0]->kurir}} == 'SiCepat' ? 'selected' : '' }}>SiCepat</option>
            <option value="GrabExpress" {{ {{$order[0]->kurir}} == 'GrabExpress' ? 'selected' : '' }}>GrabExpress</option>
            <option value="AnterAja" {{ {{$order[0]->kurir}} == 'AnterAja' ? 'selected' : '' }}>AnterAja</option>
            <option value="Lion Parcel" {{ {{$order[0]->kurir}} == 'Lion Parcel' ? 'selected' : '' }}>Lion Parcel</option>
            <option value="Ninja Xpress" {{ {{$order[0]->kurir}} == 'Ninja Xpress' ? 'selected' : '' }}>Ninja Xpress</option>
            <option value="J&T" {{ {{$order[0]->kurir}} == 'J&T' ? 'selected' : '' }}>J&T</option>
            <option value="Wahana" {{ {{$order[0]->kurir}} == 'Wahana' ? 'selected' : '' }}>Wahana</option>
            <option value="Gojek" {{ {{$order[0]->kurir}} == 'Gojek' ? 'selected' : '' }}>Gojek</option>
            <option value="JNE" {{ {{$order[0]->kurir}} == 'JNE' ? 'selected' : '' }}>JNE</option>
          @elseif($order[0]->platform == 'Tokopedia')
            <option value="GoSend Same Day" {{ $order[0]->platform == 'GoSend Same Day' ? 'selected' : '' }}>GoSend Same Day</option>
            <option value="JNE Trucking (JTR)" {{ $order[0]->platform == 'JNE Trucking (JTR)' ? 'selected' : '' }}>JNE Trucking (JTR)</option>
            <option value="SiCepat REG" {{ $order[0]->platform == 'SiCepat REG' ? 'selected' : '' }}>SiCepat REG</option>
            <option value="JNE Reguler" {{ $order[0]->platform == 'JNE Reguler' ? 'selected' : '' }}>JNE Reguler</option>
            <option value="Shopee Express Hemat" {{ $order[0]->platform == 'Shopee Express Hemat' ? 'selected' : '' }}>Shopee Express Hemat</option>
            <option value="JNE YES" {{ $order[0]->platform == 'JNE YES' ? 'selected' : '' }}>JNE YES</option>
            <option value="GrabExpress Instant" {{ $order[0]->platform == 'GrabExpress Instant' ? 'selected' : '' }}>GrabExpress Instant</option>
            <option value="Shopee Express Standard" {{ $order[0]->platform == 'Shopee Express Standard' ? 'selected' : '' }}>Shopee Express Standard</option>
            <option value="SiCepat Halu" {{ $order[0]->platform == 'SiCepat Halu' ? 'selected' : '' }}>SiCepat Halu</option>
            <option value="GrabExpress Sameday" {{ $order[0]->platform == 'GrabExpress Sameday' ? 'selected' : '' }}>GrabExpress Sameday</option>
            <option value="ID Express" {{ $order[0]->platform == 'ID Express' ? 'selected' : '' }}>ID Express</option>
            <option value="J&T Express" {{ $order[0]->platform == 'J&T Express' ? 'selected' : '' }}>J&T Express</option>
            <option value="J&T Express" {{ $order[0]->platform == 'J&T Express' ? 'selected' : '' }}>J&T Express</option>
          @else
            <option value="GOSEND Instant" {{ $order[0]->platform == 'GOSEND Instant' ? 'selected' : '' }}>GOSEND Instant</option>
            <option value="JNE_Darat" {{ $order[0]->platform == 'JNE_Darat' ? 'selected' : '' }}>JNE_Darat</option>
            <option value="SICEPAT DG" {{ $order[0]->platform == 'SICEPAT DG' ? 'selected' : '' }}>SICEPAT DG</option>
            <option value="ANTERAJA Regular" {{ $order[0]->platform == 'ANTERAJA Regular' ? 'selected' : '' }}>ANTERAJA Regular</option>
            <option value="Grab Instant" {{ $order[0]->platform == 'Grab Instant' ? 'selected' : '' }}>Grab Instant</option>
            <option value="GRAB Sameday" {{ $order[0]->platform == 'GRAB Sameday' ? 'selected' : '' }}>GRAB Sameday</option>
            <option value="GOSEND Sameday" {{ $order[0]->platform == 'GOSEND Sameday' ? 'selected' : '' }}>GOSEND Sameday</option>
            <option value="JNE CASHLESS TRUCKING" {{ $order[0]->platform == 'JNE CASHLESS TRUCKING' ? 'selected' : '' }}>JNE CASHLESS TRUCKING</option>
            <option value="JNE Cashless" {{ $order[0]->platform == 'JNE Cashless' ? 'selected' : '' }}>JNE Cashless</option>
            <option value="SAP LAND" {{ $order[0]->platform == 'SAP LAND' ? 'selected' : '' }}>SAP LAND</option>
            
          @endif

        </select>
        <!--<input type="text" name="kurir" required value="{{$order[0]->kurir}}">-->
      </address>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <input type="submit" class="btn btn-primary pull-right" value="SUBMIT DATA PEMBELI">
      <a href="https://teratur.warisangajahmada.com/admin/cek-invoice/{{$id}}" class="btn btn-danger pull-right" style="margin:0px 20px;">CANCEL</a>

    </div>
  </div>
  @csrf
  </form>
</section>
<div id="editor"></div>

<!-- /.content -->
<div class="clearfix"></div>

@endsection

@section('jslist')
@endsection

@section('jsonpage')

@endsection
