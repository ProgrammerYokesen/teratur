@extends("crudbooster::admin_template")

@section('csslist')
  <link href="{{url('/')}}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section("content")

<div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div>

<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img src="https://gudang.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
        <!--<img class="pull-right"  src="{{ CRUDBooster::getSetting('logo_partner')?asset(CRUDBooster::getSetting('logo_partner')):asset('vendor/crudbooster/assets/logo_crudbooster.png') }}" height="100px">-->
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      Dari

      <address>
        <strong>Warisan Gajahmada</strong><br>
        The Prominence Office Tower <br>
        Alam Sutera, Tangerang Selatan<br>
        Email: info@warisangajahmada.com
      </address>

    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To
      <address>
        <strong>{{$depo->nama_ukm}}</strong><br>
        {{$depo->alamat}}<br>
        {{$depo->kota}}<br>

      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <b>Nomor PO : {{$terima->documentNumber}}</b><br>
      <br>
      <b>Tanggal :</b> {{date('d-M-Y')}}<br>
      <h3 class="text-danger">{{$terima->status_terima}}</h3>
      @if($terima->status_terima!='pending')
      <?php
        $penerima = DB::table('cms_users')->where('id',$terima->diterimaOleh)->first();
      ?>
      <p>Oleh : {{$penerima->name}}</p>
      @endif
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<form class="form-group" action="{{route('distributeVerifikasi',$terima->distid)}}" method="post">

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>Product</th>
          <th>Remark</th>
          <th>Qty</th>
          @if($terima->status_terima!='pending')
          <th>Gap</th>
          <th>Keterangan</th>

          @else
          <th>Keterangan</th>
          @endif
        </tr>
        </thead>
        <tbody>
        @if($produkDikirim)
          @foreach($produkDikirim as $n => $row)

            <tr>
              <td>{{$row->productName}}</td>
              <td>Dikirim</td>
              <td>
                  @foreach($row->stock as $data)
                    <strong>{{$data['qty']}}</strong> <sup>{{$data['name']}}</sup>
                  @endforeach
              </td>
              @if($terima->status_terima!='pending')
              <td> </td>
              <td> </td>
              @endif
            </tr>
            <tr>
              <td width="25%"></td>
              <td>Diterima</td>
              @if($terima->status_terima=='pending')
              <td width="45%">
                <div class="form-group">
                  <div class="input-group">
                      @foreach($row->stock as $data)
                        <input type="number" name="qtyReceive[{{$n}}][]" class="form-control" placeholder="wajib input" required>
                        <div class="input-group-addon">.</div>
                      @endforeach
                  </div>
                </div>
              </td>
              <td width="45%"> <input type="text" name="notes[{{$n}}]" class="form-control" placeholder="Keterangan Selisih Barang"> </td>
              @else
                  <td width="20%">
                    @foreach($row->stock as $data)
                      <strong>{{$data['qtyReceive']}}</strong> <sup>{{$data['name']}}</sup>
                    @endforeach
                  </td>
              <td width="20%">
                @foreach($row->stock as $data)
                  <strong>{{$data['gap']}}</strong> <sup>{{$data['name']}}</sup>
                @endforeach
              </td>
              <td>{{$data['keterangan']}}</td>

              @endif

            </tr>
          @endforeach
        @endif

        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <span onclick="window.print();" class="btn btn-default"><i class="fa fa-print"></i> Print</span>
      @if($terima->status_terima=='pending')
      @csrf
      <input type="submit" class="btn btn-primary pull-right" value="TERIMA STOCK IN">
      @endif
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <small>Waktu cetak: {{date('d-M-Y H:i:s')}}</small>
    </div>
  </div>
</section>
</form>


<!-- /.content -->
<div class="clearfix"></div>

@endsection
