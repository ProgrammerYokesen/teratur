@extends("crudbooster::admin_template")
@section("content")
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h4>Add Products</h4>
        </div>
        <div class="box-body">
          <form action="{{route('productUpdating',$product->id)}}" method="post" name="addProduct" id="addProduct" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="Brand">Brand ID</label>
              <select class="form-control" name="Brand">
                @foreach($brands as $brand)
                  <option value="{{$brand->id}}" {{$product->brand_id == $brand->id ? 'selected' : ''}}>{{$brand->nama_ukm}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="Brand">Client</label>
              <select class="form-control" name="clientId">
                @foreach($clients as $client)
                  <option value="{{$client->id}}" {{$product->client_id == $client->id ? 'selected' : ''}}>{{$client->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name <span class="text-danger">Sinde</span></label>
              <input type="text" name="productName" class="form-control"  placeholder="Product Name" value="{{$product->productName}}">
            </div>
            <div class="form-group">
              <label for="ProductName">Product Name <span class="text-danger">Toko Online</span></label>
              <input type="text" name="productRealName" class="form-control"  placeholder="Nama produk di toko" value="{{$product->productRealName}}">
            </div>
            <div class="form-group">
              <label for="ProductName">kode item</label>
              <input type="text" name="kodeItem" class="form-control"  placeholder="kode item toko" value="{{$product->kode_item}}">
            </div>
            <div class="form-group">
            <!--<label for="Product Name">Nama Unit</label>-->
            <!--<div class="form-group">-->
            <!--  <div class="input-group">-->
            <!--    <div class="input-group-addon">Unit 1</div>-->
            <!--    <input type="text" name="stockNameUnit1" id="nameUnitChange1" class="form-control" value={{$product->stockNameUnit1}} required>-->
            <!--    <div class="input-group-addon">Unit 2</div>-->
            <!--    <input type="text" name="stockNameUnit2" id="nameUnitChange2" class="form-control" value={{$product->stockNameUnit2}} required>-->
            <!--    <div class="input-group-addon">Unit 3</div>-->
            <!--    <input type="text" name="stockNameUnit3" id="nameUnitChange3" class="form-control" value={{$product->stockNameUnit3}} required>-->
            <!--  </div>-->
            <!--</div>-->
          </div>
            <!--<div class="form-group">-->
            <!--  <label for="stockConversion1">stockConversion1</label>-->
            <!--  <input type="text" name="stockConversion1" class="form-control"  placeholder="kode item toko" value="{{$product->stockConversionUnit1}}">-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--  <label for="stockConversion2">stockConversion2</label>-->
            <!--  <input type="text" name="stockConversion2" class="form-control"  placeholder="kode item toko" value="{{$product->stockConversionUnit2}}">-->
            <!--</div>-->
            <!--<div class="form-group">-->
            <!--  <label for="stockConversion3">stockConversion3</label>-->
            <!--  <input type="text" name="stockConversion3" class="form-control"  placeholder="kode item toko" value="{{$product->stockConversionUnit3}}">-->
            <!--</div>-->
            <div class="form-group">
              <label for="Product Name">Product Description</label>
              <textarea name="productDescription" class="form-control" form="addProduct" rows="4" placeholder="Product Name" >{{$product->productDescription}}</textarea>
            </div>
            <div class="form-group">
              <label for="Product Function">Product Function</label>
              <select class="form-control" name="ProductFunction">
                @foreach($functions as $function)
                  <option value="{{$function->id}}" {{$product->productFunction == $function->id ? 'selected' : ''}}>{{$function->nama_kategori}}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="Product Function">Product Status</label>
              <select class="form-control" name="productStatus">
                <option value="active" {{$product->productStatus == 'active' ? 'selected' : ''}}>Active</option>
                <option value="inactive" {{$product->productStatus == 'inactive' ? 'selected' : ''}}>InActive</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Jual Retail</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Website</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice1" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice1}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Reseller</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice2" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice2}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="Product Name">Harga Pokok Penjualan</label>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Rp</div>
                  <input type="text" name="productPrice3" class="form-control" placeholder="tuliskan tanpa tanda baca, contoh 100000" value="{{$product->productPrice3}}">
                  <div class="input-group-addon">.00</div>
                </div>
              </div>
            </div>

            <!--<div class="form-group">-->
            <!--  <label for="Product Name">Satuan Pembelian</label>-->
            <!--  <select class="form-control" name="productNote1" required>-->

            <!--    <option value="{{$product->stockNameUnit1}}" {{$product->productNote1 == $product->stockNameUnit1 ? "selected" : ""}}>{{$product->stockNameUnit1}}</option>-->
            <!--    <option value="{{$product->stockNameUnit2}}" {{$product->productNote1 == $product->stockNameUnit2 ? "selected" : ""}}>{{$product->stockNameUnit2}}</option>-->
            <!--    <option value="{{$product->stockNameUnit3}}" {{$product->productNote1 == $product->stockNameUnit3 ? "selected" : ""}}>{{$product->stockNameUnit3}}</option>-->
            <!--  </select>-->
            <!--</div>-->
            @if(count($convertions) == 0)
              <div class="">
                <div class="row" style="align-items: center">

                  <div class="col-md-3">
                    <label for="Product Name">Nama Unit</label>
                    <div class="form-group">
                        <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" placeholder="Carton" required>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <label for="Product Name">Satuan Unit</label>
                    <div class="form-group">
                        <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" placeholder="0" required>
                    </div>
                  </div>

                  <!--<div class="col-md-3">-->
                  <!--  <label for="Product Name">Stok Awal</label>-->
                  <!--  <div class="form-group">-->
                  <!--      <input type="text" name="stockAwalUnit[]" id="nameUnitChange3" class="form-control" placeholder="0" required>-->
                  <!--  </div>-->
                  <!--</div>-->

                  <div class="col-md-2">
                    <label for=""> </label>
                    <div class="form-group">
                      <button type="button" class="btn btn-info" onclick="addInput()" style="margin-top: 3px; width: 100%">Tambah</button>
                    </div>
                  </div>

                </div>
              </div>

              <div id="moreInput"></div>
              @else
                @foreach($convertions as $index=>$convertion)
                    @if($index ==0)
                      <div class="">
                        <div class="row" style="align-items: center">

                          <div class="col-md-3">
                            <label for="Product Name">Nama Unit</label>
                            <div class="form-group">
                                <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" value="{{$convertion->nama_satuan}}" required>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <label for="Product Name">Satuan Unit</label>
                            <div class="form-group">
                                <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" value="{{$convertion->stock_convertion_unit}}" required>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <label for=""> </label>
                            <div class="form-group">
                              <button type="button" class="btn btn-info" onclick="addInput()" style="margin-top: 3px; width: 100%">Tambah</button>
                            </div>
                          </div>

                        </div>
                      </div>
                    @else
                        <div class="row" style="align-items: center">

                          <div class="col-md-3">
                            <label for="Product Name">Nama Unit</label>
                            <div class="form-group">
                                <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" value="{{$convertion->nama_satuan}}" required>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <label for="Product Name">Satuan Unit</label>
                            <div class="form-group">
                                <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" value="{{$convertion->stock_convertion_unit}}" required>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <label for=""> </label>
                            <div class="form-group">
                              <button type="button" class="btn btn-danger" style="margin-top: 3px; width: 100%" onclick="hapus()" >Hapus</button>
                            </div>
                          </div>

                        </div>
                    @endif
                    <div id="moreInput"></div>
                @endforeach
            @endif
            <div class="form-group">
              <label for="Product Name">Minimum pembelian</label>
              <select class="form-control" name="productNote2" required>
                @for ($i=1; $i < 11; $i++)
                  <option value="{{$i}}" {{$product->productNote2 == $i ? "selected" : ""}}>{{$i}}</option>
                @endfor
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Weight</label>
              <input type="text" name="productNote3" class="form-control" placeholder="jangan pakai petik '" value="{{$product->productNote3}}">
            </div>

            <div class="form-group">
              <label for="Product Name">Product Varian (Rasa)</label>
              <input type="text" name="productNote4" class="form-control" placeholder="jangan pakai petik '" value="{{$product->productNote4}}" readonly>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label BEST</label>
              <select class="form-control" name="productNote5" required>
                <option value="0" {{$product->productNote5 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote5 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote5 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label HOT</label>
              <select class="form-control" name="productNote6" required>
                <option value="0" {{$product->productNote6 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote6 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote6 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label NEW</label>
              <select class="form-control" name="productNote7" required>
                <option value="0" {{$product->productNote7 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote7 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote7 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label OFFER THIS WEEK</label>
              <select class="form-control" name="productNote10" required>
                <option value="0" {{$product->productNote10 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote10 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote10 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product label DISCOUNT</label>
              <select class="form-control" name="productNote8" required>
                <option value="0" {{$product->productNote8 == "0" ? "selected" : ""}}>--Pilih--</option>
                <option value="1" {{$product->productNote8 == "1" ? "selected" : ""}}>Ya</option>
                <option value="2" {{$product->productNote8 == "2" ? "selected" : ""}}>Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label for="Product Name">Product Discount</label>
              <div class="input-group">
                <div class="input-group-addon">Diskon</div>
                <input type="text" name="productNote9" class="form-control" placeholder="00.00" value="{{$product->productNote9}}">
                <div class="input-group-addon">%</div>
              </div>
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-block" value="Simpan" />
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto display</h4>

            </div>
            <div class="box-body">
              <?php
                $displays = unserialize($product->productImages);
                //dd($displays);
              ?>
              <div class="row">
                @if($displays)
                  @foreach ($displays as $n => $display)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/{{$display}}" alt="" width="100%"></p>
                      <p><a href="{{route('productImgDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                      <hr>
                    </div>
                  @endforeach
                @endif
              </div>

            </div>
            <div class="box-footer">
              <form action="{{route('productAddDisplay',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Name">Product Display (can attach more than one):</label>
                  <br />
                  <input type="file" class="form-control" name="photos[]" multiple/>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h4>foto detail</h4>
            </div>
            <div class="box-body">
              <?php
                $details = unserialize($product->productVideos);
              ?>
              <div class="row">
                @if($details)
                  @foreach ($details as $n => $detail)
                    <div class="col-md-4">
                      <p><img src="{{$url}}/{{$detail}}" alt="" height="100px"></p>
                      <p><a href="{{route('productDtlDelete',[$product->id,$n])}}" class="btn btn-danger btn-xs btn-block">delete</a></p>
                    </div>
                  @endforeach
                @endif
              </div>

            </div>
            <div class="box-footer">
              <form action="{{route('productAddDetail',$product->id)}}" method="post" name="addProduct" id="editDisplay" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="Product Video">Product Detail  (can attach more than one)</label>
                  <input type="file" class="form-control" name="videos[]" multiple />
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="submit" name="submitDetail" value="Upload" class="btn btn-success btn-block">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('pageJs')
<script>
    function addInput() {
      console.log('jalan')
      $('#moreInput').append(`
        <div class="row" style="align-items: center">

          <div class="col-md-3">
            <label for="Product Name">Nama Unit</label>
            <div class="form-group">
                <input type="text" name="stockNameUnit[]" id="nameUnitChange1" class="form-control" placeholder="Carton" required>
            </div>
          </div>

          <div class="col-md-3">
            <label for="Product Name">Satuan Unit</label>
            <div class="form-group">
                <input type="text" name="stockConversionUnit[]" id="nameUnitChange2" class="form-control" placeholder="0" required>
            </div>
          </div>

          <div class="col-md-2">
            <label for=""> </label>
            <div class="form-group">
              <button type="button" class="btn btn-danger" style="margin-top: 3px; width: 100%" onclick="hapus()" >Hapus</button>
            </div>
          </div>

        </div>
        `)
    }

    function hapus(){
        console.log('hapus');
    }
</script>
@endsection
