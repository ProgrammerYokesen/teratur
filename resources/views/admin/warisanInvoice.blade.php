<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>

    <style>
        .container {
            max-width: 1320px;
            margin-right: 20px;
            margin-left: 20px;
            width: 100%;
            position: relative;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            vertical-align: top;
            border-color: #bbbbbb;
            border: 1px solid #bbbbbb;
            caption-side: bottom;
            border-collapse: collapse;
            border-radius: 5px
        }

        .table>tbody {
            vertical-align: inherit;
        }

        tbody,
        td,
        tfoot,
        th,
        thead,
        tr {
            border-color: inherit;
            border-style: solid;
            border-width: 1px;
        }

        .table-striped>tbody>tr:nth-of-type(odd) {
            background-color: #dedede;
            color: black;
        }

        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }

        .table-no {
            border: none !important;
            text-align: left;
        }

        .table-no tbody,
        .table-no td,
        .table-no tfoot,
        .table-no th,
        .table-no thead,
        .table-no tr {
            border-style: none;
        }
        td{
            font-size:18px;
        }
    </style>

</head>

<body>
    <section class="invoice container">
        <div class="row page-header">
            <div class="col-4">
                <img src="https://teratur.warisangajahmada.com/images/ced-logo-grey.png" height="150px">
            </div>
            <div class="col-4">
            </div>
            <div class="col-4">
            </div>
        </div>

        <table class="table table-no" style="margin-top: 3rem">
            <thead>
                <tr>
                    <td><b> Dari </b></td>
                    <td><b> To </b></td>
                    <td><b> Detail Transaksi </b></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <address>
                            <p><strong>Warisan Perdagangan Digital</strong></p>
                            <p>
                                Ruko Crystal 8 no 18 <br>
                                Alam Sutera, Tangerang Selatan<br>
                                Email: info@warisangajahmada.com
                            </p>
                        </address>
                    </td>

                    <td>
                        <address>
                            <p><strong>{{$value->nama_pemesan}}</strong></p>
                            <p>{!! nl2br(e($value->alamat)) !!}</p>
                            <p><strong>Phone:</strong> {{$value->phone}}</p>
                        </address>
                    </td>

                    <td>
                        <p><b>No Invoice : {{$value->kode}} </b> </p>
                        <p> Payment : PAID <br>
                            Platform : Warisan Perdagangan<br>
                            Kurir : {{$value->kurir}}</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="row" style="margin-top: 3rem">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                    <tr>
                        
                            <td>{{$product->item}}</td>
                            <td>{{$product->qty}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <div id="editor"></div>
    <div class="clearfix"></div>

</body>

</html>
