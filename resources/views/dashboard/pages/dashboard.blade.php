{{-- @extends('dash-master') --}}
@extends("crudbooster::admin_template")

@section('otherAssets')
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"> --}}

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">


    {{-- Custom CSS --}}
    <link href="{{ asset('css/dashboard/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <a id="button__scroll__top"></a>
        <h2 class="fw-bolder">Stock Aggregation</h2>

        <div class="product__section mt__3">

            <h4 class="fw__semi text-danger">
                Need to Restock {{ $countRestock }}
            </h4>
            <div class="row list__product l_red">
                @foreach ($restock as $item)
                    @component('dashboard.components.red-dash-card')
                        @slot('productName') {{ $item->name }} @endslot
                        @slot('productCode') {{ $item->sku }} @endslot
                        @slot('productCount') {{ $item->stock }} @endslot
                    @endcomponent
                @endforeach()
            </div>
        </div>

        <div class="product__section mt__3">
            <div class="d-flex justify-content-between align-center mb-4">
                <h4 class="fw__semi">
                    Available Product
                </h4>
                <div class="position-relative">
                    <i class="fas fa-search position-absolute" style="padding: 10px"></i>
                    <input type="text" class="form-control" style="padding-left: 30px">
                </div>
            </div>
            <div class="row list__product justify-content-between">
                @foreach ($data as $item)
                    @component('dashboard.components.dash-card')
                        @slot('productName') {{ $item->name }} @endslot
                        @slot('productCode') {{ $item->sku }} @endslot
                        @slot('productCount') {{ $item->stock }} @endslot
                    @endcomponent
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section('pageJs')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datables.bootstrap5.js') }}"></script>
    <script>
        var btn = $('#button__scroll__top');

        $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
            btn.addClass('show');
          } else {
            btn.removeClass('show');
          }
        });
        
        btn.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({scrollTop:0}, '300');
        });
    </script>
@endsection
