{{-- @extends('dash-master') --}}
@extends("crudbooster::admin_template")

@section('otherAssets')
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous"> --}}

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

    {{-- Custom CSS --}}
    <link href="{{ asset('css/dashboard/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/dashboard/history.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <section>
        <div class="card history__card mb-5">
            <div class="row g-0 p-4">
                <div class="col-xl-4 col-md-5 col-12 img__box">
                    <img src="https://uploads-ssl.webflow.com/5cf92d7a994fb751f90ebdad/5f3e9f8da33b4c160b7e051c_Interactive%20items.png"
                        class="img-fluid rounded-start" alt="unknown">
                </div>
                <div class="col-xl-8 col-md-7 col-12">
                    <div class="card-body">
                        <h6 class="t_600_gray_16">
                            Nama Produk
                        </h6>
                        <h6 class="t_600_black_16">
                            BARDI Smart LIGHT BULB RGBWW 12W Wifi Wireless IoT ...
                        </h6>
                        <h6 class="t_600_gray_16 pt-2">
                            Kode SKU
                        </h6>
                        <h6 class="t_600_black_16">
                            BAR-001
                        </h6>
                        <h6 class="t_600_gray_16 pt-2">
                            Waktu dibuat
                        </h6>
                        <h6 class="t_600_black_16">
                            14:21, 1 Maret 2021
                        </h6>
                        <h6 class="t_600_gray_16 pt-2">
                            Terakhir diperbaharui
                        </h6>
                        <h6 class="t_600_black_16">
                            19:00, 4 Maret 2021
                        </h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="card history__card mb-5">
            <div class="card-body">
                <h4>
                    Update Stock
                </h4>
                <form action="" method="post">
                    @csrf
                    <div class="row row__update">
                        <div class="pb-2 col-md-3 d-flex align-center justify-content-between align-center">
                            <div class=" t_600_black_14">
                                Stock saat ini :
                            </div>
                            <div class="t_600_red_14">
                                15
                            </div>
                        </div>
                        <div class="pb-2 col-md-3 d-flex align-center justify-content-center align-center">
                            <div class="t_600_black_14">
                                Tambah Stock
                            </div>
                        </div>
                        <div class="pb-2 col-md-3 d-flex align-center justify-content-between align-center">
                            <input type="number" class="form-control form__control" width="90%">
                            <div class="t_600_black_14 ps-1">
                                Unit
                            </div>
                        </div>
                        <div class="pb-2 col-md-3 d-flex align-center justify-content-center align-center">
                            <button class="btn btn-dark">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div style="display: grid">
            <div class="table-responsive">
                <table id="riwayat_table" class="table table-borderless">
                    <thead class="table-dark">
                        <tr>
                            <th colspan="1"
                                class=""><h4>Riwayat Transaksi</h4></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>No. Invoice</th>
                            <th>Nama Pembeli</th>
                            <th>Kurir</th>
                            <th>Platform</th>
                            <th>Waktu Pesanan</th>
                            <th>Gudang</th>
                        </tr>
                    </thead>
                    <tbody style="
                                background-color: white">
                        <tr>
                            <td>INV/20210924/MPL/171147182</td>
                            <td>Rifki Alifianto</td>
                            <td>Sicepat</td>
                            <td>Tokopedia</td>
                            <td>2021-08-04 02:47:51</td>
                            <td>Gudang BARDI Banten</td>
                        </tr>
                        <tr>
                            <td>INV/20210924/MPL/171147182</td>
                            <td>Rifki Alifianto</td>
                            <td>Sicepat</td>
                            <td>Tokopedia</td>
                            <td>2021-08-04 02:47:51</td>
                            <td>Gudang BARDI Banten</td>
                        </tr>
                        <tr>
                            <td>INV/20210924/MPL/171147182</td>
                            <td>Rifki Alifianto</td>
                            <td>Sicepat</td>
                            <td>Tokopedia</td>
                            <td>2021-08-04 02:47:51</td>
                            <td>Gudang BARDI Banten</td>
                        </tr>
                        <tr>
                            <td>INV/20210924/MPL/171147182</td>
                            <td>Rifki Alifianto</td>
                            <td>Sicepat</td>
                            <td>Tokopedia</td>
                            <td>2021-08-04 02:47:51</td>
                            <td>Gudang BARDI Banten</td>
                        </tr>
                        <tr>
                            <td>INV/20210924/MPL/171147182</td>
                            <td>Rifki Alifianto</td>
                            <td>Sicepat</td>
                            <td>Tokopedia</td>
                            <td>2021-08-04 02:47:51</td>
                            <td>Gudang BARDI Banten</td>
                        </tr>
                        </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('pageJs')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datables.bootstrap5.js') }}"></script>
    <script>
        $(function() {
            $('#riwayat_table').DataTable();
        })
    </script>
@endsection
