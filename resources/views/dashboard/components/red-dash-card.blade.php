<div class="col-xl-2 col-md-3 col-6 px-2 mb-3">
    <div class="red_dash__card">
        <p class="t_600_black_14 dcard__name">
            {{ $productName }}
        </p>
        <p class="t_600_gray_14 mt-1">
            {{ $productCode }}
        </p>
        <p class="t_600_black_48 mt-1">
            {{ $productCount }}
        </p>
    </div>
</div>
