<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Teratur</title>
    {{-- Bootstrap --}}
    {{-- CSS only --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    {{-- Custom CSS --}}

    <link href="{{ asset('css/station/style.css?v=0.0.32') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div aria-live="polite" aria-atomic="true" class="position-relative d-print-none" style="z-index: 1">
        <!-- Position it: -->
        <!-- - `.toast-container` for spacing between toasts -->
        <!-- - `.position-absolute`, `top-0` & `end-0` to position the toasts in the upper right corner -->
        <!-- - `.p-3` to prevent the toasts from sticking to the edge of the container  -->
        <div class="toast-container position-fixed top-0 end-0 p-3 d-print-none">

            <!-- Then put toasts within -->
            <div id="toast__success" class="toast fade bg__success" role="alert" data-bs-animation="true"
                data-bs-autohide="true" data-bs-delay="7000" aria-live="assertive" aria-atomic="true"
                style="overflow: hidden">
                <div class="toast-body position-relative">
                    <div class="d-flex">
                        <img src="{{ asset('images/success.png') }}" width="40px" height="40px"
                            class="rounded me-2 inline-flex" alt="...">
                        <div>
                            <div>
                                <strong class="me-auto text-light">Success!</strong>
                            </div>
                            <div>
                                <small class="text__toast_sc text-light fw-normal">Berhasil melakukan print
                                    label</small>
                            </div>
                        </div>
                    </div>
                    <div style="cursor: pointer" class="btn__close position-absolute" data-bs-dismiss="toast"
                        aria-label="Close">
                        <i class="fas fa-times" style="color: white"></i>
                    </div>
                    <div class="progress__bar">

                    </div>
                </div>
            </div>

            <div id="toast__fail" class="toast fade bg__fail" role="alert" data-bs-animation="true"
                data-bs-autohide="true" data-bs-delay="7000" aria-live="assertive" aria-atomic="true"
                style="overflow: hidden">
                <div class="toast-body position-relative">
                    <div class="d-flex">
                        <img src="{{ asset('images/fail.png') }}" width="40px" height="40px"
                            class="rounded me-2 inline-flex" alt="...">
                        <div>
                            <div>
                                <strong class="me-auto text-light">Failed!</strong>
                            </div>
                            <div>
                                <small class="text__toast_sc text-light fw-normal">Berhasil melakukan print
                                    label</small>
                            </div>
                        </div>
                    </div>
                    <div style="cursor: pointer" class="btn__close position-absolute" data-bs-dismiss="toast"
                        aria-label="Close">
                        <i class="fas fa-times" style="color: white"></i>
                    </div>
                    <div class="progress__bar">

                    </div>
                </div>
            </div>
            <div id="toast__info" class="toast fade bg__info" role="alert" data-bs-animation="true"
                data-bs-autohide="true" data-bs-delay="7000" aria-live="assertive" aria-atomic="true"
                style="overflow: hidden">
                <div class="toast-body position-relative">
                    <div class="d-flex">
                        <img src="{{ asset('images/info.png') }}" width="40px" height="40px"
                            class="rounded me-2 inline-flex" alt="...">
                        <div>
                            <div>
                                <strong class="me-auto text-light">INFO!</strong>
                            </div>
                            <div>
                                <small class="text__toast_sc text-light fw-normal">Berhasil melakukan print
                                    label</small>
                            </div>
                        </div>
                    </div>
                    <div style="cursor: pointer" class="btn__close position-absolute" data-bs-dismiss="toast"
                        aria-label="Close">
                        <i class="fas fa-times" style="color: white"></i>
                    </div>
                    <div class="progress__bar">

                    </div>
                </div>
            </div>
            <div id="toast__warning" class="toast fade bg__warning" role="alert" data-bs-animation="true"
                data-bs-autohide="true" data-bs-delay="7000" aria-live="assertive" aria-atomic="true"
                style="overflow: hidden">
                <div class="toast-body position-relative">
                    <div class="d-flex">
                        <img src="{{ asset('images/info.png') }}" width="40px" height="40px"
                            class="rounded me-2 inline-flex" alt="...">
                        <div>
                            <div>
                                <strong class="me-auto text-light">warning!</strong>
                            </div>
                            <div>
                                <small class="text__toast_sc text-light fw-normal">Berhasil melakukan print
                                    label</small>
                            </div>
                        </div>
                    </div>
                    <div style="cursor: pointer" class="btn__close position-absolute" data-bs-dismiss="toast"
                        aria-label="Close">
                        <i class="fas fa-times" style="color: white"></i>
                    </div>
                    <div class="progress__bar">

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('station.components.navbar')

    <div class="container " style="margin-top: 3rem">
        @yield('content')
        <div aria-live="polite" aria-atomic="true" style="z-index: 1" class='d-print-none'>
            <div class="toast-container position-absolute bottom-0 end-0 p-3">
                <div id="toast__card" class="toast fade bg-light" role="alert" data-bs-animation="true"
                    data-bs-autohide="true" data-bs-delay="7000" aria-live="assertive" aria-atomic="true"
                    style="overflow: hidden">
                    <div class="toast-header bg-dark position-relative" style="overflow: hidden">
                        <div class="d-flex justify-content-between w-100 align-center">
                            <div class="text-white">Pesanan berhasil di scan</div>
                            <div class="d-flex">
                                @if (!empty($getOrder[0]->logistic_type == 'dropoff'))
                                    <div id="badge__card" class="mr__1 badge p-2 text-uppercase">DROP</div>
                                @else
                                    <div id="badge__card" class="mr__1 badge p-2 text-uppercase">PICK UP</div>
                                @endif
                                <div style="cursor: pointer" class="btn__close" data-bs-dismiss="toast"
                                    aria-label="Close">
                                    <i class="fas fa-times" style="color: white"></i>
                                </div>
                            </div>
                        </div>
                        <div class="progress__bar_card"></div>
                    </div>
                    <div class="toast-body position-relative bg-white">
                        <ul id="list__toast_card" class="list-group list-group-flush t_600_black_14">
                            @if (!empty($getOrder[0]->order))
                                @foreach ($getOrder[0]->order as $order)
                                    <li class="list-group-item d-flex align-center">
                                        <img class="mr__1" width="45px" height="45px" style="cursor: pointer"
                                            onclick="openImage('{{ $order->photo }}')" src="{{ $order->photo }}"
                                            alt="">
                                        <div>
                                            <div>
                                                {{ $order->initial }}
                                            </div>
                                            <div class="fw-bold text-uppercase">
                                                {{ $order->qty }} BARANG
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade d-print-none" id="modalImage" tabindex="-1" aria-labelledby="modalImageLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <img id="modal__img" class="img-fluid" src="" alt="unknown">
                </div>
            </div>
        </div>

        <!-- modal resi -->
        <div class="modal fade" tabindex="-1" id="modalResi">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <form method="post" action="{{route('updatePLogistic')}}" id="resiForm">

              <div class="modal-body modal-resiContent">
              </div>
              </form>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="showProduct" tabindex="-1" aria-labelledby="showProductLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-dark">
                        <h5 class="modal-title text-light">Detail Order</h5>
                        <button type="button" class="bg-light btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <ul id="list__show_product" class="list-group list-group-flush t_600_black_14">
                            <li class="list-group-item d-flex align-center">
                                <img class="mr__1" width="45px" height="45px"
                                    src="https://i.pinimg.com/564x/86/8b/e2/868be2670da5c368733eef11e793e3f7.jpg"
                                    alt="">
                                <div>
                                    <div>
                                        Orderan 1 1 1 1
                                    </div>
                                    <div class="fw-bold text-uppercase">
                                        12 BARANG
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-center">
                                <img class="mr__1" width="45px" height="45px"
                                    src="https://i.pinimg.com/564x/86/8b/e2/868be2670da5c368733eef11e793e3f7.jpg"
                                    alt="">
                                <div>
                                    <div>
                                        Orderan 1 1 1 1
                                    </div>
                                    <div class="fw-bold text-uppercase">
                                        12 BARANG
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @yield('printPage')
        {{-- JQUERY --}}
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
        </script>
        <script src="https://rawgit.com/kabachello/jQuery-Scanner-Detection/master/jquery.scannerdetection.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            function openImage(url) {
                $("#modal__img").attr("src", url);
                $('#modalImage').modal('toggle');
            }

            function showOrder(invoice) {
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('detailDone') }}",
                    type: "POST",
                    data: {
                        id: invoice
                    },
                    success: function(response) { 
                        $('#showProduct').modal('toggle');
                        if (response.length > 0) {

                            $('#list__show_product').empty()

                            response.map((item) => {
                                $('#list__show_product').append(`
                                    <li class="list-group-item d-flex align-center">
                                        <img class="mr__1" width="45px" height="45px"
                                            src="${item.photo}"
                                            alt="">
                                        <div>
                                            <div>
                                                ${item.item}
                                            </div>
                                            <div class="fw-bold text-uppercase">
                                                ${item.qty} BARANG
                                            </div>
                                        </div>
                                    </li>
                                `)
                            })
                        }
                    },
                    error: function(err) {
                        console.log("error")
                        console.log(err)
                        Swal.fire("Gagal!", err, "error");
                    }
                });
            }

            // function showOrder(data) {
            //     $('#showProduct').modal('toggle');

            //     if (data.length > 0) {
            //         data.map((item) => {
            //             $('#list__show_product').empty()
            //             $('#list__show_product').append(`
            //                 <li class="list-group-item d-flex align-center">
            //                     <img class="mr__1" width="45px" height="45px"
            //                         src="${item.image}"
            //                         alt="">
            //                     <div>
            //                         <div>
            //                             ${item.productName}
            //                         </div>
            //                         <div class="fw-bold text-uppercase">
            //                             ${item.qty} BARANG
            //                         </div>
            //                     </div>
            //                 </li>
            //             `)
            //         })
            //     }
            // }

            function checkURL(urlPath) {
                return window.location.href.substring(window.location.href.lastIndexOf('/') + 1).includes(urlPath)
            }

            // show toast
            function showToast(elm, text) {
                new bootstrap.Toast($(elm)).show()
                $('.text__toast_sc').text(text)
                let diff = 100

                let intrv = setInterval(() => {
                    diff--
                    $('.progress__bar').css("width", `${diff}%`)

                    if (diff == 0) {
                        clearInterval(intrv)
                    }
                }, 70);
            }

            // show toast card
            function showToastCard(
                elm,
                type,
                child
            ) {
                new bootstrap.Toast($(elm)).show()
                let badge = type == "pickUp" ? "bg-info" : "bg-warning"
                $("#badge__card").addClass(badge)
                $("#badge__card").text(type)
                $("#list__toast_card").append(child)
                let diff = 100

                let intrv = setInterval(() => {
                    diff--
                    $('.progress__bar_card').css("width", `${diff}%`)

                    if (diff == 0) {
                        clearInterval(intrv)
                    }
                }, 70);
            }

            $(function() {
                if (checkURL("pack") || checkURL("stations")) {
                    $('.nav-item').eq(1).addClass("nav__active")
                } else if (checkURL("drop")) {
                    $('.nav-item').eq(2).addClass("nav__active")
                } else if (checkURL("pick")) {
                    $('.nav-item').eq(3).addClass("nav__active")
                } else if (checkURL("done")) {
                    $('.nav-item').eq(4).addClass("nav__active")
                }
            })
        </script>

        @yield('jsPage')
    </div>
</body>


</html>
