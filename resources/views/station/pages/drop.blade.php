@extends('master')

@section('content')
    <section class="container">
        <div class="chooser__filter" style="margin-bottom: 1rem">
            <button type="button" class="btn primer__btn" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                Filter
            </button>
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Filter</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form method="post" action="{{ route('searchOrder', ['type' => 'dropoff']) }}">
                            @csrf
                            <div id="qToko"></div>
                            <div class="modal-body">
                                <div class="mb-3 row">
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="JNE"
                                            id="jne_kurir">
                                        <label class="form-check-label" for="jne_kurir">
                                            JNE
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="JNT"
                                            id="jnt_kurir">
                                        <label class="form-check-label" for="jnt_kurir">
                                            JNT
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="SiCepat"
                                            id="cepat_kurir">
                                        <label class="form-check-label" for="cepat_kurir">
                                            Si Cepat
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Gojek"
                                            id="gojek_kurir">
                                        <label class="form-check-label" for="gojek_kurir">
                                            Gojek
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Grab"
                                            id="grab_kurir">
                                        <label class="form-check-label" for="grab_kurir">
                                            Grab
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Ninja"
                                            id="ninja_kurir">
                                        <label class="form-check-label" for="ninja_kurir">
                                            Ninja
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Wahana"
                                            id="wahana_kurir">
                                        <label class="form-check-label" for="wahana_kurir">
                                            Wahana
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Shopee"
                                            id="shopee_kurir">
                                        <label class="form-check-label" for="shopee_kurir">
                                            Shopee
                                        </label>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <div class="col-md-5 col-sm-5">
                                        <input type="date" class="form-control" name="start_date">
                                    </div>
                                    <div class="col-md-2 col-sm-2 text-center pt-2">
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <input type="date" class="form-control" name="end_date">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <select id="select__toko" class="form-select" name="tokoSelect"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Tambah Toko</option>
                                        @foreach ($allStore as $store)
                                            <option value="{{ $store->store_name }}">{{ $store->store_name }}</option>

                                        @endforeach
                                    </select>
                                    <ul id="list_toko" class="list-group">
                                    </ul>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary search_filter"
                                    data-bs-dismiss="modal">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="tokped_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="tokped__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>TOKOPEDIA {{ $countTokpedDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasTokpedDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_tokped" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{ $data->store_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="shopee_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="shopee__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_shopee" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Shopee {{ $countShopeeDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasShopeeDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_shopee" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{ $data->store_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="blibli_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="blibli__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_blibli" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Blibli {{ $countBlibliDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasBlibliDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_blibli" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{ $data->store_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="jdid_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="jdid__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_jdid" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>JD.ID {{ $countJdidDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasJdidDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_jdid" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{ $data->store_name }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="warisan_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="warisan__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_warisan" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>warisan {{ $countWarisanDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasWarisanDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_warisan" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{$data->store_name}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="harbolnas_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="harbolnas__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_harbolnas" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Harbolnas {{ $countHarbolnasDrop }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datasHarbolnasDrop as $data)
                        <tr>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input check__toko_harbolnas" type="checkbox"
                                        value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $data->kode }}
                                    </label>
                                </div>
                            </td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>-</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--<div id="sinde_section" class="mb__3">-->
        <!--    <div class="d-flex justify-content-end" style="margin-bottom: 1rem">-->
        <!--        <button id="ha__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>-->
        <!--    </div>-->
        <!--    <table id="drop__table_sinde" class="table table-borderless" style="width:100%">-->
        <!--        <thead class="table-dark">-->
        <!--            <tr>-->
        <!--                <th>Sinde {{ $countJdidDrop }} PAKET</th>-->
        <!--                <th>Nama Pemesan</th>-->
        <!--                <th>Kurir</th>-->
        <!--                <th>Tgl Pemesanan</th>-->
        <!--                <th>Nama Toko</th>-->
        <!--            </tr>-->
        <!--        </thead>-->
        <!--        <tbody>-->
        <!--            @foreach ($datasHarbolnasDrop as $data)-->
        <!--                <tr>-->
        <!--                    <td>-->
        <!--                        <div class="form-check">-->
        <!--                            <input class="form-check-input check__toko_harbolnas" type="checkbox"-->
        <!--                                value="{{ $data->kode }}" id="item__{{ $data->kode }}">-->
        <!--                            <label class="form-check-label" for="flexCheckChecked">-->
        <!--                                {{ $data->kode }}-->
        <!--                            </label>-->
        <!--                        </div>-->
        <!--                    </td>-->
        <!--                    <td>{{ $data->nama_pemesan }}</td>-->
        <!--                    <td>{{ $data->kurir }}</td>-->
        <!--                    <td>{{ $data->created_at }}</td>-->
        <!--                    <td>-</td>-->
        <!--                </tr>-->
        <!--            @endforeach-->
        <!--        </tbody>-->
        <!--    </table>-->
        <!--</div>-->
    </section>
@endsection

@section('jsPage')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datables.bootstrap5.js') }}"></script>

    <script>
        var itemTokped = []
        var itemShopee = []
        var itemBlibli = []
        var itemJdid = []
        var itemHarbolnas = []
        var itemWarisan = []
        var tokpedCount = 0
        var shopeeCount = 0
        var blibliCount = 0
        var jdidCount = 0
        var harbolnasCount = 0
        var warisanCount = 0

        // if(itemWarisan)

        function addQuery(name, value) {
            $('#qToko').append(`
            <input type="hidden" name="${name}[]" value="${value}">
            `)
        }

        $(document).ready(function() {
            var filters = []
            $('#drop__table_shopee').DataTable();
            $('#drop__table_blibli').DataTable();
            $('#drop__table').DataTable();
            $('#drop__table_jdid').DataTable();
            $('#drop__table_harbolnas').DataTable();
            $('#drop__table_warisan').DataTable();

            $('#select__toko').on('change', function() {
                let choosed = this.value
                let text = $(this).find('option').filter(':selected').text()

                // console.log(choosed)
                // console.log(text)
                if (!filters.includes(text)) {
                    filters.push(text)
                    addQuery('toko', choosed)

                    $('#list_toko').append(`
                    <li class="list-group-item">${text}</li>
                    `)
                }
            })

            // warisan handler
            $('#drop__table_warisan').on('change', '.check__toko_warisan', function() {
                if (this.checked) {
                    warisanCount++
                } else {
                    warisanCount--
                }

                if (warisanCount > 0) {
                    $('#warisan__btn').prop('disabled', false)
                } else {
                    $('#warisan__btn').prop('disabled', true)
                }

                console.log(warisanCount)
            });

            $('#warisan__btn').on('click', function() {
                let box = $('.check__toko_warisan')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                        itemWarisan.push(item.value)
                    }
                })
                let data = {
                    invoices
                }
                $('.modal-resiContent').empty();
                $('.modal-resiContent').append(`
                    <div class="mb-3 orderData" data-orderId=${invoices[0]}>
                      <label for="noResi" class="form-label">No Resi</label>
                      <input type="text" name="inputResi" class="form-control" id="noResi" aria-describedby="resi" required>

                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  `)

                $('#modalResi').modal('show');
                // console.log(invoices);
                // $.ajax({
                //     url: "{{ route('updatePLogistic') }}",
                //     type: 'POST',
                //     data: data,
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     },
                //     success: function(data) {
                //         console.log(data)
                //     }
                // })
                // submit ajax, tokped button disable jadi true lagi
                // showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                // setTimeout(function() {
                //     location.reload();
                // }, 5000);
            })
            $('#resiForm').on('submit', function(e){
              e.preventDefault();
              let id = [$('.orderData').attr('data-orderId')];
              let resi = $('#noResi').val();
              let data = {
                invoices : id,
                resi
              }
              // console.log(id, itemWarisan, resi, data);
              $.ajax({
                  url: "{{ route('updatePLogistic') }}",
                  type: 'POST',
                  data: data,
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data) {
                      console.log(data)
                      if(itemWarisan.length > 1){
                        // $('#modalResi').modal('hide');
                        itemWarisan.shift();
                        $('.modal-resiContent').empty();
                        $('.modal-resiContent').append(`
                            <div class="mb-3 orderData" data-orderId=${itemWarisan[0]}>
                              <label for="noResi" class="form-label">No Resi</label>
                              <input type="text" name="inputResi" class="form-control" id="noResi" aria-describedby="resi" required>

                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          `)
                          showToast("#toast__success", `1 pesanan telah selesai dan dipindahkan ke done`)
                        $('#modalResi').modal('show');
                      }else if(itemWarisan.length == 1){
                        itemWarisan.shift();
                        showToast("#toast__success", `1 pesanan telah selesai dan dipindahkan ke done`)
                        $('#modalResi').modal('toggle');
                        setTimeout(function() {
                            location.reload();
                        }, 5000);
                      }else{
                        console.log('sudah habis')
                        $('#modalResi').modal('toggle');
                      }
                      console.log(data);
                  }
              })

            })
            // harbolnas handler
            $('#drop__table_harbolnas').on('change', '.check__toko_harbolnas', function() {
                if (this.checked) {
                    harbolnasCount++
                } else {
                    harbolnasCount--
                }

                if (harbolnasCount > 0) {
                    $('#harbolnas__btn').prop('disabled', false)
                } else {
                    $('#harbolnas__btn').prop('disabled', true)
                }

                console.log(harbolnasCount)
            });

            $('#harbolnas__btn').on('click', function() {
                let box = $('.check__toko_harbolnas')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                    }
                })
                let data = {
                    invoices
                }
                // console.log(invoices);
                $.ajax({
                    url: "{{ route('updatePLogistic') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log(data)
                    }
                })
                // submit ajax, tokped button disable jadi true lagi
                showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                setTimeout(function() {
                    location.reload();
                }, 5000);
            })

            // TOKPED HANDLER
            $('#drop__table').on('change', '.check__toko_tokped', function() {
                if (this.checked) {
                    tokpedCount++
                } else {
                    tokpedCount--
                }

                if (tokpedCount > 0) {
                    $('#tokped__btn').prop('disabled', false)
                } else {
                    $('#tokped__btn').prop('disabled', true)
                }

                console.log(tokpedCount)
            });

            $('#tokped__btn').on('click', function() {
                let box = $('.check__toko_tokped')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                    }
                })
                let data = {
                    invoices
                }
                // console.log(invoices);
                $.ajax({
                    url: "{{ route('updatePLogistic') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log(data)
                    }
                })
                // submit ajax, tokped button disable jadi true lagi
                showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                setTimeout(function() {
                    location.reload();
                }, 5000);
            })

            // SHOPEE HANDLER
            $('#drop__table_shopee').on('change', '.check__toko_shopee', function() {
                if (this.checked) {
                    shopeeCount++
                } else {
                    shopeeCount--
                }

                if (shopeeCount > 0) {
                    $('#shopee__btn').prop('disabled', false)
                } else {
                    $('#shopee__btn').prop('disabled', true)
                }

                console.log(shopeeCount)
            });

            $('#shopee__btn').on('click', function() {
                let box = $('.check__toko_shopee')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                    }

                })
                let data = {
                    invoices
                }
                // console.log(invoices);
                $.ajax({
                    url: "{{ route('updatePLogistic') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log(data)
                    }
                })

                // submit ajax, shopee button disable jadi true lagi
                showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                setTimeout(function() {
                    location.reload();
                }, 5000);
            })

            // BLIBLI HANDLER
            $('#drop__table_blibli').on('change', '.check__toko_blibli', function() {
                if (this.checked) {
                    blibliCount++
                } else {
                    blibliCount--
                }

                if (blibliCount > 0) {
                    $('#blibli__btn').prop('disabled', false)
                } else {
                    $('#blibli__btn').prop('disabled', true)
                }

                console.log(blibliCount)
            });

            $('#blibli__btn').on('click', function() {
                let box = $('.check__toko_blibli')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                    }
                })
                let data = {
                    invoices
                }
                // console.log(invoices);
                $.ajax({
                    url: "{{ route('updatePLogistic') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log(data)
                    }
                })

                // submit ajax, shopee button disable jadi true lagi
                showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                setTimeout(function() {
                    location.reload();
                }, 5000);
            })

            // JD.ID HANDLER
            $('#drop__table_jdid').on('change', '.check__toko_jdid', function() {
                if (this.checked) {
                    jdidCount++
                } else {
                    jdidCount--
                }

                if (jdidCount > 0) {
                    $('#jdid__btn').prop('disabled', false)
                } else {
                    $('#jdid__btn').prop('disabled', true)
                }

                console.log(jdidCount)
            });

            $('#jdid__btn').on('click', function() {
                let box = $('.check__toko_jdid')
                let count = 0
                let invoices = []
                box.map((idx, item) => {
                    if (item.checked) {
                        // mapping the checked value
                        count++
                        // console.log(item.value)
                        invoices.push(item.value)
                    }
                })
                let data = {
                    invoices
                }
                // console.log(invoices);
                $.ajax({
                    url: "{{ route('updatePLogistic') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log(data)
                    }
                })

                // submit ajax, shopee button disable jadi true lagi
                showToast("#toast__success", `${count} pesanan telah selesai dan dipindahkan ke done`)
                setTimeout(function() {
                    location.reload();
                }, 5000);
            })

            // $('.search_filter').on('click', function(e){
            //      e.preventDefault();
            //     let checkboxes = [];
            //     $('.form-check-input:checkbox:checked').each(function(){
            //         checkboxes.push($(this).val())
            //     });
            //     let startDate = $('input[name="start_date"]').val();
            //     let endDate = $('input[name="end_date"]').val();
            //     let toko = $('#select__toko').val();

            //     console.log(startDate,endDate,checkboxes, toko)
            //     fetch_data(checkboxes, startDate, endDate, toko)
            // })

            // function fetch_data(checkboxes, startDate, endDate, toko){
            //     $.ajax({
            //         url:'search/order?chekboxes='+checkboxes+"&startDate="+startDate+"&endDate="+endDate+"&toko="+toko,
            //         type:'GET',
            //         success: function(data) {
            //             console.log(data)
            //         }
            //     })
            // }
        });
    </script>
@endsection
