@extends('master')

@section('content')
    <section class="container ">
        <!--<div>-->
        <!--        <input id="tes" type="text" autofocus>-->
        <!--</div>-->
        <div class="d-flex justify-content-between d-print-none" style="align-items: center">
            <div class="chooser__filter">
                <select id="select__station" class="form-select form-select-sm" aria-label="form-select-sm example">
                    @foreach ($stations as $station)
                        @if ($id == $station->id)
                            <option value="{{ $station->id }}" selected>{{ $station->name }}</option>
                        @else
                            <option value="{{ $station->id }}">{{ $station->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div>

                @if ($id != 5)
                    @if (count($getOrder) > 0 && $getOrder[0]->scan == 0)
                        <div>
                            <h4 class='qr_text d-none'>Belum Scan Qr Code</h4>
                        </div>
                    @elseif(count($getOrder) > 0 && $getOrder[0]->scan == 1)
                        <div>
                            <h4 class='qr_text_done d-none'>Sudah Scan QR Code dan panggil kurir</h4>
                        </div>
                    @endif
                    <div class="d-flex justify-content-end timer_components">
                        <h1 id="timer1" class="d-none" style="margin-right:2rem;color:orange">01:00</h1>
                        <h1 id="timer2">{{ $timer->minutes }}:{{ $timer->second }}</h1>
                    </div>
                @endif
                <h3 id="timeout__text" class="d-none">Waktu Habis</h3>
            </div>
        </div>

        <div class="row order-list d-print-none">
            @foreach ($getOrder as $key => $order)
                <div class="col-xl-4 col-md-6 col-12" style="margin-top: 2rem" id="{{ $key }}">
                    @if ($key == 0 && $id != 5)
                        <div class="card custom__card station__1 blink_green">
                        @else
                            <div class="card custom__card station__1 ">
                    @endif
                    <div class="card-header d-flex justify-content-between">
                        <p class="t_600_white_36">
                            Station {{ $order->station }}
                        </p>
                        <p class="t_600_white_14">
                            {{ $order->kode }}
                        </p>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush t_600_black_14">
                            @foreach ($order->order as $eachOrder)
                                <li class="list-group-item d-flex align-center">
                                    <img class="mr__1" width="60px" height="60px" style="cursor: pointer"
                                        onclick="openImage('{{ $eachOrder->photo }}')" src="{{ $eachOrder->photo }}"
                                        alt="">
                                    <div>
                                        <div>
                                            <!--{{ $eachOrder->item }}-->
                                            {{ $eachOrder->item }}<br/>
                                            @if ($eachOrder->sub)
                                                @foreach ($eachOrder->sub as $subproduct)
                                                  -{{$subproduct}}<br/>
                                                @endforeach
                                            @endif
                                        </div>
                                        <div class="fw-bold text-uppercase">
                                            {{ $eachOrder->qty }} barang
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @if ($order->logistic_type == 'pickUp')
                        <div class="card-footer bg-info">
                            <p class="t_600_black_18">
                                PICK UP-{{ $order->platform }}-{{ $order->kurir }}
                            </p>
                        </div>
                    @else
                        <div class="card-footer bg-warning">
                            <p class="t_600_black_18">
                                DROP-{{ $order->platform }}-{{ $order->kurir }}
                            </p>
                        </div>
                    @endif
                </div>
        </div>
        @endforeach
        </div>
        

        @if ($getOrder[0]->platform == 'Tokopedia')
            <div class='d-none qr_print'>
                <div>
                    <canvas id="pdf_renderer" style="width:300px;height:450px;"></canvas>
                </div>
                <div class=''>
                    <canvas id="qr_renderer" style="width:150px;height:150px;"></canvas>
                </div>
            </div>

        @elseif($getOrder[0]->platform == 'Shopee')
            <div class='d-none qr_print_shopee'>
                <div>
                    <canvas id="pdf_renderer_shopee" style="width:560px;height:440px;"></canvas>
                </div>
                <div class=''>
                    <canvas id="qr_renderer_shopee" style="width:150px;height:150px;"></canvas>
                </div>
            </div>
        @elseif($getOrder[0]->platform == 'Blibli')
            <div class='d-none qr_print_blibli'>
                <div>
                    <canvas id="pdf_renderer_blibli" style="width:280px;height:420px;"></canvas>
                </div>
                <div class=''>
                    <canvas id="qr_renderer_blibli" style="width:150px;height:150px;"></canvas>
                </div>
            </div>
        @elseif($getOrder[0]->platform == 'JDID')
            <div class='d-none qr_print_jdid'>
                <div>
                    <canvas id="pdf_renderer_jdid" style="width:280px;height:420px;"></canvas>
                </div>
                <div class= ''>
                    <canvas id="qr_renderer_jdid" style="width:150px;height:150px;"></canvas>
                </div>
            </div>
        @elseif($getOrder[0]->platform == 'Warisan')
            <div class='qr_print_warisan'>
                <div>
                    <canvas id="pdf_renderer_warisan" style="width:480px;height:800px;"></canvas>
                </div>
                <div class= ''>
                    <canvas id="qr_renderer_warisan" style="width:150px;height:150px;"></canvas>
                </div>
            </div>
        @else
            <div class='qr_print_harbolnas'>
                <div>
                    <canvas id="pdf_renderer_harbolnas" style="width:450px;height:850px;"></canvas>
                </div>
                <div class= ''>
                    <canvas id="qr_renderer_harbolnas" style="width:150px;height:150px;"></canvas>
                </div>
            </div>
        @endif

    </section>
    
    <!--<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_induk">-->
    <!--  Launch demo modal-->
    <!--</button>-->

    
    <div class="modal fade" id="modal_induk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body text-center">
            <h2 style="margin-bottom: 25px; font-size: 24px">Produk ini belum mempunyai Induk Produk!</h2>
            <h5 style="margin-bottom: 25px; font-size: 16px">Silahkan hubungi admin untuk menambahkan induk produk supaya tidak terjadi error.</h5>
            <a id="idQueue" href="#">
                <button type="button" class="btn btn-primary">OK</button>
            </a>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('jsPage')

    @if (Session::has('success') == true)
        <script>
            showToastCard("#toast__card", "pick up");
        </script>
    @else
        <script>
            console.log('tidak')
        </script>
    @endif
    <script>
        $(document).scannerDetection({
            timeBeforeScanTest: 1000, // wait for the next character for upto 200ms
            avgTimeByChar: 500, // it's not a barcode if a character takes longer than 100ms
            onComplete: function(barcode, qty) {

                let orderId = "<?php echo $getOrder[0]->id; ?>";
                $.ajax({
                    url: "/admin/logistic/" + orderId,
                    type: 'Get',
                    success: function(data) {

                        // if(data == 'success'){
                        showToastCard("#toast__card", "pick up");
                        $('.qr_text_done').removeClass("d-none");
                        $('.qr_text').addClass("d-none");
                        $('#done-btn').prop("disabled", false);
                        // }else{
                        //     showToastCard("#toast__fail", "Gagal Melakukan Scan");
                        // }

                    }
                })

            } // main callback function
        });
    </script>
    @if ($getOrder[0]->platform == 'Tokopedia')
        <script>
            var myStateTokped = {
                pdf: null,
                currentPage: 1,
                zoom: 16
            }
            let invoice= "<?php echo env('APP_INVOICE'); ?>";
            let sticker ="<?php echo env('APP_QR'); ?>";
            let buktiBayarTokped = "<?php echo $getOrder[0]->buktiBayar; ?>";
            // console.log(buktiBayarTokped)
            pdfjsLib.getDocument(invoice + buktiBayarTokped).then((pdf) => {
                myStateTokped.pdf = pdf;
                renderTokped();
            });

            function renderTokped() {
                myStateTokped.pdf.getPage(myStateTokped.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStateTokped.zoom);
                    // var dpr = window.devicePixelRatio || 1;
                    // var rect = canvas.getBoundingClientRect();
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                        console.log('finished');
                    }, function(reason) {
                        console.log('stopped ' + reason);
                    });
                });
            }
            var myState1 = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }
            let buktiStickerTokped = "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument(sticker + buktiStickerTokped).then((pdf) => {
                myState1.pdf = pdf;
                rendererTokped();
            });
            function rendererTokped() {
                myState1.pdf.getPage(myState1.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @elseif($getOrder[0]->platform == 'Shopee')
        <script>
            $(function() {
                $('#timer1').removeClass('d-none')
            })
            var myStateShopee = {
                pdf: null,
                currentPage: 1,
                zoom: 4
            }
            let invoice= "<?php echo env('APP_INVOICE'); ?>";
            let sticker ="<?php echo env('APP_QR'); ?>";
            let buktiBayarShopee = "<?php echo $getOrder[0]->buktiBayar; ?>";
            console.log(buktiBayarShopee);
            pdfjsLib.getDocument(invoice + buktiBayarShopee).then((pdf) => {
                myStateShopee.pdf = pdf;
                renderShopee();
            });

            function renderShopee() {
                myStateShopee.pdf.getPage(myStateShopee.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer_shopee");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStateShopee.zoom);
                    // var dpr = window.devicePixelRatio || 1;
                    // var rect = canvas.getBoundingClientRect();
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                        console.log('finished');
                    }, function(reason) {
                        console.log('stopped ' + reason);
                    });
                });
            }
            var myState1Shopee = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }
            let buktiStickerShopee = "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument(sticker + buktiStickerShopee).then((pdf) => {
                myState1Shopee.pdf = pdf;
                rendererShopee();
            });

            function rendererShopee() {
                myState1Shopee.pdf.getPage(myState1Shopee.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer_shopee");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1Shopee.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @elseif($getOrder[0]->platform == 'Blibli')
        <script>
            var myStateBlibli = {
                pdf: null,
                currentPage: 1,
                zoom: 10
            }
            let invoice= "<?php echo env('APP_INVOICE'); ?>";
            let sticker ="<?php echo env('APP_QR'); ?>";
            let buktiBayarBlibli = "<?php echo $getOrder[0]->buktiBayar; ?>";
            pdfjsLib.getDocument(invoice + buktiBayarBlibli).then((pdf) => {
                myStateBlibli.pdf = pdf;
                renderBlibli();
            });

            function renderBlibli() {
                myStateBlibli.pdf.getPage(myStateBlibli.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer_blibli");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStateBlibli.zoom);
                    // var dpr = window.devicePixelRatio || 1;
                    // var rect = canvas.getBoundingClientRect();
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                        // console.log('finished');
                    }, function(reason) {
                        console.log('stopped ' + reason);
                    });
                });
            }
            var myState1Blibli = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }
            let buktiStickerBlibli = "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument(sticker + buktiStickerBlibli).then((pdf) => {
                myState1Blibli.pdf = pdf;
                rendererBlibli();
            });

            function rendererBlibli() {
                myState1Blibli.pdf.getPage(myState1Blibli.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer_blibli");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1Blibli.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @elseif($getOrder[0]->platform == 'JDID')
        <script>
            var myStatejdid = {
                pdf: null,
                currentPage: 1,
                zoom: 10
            }
            let invoice= "<?php echo env('APP_INVOICE'); ?>";
            let sticker ="<?php echo env('APP_QR'); ?>";
            let buktiBayarjdid = "<?php echo $getOrder[0]->buktiBayar; ?>";
            pdfjsLib.getDocument(invoice + buktiBayarjdid).then((pdf) => {
                myStatejdid.pdf = pdf;
                renderjdid();
            });

            function renderjdid() {
                myStatejdid.pdf.getPage(myStatejdid.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer_jdid");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStatejdid.zoom);
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                      console.log('finished');
                    }, function (reason) {
                      console.log('stopped ' + reason);
                    });
                });
            }
            var myState1jdid = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }
            let buktiStickerjdid = "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument(sticker + buktiStickerjdid).then((pdf) => {
                myState1jdid.pdf = pdf;
                rendererjdid();
            });

            function rendererjdid() {
                myState1jdid.pdf.getPage(myState1jdid.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer_jdid");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1jdid.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @elseif($getOrder[0]->platform == 'Warisan')
        <script>
            var myStatewarisan = {
                pdf: null,
                currentPage: 1,
                zoom: 5
            }
            let buktiBayarwarisan = "<?php echo $getOrder[0]->buktiBayar; ?>";
            pdfjsLib.getDocument("https://teratur.warisangajahmada.com/" + buktiBayarwarisan).then((pdf) => {
                myStatewarisan.pdf = pdf;
                renderwarisan();
            });

            function renderwarisan() {
                myStatewarisan.pdf.getPage(myStatewarisan.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer_warisan");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStatewarisan.zoom);
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                      console.log('finished');
                    }, function (reason) {
                      console.log('stopped ' + reason);
                    });
                });
            }
            var myState1warisan = {
                pdf: null,
                currentPage: 1,
                zoom: 1
            }
            let buktiStickerwarisan = "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument("https://teratur.warisangajahmada.com/" + buktiStickerwarisan).then((pdf) => {
                myState1warisan.pdf = pdf;
                rendererwarisan();
            });

            function rendererwarisan() {
                myState1warisan.pdf.getPage(myState1warisan.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer_warisan");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1warisan.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @else
        <script>
            var myStateHarbolnas = {
                pdf: null,
                currentPage: 1,
                zoom: 7
            }
            let invoice= "<?php echo env('APP_INVOICE'); ?>";
            let sticker ="<?php echo env('APP_QR'); ?>";
            let buktiBayarHarbolnas = "<?php echo $getOrder[0]->buktiBayar; ?>";
            pdfjsLib.getDocument(invoice + buktiBayarHarbolnas).then((pdf) => {
                myStateHarbolnas.pdf = pdf;
                renderHarbolnas();
            });

            function renderHarbolnas() {
                myStateHarbolnas.pdf.getPage(myStateHarbolnas.currentPage).then((page) => {

                    var canvas = document.getElementById("pdf_renderer_harbolnas");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myStateHarbolnas.zoom);
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    // ctx.scale(dpr, dpr);
                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    }).promise.then(function() {
                      console.log('finished');
                    }, function (reason) {
                      console.log('stopped ' + reason);
                    });
                });
            }
            var myState1Harbolnas = {
                pdf: null,
                currentPage: 1,
                zoom: 7
            }
            let buktiStickerHarbolnas= "<?php echo $getOrder[0]->invoice_toko; ?>";
            pdfjsLib.getDocument(sticker + buktiStickerHarbolnas).then((pdf) => {
                myState1Harbolnas.pdf = pdf;
                rendererHarbolnas();
            });

            function rendererHarbolnas() {
                myState1Harbolnas.pdf.getPage(myState1Harbolnas.currentPage).then((page) => {

                    var canvas = document.getElementById("qr_renderer_harbolnas");
                    var ctx = canvas.getContext('2d');

                    var viewport = page.getViewport(myState1Harbolnas.zoom);

                    canvas.width = viewport.width;
                    canvas.height = viewport.height;

                    page.render({
                        canvasContext: ctx,
                        viewport: viewport
                    });
                });
            }
        </script>
    @endif
    <script>
        var resultHour = 0;
        var resultMin = 0;
        var resultSec = 0;
        var intervalTime;
        var intervalShopee;
        var click = 0
        var isMulai = true
        var platform = '<?php echo $getOrder[0]->platform; ?>'
        var logistic_type = '<?php echo $getOrder[0]->logistic_type; ?>'


        function formatNumber(number) {
            return number >= 10 ? `${number}` : `0${number}`
        }

        function timerCount(number, type) {
            if (type == "hour") {
                number = number >= 0 ? number - 1 : 23;
            } else {
                number = number >= 0 ? number - 1 : 59;
            }


            let res = formatNumber(number)
            return res
        }

        function stopWatchCount(number, type) {
            number = number <= 59 ? number + 1 : 0;

            return number
        }

        function handleTimer(min, sec, id) {
            min = parseInt(min)
            sec = parseInt(sec)

            let resMinutes = formatNumber(min)
            let resSecond = sec > 0 ? timerCount(sec, 'sec') : formatNumber(sec)

            if (sec == 0) {
                resSecond = timerCount(-1, 'sec')

                if (min > 0) {
                    resMinutes = timerCount(min, 'min')
                }
            }

            $(`#timer${id}`).text(`${resMinutes}:${resSecond}`)
        }

        function handleStopWatch(hour, min, sec) {
            sec = sec + 1
            if (sec > 59) {
                sec = 0
                min = min + 1

                if (min > 59) {
                    min = 0
                    hour = hour + 1
                }
            }

            resultHour = hour
            resultMin = min
            resultSec = sec
        }

        $(function() {

            let id = "<?php echo $id; ?>";
            let orderId = "<?php echo $getOrder[0]->id; ?>";
            var myFrame = $("#qr");
            let buktiBayar = "<?php echo $getOrder[0]->buktiBayar; ?>";
            myFrame.ready(function() {
                var content = `tes`
                let check = myFrame.contents().find("body");
            });

            $('#timeout__text').addClass('d-none')
            console.log(id);
            if (id != 5) {
                $('#timer-btn').removeClass('d-none')
                $('#label-btn').removeClass('d-none')
                $('#finish-btn').removeClass('d-none')
                // $('#start-btn').removeClass('d-none')
            } else {
                $('#timer-btn').addClass('d-none')
                $('#label-btn').addClass('d-none')
                $('#finish-btn').addClass('d-none')
                $('#start-btn').addClass('d-none')
            }
            $('#select__station').on('change', function(e) {
                let value = this.value
                console.log(value, e, id);
                window.location.href = '/admin/pack/' + value;
            })
            var lastID = "<?php echo count($getOrder) > 0 ? $getOrder[count($getOrder) - 1]->kode : ''; ?>"
            // update id baru;
            let bookinganId = "<?php echo count($getOrder) > 0 ? $getOrder[0]->id : ''; ?>"
            
            
            $('#start-btn').on('click', function() {
                let id = bookinganId
                // console.log('tes');
                 $.ajax({
                    url: `/proses-shopee-blibli/${id}`,
                    type: 'GET',
                    success: function(data) {
                        console.log('==================')
                        console.log(data)
                        console.log('==================')
                        $('.qr_text').removeClass("d-none");
                                resultHour = 0
                                resultMin = 0
                                resultSec = 0
                                isMulai = false
                                $('#label-btn').addClass('blink')
                                $('#start-btn').addClass('d-none')
                                $('#timer-btn').removeClass('blink')
                                $('#finish-btn').removeClass('blink')
                                $('#qr').removeClass('d-none')
                                $('#qr_new').removeClass('d-none')
                                $('#label').removeClass('d-none')
                                if (id != 5) {
                                    intervalTime = setInterval(function() {
                                        timer()
                                        handleStopWatch(resultHour, resultMin, resultSec)
                                    }, 1000);
                
                                    if (platform == "Shopee") {
                                        intervalShopee = setInterval(function() {
                                            shopeeTimer()
                                        }, 1000)
                                    }
                                }
                    //     if(data.status == 'failed'){
                    //         $('#modal_induk').modal('toggle')
                    //         $("#idQueue").attr("href", `/lab-queue/${id}`);
                    //         return
                    //     }else if(data.status == 'success'){
                    //         $('.qr_text').removeClass("d-none");
                    //             resultHour = 0
                    //             resultMin = 0
                    //             resultSec = 0
                    //             isMulai = false
                    //             $('#label-btn').addClass('blink')
                    //             $('#start-btn').addClass('d-none')
                    //             $('#timer-btn').removeClass('blink')
                    //             $('#finish-btn').removeClass('blink')
                    //             $('#qr').removeClass('d-none')
                    //             $('#qr_new').removeClass('d-none')
                    //             $('#label').removeClass('d-none')
                    //             if (id != 5) {
                    //                 intervalTime = setInterval(function() {
                    //                     timer()
                    //                     handleStopWatch(resultHour, resultMin, resultSec)
                    //                 }, 1000);
                
                    //                 if (platform == "Shopee") {
                    //                     intervalShopee = setInterval(function() {
                    //                         shopeeTimer()
                    //                     }, 1000)
                    //                 }
                    //             }
                    //     }
                    }
                    // error: function(XMLHttpRequest) {
                    //     $('#modal_induk').modal('toggle')
                    //     $("#idQueue").attr("href", `/lab-queue/${id}`);

                    //     console.log(XMLHttpRequest)
                    //     console.log('==================')
                    //     console.log('======error=======')
                    //     console.log('==================')
                    //     return;
                    // }
                })
                
                
                    
            })

            // //check is both of print button has been clicked
            // function checkPrintButton() {
            //     if (click == 1 && click2 == 1) {
            //         $('#qr').addClass('d-none')
            //         $('#qr_new').addClass('d-none')
            //         $('#label').addClass('d-none')
            //         $('#done-btn').removeClass('d-none')
            //         $('#label-btn').removeClass('blink')

            //     }
            // }

            // let pages = [];
            $('#qr, #qr_new').on('click', function() {
                let buktiBayar = "<?php echo $getOrder[0]->buktiBayar; ?>";
                if (click == 0) {
                    // if(platform == 'Shopee' &&)
                    window.print();
                    click = 1
                    if(logistic_type == 'pickUp'){
                        $('#pickup-btn').prop("disabled", false);
                        $('#pickup-btn').removeClass('d-none')
                        $('#qr').addClass('d-none')
                        $('#qr_new').addClass('d-none')
                        $('#label').addClass('d-none')
                        $('#label-btn').removeClass('blink')
                        $('#qr').css('opacity', '0.6');
                        $('#qr_new').css('opacity', '0.6');
                    }else{
                        $('#done-btn').prop("disabled", false);
                        $('#qr').addClass('d-none')
                        $('#qr_new').addClass('d-none')
                        $('#label').addClass('d-none')
                        $('#done-btn').removeClass('d-none')
                        $('#label-btn').removeClass('blink')
                        $('#qr').css('opacity', '0.6');
                        $('#qr_new').css('opacity', '0.6');
                    }

                }

                showToast("#toast__success", "Berhasil melakukan print QR")
            })

            $('#pickup-btn').on('click', function() {
                $.ajax({
                    url: "/admin/logistic/" + orderId,
                    type: 'GET',
                    success: function(data) {
                        showToastCard("#toast__card", "pick up");
                        $('#done-btn').prop("disabled", false);
                        $('#done-btn').removeClass('d-none')
                        $('#pickup-btn').prop("disabled", true);
                        $('#pickup-btn').addClass('d-none')
                        // console.log(data)
                    }
                })
            })

            // RESETTING ALL STUFF
            function resetTime() {
                clearInterval(intervalTime)
                $(`#timer2`).text(`03:00`)
            }

            function resetPrintButton() {
                click = 0
                click2 = 0
                $('#qr').css('opacity', '1');
                $('#qr_new').css('opacity', '1');
                $('#label').css('opacity', '1');
            }

            function resetTextTimeout() {
                $('#timeout__text').addClass('d-none')
            }

            $('#done-btn').on('click', function() {
                resetTime()
                resetPrintButton()
                resetTextTimeout()
                isMulai = true
                $('#timer1').addClass('d-none')
                $('#timer-btn').addClass('blink')
                $('#start-btn').removeClass('d-none')
                $('#done-btn').addClass('d-none')
                let hour = resultHour * 3600
                let minutes = resultMin * 60
                let second = resultSec
                let time = hour + minutes + second;
                // console.log(typeof(time), time, minutes, second)
                let orderId = bookinganId;
                let data = {
                    time,
                    orderId
                }
                // console.log(data);
                $.ajax({
                    url: "{{ route('updatePacking') }}",
                    type: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        // console.log(data, 'tes')
                        showToast("#toast__info",
                            `Pesanan telah selesai dan dipindahkan ke menu ${data}`)
                        $('.qr_text').addClass("d-none");
                    }
                })
            })

            function timer() {
                let time1 = $('#timer1').text().split(":")
                let time2 = $('#timer2').text().split(":")

                // handleTimer(time2[0], time2[1], 2)

                if (time2[0] != "00" || time2[1] != "00") {

                    handleTimer(time2[0], time2[1], 2)

                } else if (time2[0] == "00" && time2[1] == "00") {

                    $('#timeout__text').removeClass('d-none')


                    $('#label-btn').removeClass('blink')

                    $('#timer').addClass('d-none')

                    let orderId = "<?php echo $getOrder[0]->id; ?>";

                }
            }

            function shopeeTimer() {
                let time1 = $('#timer1').text().split(":")

                handleTimer(time1[0], time1[1], 1)

                if (time1[0] == "00" && time1[1] == "00") {
                    $('#qr_new').prop("disabled", false);
                    $('#qr').prop("disabled", false);

                    // clear inter val shopee
                    clearInterval(intervalShopee)
                } else {
                    $('#qr_new').prop("disabled", true);
                    $('#qr').prop("disabled", true);
                }
            }


            var secondLast = "<?php echo count($getOrder) > 1 ? $getOrder[count($getOrder) - 2]->kode : ''; ?>"
            var lastCount = parseInt("<?php echo count($getOrder); ?>")
            let count = 0;
            setInterval(function() {
                let station = "<?php echo $getOrder[0]->station; ?>";
                let currentStation = "<?php echo $id; ?>";
                // console.log('station = ' + station, 'current' + currentStation, 'id' + id);
                // if(id == 5){
            
                $.ajax({
                    url: "/update/order/" + currentStation + "/"+ bookinganId + "/" + count,
                    type: 'Get',
                    success: function(data) {
                        console.log(data, data[0])
                        platform = data[0].platform;
                        logistic_type = data[0].logistic_type;
                        bookinganId = data[0].id;
                        $('.order-list').empty();
                        $('.qr_print_blibli').empty();
                        $('.qr_print_jdid').empty();
                        $('.qr_print_harbolnas').empty();
                        $('.qr_print_shopee').empty();
                        $('.qr_print').empty();
                        let orderCount = data.length
                        let packCounter = $('.pack_counter').text();
                        if (packCounter != orderCount) {
                            $('.pack_counter').text(orderCount);
                        }
                        // console.log(orderCount, packCounter);

                        if (orderCount > 0 && lastCount > 0) {
                            if (isMulai) {
                                // console.log('tes')
                                $('#start-btn').removeClass('d-none')
                            }
                            if (data[0].platform == 'Tokopedia') {
                                $('.qr_print').append(`
                                    <div>
                                        <canvas id="pdf_renderer" style="width:300px;height:450px;"></canvas>
                                    </div>
                                    <div class= ''>
                                        <canvas id="qr_renderer" style="width:150px;height:150px;"></canvas>
                                    </div>
                                `)
                            } else if (data[0].platform == 'Shopee') {
                                $('.qr_print_shopee').append(`
                                        <div>
                                            <canvas id="pdf_renderer_shopee" style="width:560px;height:440px;"></canvas>
                                        </div>
                                        <div class= ''>
                                            <canvas id="qr_renderer_shopee" style="width:150px;height:150px;"></canvas>
                                        </div>
                                `)
                            } else if (data[0].platform == 'Blibli') {
                                $('.qr_print_blibli').append(`
                                    <div>
                                        <canvas id="pdf_renderer_blibli" style="width:280px;height:420px;"></canvas>
                                    </div>
                                    <div class= ''>
                                        <canvas id="qr_renderer_blibli" style="width:150px;height:150px;"></canvas>
                                    </div>
                                `)
                            } else if (data[0].platform == 'JDID') {
                                $('.qr_print_jdid').append(`
                                    <div>
                                        <canvas id="pdf_renderer_jdid" style="width:280px;height:420px;"></canvas>
                                    </div>
                                    <div class= ''>
                                        <canvas id="qr_renderer_jdid" style="width:150px;height:150px;"></canvas>
                                    </div>
                                `)
                            } else if(data[0].platform == 'Warisan'){
                                $('.qr_print_jdid').append(`
                                    <div class='qr_print_warisan'>
                                        <div>
                                            <canvas id="pdf_renderer_warisan" style="width:480px;height:800px;"></canvas>
                                        </div>
                                        <div class= ''>
                                            <canvas id="qr_renderer_warisan" style="width:150px;height:150px;"></canvas>
                                        </div>
                                    </div>
                                `)
                            }else {
                                $('.qr_print_harbolnas').append(`
                                    <div>
                                        <canvas id="pdf_renderer_harbolnas" style="width:450px;height:850px;"></canvas>
                                    </div>
                                    <div class= ''>
                                        <canvas id="qr_renderer_harbolnas" style="width:150px;height:150px;"></canvas>
                                    </div>
                                `)
                            }
                            if (lastCount < orderCount) {
                                lastCount = orderCount
                                showToast("#toast__success",
                                    "Terdapat pesanan baru yang masuk!")
                            } else {
                                if (orderCount >= 2) {
                                    // handle jika ada pesanan masuk saat packer melakukan checkout
                                    if (data[orderCount - 1].kode !== lastID &&
                                        data[orderCount - 2].kode !== secondLast) {
                                        lastID = data[orderCount - 1].kode
                                        secondLast = data[orderCount - 2].kode

                                        showToast("#toast__success",
                                            "Terdapat pesanan baru yang masuk!")
                                    }
                                } else if (data[orderCount - 1].kode !== lastID) {
                                    // jika pesanan cm ada 1
                                    // di saat bersamaan checkout pesanan dan ada pesanan baru masuk
                                    secondLast = ""
                                    lastID = data[orderCount - 1].kode
                                    showToast("#toast__success",
                                        "Terdapat pesanan baru yang masuk!")
                                }
                            }
                        } else {
                            $('#start-btn').addClass('d-none')
                        }

                        $.each(data, function(i, e) {
                            // console.log( i, e.buktiBayar, data[0]);
                            let orderList = '';

                            $.each(e.order, function(key, value) {
                                // isi url image ke src sm function openImage
                                orderList += `
                                        <li class='list-group-item d-flex align-center'>
                                            <img class="mr__1" width="60px" height="60px" style="cursor: pointer"
                                                onclick="openImage('${value.photo}')"
                                                src='${value.photo}' alt="">
                                            <div>

                                                <div>
                                                    ${value.item}
                                                </div>
                                                <div class="fw-bold text-uppercase">
                                                    ${value.qty} barang
                                                </div>
                                            </div>
                                        </li>`
                            });

                            let blink = '';
                            if (i == 0 && currentStation != 5) {
                                blink +=
                                    '<div class="card custom__card station__1 blink_green">'
                            } else {
                                blink +=
                                    '<div class="card custom__card station__1">'
                            }

                            let logistic = '';
                            if (e.logistic_type == 'pickUp' && i == 0) {
                                logistic +=
                                    `<div class='card-footer bg-info data' data-buktiBayar=${e.buktiBayar} ><p class='t_600_black_18'> PICK UP-${e.platform}-${e.kurir} </p></div>`
                            } else if (e.logistic_type == 'pickUp' && i != 0) {
                                logistic +=
                                    `<div class='card-footer bg-info' ><p class='t_600_black_18'> PICK UP-${e.platform}-${e.kurir} </p></div>`
                            } else if (e.logistic_type == 'dropoff' && i == 0) {
                                logistic +=
                                    `<div class="card-footer bg-warning data" data-buktiBayar=${e.buktiBayar}><p class="t_600_black_18">DROP-${e.platform}-${e.kurir}</p></div>`
                            } else {
                                logistic +=
                                    `<div class="card-footer bg-warning"><p class="t_600_black_18">DROP-${e.platform}-${e.kurir}</p></div>`
                            }

                            $('.order-list').append(
                                `<div class="col-xl-4 col-md-6 col-12" style="margin-top: 2rem" id="${i}">
                                        ${blink}
                                            <div class="card-header d-flex justify-content-between">
                                                <p class="t_600_white_36">
                                                    Station ${e.station}
                                                </p>
                                                <p class="t_600_white_14">
                                                    ${e.kode}
                                                </p>
                                            </div>
                                            <div class="card-body">
                                                <ul class="list-group list-group-flush t_600_black_14" id=order${i}>
                                                    ${orderList}
                                                </ul>
                                            </div>
                                            ${logistic}
                                        </div>
                                    </div>`
                            )
                        })
                        if (orderCount > 0) {
                            if (data[0].platform == 'Tokopedia') {
                                // tokped
                                var myStateTokped = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 16
                                }
                                let buktiBayarTokped = data[0].buktiBayar;
                                // console.log('buktiBayar ='+buktiBayarTokped, data[0].buktiBayar)
                                if (buktiBayarTokped != data[0].buktiBayar) {
                                    buktiBayarTokped = data[0].buktiBayar
                                }
                                let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                let sticker ="<?php echo env('APP_QR'); ?>";
                                pdfjsLib.getDocument(invoice +
                                    buktiBayarTokped).then((pdf) => {
                                    myStateTokped.pdf = pdf;
                                    renderTokped();
                                });

                                function renderTokped() {
                                    myStateTokped.pdf.getPage(myStateTokped.currentPage).then((
                                        page) => {

                                        var canvas = document.getElementById(
                                            "pdf_renderer");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myStateTokped
                                            .zoom);
                                        // var dpr = window.devicePixelRatio || 1;
                                        // var rect = canvas.getBoundingClientRect();
                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;
                                        // ctx.scale(dpr, dpr);
                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        }).promise.then(function() {
                                            console.log('finished');
                                        }, function(reason) {
                                            console.log('stopped ' + reason);
                                        });
                                    });
                                }
                                var myState1 = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 1
                                }
                                let buktiStickerTokped = data[0].invoice_toko;
                                if (buktiStickerTokped != data[0].invoice_toko) {
                                    buktiStickerTokped = data[0].invoice_toko
                                }
                                pdfjsLib.getDocument(sticker +
                                    buktiStickerTokped).then((pdf) => {
                                    myState1.pdf = pdf;
                                    rendererTokped();
                                });

                                function rendererTokped() {
                                    myState1.pdf.getPage(myState1.currentPage).then((page) => {

                                        var canvas = document.getElementById(
                                            "qr_renderer");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myState1.zoom);

                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;

                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        });
                                    });
                                }
                            } else if (data[0].platform == 'Shopee') {

                                // shopee
                                var myStateShopee = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 4
                                }
                                let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                let sticker ="<?php echo env('APP_QR'); ?>";
                                let buktiBayarShopee = data[0].buktiBayar;
                                if (buktiBayarShopee != data[0].buktiBayar) {
                                    buktiBayarShopee = data[0].buktiBayar
                                }
                                pdfjsLib.getDocument(invoice +
                                    buktiBayarShopee).then((pdf) => {
                                    myStateShopee.pdf = pdf;
                                    renderShopee();
                                });

                                function renderShopee() {
                                    myStateShopee.pdf.getPage(myStateShopee.currentPage).then((
                                        page) => {

                                        var canvas = document.getElementById(
                                            "pdf_renderer_shopee");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myStateShopee
                                            .zoom);
                                        // var dpr = window.devicePixelRatio || 1;
                                        // var rect = canvas.getBoundingClientRect();
                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;
                                        // ctx.scale(dpr, dpr);
                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        }).promise.then(function() {
                                            console.log('finished');
                                        }, function(reason) {
                                            console.log('stopped ' + reason);
                                        });
                                    });
                                }
                                var myState1Shopee = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 1
                                }
                                let buktiStickerShopee = data[0].invoice_toko;
                                if (buktiStickerShopee != data[0].invoice_toko) {
                                    buktiStickerShopee = data[0].invoice_toko
                                }
                                pdfjsLib.getDocument(sticker +
                                    buktiStickerShopee).then((pdf) => {
                                    myState1Shopee.pdf = pdf;
                                    rendererShopee();
                                });

                                function rendererShopee() {
                                    myState1Shopee.pdf.getPage(myState1Shopee.currentPage).then(
                                        (page) => {

                                            var canvas = document.getElementById(
                                                "qr_renderer_shopee");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myState1Shopee
                                                .zoom);

                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;

                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            });
                                        });
                                    }
                                }else if(data[0].platform == 'Blibli'){
                                // blibli
                                count++;
                                var myStateBlibli = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 10
                                }
                                let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                let sticker ="<?php echo env('APP_QR'); ?>";
                                let buktiBayarBlibli = data[0].buktiBayar;
                                if (buktiBayarBlibli != data[0].buktiBayar) {
                                    buktiBayarBlibli = data[0].buktiBayar
                                }
                                pdfjsLib.getDocument(invoice +
                                    buktiBayarBlibli).then((pdf) => {
                                    myStateBlibli.pdf = pdf;
                                    renderBlibli();
                                });

                                function renderBlibli() {
                                    myStateBlibli.pdf.getPage(myStateBlibli.currentPage).then((
                                        page) => {

                                        var canvas = document.getElementById(
                                            "pdf_renderer_blibli");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myStateBlibli
                                            .zoom);
                                        // var dpr = window.devicePixelRatio || 1;
                                        // var rect = canvas.getBoundingClientRect();
                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;
                                        // ctx.scale(dpr, dpr);
                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        }).promise.then(function() {
                                            console.log('finished');
                                        }, function(reason) {
                                            console.log('stopped ' + reason);
                                        });
                                    });
                                }
                                var myState1Blibli = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 1
                                }
                                let buktiStickerBlibli = data[0].invoice_toko;
                                if (buktiStickerBlibli != data[0].invoice_toko) {
                                    buktiStickerBlibli = data[0].invoice_toko
                                }
                                pdfjsLib.getDocument(sticker +
                                    buktiStickerBlibli).then((pdf) => {
                                    myState1Blibli.pdf = pdf;
                                    rendererBlibli();
                                });

                                function rendererBlibli() {
                                    myState1Blibli.pdf.getPage(myState1Blibli.currentPage).then(
                                        (page) => {

                                            var canvas = document.getElementById(
                                                "qr_renderer_blibli");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myState1Blibli
                                                .zoom);

                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;

                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            });
                                        });
                                }
                            }else if (data[0].platform == 'JDID'){
                                    var myStatejdid = {
                                        pdf: null,
                                        currentPage: 1,
                                        zoom: 10
                                    }
                                    let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                    let sticker ="<?php echo env('APP_QR'); ?>";
                                    let buktiBayarjdid = data[0].buktiBayar;
                                    pdfjsLib.getDocument(invioce + buktiBayarjdid).then((pdf) => {
                                        myStatejdid.pdf = pdf;
                                        renderjdid();
                                    });

                                    function renderjdid() {
                                        myStatejdid.pdf.getPage(myStatejdid.currentPage).then((page) => {

                                            var canvas = document.getElementById("pdf_renderer_jdid");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myStatejdid.zoom);
                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;
                                            // ctx.scale(dpr, dpr);
                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            }).promise.then(function() {
                                              console.log('finished');
                                            }, function (reason) {
                                              console.log('stopped ' + reason);
                                            });
                                        });
                                    }
                                    var myState1jdid = {
                                        pdf: null,
                                        currentPage: 1,
                                        zoom: 1
                                    }
                                    let buktiStickerjdid = data[0].invoice_toko;
                                    pdfjsLib.getDocument(sticker + buktiStickerjdid).then((pdf) => {
                                        myState1jdid.pdf = pdf;
                                        rendererjdid();
                                    });

                                    function rendererjdid() {
                                        myState1jdid.pdf.getPage(myState1jdid.currentPage).then((page) => {

                                            var canvas = document.getElementById("qr_renderer_jdid");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myState1jdid.zoom);

                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;

                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            });
                                        });
                                    }
                            } else if(data[0].platform == 'Warisan'){
                                var myStatewarisan = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 5
                                }
                                let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                let sticker ="<?php echo env('APP_QR'); ?>";
                                let buktiBayarwarisan = data[0].buktiBayar;
                                pdfjsLib.getDocument(invoice + buktiBayarwarisan).then((pdf) => {
                                    myStatewarisan.pdf = pdf;
                                    renderwarisan();
                                });

                                function renderwarisan() {
                                    myStatewarisan.pdf.getPage(myStatewarisan.currentPage).then((page) => {

                                        var canvas = document.getElementById("pdf_renderer_warisan");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myStatewarisan.zoom);
                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;
                                        // ctx.scale(dpr, dpr);
                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        }).promise.then(function() {
                                          console.log('finished');
                                        }, function (reason) {
                                          console.log('stopped ' + reason);
                                        });
                                    });
                                }
                                var myState1warisan = {
                                    pdf: null,
                                    currentPage: 1,
                                    zoom: 1
                                }
                                let buktiStickerwarisan = data[0].invoice_toko;
                                pdfjsLib.getDocument(sticker + buktiStickerwarisan).then((pdf) => {
                                    myState1warisan.pdf = pdf;
                                    rendererwarisan();
                                });

                                function rendererwarisan() {
                                    myState1warisan.pdf.getPage(myState1warisan.currentPage).then((page) => {

                                        var canvas = document.getElementById("qr_renderer_warisan");
                                        var ctx = canvas.getContext('2d');

                                        var viewport = page.getViewport(myState1warisan.zoom);

                                        canvas.width = viewport.width;
                                        canvas.height = viewport.height;

                                        page.render({
                                            canvasContext: ctx,
                                            viewport: viewport
                                        });
                                    });
                                }
                            }else {
                                    var myStateHarbolnas = {
                                        pdf: null,
                                        currentPage: 1,
                                        zoom: 7
                                    }
                                    let invoice= "<?php echo env('APP_INVOICE'); ?>";
                                    let sticker ="<?php echo env('APP_QR'); ?>";
                                    let buktiBayarHarbolnas = data[0].buktiBayar
                                    // console.log(data[0].buktiBayar);
                                    pdfjsLib.getDocument(invoice + buktiBayarHarbolnas).then((pdf) => {
                                        myStateHarbolnas.pdf = pdf;
                                        renderHarbolnas();
                                    });

                                    function renderHarbolnas() {
                                        myStateHarbolnas.pdf.getPage(myStateHarbolnas.currentPage).then((page) => {

                                            var canvas = document.getElementById("pdf_renderer_harbolnas");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myStateHarbolnas.zoom);
                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;
                                            // ctx.scale(dpr, dpr);
                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            }).promise.then(function() {
                                              console.log('finished');
                                            }, function (reason) {
                                              console.log('stopped ' + reason);
                                            });
                                        });
                                    }
                                    var myState1Harbolnas = {
                                        pdf: null,
                                        currentPage: 1,
                                        zoom: 1
                                    }
                                    let buktiStickerHarbolnas= data[0].invoice_toko;
                                    pdfjsLib.getDocument(sticker + buktiStickerHarbolnas).then((pdf) => {
                                        myState1Harbolnas.pdf = pdf;
                                        rendererHarbolnas();
                                    });

                                    function rendererHarbolnas() {
                                        myState1Harbolnas.pdf.getPage(myState1Harbolnas.currentPage).then((page) => {

                                            var canvas = document.getElementById("qr_renderer_harbolnas");
                                            var ctx = canvas.getContext('2d');

                                            var viewport = page.getViewport(myState1Harbolnas.zoom);

                                            canvas.width = viewport.width;
                                            canvas.height = viewport.height;

                                            page.render({
                                                canvasContext: ctx,
                                                viewport: viewport
                                            });
                                        });
                                    }
                            }
                        }
                    }
                });

                // }
            }, 10000)
        })
    </script>
@endsection
