@extends('master')

@section('content')
    <section class="container">
        <div class="chooser__filter">
            <select id="select__station" class="form-select form-select-sm" aria-label=".form-select-sm example">
                @foreach($stations as $station)
                    @if($i == $station->id)
                        <option value="{{$station->id}}" selected>{{$station->name}}</option>
                    @else
                        <option value="{{$station->id}}">{{$station->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="row">
            <div class="col-xl-4 col-md-6 col-12" style="margin-top: 2rem">
                <div class="card custom__card station__1">
                    <div class="card-header d-flex justify-content-between">
                        <p class="t_600_white_36">
                            Station 1
                        </p>
                        <p class="t_600_white_14">
                            INV/20210924/MPL/171147182
                        </p>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush t_600_black_14">
                            <li class="list-group-item">An item</li>
                            <li class="list-group-item">A second item</li>
                            <li class="list-group-item">A third item</li>
                        </ul>
                    </div>
                    <div class="card-footer bg-info">
                        <p class="t_600_black_18">
                            PICK UP
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
@endsection

@section('jsPage')
    <script>
        $(function() {
            $('#select__station').on('change', function(e) {
                let value = this.value
                console.log(value)
                
                // FILTERING CODE
            })
        })
    </script>
@endsection
