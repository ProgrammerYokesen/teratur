@extends('master')

@section('content')
    <section class="container">
        <div class="chooser__filter" style="margin-bottom: 1rem">
            <button type="button" class="btn primer__btn" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                Filter
            </button>

            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Filter</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form method="post" action="{{ route('searchOrder', ['type' => 'all']) }}">
                            @csrf
                            <div id="qToko"></div>
                            <div class="modal-body">
                                <div class="mb-3 row">
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="JNE"
                                            id="jne_kurir">
                                        <label class="form-check-label" for="jne_kurir">
                                            JNE
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="JNT"
                                            id="jnt_kurir">
                                        <label class="form-check-label" for="jnt_kurir">
                                            JNT
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="SiCepat"
                                            id="cepat_kurir">
                                        <label class="form-check-label" for="cepat_kurir">
                                            Si Cepat
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Gojek"
                                            id="gojek_kurir">
                                        <label class="form-check-label" for="gojek_kurir">
                                            Gojek
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Grab"
                                            id="grab_kurir">
                                        <label class="form-check-label" for="grab_kurir">
                                            Grab
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Ninja"
                                            id="ninja_kurir">
                                        <label class="form-check-label" for="ninja_kurir">
                                            Ninja
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Wahana"
                                            id="wahana_kurir">
                                        <label class="form-check-label" for="wahana_kurir">
                                            Wahana
                                        </label>
                                    </div>
                                    <div class="mt-2 col-md-6 col-sm-12">
                                        <input class="form-check-input" name="kurir[]" type="checkbox" value="Shopee"
                                            id="shopee_kurir">
                                        <label class="form-check-label" for="shopee_kurir">
                                            Shopee
                                        </label>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <div class="col-md-5 col-sm-5">
                                        <input type="date" class="form-control" name="start_date">
                                    </div>
                                    <div class="col-md-2 col-sm-2 text-center pt-2">
                                        <i class="fas fa-long-arrow-alt-right"></i>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <input type="date" class="form-control" name="end_date">
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <select id="select__toko" class="form-select" name="tokoSelect"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Tambah Toko</option>
                                        @foreach ($allStore as $store)
                                            <option value="{{ $store->store_name }}">{{ $store->store_name }}</option>

                                        @endforeach
                                    </select>
                                    <ul id="list_toko" class="list-group">
                                    </ul>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary search_filter"
                                    data-bs-dismiss="modal">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="tokped_section" class="mb__3">
            {{-- <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="tokped__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div> --}}
            <table id="drop__table" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>TOKOPEDIA {{ $countTokpedEnd }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($datasTokpedEnd)
                        @foreach ($datasTokpedEnd as $data)
                        <tr>
                            <td>{{ $data->kode }}</td>
                            <td>{{ $data->nama_pemesan }}</td>
                            <td>{{ $data->kurir }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>{{ $data->store_name }}</td>
                            <td>
                                <div class="text-center" style="cursor:pointer">
                                    <i class="fas fa-eye" onclick="showOrder({{$data->id}})"></i>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>
        <div id="shopee_section" class="mb__3">
            {{-- <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="shopee__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div> --}}
            <table id="drop__table_shopee" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Shopee {{ $countShopeeEnd }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($datasShopeeEnd)
                        @foreach ($datasShopeeEnd as $data)
                            <tr>
                                <td>{{ $data->kode }}</td>
                                <td>{{ $data->nama_pemesan }}</td>
                                <td>{{ $data->kurir }}</td>
                                <td>{{ $data->created_at }}</td>
                                <td>{{ $data->store_name }}</td>
                                <td>
                                    <div class="text-center" style="cursor:pointer">
                                        <i class="fas fa-eye" onclick="showOrder({{$data->id}})"></i>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>
        <div id="blibli_section" class="mb__3">
            {{-- <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="blibli__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div> --}}
            <table id="drop__table_blibli" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Blibli {{ $countBlibliEnd }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($datasBlibliEnd)
                            @foreach ($datasBlibliEnd as $data)
                                <tr>
                                    <td>{{ $data->kode }}</td>
                                    <td>{{ $data->nama_pemesan }}</td>
                                    <td>{{ $data->kurir }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>{{ $data->store_name }}</td>
                                    <td>
                                        <div class="text-center" style="cursor:pointer">
                                            <i class="fas fa-eye" onclick="showOrder({{$data->id}})"></i>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                    @endif


                </tbody>
            </table>
        </div>
        <div id="jdid_section" class="mb__3">
            <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="jdid__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div>
            <table id="drop__table_jdid" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>JD.ID {{ $countJdidEnd }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($datasJdidEnd)
                            @foreach ($datasJdidEnd as $data)
                                <tr>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input check__toko_jdid" type="checkbox"
                                                value="{{ $data->kode }}" id="item__{{ $data->kode }}">
                                            <label class="form-check-label" for="flexCheckChecked">
                                                {{ $data->kode }}
                                            </label>
                                        </div>
                                    </td>
                                    <td>{{ $data->nama_pemesan }}</td>
                                    <td>{{ $data->kurir }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>{{ $data->store_name }}</td>
                                    <td>
                                        <div class="text-center" style="cursor:pointer">
                                            <i class="fas fa-eye" onclick="showOrder({{$data->id}})"></i>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                    @endif

                </tbody>
            </table>
        </div>
        <div id="harbolnas_section" class="mb__3">
            {{-- <div class="d-flex justify-content-end" style="margin-bottom: 1rem">
                <button id="harbolnas__btn" class="btn btn-success" disabled>SUDAH DIBAWA KURIR</button>
            </div> --}}
            <table id="drop__table_harbolnas" class="table table-borderless" style="width:100%">
                <thead class="table-dark">
                    <tr>
                        <th>Harbolnas 12.12 {{ $countHarbolnasEnd }} PAKET</th>
                        <th>Nama Pemesan</th>
                        <th>Kurir</th>
                        <th>Tgl Pemesanan</th>
                        <th>Nama Toko</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($datasHarbolnasEnd)
                            @foreach ($datasHarbolnasEnd as $data)
                                <tr>
                                    <td>{{ $data->kode }}</td>
                                    <td>{{ $data->nama_pemesan }}</td>
                                    <td>{{ $data->kurir }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>{{ $data->store_name }}</td>
                                    <td>
                                        <div class="text-center" style="cursor:pointer">
                                            <i class="fas fa-eye" onclick="showOrder({{$data->id}})"></i>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                    @endif


                </tbody>
            </table>
        </div>
    </section>
@endsection

@section('jsPage')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datables.bootstrap5.js') }}"></script>

    <script>
        function addQuery(name, value) {
            $('#qToko').append(`
            <input type="hidden" name="${name}[]" value="${value}">
            `)
        }

        $(document).ready(function() {
            var filters = []
            $('#drop__table_shopee').DataTable();
            $('#drop__table_blibli').DataTable();
            $('#drop__table').DataTable();
            $('#drop__table_jdid').DataTable();
            $('#drop__table_harbolnas').DataTable();

            $('#select__toko').on('change', function() {
                let choosed = this.value
                let text = $(this).find('option').filter(':selected').text()

                // console.log(choosed)
                // console.log(text)
                if (!filters.includes(text)) {
                    filters.push(text)
                    addQuery('toko', choosed)

                    $('#list_toko').append(`
                    <li class="list-group-item">${text}</li>
                    `)
                }
            })

            // $('#start__btn').on('click', function() {
            //     Swal.fire(
            //         'The Internet?',
            //         'That thing is still around?',
            //         'success'
            //     )
            // })
        });
    </script>
@endsection
