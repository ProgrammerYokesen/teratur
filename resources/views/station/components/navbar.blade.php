<nav class="navbar navbar-expand-lg navbar-light bg-light d-print-none">
    <div class="container">
        {{-- <a class="navbar-brand" href="/station/pack">TERATUR</a> --}}
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a href="https://teratur.gudangin.id/admin/dashboard" style="text-decoration: none;color: rgba(0,0,0,.55);" class='nav-item me-4'><i class="fas fa-arrow-circle-left" ></i>DASHBOARD</a>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">

            <ul class="navbar-nav me-auto">
                <li class="nav-item" style="margin-right: 1rem">
                    <a class="nav-link d-flex" style="align-items: center" href="{{ route('pack', ['id' => 5]) }}">
                        <div>PACK</div>
                        <div class="badge bg-danger custom__badge pack_counter">{{ $countAllPack }}</div>
                    </a>
                </li>

                <li class="nav-item" style="margin-right: 1rem">
                    <a class="nav-link d-flex" style="align-items: center" href="{{ route('drop') }}">
                        <div>DROP</div>
                        <div class="badge bg-danger custom__badge">{{ $countAllDrop }}</div>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 1rem">
                    <a class="nav-link d-flex" style="align-items: center" href="{{ route('pick') }}">
                        <div>PICK UP</div>
                        <div class="badge bg-danger custom__badge">{{ $countAllPick }}</div>
                    </a>
                </li>
                <li class="nav-item" style="margin-right: 1rem">
                    <a class="nav-link d-flex" style="align-items: center" href="{{ route('done') }}">
                        <div>DONE</div>
                        <div class="badge bg-danger custom__badge">{{ $countAllEnd }}</div>
                    </a>
                </li>
            </ul>


            @if ($id != 5)
                    <div class="d-flex align-center">
                        <button type="button" id="timer-btn" class="pulse__btn btn btn-danger blink"></button>
                        <button type="button" id='label-btn' class="pulse__btn btn btn-warning"></button>
                        <button type="button" id='finish-btn' class="pulse__btn btn btn-success"></button>
                @if (count($getOrder) < 1 )
                        <button type="button" id='start-btn' class="box_pulse__btn btn btn-success d-none">MULAI</button>
                @else
                        <button type="button" id='start-btn' class="box_pulse__btn btn btn-success">MULAI</button>
                @endif
                @if ($getOrder[0]->platform == 'Tokopedia' )
                        <a type="button" id='qr_new' href="{{route('label', $getOrder[0]->id)}}" target="_blank" class="mr__1 box_pulse__btn btn btn-warning d-none">PRINT LABEL DAN QR</a>
                @else
                        <button type="button" id='qr' class="mr__1 box_pulse__btn btn btn-warning d-none">PRINT LABEL DAN QR</button>
                @endif
                @if ($getOrder[0]->logistic_type == 'pickUp')
                        <button type="button" id='pickup-btn' class="box_pulse__btn btn btn-info d-none" disabled>Request PickUp</button>
                @endif
                        <button type="button" id='done-btn' class="box_pulse__btn btn btn-info d-none" disabled>selesai</button>
                    </div>
            @endif

        </div>
    </div>
</nav>
