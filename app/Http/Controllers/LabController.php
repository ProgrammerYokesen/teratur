<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LabController extends Controller
{
    public function insertQueueTime($id){
        $getData = DB::table('bookingan')->where('status', 'packing')->where('id', $id)->first();
        $update = DB::table('bookingan')->where('id', $getData->id)->update([
            'station_queue_time'=>date('Y-m-d H:i:s')
        ]);
        return redirect()->back();
        
    }
    
    public function testTime(){
        // $currentTime = date('Y-m-d H:i:s');
        // $startTimeLimit = date('Y-m-d').' 15:00:00';
        
        // if($currentTime < $startTimeLimit){
        //     dd('dibawah', $currentTime, $startTimeLimit);
        // }else{
        //     dd('diatas', $currentTime, $startTimeLimit);
        // }
    }
}
