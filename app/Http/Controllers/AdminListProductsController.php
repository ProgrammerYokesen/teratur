<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use GuzzleHttp\Exception\RequestException;
    use GuzzleHttp\Psr7;
    use GuzzleHttp\Client;
    use App\Http\Controllers\JDID\SellerPriceUpdatePriceBySkuIdsRequest;
    use App\Http\Controllers\JDID\JdClient;
    
	class AdminListProductsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = false;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "produk_marketplace";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Product Name","name"=>"id"];
			$this->col[] = ["label"=>"Stock","name"=>"stock"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Price","name"=>"price"];
			$this->col[] = ["label"=>"Platform","name"=>"platform"];
			$this->col[] = ["label"=>"Updated At","name"=>"updated_at"];
			$this->col[] = ["label"=>"Nama Toko","name"=>"shop_id"];
			$this->col[] = ["label"=>"Gudang","name"=>"shop_id"];
			$this->col[] = ["label"=>"Id","name"=>"id"];
			$this->col[] = ["label"=>"SKU","name"=>"sku"];
			$this->col[] = ["label"=>"Inisial","name"=>"initial"];
			$this->col[] = ["label"=>"Kode Item","name"=>"kode_item"];
			$this->col[] = ["label"=>"Shop Id","name"=>"shop_id"];
			$this->col[] = ["label"=>"Has Variant","name"=>"has_variant"];
			$this->col[] = ["label"=>"Induk Barang","name"=>"induk_barang_id","join"=>"products,productName"];
			$this->col[] = ["label"=>"Stock Convertion_unit","name"=>"stock_convertion_unit"];
			$this->col[] = ["label"=>"Convertion Id","name"=>"convertion_id","join"=>"convertion,nama_satuan"];
			$this->col[] = ["label"=>"variant","name"=>"variant_name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Updated At','name'=>'updated_at','type'=>'datetime','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Initial','name'=>'initial','type'=>'text','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Harga','name'=>'price','type'=>'text','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Kode Item','name'=>'kode_item','type'=>'text','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Jenis Barang','name'=>'jenis_barang','type'=>'text','width'=>'col-sm-9'];
			$this->form[] = ['label'=>'Stock Convertion_unit','name'=>'stock_convertion_unit','type'=>'select','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Updated At','name'=>'updated_at','type'=>'datetime','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Initial','name'=>'initial','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Harga','name'=>'price','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Kode Item','name'=>'kode_item','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Jenis Barang','name'=>'jenis_barang','type'=>'text','width'=>'col-sm-9'];
			//$this->form[] = ['label'=>'Stock Convertion_unit','name'=>'stock_convertion_unit','type'=>'select','validation'=>'required','width'=>'col-sm-9'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
            $this->addaction[] = ['label'=>'','url'=>CRUDBooster::mainpath('edit-products/[id]'),'icon'=>'fa fa-pencil','color'=>'success', 'showIf'=>"[has_variant] == 0"];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    	if($column_index == 0){
	    	    $row = CRUDBooster::first($this->table,$column_value);
	    	  //  dd($row);
	    	  if($row->is_variant == 1){
	    	      $getVariantParent = DB::table('produk_marketplace')->where('id', $row->parent_produk_id)->first();
	    	      $getName = DB::table('produk')->where('id', $getVariantParent->parent_produk_id)->first();
	    	      $column_value = $getName->product_name.' varian '.$row->sku;
	    	  }else{
	    	      $getName = DB::table('produk')->where('id', $row->parent_produk_id)->first();
                  $column_value = $getName->product_name;
	    	  }
	    	}
	    	if($column_index == 6){
	    	    $shopName = DB::table('Registered_store')->where('shop_id', $column_value)->first();
	    	  //  dd($shopName);
	    	    $column_value= $shopName->store_name;
	    	}
	    	if($column_index == 7){
	    	    $shopName = DB::table('Registered_store')->where('Registered_store.shop_id', $column_value)->join('data_toko', 'Registered_store.data_toko', 'data_toko.id')->first();
	    	  //  dd($shopName);
	    	    $column_value= $shopName->nama_ukm;
	    	}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here
	       // dd($postdata['price']);
            $products = DB::table('produk_marketplace')->where('id', $id)->first();
            $shop = DB::table('Registered_store')->where('shop_id', $products->shop_id)->first();
            if($products->platform == 'Tokopedia'){
                $client = new Client();
                $clientId = env('TOKOPEDIA_CLIENT_ID');
                $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                $clientData = base64_encode("{$clientId}:{$clientSecret}");
        
                $appID = (int)env('TOKOPEDIA_APP_ID');
                try{
                $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                    'Authorization'=> "Basic $clientData",
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1'
                    ]])->getBody()->getContents();
                }catch (RequestException $e) {
                    // dd(Psr7\str($e->getRequest()));
                    echo Psr7\str($e->getRequest());
                    if ($e->hasResponse()) {
                        echo Psr7\str($e->getResponse());
                    }
                }
                $token = json_decode($res);
                
                $shop_id = $shop->shop_id;
                $product_id = (int)$products->produk_id;
                $price = (int)$postdata['price'];
                // dd($sku, gettype($price), $shop_id, $token->access_token, $appID);
                $data = Array();
                $data[0]['product_id'] = $product_id;
                $data[0]['new_price'] = $price;
                // dd($data);
                $clientUpdate = new Client();
                $resPrice = $clientUpdate->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/price/update?shop_id=$shop_id", ['headers' => [
                    'Authorization' => "Bearer $token->access_token",
                    'Content-Type'=> 'application/json'
                ],
                    'json'=>$data
                ])->getBody()->getContents();
                $responsePrice = json_decode($resPrice);
                // dd($responsePrice);
            }elseif($products->platform == 'Shopee'){
                $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $products->shop_id)->first();
                $shop_id = $shop->shop_id;
                $access_token = $shop->access_token;
                $timestamp = time();
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $path = "/api/v2/product/update_price";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $price = (int)$postdata['price'];
                if($products->is_variant == 1 ){
                    $getItemId = DB::table('produk_marketplace')->where('id', $products->parent_produk_id)->first();
                    $itemId = (int)$getItemId->produk_id;
                    $modelId = (int)$products->produk_id;
                }else{
                    $itemId = (int)$products->produk_id;
                    $modelId = 0;
                }
                $data = array();
                $data[0]['model_id'] = $modelId;
                $data[0]['original_price'] = $price;
                $clientUpdatePrice = new Client();
                // dd(gettype($price));
                $resUpdate = $clientUpdatePrice->post("https://partner.shopeemobile.com/api/v2/product/update_price?shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp", ['headers'=> [
                    'Content-Type' => 'application/json',
                ],
                    'json'=>[
                        'item_id'=>$itemId,
                        'price_list'=>$data
                    ]
                ])->getBody()->getContents();
                $getUpdateResult = json_decode($resUpdate);
            }elseif($products->platform == 'Blibli'){
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                $blibliSku = $products->produk_id;
                $sellerKey = $shop->seller_api_key;
                $shopId = $shop->shop_id;
                $price = $postdata['price'];
                
                $updatePrice = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&storeCode='.$shopId.'&username=daniel.yokesen@gmail.com&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $sellerKey
                    ],
                        'json'=>[
                            'buyable'=>true,
                            'price'=>[
                                'regular'=>$price
                            ],
                        ]
                    ])->getBody();
                $getUpdateResponse = $updatePrice->getContents();
                $data = json_decode($getUpdateResponse );
                // dd($data, $updatePrice);
            }else{
                $skuId = (int)$products->produk_id;
                $price = (int)$postdata['price'];
                $c = new JdClient();
                $c->appKey = env('APP_KEY');
                $c->appSecret = env('APP_SECRET');
                $c->accessToken = $shop->access_token;
                $c->serverUrl = "https://open-api.jd.id/routerjson";
                $req = new SellerPriceUpdatePriceBySkuIdsRequest();
                $req->setSalePrice( $price );
                $req->setSkuId($skuId);
                
                $resp = $c->execute($req, $c->accessToken);
                // dd($resp);
            }
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}