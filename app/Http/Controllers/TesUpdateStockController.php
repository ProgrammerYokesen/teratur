<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TesUpdateStockController extends Controller
{
    public function tesUpdateStock(){
        $getOrder = DB::table('bookingan')->where('id', $bookinganId)->first();
        $getDetailProduk = DB::table('order_from_marketplace_detail')->where('orderId', $bookinganId)->get();
        foreach($getDetailProduk as $produk){
            // update satuan
                // dd('satuan');
              $converterNumber = DB::table('convertion')->where('id', $produkTerjual->convertion_id)->first();
              $convertProdukTerbeli = $produk->qty * $converterNumber->stock_convertion_unit;

              // ambil stock
              $stok_induk = DB::table('stock')->where('induk_produk_id',$produk_induk->id)->orderby('id','desc')->first();
              $stok_depo = DB::table('stock')->where('data_toko_id',$produk_induk->brand_id)->where('induk_produk_id',$produk_induk->id)->orderby('id','desc')->first();
            
              // ambil stock number
              $stock_induk_numbers = DB::table('stock_number')->where('stock_id', $stok_induk->id)->get();
              $stock_depo_number = DB::table('stock_number')->where('stock_id', $stok_induk->id)->get();
              $convertions = DB::table('convertion')->where('induk_produk_id',$indukProdukId)->get();
            //   dd($stock_induk_numbers);
              $stockIndukConverted = 0;
              $stockDepoConverted = 0;
              foreach($stock_induk_numbers as $index=>$stockNumber){
                  $convertion = DB::table('convertion')->where('id', $stockNumber->convertion_id)->first();
                  $stockIndukConverted = $stockIndukConverted + ($stockNumber->bookStockUnit_induk * $convertion->stock_convertion_unit);
                  $stockDepoConverted = $stockIndukConverted + ($stockNumber->bookStockUnit_induk * $convertion->stock_convertion_unit);
              }
                  $currentStockInduk = $stockIndukConverted - $convertProdukTerbeli;
                //   dd($currentStockInduk, $stockIndukConverted, $convertProdukTerbeli);
                  $currentStockDepo = $stockDepoConverted - $convertProdukTerbeli;
                
                  $cek = DB::table('stock')->where('data_toko_id', $produk_induk->brand_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $indukProdukId)->count();
                  if($cek < 1){
                    //   return response()->json($produk_induk);
                      $insertStockPenjualan = DB::table('stock')->insertGetId([
                          'data_toko_id'=>$produk_induk->brand_id,
                          'induk_produk_id'=>$indukProdukId,
                          'tipe'=>'penjualan',
                          'created_at'=>date('Y-m-d H:i:s'),
                          'documentNumber'=>$getOrder->kode
                      ]);
                  }else {
                      $lastData = DB::table('stock')->where('data_toko_id', $produk_induk->brand_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $indukProdukId)->first();
                      $insertStockPenjualan = $lastData->id;
                      
                  }
                  
                  $moduloSelisihUnitInduk = 0;
                  $moduloSelisihUnitDepo = 0;
                //   dd($convertions, $currentStockInduk);
                  // dalam codingan coach tidak diperhitungkan apakah bundle atau satuan lain yang lebih kecil dapat dijadikan satuan atasnya
                  foreach($convertions as $index=>$convertion){
                      if($index == 0){
                          $modSelisihUnitInduk = $currentStockInduk % $convertion->stock_convertion_unit;
                          $divSelisihUnitInduk  = $currentStockInduk - $modSelisihUnitInduk;
                          $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitInduk = $modSelisihUnitInduk;
                        //   dd($modSelisihUnitInduk, $currentStockInduk, $convertion->stock_convertion_unit, $moduloSelisihUnitInduk);
                          $modSelisihUnitDepo = $currentStockDepo % $convertion->stock_convertion_unit;
                          $divSelisihUnitDepo = $currentStockDepo - $modSelisihUnitDepo;
                          $convertSelisihUnitDepo = $divSelisihUnitDepo / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitDepo = $modSelisihUnitDepo;
                      }else{
                          $modSelisihUnitInduk = $moduloSelisihUnitInduk % $convertion->stock_convertion_unit;
                          $divSelisihUnitInduk = $moduloSelisihUnitInduk - $modSelisihUnitInduk;
                          $convertSelisihUnitInduk = $divSelisihUnitInduk / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitInduk = $modSelisihUnitInduk;
                            // dd($moduloSelisihUnitInduk , $modSelisihUnitInduk, $convertSelisihUnitInduk, $divSelisihUnitInduk, $convertion);
                          $modSelisihUnitDepo = $moduloSelisihUnitIndukDepo % $convertion->stock_convertion_unit;
                          $divSelisihUnitDepo = $moduloSelisihUnitIndukDepo - $modSelisihUnitDepo;
                          $convertSelisihUnitDepo = $divSelisihUnitDepo / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitDepo = $modSelisihUnitDepo;
                      }
                      
                      $check = DB::table('stock_number')->where('stock_id', $insertStockPenjualan)->where('convertion_id', $convertion->id)->count();
                    //   dd($check);
                    if($stock_induk_numbers[$index]->stockOpnameUnit_induk == null){
                        $stockOpnameUnit_induk = 0;
                    }else {
                        $stockOpnameUnit_induk = $stock_induk_numbers[$index]->stockOpnameUnit_induk;
                    }
                      if($check < 1){
                        //   dd($stock_induk_numbers, $index, $convertions);
                          if($convertion->id == $produkTerjual->convertion_id){
                              $insertNumberOfStock = DB::table('stock_number')->insert([
                                'stock_id'=>$insertStockPenjualan,
                                'convertion_id'=>$convertion->id,
                                'unit'=>$produk->qty,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'bookStockUnit_depo'=>$convertSelisihUnit,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'created_at'=>date('Y-m-d H:i:s')
                              ]);
                          }else{
                              $insertNumberOfStock = DB::table('stock_number')->insert([
                                'stock_id'=>$insertStockPenjualan,
                                'convertion_id'=>$convertion->id,
                                'unit'=>0,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'bookStockUnit_depo'=>$convertSelisihUnit,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'created_at'=>date('Y-m-d H:i:s')
                              ]);
                          }
                      }
                      
                      
                       

                  }
            }
            // dd($stok_induk, $convertProdukTerbeli, $stock_induk_numbers, $convertions);

            // update to marketplace
            // $otherProducts = DB::table('produk_marketplace')->where('induk_barang_id', $indukProdukId)->where('produk_id', '!=', $produkTerjual->produk_id)->get();
            $otherProducts = DB::table('produk_marketplace')->where('induk_barang_id', $indukProdukId)->get();
            // dd($currentStockInduk, $otherProducts);
            // dd($otherProducts);
            foreach($otherProducts as $product){
               $getConversionProduk = DB::table('convertion')->where('id',$product->convertion_id)->first();
                $stock = floor($currentStockInduk / $getConversionProduk->stock_convertion_unit);
                // dd($getConversionProduk, $stock);
                if($getOrder->platform == 'Tokopedia'){
                    // cek kode hitung
                        // cek marketplace
                    if($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){

                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        // $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        // dd($access_token, $shop);
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($product->is_variant == 1){
                            $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                            $item_id = $getParent->produk_id;
                            $model_id = $product->produk_id;
                            // dd($getParent,$item_id, $model_id );
                        }else{
                            $item_id = $product->produk_id;
                            $model_id = 0;
                        }

                        try{
                            $data[0]['model_id']=(int)$model_id;
                            $data[0]['normal_stock']=(int)$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // dd($responseShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message, $ex );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } else if($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                    }else{
                      $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                      $access_token = $shop->access_token;
                      $data[0]['product_id']=(string) $product->produk_id;
                      $data[0]['stock_value']=(int) $stock;
                      $stockUpdateClient = new Client();
                      $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                          ['Authorization' => "Bearer $access_token"],
                          'json'=>$data
                      ])->getBody()->getContents();
                      $responseLabel = json_decode($resLabel);
                    }

                }elseif($getOrder->platform == 'Shopee'){
                    // ambil data bookingan
                    $getToko = DB::table('Registered_store')->where('shop_id', $getOrder->nmToko)->first();
                    // $getToko = DB::table('Registered_store')->where('shop_id', $getOrder->nmToko)->where('Platform', 'Shopee')->first();
                    $access_token=$getToko->access_token;
                    $shop_id=$getToko->shop_id;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $order_sn = $getOrder->kode;
                    // $package_number = $getToko->package_id;
                    $timestamp = time();

                    // shipping parameter
                        $path = "/api/v2/logistics/get_shipping_parameter";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShippingParameter = new Client();
                        $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                        $responseShippingParameter = json_decode($resShippingParameter);
                        $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                        $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                        $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                        // dd($responseShippingParameter);
                    // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
                    $logistic_type = '';
                    if( $responseShippingParameter->response->info_needed->pickup){
                        $logistic_type = 'pickUp';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                // 'package_number'=>$package_number,
                                'pickup'=>[
                                    'address_id'=>$address_id,
                                    'pickup_time_id'=>$pickup_time_id,
                                    // 'tracking_number'=>$trackingNumber,
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder);

                    }elseif($branch_id){
                        $logistic_type = 'dropoff';
                        // echo 'dropoff<br/>';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                'dropoff'=>[
                                    'address_id'=>$address_id,
                                    'pickup_time_id'=>$pickup_time_id,
                                    // 'tracking_number'=>$trackingNumber,
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder, 'tes');
                    }else{
                        $logistic_type = 'dropoff';
                        // echo 'dropoff<br/>';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                'dropoff'=>[
                                    'tracking_no'=>'',
                                    'branch_id'=>0,
                                    'sender_real_name'=>'',
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder, 'set');
                    }

                    // get package number
                    $path = "/api/v2/order/get_order_detail";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientOrderList = new Client();
                    $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $responseOrderList = json_decode($resOrderList);
                    $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;

                    // get Tracking Number
                    $path = "/api/v2/logistics/get_tracking_number";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientTrackingNumber = new Client();
                    $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                    $responseTrackingNumber = json_decode($resTrackingNumber);
                    $trackingNumber = $responseTrackingNumber->response->tracking_number;

                    // get shipping document parameter
                    $path = "/api/v2/logistics/get_shipping_document_parameter";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $data = Array();
                    $data[0]['order_sn']=$order_sn;
                    $data[0]['package_number']=$package_number;
                    $clientShippingDocumentParameter = new Client();
                    $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],

                            'json'=>[
                                'order_list'=>$data
                            ]
                    ])->getBody()->getContents();
                    $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                    $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                    // CREATE SHIPPING DOCUMENT
                        $path = "/api/v2/logistics/create_shipping_document";
                        $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $data = array();
                        $data[0]['order_sn'] = $order_sn;
                        $data[0]['package_number'] = $package_number;
                        $data[0]['tracking_number'] = $trackingNumber;
                        $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                        $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                            'headers' => [
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ],

                            'json' => [
                                'order_list' => $data
                            ]
                            // 'body'=>$parameter

                        ])->getBody()->getContents();
                        $resultCreated = json_decode($createdDocument);

                    // get shipping document result
                    $path = "/api/v2/logistics/get_shipping_document_result";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDocResult = Array();
                    $dataDocResult[0]["order_sn"]=$order_sn;
                    $dataDocResult[0]["package_number"]=$package_number;
                    $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                    // dd($dataDocResult);
                    $clientShippingDocResult = new Client();
                    $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                    ],
                        'json'=>[
                            'order_list'=>$dataDocResult
                        ]
                    ])->getBody()->getContents();
                    $responseShippingDocResult = json_decode($resShippingDocResult);
                    // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
                // download shipping document
                    $path = "/api/v2/logistics/download_shipping_document";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDownload = Array();
                    $dataDownload[0]["order_sn"]=$order_sn;
                    $dataDownload[0]["package_number"]=$package_number;
                    $clientDownloadShipDoc = new Client();
                    $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                        ],
                        'json'=>[
                            'shipping_document_type'=>$suggest_shipping_document_type,
                            'order_list'=>$dataDownload,
                            ]
                        ])->getBody()->getContents();
                    $resShippingLabel = json_decode($resDownloadShipDoc);
                    $name = "Shopee-$order_sn.pdf";
                    \Storage::put("public/label/$name", $resDownloadShipDoc);
                    // $path = 'storage/label/'.$name;
                    // update bukti bayar
                    $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                        'buktiBayar'=>$name,
                        'awbNo'=>$trackingNumber
                    ]);
                    $pdfBuktiBayar = base64_encode($resDownloadShipDoc);
                    $clientSendImage = new Client();
                    
                    $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                          'json'=>[
                              
                                  'name'=>$name,
                                  'contentBuktibayar'=> $pdfBuktiBayar,
                          ]
                    ])->getBody()->getContents();
                    
                    if($product->platform == 'Shopee'){

                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        // $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        // dd($access_token, $shop);
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($product->is_variant == 1){
                            $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                            $item_id = $getParent->produk_id;
                            $model_id = $product->produk_id;
                            // dd($getParent,$item_id, $model_id );
                        }else{
                            $item_id = $product->produk_id;
                            $model_id = 0;
                        }

                        try{
                            $data[0]['model_id']=(int)$model_id;
                            $data[0]['normal_stock']=(int)$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // dd($responseShopeeStock);
                            // updateStock
                            if($product->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message, $ex );
                        }
                    }elseif($product->platform == 'Blibli'){
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }

                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                      }else{
                            $client = new Client();
                            $data[0]['product_id']=(string) $product->produk_id;
                            $data[0]['stock_value']=(int) $stock;
                            $resUpdateStock = $client->post("https://sindehomedelivery.com/api/stock/overwrite",['headers'=>
                                ['Authorization' => "Bearer $access_token"],
                                'json'=>$data
                            ])->getBody()->getContents();
                      }
                }elseif($getOrder->platform == 'Blibli'){
                    
                    if($product->platform == 'Blibli'){
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }

                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);

                      }else{
                          $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                          $access_token = $shop->access_token;
                          $data[0]['product_id']=(string) $product->produk_id;
                          $data[0]['stock_value']=(int) $stock;
                          $stockUpdateClient = new Client();
                          $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                              ['Authorization' => "Bearer $access_token"],
                              'json'=>$data
                          ])->getBody()->getContents();
                          $responseLabel = json_decode($resLabel);
                      }
                }elseif($getOrder->platform == 'JDID'){
                    
                    if($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store)->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);

                    }else{
                        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                        $access_token = $shop->access_token;
                        $data[0]['product_id']=(string) $product->produk_id;
                        $data[0]['stock_value']=(int) $stock;
                        $stockUpdateClient = new Client();
                        $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                            ['Authorization' => "Bearer $access_token"],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseLabel = json_decode($resLabel);
                      }
                }elseif($getOrder->platform == 'Sinde Home Delivey'){

                    if($product->platform == 'Sinde Home Delivery'){
                        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                        $access_token = $shop->access_token;
                        $data[0]['product_id']=(string) $product->produk_id;
                        $data[0]['stock_value']=(int) $stock;
                        $stockUpdateClient = new Client();
                        $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                            ['Authorization' => "Bearer $access_token"],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseLabel = json_decode($resLabel);
                    
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                    }else{
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    }
                    // warisan
                }else{
                    
                  if($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                  }elseif($product->platform == 'Tokopedia'){
                       $client = new Client();
                      $clientId = env('TOKOPEDIA_CLIENT_ID');
                      $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                      $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                      $clientData = base64_encode("{$clientId}:{$clientSecret}");
                      $appID = env('TOKOPEDIA_APP_ID');
                      try{
                          $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                          'Authorization'=> "Basic $clientData",
                          'Content-Length' => 0,
                          'User_Agent' => 'PostmanRuntime/7.17.1'
                          ]])->getBody()->getContents();
                      }catch (RequestException $e) {
                          // dd(Psr7\str($e->getRequest()));
                          echo Psr7\str($e->getRequest());
                          if ($e->hasResponse()) {
                              echo Psr7\str($e->getResponse());
                          }
                      }
                      $token = json_decode($res);
                      try{
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                          $shop_id= (int)$shop->shop_id;
                          $fsId = (int)$appID;
                          // $stock = $item->stock - $produk->qty;
                          // if($getOrder->shop_id == 3897048){
                          //     $stock = $stockProduct;

                          // }
                      // dd($token, $shop_id, $appID);
                          $data = Array();
                          $data[0]['product_id']=(int)$product->produk_id;
                          $data[0]['new_stock']=$stock;
                          // $shop_id = $shop['shopsId'];
                          // dd($shop, $shop_id, $stock, $data, $token);
                          $clientStock = new Client();
                          $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                              'Authorization'=> "Bearer $token->access_token",
                              'Content-Type'=> 'application/json'
                          ],
                              'json'=>$data
                          ])->getBody()->getContents();
                          $responseStock = json_decode($resStock);
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                              'stock'=>$stock
                          ]);
                      }catch (\GuzzleHttp\Exception\ClientException $ex) {
                          // $responseBody = $exception->getResponse()->getBody(true);
                          $response = json_decode($ex->getResponse()->getBody()->getContents());
                          // dd($response, $e);
                      }
                  }elseif($product->platform == 'Shopee'){
                       $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                      $shop_id= (int) $shop->shop_id;
                      $access_token = $shop->access_token;
                      $partner_id = (int)env('SHOPEE_PARTNER_ID');
                      $partner_key = env('SHOPEE_PARTNER_KEY');
                      $timestamp = time();
                      $path = "/api/v2/product/update_stock";
                      $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                      $sign = hash_hmac('sha256', $salt, $partner_key);
                      $model_id = 0;
                      // $stock = $item->stock - $produk->qty;
                          // cek apakah model perlu atau tidak(variant)?
                      if($item->has_variant == 1){
                          $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                          $model_id = $getModelId->produk_id;
                          // $stock = $getModelId->stock - $produk->qty;
                      }

                      try{
                          $data[0]['model_id']=$model_id;
                          $data[0]['normal_stock']=$stock;
                          $clientShopeeStock = new Client();
                          $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                              'Content-Type'=>'application/json',
                          ],
                              'json'=>
                                  [
                                      'item_id'=>(int)$item->produk_id,
                                      'stock_list'=>$data
                                  ]
                          ])->getBody()->getContents();
                          $responseShopeeStock = json_decode($resShopeeStock);
                          // updateStock
                          if($item->has_variant == 1){
                              $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                  'stock'=>$stock
                                  ]);

                          }else{
                              // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                              //     'stock'=>$stock
                              //     ]);
                              $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                              'stock'=>$stock
                              ]);
                          }
                      }catch(\GuzzleHttp\Exception\ClientException $ex){
                          $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                          dd($response->message );
                      }
                  }elseif($product->platform == 'Blibli'){
                      $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                      $sellerApiKey = $shop->seller_api_key;
                      $storeCode = $shop->shop_id;
                      // $stock = $item->stock - $produk->qty;
                      $client = new Client();
                      $clientId = env('BLIBLI_CLIENT_ID');
                      $clientKey = env('BLIBLI_CLIENT_KEY');
                      $Authorization = base64_encode("{$clientId}:{$clientKey}");
                      $blibliSku = $item->produk_id;

                      try{
                          $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $sellerApiKey
                            ],
                                'json'=>
                                    [
                                        'availableStock'=>$stock,
                                    ]
                            ])->getBody();
                        $res = $res->getContents();
                        $response = json_decode($res);
                        $datas = $response->content;
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                            'stock'=>$stock
                        ]);
                       }catch (\GuzzleHttp\Exception\ClientException $ex) {
                          // $responseBody = $exception->getResponse()->getBody(true);
                          $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                          dd($response );
                      }
                  }elseif($product->platform == 'JDID'){
                    // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                    $skuNo = $product->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    // $skuEncode = json_encode($skuId);
                    // $c = new JdClient();
                    // $c->appKey = env('APP_KEY');
                    // $c->appSecret = env('APP_SECRET');
                    // $c->accessToken = $shop->access_token;
                    // $c->serverUrl = "https://open-api.jd.id/routerjson";
                    // $req = new EpistockQueryEpiMerchantWareStockRequest();
                    // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                    // $respStock = $c->execute($req, $c->accessToken);
                    // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                    // // $currentStock = $stock - $produk->qty;
                    // // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                        'stock'=>$stock
                    ]);
                  } else{
                      $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                      $access_token = $shop->access_token;
                      $data[0]['product_id']=(string) $product->produk_id;
                      $data[0]['stock_value']=(int) $stock;
                      $stockUpdateClient = new Client();
                      $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                          ['Authorization' => "Bearer $access_token"],
                          'json'=>$data
                      ])->getBody()->getContents();
                      $responseLabel = json_decode($resLabel);
                  }
                }
            }
        }
              

    }
}
