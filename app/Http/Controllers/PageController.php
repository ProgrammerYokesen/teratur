<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class PageController extends Controller
{
    // public function invoiceDummy()
    // {
    //     return view('admin.dummy');
    // }

    // public function printDummy()
    // {
    //     $pdf = new \PDF();

    //     $pdf = \PDF::loadView('admin.dummy')->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
    //     $name = 'Harbolnas_x.pdf';
    //     \Storage::put('public/label/xxx.pdf', $pdf->output());
    //     // $path = "storage/label/$name";
    // }

    public function products()
    {
        return view('admin.products');
    }

    public function stationSatu()
    {
        return view('admin.station-satu');
    }

    public function pack($id)
    {

        if ($id == 1) {
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', 1)->orderBy('station_queue_time', 'asc')->get();
        } elseif ($id == 2) {
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', 2)->orderBy('station_queue_time', 'asc')->get();
        } elseif ($id == 3) {
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', 3)->orderBy('station_queue_time', 'asc')->get();
        } elseif ($id == 4) {
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', 4)->orderBy('station_queue_time', 'asc')->get();
        } elseif ($id == 6){
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', 6)->orderBy('station_queue_time', 'asc')->get();
        } else {
            $getOrder = DB::table('bookingan')->where('bookingan.status', 'packing')->get();
        }
        $countData = count($getOrder);
        if ($countData > 0) {
            foreach ($getOrder as $key => $value) {
                $getDetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->get();
                if (count($getDetail) > 0) {
                    foreach ($getDetail as $index => $detail) {
                        if($value->platform == 'harbolnas 12.12'){
                            // dd($value);
                            $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Tokopedia')->first();
                            // dd($product);
                            $getDetail[$index]->photo = $product->photo;
                            $getDetail[$index]->initial = $product->initial;
                        }else{
                            $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', $value->platform)->first();
                            $getDetail[$index]->photo = $product->photo;
                            $getDetail[$index]->initial = $product->initial;
                        }
                        if($value->platform == 'Warisan'){
                            $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Warisan')->first();
                            // cekinduk apakah assembly atau tidak
                            $induk = DB::table('products')->where('id',$product->induk_barang_id)->first();
                            if($induk->jenis_barang == 'assembly'){
                               $getsubproducts = DB::table('assembly_subproduct')->where('assembly_id', $induk->id)->get();
                               foreach($getsubproducts as $iteration=>$product){
                                   $getproduct = DB::table('products')->where('id', $product->product_id)->first();
                                   $getDetail[$index]->sub[$iteration] = $getproduct->productName;
                               }
                            }
                            
                        }
                    }
                    $getOrder[$key]->order = $getDetail;
                }
            }
            $getOrder = $getOrder->filter(function ($item) {
                return count($item->order) > 0;
            })->values();
        }
        $stations = DB::table('station')->get();

        // dropoff
        $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        // dd($datasTokped);
        $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasJdidDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasHarbolnasDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'harbolnas')->count();
        $warisanDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Warisan')->count();


        $countAllDrop = $datasTokpedDrop + $datasBlibliDrop + $datasShopeeDrop + $datasJdidDrop + $datasHarbolnasDrop + $warisanDrop;
        $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();

        // pick
        $datasTokped = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasBlibli = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopee = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasJdid = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

        $countAllPick = $datasTokped + $datasBlibli + $datasShopee + $datasJdid;
        $timer = DB::table('packing_time')->where('status', 'active')->first();
        // if ($id == 2 || $id ==5){
        //     dd([$timer,$countAllDrop,$countAllPack,$getOrder,$stations,$id]);
        // }
        // dd($getOrder);
        return view('station.pages.station', compact('timer', 'countAllDrop', 'countAllPack', 'countAllPick', 'getOrder',  'stations', 'id'));
    }

    public function drop()
    {
        // drop
        $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        // dd($datasTokped);
        $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasJdidDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasHarbolnasDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'harbolnas 12.12')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at')->get();
        $datasWarisanDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Warisan')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();

        $countTokpedDrop = count($datasTokpedDrop);
        $countBlibliDrop = count($datasBlibliDrop);
        $countShopeeDrop = count($datasShopeeDrop);
        $countJdidDrop = count($datasJdidDrop);
        $countHarbolnasDrop = count($datasHarbolnasDrop);
        $countWarisanDrop = count($datasWarisanDrop);
        $countAllDrop = $countTokpedDrop + $countBlibliDrop + $countShopeeDrop + $countJdidDrop + $countHarbolnasDrop + $countWarisanDrop;
        $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();

        // pick
        $datasTokped = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasBlibli = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopee = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasJdid = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

        $countAllPick = $datasTokped + $datasBlibli + $datasShopee;
        $id = 5;

        $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();
        return view('station.pages.drop', compact('datasHarbolnasDrop','countHarbolnasDrop', 'countWarisanDrop', 'datasWarisanDrop','countJdidDrop', 'datasJdidDrop', 'datasTokpedDrop', 'datasBlibliDrop', 'datasShopeeDrop', 'countTokpedDrop', 'countBlibliDrop', 'countShopeeDrop', 'countAllDrop', 'countAllPack', 'countAllPick', 'id', 'allStore'));
    }

    public function done()
    {

        // done

        $datasTokpedEnd = DB::table('bookingan')->where('bookingan.status', "end order")->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name', 'bookingan.id')->get();
        // dd($datasTokped);
        $datasBlibliEnd = DB::table('bookingan')->where('bookingan.status', "end order")->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasShopeeEnd = DB::table('bookingan')->where('bookingan.status', "end order")->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasJdidEnd = DB::table('bookingan')->where('bookingan.status', "end order")->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $datasHarbolnasEnd = DB::table('bookingan')->where('bookingan.status', "end order")->where('bookingan.platform', 'harbolnas12.12')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();

        $countTokpedEnd = count($datasTokpedEnd);
        $countBlibliEnd = count($datasBlibliEnd);
        $countShopeeEnd = count($datasShopeeEnd);
        $countHarbolnasEnd = count($datasHarbolnasEnd);
        $countJdidEnd = count($datasJdidEnd);
        $countAllEnd = $countTokpedEnd + $countBlibliEnd + $countShopeeEnd + $countJdidEnd;

        // drop
        $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->get();
        // dd($datasTokped);
        $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->get();
        $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->get();
        $datasJdidDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->get();
        $datasHarbolnasDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'harbolnas 12.12')->get();
        $datasWarisanDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Warisan')->get();

        $countTokpedDrop = count($datasTokpedDrop);
        $countBlibliDrop = count($datasBlibliDrop);
        $countShopeeDrop = count($datasShopeeDrop);
        $countJdidDrop = count($datasJdidDrop);
        $countHarbolnasDrop = count($datasHarbolnasDrop);
        $countWarisanDrop = count($datasWarisanDrop);
        $countAllDrop = $countTokpedDrop + $countBlibliDrop + $countShopeeDrop + $countJdidDrop + $datasHarbolnasDrop + $countWarisanDrop;
        $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();

        // pick
        $datasTokped = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasBlibli = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopee = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasJdid = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $id = 5;
        $countAllPick = $datasTokped + $datasBlibli + $datasShopee + $datasJdid;
        $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();

        // $tokpedPaginate = collect($datasTokpedEnd);
        // $productsTokped = $this->paginate($tokpedPaginate, 'Tokopedia');

        // $blibliPaginate = collect($datasBlibliEnd);
        // $productsBlibli = $this->paginate($blibliPaginate, 'Blibli');

        // $shopeePaginate = collect($datasShopeeEnd);
        // $productsShopee = $this->paginate($shopeePaginate, 'Shopee');

        // $JdidPaginate = collect($datasJdidEnd);
        // $productsJdid = $this->paginate($JdidPaginate, 'Jdid');

        // $HarbolnasPaginate = collect($datasHarbolnasEnd);
        // $productsHarbolnas = $this->paginate($HarbolnasPaginate, 'Harbolnas');

        // dd($datasTokpedEnd[0]);
        return view('station.pages.done', compact('datasHarbolnasEnd', 'countJdidEnd', 'datasJdidEnd', 'datasTokpedEnd', 'datasShopeeEnd', 'datasBlibliEnd', 'countTokpedEnd', 'countBlibliEnd', 'countShopeeEnd', 'countAllDrop', 'countAllEnd', 'countAllPack', 'countAllPick', 'id', 'allStore'));
    }



    public function pick()
    {
        // drop
        $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        // dd($datasTokped);
        $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasJdidDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
        $datasHarbolnasDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'harbolnas')->count();
        $datasWarisanDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Warisan')->count();

        $countAllDrop = $datasTokpedDrop + $datasBlibliDrop + $datasShopeeDrop + $datasJdidDrop + $datasHarbolnasDrop;
        // dd($countAllDrop,$datasTokpedDrop, $datasBlibliDrop, $datasShopeeDrop, $datasJdidDrop, $datasHarbolnasDrop);
        $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();
        // pick
        $datasTokped = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.id', 'bookingan.status_marketplace', 'bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        foreach($datasTokped as $index=>$datum){
            $orderMessage = DB::table('order_message')->where('orderId', $datum->id)->orderBy('created_at', 'desc')->first();
            if($orderMessage){
                if($datum->status_marketplace == 450){
                    $status = "menunggu kurir \n";
                }else{
                    $status = "pengiriman \n";
                }
                $datasTokped[$index]->status = new \stdClass;
                $datasTokped[$index]->status = "status : $status";
                
                $datasTokped[$index]->message = new \stdClass;
                $datasTokped[$index]->message = "message : $orderMessage->message";
            }else{
                $datasTokped[$index]->status = new \stdClass;
                $datasTokped[$index]->status = "status : $status";
                
                $datasTokped[$index]->message = new \stdClass;
                $datasTokped[$index]->message = " ";
            }
        }
        
        $datasBlibli = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.id','bookingan.kode','bookingan.status_marketplace', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        foreach($datasBlibli as $index=>$datum){
            $orderMessage = DB::table('order_message')->where('orderId', $datum->id)->orderBy('created_at', 'desc')->first();
            if($orderMessage){
                if($datum->status_marketplace == 'PU'){
                    $status = "menunggu kurir \n";
                }else{
                    $status = "pengiriman \n";
                }
                $datasBlibli[$index]->status = new \stdClass;
                $datasBlibli[$index]->status = "status : $status";
                
                $datasBlibli[$index]->message = new \stdClass;
                $datasBlibli[$index]->message = "message : $orderMessage->message";
            }else{
                $datasBlibli[$index]->status = new \stdClass;
                $datasBlibli[$index]->status = "status : $status";
                
                $datasBlibli[$index]->message = new \stdClass;
                $datasBlibli[$index]->message = " ";
            }
        }
        
        $datasShopee = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.id','bookingan.kode','bookingan.status_marketplace', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        foreach($datasShopee as $index=>$datum){
            $orderMessage = DB::table('order_message')->where('orderId', $datum->id)->orderBy('created_at', 'desc')->first();
            if($orderMessage){
                if($datum->status_marketplace == 'PROCESSED'){
                    $status = "menunggu kurir \n";
                }else{
                    $status = "pengiriman \n";
                }
                $datasShopee[$index]->status = new \stdClass;
                $datasShopee[$index]->status = "status : $status";
                
                $datasShopee[$index]->message = new \stdClass;
                $datasShopee[$index]->message = "message : $orderMessage->message";
            }else{
                $datasShopee[$index]->status = new \stdClass;
                $datasShopee[$index]->status = "status : $status";
                
                $datasShopee[$index]->message = new \stdClass;
                $datasShopee[$index]->message = " ";
            }
        }
        $datasJdid = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'JDID')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->select('bookingan.kode', 'bookingan.nama_pemesan', 'bookingan.kurir', 'bookingan.created_at', 'Registered_store.store_name')->get();
        $countTokped = count($datasTokped);
        $countBlibli = count($datasBlibli);
        $countShopee = count($datasShopee);
        $countJdid = count($datasJdid);
        // dd($datasJdid);
        $countAllPick = $countTokped + $countBlibli + $countShopee + $countJdid;
        $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();
        $id = 5;
        return view('station.pages.pick', compact('countJdid', 'datasJdid', 'datasTokped', 'datasBlibli', 'datasShopee', 'countTokped', 'countBlibli', 'countShopee', 'countAllPick', 'countAllDrop', 'countAllPack', 'id', 'allStore'));
    }

    public function station()
    {
        return view('station.pages.station');
    }

    public function balasPage($id)
    {
        $getChat = DB::table('message_list')->where('id', $id)->first();
        $platform = $getChat->platform;
        if ($getChat->platform == 'tokopedia' || $getChat->platform == 'Tokopedia') {
            $getAllChat = DB::table('message_list')->where('platform', 'Tokopedia')->where('msg_id', $getChat->msg_id)->get()->toArray();

            $replied = array();
            foreach ($getAllChat as $key => $chat) {
                $getAllChat[$key]->role = 'customer';
                $getAllChat[$key]->time = date('h:i a', strtotime($chat->created_at));
                $getAllChat[$key]->date = date('Y-m-d', strtotime($chat->created_at));
                $AdminChat = DB::table('cs_chat_replied')->where('msg_list_id', $chat->id)->join('cms_users', 'cms_users.id', 'cs_chat_replied.name_id')->select('cms_users.name', 'cs_chat_replied.msg', 'cs_chat_replied.time')->get();
                $update = DB::table('message_list')->where('platform', 'Tokopedia')->where('id', $chat->id)->update([
                    'updated_at'=>date('Y-m-d, H:i:s'),
                    'status'=>1
                ]);
                if (count($AdminChat) > 0) {
                    foreach ($AdminChat as $chat) {
                        $getChat = new \stdClass;
                        $getChat->nama = $chat->name;
                        $getChat->msg = $chat->msg;
                        $getChat->role = 'cs';
                        $getChat->created_at = $chat->time;
                        $getChat->time = date('h:i a', strtotime($chat->time));
                        $getChat->date = date('Y-m-d', strtotime($chat->time));
                        array_push($replied, $getChat);
                    }
                }
            }
            $chatData = array_merge($getAllChat, $replied);
            $finalData = collect($chatData)->sortBy('created_at')->values();
            $lastMessage = collect($chatData)->sortByDesc('created_at')->values()->first();
            $lastMessageTime = date('Y-m-d', strtotime($lastMessage->created_at));
            // dd($lastMessageTime, $finalData);
        } else {
            $getAllChat = DB::table('message_list')->where('platform', 'Shopee')->where('shop_id', $getChat->shop_id)->where('to_id', $getChat->to_id)->get()->toArray();
            $replied = array();
            foreach ($getAllChat as $key => $chat) {
                $getAllChat[$key]->role = 'customer';
                $getAllChat[$key]->time = date('h:i a', strtotime($chat->created_at));
                $getAllChat[$key]->date = date('Y-m-d', strtotime($chat->created_at));
                $update = DB::table('message_list')->where('platform', 'Shopee')->where('id', $chat->id)->update([
                    'updated_at'=>date('Y-m-d, H:i:s'),
                    'status'=>1
                ]);
                $AdminChat = DB::table('cs_chat_replied')->where('msg_list_id', $chat->id)->join('cms_users', 'cms_users.id', 'cs_chat_replied.name_id')->select('cms_users.name', 'cs_chat_replied.msg', 'cs_chat_replied.time')->get();
                if (count($AdminChat) > 0) {
                    foreach ($AdminChat as $chat) {
                        $getChat = new \stdClass;
                        $getChat->nama = $chat->name;
                        $getChat->msg = $chat->msg;
                        $getChat->role = 'cs';
                        $getChat->created_at = $chat->time;
                        $getChat->time = date('h:i a', strtotime($chat->time));
                        $getChat->date = date('Y-m-d', strtotime($chat->time));
                        array_push($replied, $getChat);
                    }
                }
            }
            // dd($getAllChat, $getChat->to_id);
            $chatData = array_merge($getAllChat, $replied);
            $finalData = collect($chatData)->sortBy('created_at')->values();
            $lastMessage = collect($chatData)->sortByDesc('created_at')->values()->first();
            $lastMessageTime = date('Y-m-d', strtotime($lastMessage->created_at));
        }
        // dd($finalData, $chatData, $getAllChat, $replied);
        return view('admin.balasTes', compact('finalData', 'id', 'platform', 'lastMessageTime'));
    }

    public function editProductPage($id){
        $product = DB::table('produk_marketplace')->where('produk_marketplace.id', $id)->first();
        // $induk = DB::table('products')->where('id', $product->induk_barang_id)->first();
        $convert = DB::table('convertion')->where('induk_produk_id', $product->induk_barang_id)->get();
        $name = '';
        if($product->is_variant == 1){
            $getName = DB::table('produk_marketplace')->where('produk_marketplace.id', $product->parent_produk_id)->join('produk', 'produk_marketplace.parent_produk_id', 'produk.id')->first();
            $name = $getName->product_name;
        }else{
            $getName = DB::table('produk')->where('id', $product->parent_produk_id)->first();
            $name = $getName->product_name;
        }
        // dd($name, $product);
        if($product->platform == 'Shopee'){
            $gudang = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
            $toko = $gudang->toko;
        }else{
            $gudang = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
            $toko = $gudang->data_toko;
        }
        $indukProduks = DB::table('products')->get();

        return view('admin.editProducts', compact('product', 'indukProduks', 'toko', 'name', 'convert'));
    }

    public function detailDone(Request $request){
        $getItem = DB::table('order_from_marketplace_detail')->where('id', $request->id)->get();
        foreach($getItem as $index=>$item){
            if($item->model_id){
                $photo = DB::table('produk_marketplace')->where('model_id', $item->model_id)->first();
                $getItem[$index]->photo = $photo->photo;
            }else{
                $photo = DB::table('produk_marketplace')->where('produk_id', $item->product_id)->first();
                $getItem[$index]->photo = $photo->photo;
            }
        }

        return response()->json($getItem);
    }


    public function paginate($items, $platform, $perPage = 10, $page = null, $options = [])
    {
        // $page = $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // $items = $items instanceof Collection ? $items : Collection::make($items);
        // return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        // set current page
        if($platform == 'Tokopedia'){
            $currentPageTokped = LengthAwarePaginator::resolveCurrentPage();
        }elseif($platform == 'Shopee'){
            $currentPageShopee = LengthAwarePaginator::resolveCurrentPage();
        }elseif($platform == 'Blibli'){
            $currentPageBlibli = LengthAwarePaginator::resolveCurrentPage();
        }elseif($platform == 'JDID'){
            $currentPageJdid = LengthAwarePaginator::resolveCurrentPage();
        }elseif($platform == 'Warisan'){
            $currentPageWarisan = LengthAwarePaginator::resolveCurrentPage();
        }elseif($paltform == 'Sinde Home Delivery'){
            $currentPageSHD = LengthAwarePaginator::resolveCurrentPage();
        }elseif($platform == 'Harbolnas'){
            $currentPageHarbolnas = LengthAwarePaginator::resolveCurrentPage();
        }else{
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
        }
        // set limit
        // $perPage = 20;
        // generate pagination
        $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
        return $results;
    }
}
