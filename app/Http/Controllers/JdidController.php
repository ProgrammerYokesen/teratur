<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;
use App\Http\Controllers\JDID\JdClient;
use App\Http\Controllers\JDID\SellerOrderGetOrderIdListByConditionRequest;
use App\Http\Controllers\JDID\SellerOrderGetOrderInfoByOrderIdRequest;
use App\Http\Controllers\JDID\SellerOrderPrintOrderRequest;
use App\Http\Controllers\JDID\SellerProductGetWareInfoListByVendorIdRequest;
use App\Http\Controllers\JDID\SellerProductGetSkuInfoBySpuIdAndVenderIdRequest;
use App\Http\Controllers\JDID\EpistockQueryEpiMerchantWareStockRequest;

class JdidController extends Controller
{
    public function getAuth($id){
        $appKey = env('APP_KEY');
        $redirectUrl = "https://teratur.gudangin.id/jdid-redirect";
        $url = "https://oauth.jd.id/oauth2/to_login?app_key=$appKey&response_type=code&redirect_uri=$redirectUrl&state=20200428&scope=snsapi_base";
        return \Redirect::to($url);
    }

    public function jdidRedirect(Request $request){
        // dd( $request->url(), $request->code, $request->state );
        $code = $request->code;
        $appSecret = env('APP_SECRET');
        $appKey = env('APP_KEY');
        $client = new Client();
        $resToken = $client->get("https://oauth.jd.id/oauth2/access_token?app_key=$appKey&app_secret=$appSecret&grant_type=authorization_code&code=$code")->getBody()->getContents();
        $dcodeRes = json_decode($resToken);

        $insert = DB::table('Registered_store')->where('id', 53)->update([
            'access_token'=>$dcodeRes->access_token,
            'access_token_created'=>date('Y-m-d H:i:s'),
            'expired_in_second'=>$dcodeRes->expires_in,
            'refresh_token'=>$dcodeRes->refresh_token
        ]);
        // dd($dcodeRes);
        return \Redirect::to('https://teratur.gudangin.id/admin');
    }

    public function jdidOrder(){
        $shops = DB::table('Registered_store')->where('Platform','JDID')->where('status', 'active')->get();
        // dd($shops);
        foreach($shops as $shop){
           $page = 1;
           $more = true;
           while($more == true){
            //   echo 'masuk<br/>';
            $count = $this->getOrderDetail($shop, $page);
            echo $count.'<br/>';
            if($count == 5){
                echo 'true<br/>';
                $more =  true;
            }else{
                echo 'false<br/>';
                $more= false;
            }
            $page = $page + 1;
           }
           echo 'finish';
            // dd($resp->jingdong_seller_order_getOrderIdListByCondition_response->result->model);
        }
    }

    public function getOrderDetail($shop, $page){
        echo $page.'<br/>';
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";

        $req = new SellerOrderGetOrderIdListByConditionRequest();
        $req->setOrderStatus(1);
        // $req->setOrderStatus(6);
        $req->setPageNo($page);
        $req->setPageSize(20);
        $respList = $c->execute($req, $c->accessToken);
        // dd($respList);
        // $c = new JdClient();
        // $c->appKey = env('APP_KEY');
        // $c->appSecret = env('APP_SECRET');
        // $c->accessToken = $shop->access_token;
        // $c->serverUrl = "https://open-api.jd.id/routerjson";
        // $req = new SellerOrderGetOrderInfoByOrderIdRequest();
        // $req->setOrderId($data);
        // $req->setOrderId(1069562476);

        // $resp = $c->execute($req, $c->accessToken);
        // dd($respList->jingdong_seller_order_getOrderIdListByCondition_response->result->model);
        if(!empty($respList->jingdong_seller_order_getOrderIdListByCondition_response->result->model)){
            foreach($respList->jingdong_seller_order_getOrderIdListByCondition_response->result->model as $index=>$data){
                echo $index.'<br/>';

                $check = DB::table('bookingan')->where('kode', $data)->count();
                if($check < 1){
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new SellerOrderGetOrderInfoByOrderIdRequest();
                    $req->setOrderId($data);

                    $resp = $c->execute($req, $c->accessToken);
                    // dd($resp);
                    // print label
                    $req = new SellerOrderPrintOrderRequest();
                    $req->setPrintType( 1 );
                    $req->setPrintType( 2 );
                    $req->setPrintNum( 1 );
                    // $req->setOrderId($data);
                    $req->setOrderId($data);
                    $respLabel = $c->execute($req, $c->accessToken);
                    $label = base64_decode($respLabel->jingdong_seller_order_printOrder_response->result->model->content);
                    $uniqueFileName = "JDID-".$data.'.pdf';
                    \Storage::put('public/label/'.$uniqueFileName, $label);
                    $filePath = 'storage/label/'.$uniqueFileName;
                    // dd($label);
                    $getStation = DB::table('station')->where('first_queue', 1)->first();
                    // dd($getStation);
                    $nextStation = $getStation->id + 1;
                    if($nextStation > 2){
                        $nextStation = 1;

                    }
                    $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                        'first_queue'=>0,
                    ]);
                    $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                        'first_queue'=>1,
                        'status'=>'empty',
                    ]);


                    $orderData = $resp->jingdong_seller_order_getOrderInfoByOrderId_response->result->model;
                    $insert = DB::table('bookingan')->insertGetId([
                    'nama_pemesan'=>$orderData->customerName,
                    'phone'=>$orderData->phone,
                    'jmlh_item'=>$orderData->orderSkuNum,
                    'totalBayar'=>$orderData->paySubtotal,
                    'kode'=>$data,
                    'platform'=>'JDID',
                    'kurir'=>$orderData->carrierCompany,
                    'toko'=>$shop->data_toko,
                    'alamat'=>$orderData->address,
                    'status'=>'packing',
                    'station'=>$getStation->id,
                    'nmToko'=>$shop->shop_id,
                    'kota'=>strtoupper($orderData->city),
                    'awbNo'=>$orderData->expressNo,
                    'buktiBayar'=>$filePath,
                    'logistic_type'=>'pickUp'
                    ]);

                     $logistic_type= 'pickUp';
                    $pdf = new \PDF();
                    $customPaper = array(0,0,270.28,270.64);
                    $pdf = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaper);
                    $name = $insert.'.pdf';
                    \Storage::put('public/sticker/'.$name, $pdf->output());
                    $content = $pdf->download($name);
                    $filePath = 'storage/sticker/'.$name;
                    // dd($responseShippingParameter,$logistic_type, $filePath);
                    $updateInvoice_toko = DB::table('bookingan')->where('id', $insert)->update([
                        'invoice_toko'=>$filePath,
                    ]);

                    $sku = DB::table('produk_marketplace')->where('Platform', 'JDID')->where('produk_id', $item->skuId)->first();
                    foreach($orderData->orderSkuinfos as $item){
                        $insertBarang = DB::table('order_from_marketplace_detail')->insert([
                        'orderId'=>$insert,
                        'product_id'=>$item->skuId,
                        'item'=>$item->skuName,
                        'qty'=>$item->skuNumber,
                        'hargaSatuan'=>$item->costPrice / $item->skuNumber,
                        'hargaTotal'=>$item->costPrice,
                        'sku'=>$sku->sku,
                        'kode_item'=>$sku->kode_item,
                        ]);
                    }
                }else{
                    echo 'sudah ada <br/>';
                }

            }

        }else{
            echo 'tidak ada <br/>';
        }
        if(!empty($respList->jingdong_seller_order_getOrderIdListByCondition_response->result->model)){
            $count = count($respList->jingdong_seller_order_getOrderIdListByCondition_response->result->model);
        }else{
            $count = 0;

        }
        return $count;

    }

    public function getProductJDID(){
        $shops = DB::table('Registered_store')->where('Platform', 'JDID')->where('status', 'active')->get();

        foreach($shops as $shop){
            $page = 1;

            do{
                $spuShop = $this->getSpu($page, $shop);
                if($spuShop == 20){
                    $more = true;
                }else{
                    $more = false;
                }
                $page = $page + 1;
            }while($more == true);


        }

        echo 'finish';
    }

    public function getSpu($page, $shop){
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";
        $req = new SellerProductGetWareInfoListByVendorIdRequest();
        $req->setPage($page);
        $req->setSize( 20 );

        $respSpuShop = $c->execute($req, $c->accessToken);

        // dd($respSpuShop);
        foreach($respSpuShop->jingdong_seller_product_getWareInfoListByVendorId_response->returnType->model->spuInfoVoList as $spu){
            $sku = $this->getsku($page, $shop, $spu);

        }
        $count = count($respSpuShop->jingdong_seller_product_getWareInfoListByVendorId_response->returnType->model->spuInfoVoList);
        return $count;
    }

    public function getsku($page, $shop, $spu){
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";
        $req = new SellerProductGetSkuInfoBySpuIdAndVenderIdRequest();
        $req->setSpuId( $spu->spuId );

        $respSku = $c->execute($req, $c->accessToken);
        // dd($respSku, $shop);
        foreach($respSku->jingdong_seller_product_getSkuInfoBySpuIdAndVenderId_response->returnType->model as $sku){
            $skuId = (object)[
                    'skuId'=>$sku->skuId
                ];
            $skuEncode = json_encode($skuId);
            // dd($skuEncode);
            $skuNo = $sku->skuId;
            $c = new JdClient();
            $c->appKey = env('APP_KEY');
            $c->appSecret = env('APP_SECRET');
            $c->accessToken = $shop->access_token;
            $c->serverUrl = "https://open-api.jd.id/routerjson";
            $req = new EpistockQueryEpiMerchantWareStockRequest();
            // $req->setWareStockQueryListStr([{'sku':$sku->skuId}]);
            $req->setWareStockQueryListStr("[{'skuId':$sku->skuId}]");

            $respStock = $c->execute($req, $c->accessToken);
            // dd($respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1], $sku);
            $check = DB::table('produk')->where('product_name', $sku->skuName)->where('gudang_id', $shop->data_toko)->count();
            $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
            if($stock < 5){
              $notifSendTo = DB::table('cms_users')->where(function ($query) {
                $query->where('id_cms_privileges', '=', 1)
                      ->orWhere('id_cms_privileges', '=', 2);
                    })
                      ->pluck('id');
                $toko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
                $storeName = $toko->store_name;
                // $config['content'] = "Stock Product $item->basic->name di JDID ".$storeName." kurang dari 5";
                // // $config['to'] = $toko->link_toko;
                // $config['id_cms_users'] = $notifSendTo;
                // \CRUDBooster::sendNotification($config);
           }
            echo 'check: '.$check.'<br/>';

            $gambar = DB::table('produk_marketplace')->where('sku', $sku->sellerSkuId)->where('platform', 'Tokopedia')->first();
            if($check < 1){
                $insertProduk = DB::table('produk')->insertGetId([
                'product_name'=>$sku->skuName,
                'gudang_id'=>$shop->data_toko,
                'created_at'=>date('Y-m-d H:i:s'),
                ]);

                $checkDetail = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->count();
                echo $checkDetail.'<br/>';
                if($checkDetail < 1){
                    $insert = DB::table('produk_marketplace')->insert([
                    'parent_produk_id'=>$insertProduk,
                    'produk_id'=>$sku->skuId,
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock,
                    'spu'=>$spu->spuId
                    ]);

                }else{
                    $update = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->update([
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock,
                    'spu'=>$spu->spuId
                    ]);
                }
            }else{

                $updateProduk = DB::table('produk')->where('product_name', $sku->skuName)->update([
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);

                $checkDetail = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->count();

                echo $checkDetail.'<br/>';
                if($checkDetail < 1){

                    $getProduk = DB::table('produk')->where('product_name', $sku->skuName)->first();
                    // dd($sku->jdPrice,$sku->sellerSkuId,$gambar->photo, $sku->skuId, $getProduk->id, $shop->shop_id, $spu);
                    $insert = DB::table('produk_marketplace')->insert([
                    'parent_produk_id'=>$getProduk->id,
                    'produk_id'=>$sku->skuId,
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock,
                    'spu'=>$spu->spuId
                    ]);
                }else{
                  // dd('tes');
                   $update = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->update([
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock,
                    'spu'=>$spu->spuId
                    ]);
                }
            }
        }
        echo 'sukses <br/>';
    }

    public function jdidLabel(){
        $shop = DB::table('Registered_store')->where('Platform', 'JDID')->where('status', 'active')->first();
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $req = new SellerOrderPrintOrderRequest();
        $req->setPrintType( 1 );
        $req->setPrintType( 2 );
        $req->setPrintNum( 1 );
        // $req->setOrderId($data);
        $req->setOrderId(1069780693);
        $respLabel = $c->execute($req, $c->accessToken);
        $label = base64_decode($respLabel->jingdong_seller_order_printOrder_response->result->model->content);
        // $uniqueFileName = "JDID-".$data.'.pdf';
        // \Storage::put('public/label/'.$uniqueFileName, $label);
        // $filePath = 'storage/label/'.$uniqueFileName;
        dd($label, $respLabel);
    }

    public function cekJdid(){
        $shop = DB::table('Registered_store')->where('Platform', 'JDID')->where('status', 'active')->first();
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";
        $req = new SellerOrderGetOrderInfoByOrderIdRequest();
        $req->setOrderId(1072053343);

        $resp = $c->execute($req, $c->accessToken);
        dd($resp);
    }
    public function cekJdidOrderStatus(){
        $shop = DB::table('Registered_store')->where('Platform', 'JDID')->where('status', 'active')->first();
        $orders = DB::table('bookingan')->whereIn('status', ['accept order', 'packing', 'logistic'])->where('platform', 'JDID')->get();
        foreach($orders as $order){
            $status = $order->status;
            $orderId = $order->kode;
            $c = new JdClient();
            $c->appKey = env('APP_KEY');
            $c->appSecret = env('APP_SECRET');
            $c->accessToken = $shop->access_token;
            $c->serverUrl = "https://open-api.jd.id/routerjson";
            $req = new SellerOrderGetOrderInfoByOrderIdRequest();
            $req->setOrderId($orderId);

            $resp = $c->execute($req, $c->accessToken);
            // dd($resp);
            $orderStatus = $resp->jingdong_seller_order_getOrderInfoByOrderId_response->result->model->orderState;
            // dd($orderStatus);
            if($orderStatus == 6){
                $status = 'end order';
            }elseif($orderStatus == 7){
                $status == 'logistic';
            }

            $update = DB::table('bookingan')->where('kode', $orderId)->where('platform', 'JDID')->update([
                'status_marketplace'=>$status,
            ]);
        }

    }

}
