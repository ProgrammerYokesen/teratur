<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use CRUDBooster;
use phpseclib3\Crypt\RSA;
use phpseclib3\Crypt\PublicKeyLoader;
ini_set('memory_limit', '512M');

class tokopediaController extends Controller
{
   public function orderTokped() {

        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');

        // dd($notifSendTo);

        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        // dd($token);
        // private key
        $key = file_get_contents(\Storage::path('/uploads/key/private.txt'));
        // $key = file_get_contents(\Storage::path('/key/private.txt'));
        $privateKey = PublicKeyLoader::load($key, $password = false)->withHash('sha256')->withMGFHash('sha256');

        // tokped order api
        date_default_timezone_set("Asia/Jakarta");

        $current_time = Carbon::now()->timestamp + 86400;
        $start_time = Carbon::now()->timestamp - 86400;
        // $current_time = 1627516800 + 172800;
        // $current_time = 1627776000;
        // $start_time = 1627516800;

        // $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->get();
        // $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->where('status', 'active')->get();
        $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->where('status', 'active')->whereIn('id', array(4,5,6,7,77))->get();
        // $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->where('status', 'active')->whereIn('id', array(77))->get();
        // dd($shopsData);
        // try{
            foreach ($shopsData as $namaToko=> $value) {
                // dd($key);
                $id_number = $value->shop_id;
                $clientOrder = new Client();
                // ganti dengan yang atas
                $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=$id_number&from_date=$start_time&to_date=$current_time&page=1&per_page=50&status=220", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                // $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=$id_number&from_date=$start_time&to_date=$current_time&page=1&per_page=50", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                $orders = json_decode($resOrder);
                $ordersData = $orders->data;
                // dd($ordersData);
                if($ordersData != null) {
                    // satu order

                    foreach ($ordersData as $orderData) {
                        // decrypt code
                        echo 'masuk<br/>';
                        $secret = base64_decode($orderData->encryption->secret);
                        $secretKey = $privateKey->decrypt($secret);
                        $bcontent = base64_decode($orderData->encryption->content);
                        $bnonce = substr($bcontent, strlen($bcontent) - 12, strlen($bcontent));
                        $bcipher = substr($bcontent, 0, strlen($bcontent) - 12);

                        // default tag
                        $taglength = 16;
                        $tag = substr($bcipher, strlen($bcipher) - $taglength, strlen($bcipher));
                        $acipher = substr($bcipher, 0, strlen($bcipher) - $taglength);
                        $result = openssl_decrypt(
                            $acipher,
                            "aes-256-gcm",
                            $secretKey,
                            OPENSSL_RAW_DATA,
                            $bnonce,
                            $tag);
                        $decodeResult = json_decode($result);
                        // dd($decodeResult);
                        $nmPembeli = $decodeResult->buyer->name;
                        $phone = $decodeResult->buyer->phone;
                        $address = $decodeResult->recipient->address->address_full;
                            $shopId = $orderData->shop_id;
                            $orderId = $orderData->order_id;
                            $invoice = $orderData->invoice_ref_num;
                            // get total qty and price
                            $qty = count($orderData->products);
                            $totalqty = 0;
                            $totalPaid = 0;
                            for($i = 0; $i<$qty; $i++){
                                $totalqty = $orderData->products[$i]->quantity + $totalqty;
                                $totalPaid = $orderData->products[$i]->total_price + $totalPaid;

                            }

                        // // array_push($tokpedOrders, $orderData->shop_id);
                        $kota = $orderData->recipient->address->city;
                        $invoice = $orderData->invoice_ref_num;
                        // // get product name
                        $products = $orderData->products;

                        // ambil nama buyer
                        //cek nama buyer
                        $buyerSingleOrder = new Client();

                        $dataSingleOrder = $buyerSingleOrder->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();

                        $nameOrder = json_decode($dataSingleOrder);
                        $nameOrderHistory = $nameOrder->data->order_info->order_history;

                        // cek di DB data sudah ada atau belum
                        $checkData = DB::table('bookingan')->where('orderId', $orderData->order_id)->first();
                        // dd($checkData);
                        // if($checkData==NULL  && $orderData->order_status == 220) {
                        if(!$checkData) {
                            // cek kurir
                            $kurir = $orderData->logistics->shipping_agency;
                            if($kurir == 'GoSend' || $kurir == 'GrabExpress' || $kurir == 'Ninja Xpress' || $kurir == 'SiCepat' || $kurir = 'AnterAja'){
                                $logistic_type = 'pickUp';
                            }else{
                                $logistic_type = 'dropoff';
                            }

                            // assign station
                            $getStation = DB::table('station')->where('first_queue', 1)->first();
                            $station = $getStation->id;
                            $nextStation = $getStation->id + 1;
                            if($nextStation > 2){
                                $nextStation = 1;

                            }

                            $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                                'first_queue'=>0,
                            ]);
                            $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                                'first_queue'=>1,
                                'status'=>'empty',
                            ]);
                            
                            if($logistic_type == 'pickUp' && ($orderData->logistics->service_type == 'Instant Courier' || $orderData->logistics->service_type == 'Instant' || $orderData->logistics->service_type == 'Same Day')){
                              $station = 6;
                            }
                            
                            $kota = $orderData->recipient->address->city;
                            $toko = DB::table('Registered_store')->where('platform', 'Tokopedia')->where('shop_id', $shopId)->first();
                            $insert = DB::table('bookingan')->insertGetId([
                                'payment_method'=>$nameOrder->data->payment_info->gateway_name,
                                'toko'=>$toko->data_toko,
                                'orderId' => $orderData->order_id, 
                                'kode'=>$invoice, 
                                'platform' => 'Tokopedia', 
                                'nmToko' => $orderData->shop_id, 
                                'created_at' => date('Y-m-d H:i:s'),
                                'phone'=>$phone, 
                                'kurir'=>$kurir,
                                'logistic_service'=>$orderData->logistics->service_type,
                                'nama_pemesan'=>$nmPembeli, 
                                'alamat'=>$address, 
                                'jmlh_item'=>$totalqty, 
                                'totalBayar'=>$totalPaid, 
                                'logistic_type' => $logistic_type, 
                                'status'=>'packing',
                                'station'=>$station,
                                'kota'=>strtoupper($kota),
                                'district'=>$orderData->recipient->district,
                                'province'=>$orderData->recipient->province,
                                'postalcode'=>$orderData->recipient->postal_code,
                                'station_queue_time'=>date('Y-m-d H:i:s'),
                                'nation'=>$orderData->recipient->country]);

                            foreach ($products as $product) {
                                $getProdukPermarketplace = DB::table('produk_marketplace')->where('produk_id', $product->id)->first();
                                // $kodeBarang = explode('-', $produk->kode_item)[0];
                                DB::table('order_from_marketplace_detail')->insert([
                                    'sku'=> $product->sku,
                                    'orderId'=>$insert, 
                                    'product_id'=>$product->id, 
                                    'item'=> $product->name, 
                                    'qty'=>$product->quantity, 
                                    'hargaSatuan'=>$product->price, 
                                    'hargaTotal'=>$product->total_price, 
                                    'kode_item'=>$getProdukPermarketplace->kode_item]);
                            }

                            // accept order
                            $orderId = $orderData->order_id;
                            $accepAppId = (int)$appID;
                            $acceptOrder = $client->post("https://fs.tokopedia.net/v1/order/$orderId/fs/$accepAppId/ack", ['headers' => [
                                'Authorization'=> "Bearer $token->access_token"
                            ]])->getBody()->getContents();
                            $decodeOrderAccepted = json_decode($acceptOrder);
                            $orderAccepted = $decodeOrderAccepted->data;

                            $clientShipping = new Client();
                            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' =>[
                                'Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                            $shippingLabel = json_decode($resShippingLabel);
                            // $pdf = new \PDF();
                            // $customPaper = array(0,0,500.28,600.64);
                            // $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setPaper($customPaper);
                            // $name = 'Tokopedia-'.$orderId.'.pdf';
                            
                            // $path = "storage/label/$name";
                            
                            // create invoice
                            $clientCreateInvoice = new Client();
                            
                            $resInvoice = $clientCreateInvoice->post('https://api.gudangin.id/api/tokped-label',[
                                  'json'=>[
                                          'invoice'=>$invoice
                                  ]
                            ])->getBody()->getContents();
                            // qr
                            $order_sn = $invoice;
                            $pdfQr = new \PDF();
                            $customPaperQr = array(0,0,270.28,270.64);
                            $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                            $nameQr = $orderId.'.pdf';
                            
                            $content = $pdfQr->download($nameQr);
                            // $filePath = 'storage/sticker/'.$nameQr;
                            $updateInvoice_toko = DB::table('bookingan')->where('id', $insert)->update([
                                'invoice_toko'=>$nameQr,
                            ]);
                            \Storage::put('public/label/'.$name, $pdf->output());
                            // \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                            // $storeName = $value->store_name;
                            // $pdfBuktiBayar = base64_encode($pdf->output());
                            $pdfQr= base64_encode($pdfQr->output());
                            $clientSendImage = new Client();
                            
                            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                                  'json'=>[
                                          'nameQr'=>$nameQr,
                                          'contentQr'=>$pdfQr
                                  ]
                            ])->getBody()->getContents();
                            
                            
                            // satu produk
                            // $config['content'] = "Order Masuk di Tokopedia ".$storeName;
                            // $config['to'] = $value->link_toko;
                            // $config['id_cms_users'] = $notifSendTo;
                            // \CRUDBooster::sendNotification($config);
                            // dd('tes');

                            // dd($resShippingLabel, $pdf, $orderId, $insert, $orderAccepted, $accepAppId);

                            // $this->label($insert, $orderId, $orderAccepted, $accepAppId);

                        }
                    }
                    // return 'success data inserted';
                }else {
                    // return 'no data';
                }
            }
        // }catch (\GuzzleHttp\Exception\ClientException $e) {
        //     echo $e;
        //     dd('error');
        //     dd(json_decode($e->getResponse()->getBody()->getContents()));
        // }
        // dd($tokpedOrders);
        return 'success';
    }

    public function orderStatus(){



        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        $token = json_decode($res);

        $getAllorders = DB::table('order_from_marketplace')->where('platform', 'Tokopedia')->get();
        $allOrders = collect($getAllorders)->filter(function ($order) {return $order->order_status != '700';});
        // dd($allOrders);
        // get product by shops id
        foreach ($allOrders as $key => $value) {
            $invoice = $value->Invoice;
            // dd($value);
            $clientProduct = new Client();
            $resGetProducts = $clientProduct->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice",['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $resGetProduct = json_decode($resGetProducts);
            $status = $resGetProduct->data->order_status;
            // dd($resGetProduct);

            // dd($statusString == $value->order_status);
            if($status != $value->order_status) {
                $updateOrderStatus = DB::table('order_from_marketplace')->where('Invoice', $invoice)->update([
                    'order_status'=>$status,
                ]);
                if($status == 450){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'barang siap dikirim',
                    ]);
                }
                elseif($status == 540){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pesanan dikirim',
                    ]);
                }
                elseif($status == 600){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pengiriman selesai',
                    ]);
                }
            }
        }

    }



    public function requestPickUp($id){
        $getProduct = DB::table('bookingan')->where('bookingan.id', $id)->join('order_from_marketplace', 'bookingan.kode', 'order_from_marketplace.Invoice')->select('order_from_marketplace.orderId', 'order_from_marketplace.nmToko')->first();
        $orderId = (int)$getProduct->orderId;
        // dd($orderId);
        $nmToko = (int)$getProduct->nmToko;
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        try{
            $requestPickUp = $client->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/pick-up", ['headers' => [
            'Authorization'=> "Bearer $token->access_token",
            'Content-Type' => "application/json"
            ],
            'json'=>
                [
                    "order_id"=>$orderId,
                    "shop_id"=>$nmToko
                ]

            ])->getBody()->getContents();

            $decodeRequestPickUp = json_decode($requestPickUp);
        }catch (RequestException $e){
            // dd(Psr7\str($e->getRequest()));
            $error = (json_decode($e->getResponse()->getBody()->getContents()));
                dd($error);
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
        }

        // dd($decodeRequestPickUp);
        $pickUpRequest = $decodeRequestPickUp->data;
        if($pickUpRequest != null){
            DB::table('bookingan')->where('id', $id)->update([
                'pick_up'=>null
            ]);

        }
        return redirect()->back();
    }

    public function cetakLabel($kode){
        $kode = decrypt($kode);
        $getOrderId = DB::table('order_from_marketplace')->where('Invoice', $kode)->select('orderId')->first();
        $orderId = $getOrderId->orderId;
         $appID = env('TOKOPEDIA_APP_ID');
        try{
            $client = new Client();
            $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        // dd($token);
        $clientShipping = new Client();
        $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        // dd($resShippingLabel);
        $shippingLabel = json_decode($resShippingLabel);
        $pdf = new \PDF();
        $customPaper = array(0,0,500.28,600.64);
        $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setPaper($customPaper);
        $name = 'Tokopedia-'.$orderId.'.pdf';
        \Storage::put('public/label/'.$name, $pdf->output());
        $path = "storage/label/$name";
        return view('admin.labelPdf', compact('resShippingLabel'));
    }

    public function getTheProduct($shopId, $page, $gudang){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        echo $shopId.'<br/>';
        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $clientProduct = new Client();
        $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?shop_id=$shopId&page=$page&per_page=40&sort=7", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        // $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?product_id=1988194709", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $response = json_decode($resProduct);
        $items = $response->data;
        // dd($items, $response);
        if(!empty($items)){
            $countRow = count($items);
        }else{
            $countRow = 0;

        }
        if($countRow > 0){
            foreach($items as $item){
              $has_variant = $item->variant->isVariant == true ? 1 : 0;
              $check = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->count();
              $dataProduk = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->first();
                echo 'check: '.$check.' '.$item->basic->productID.' '.$item->basic->name.'<br/>';
                if($item->other->sku){
                    echo 'SKU<br/>';
                    $sku = $item->other->sku;
                }else{
                    echo 'no SKU<br/>';
                    $sku = null;
                }
              if($item->mustInsurance == false){
                  $mustInsurance = 0;
              }else {
                  $mustInsurance = 1;
              }
              if($item->main_stock < 5){
                  $notifSendTo = DB::table('cms_users')->where(function ($query) {
                    $query->where('id_cms_privileges', '=', 1)
                          ->orWhere('id_cms_privileges', '=', 2);
                        })
                          ->pluck('id');
                    $toko = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                    $storeName = $toko->store_name;
                    // dd($item, $storeName);
                    // $config['content'] = "Stock Product ".$item->basic->name." di Tokopedia ".$storeName." kurang dari 5";
                    // $config['to'] = $toko->link_toko;
                    // $config['id_cms_users'] = $notifSendTo;
                    // \CRUDBooster::sendNotification($config);
              }
              echo $item->basic->status.' <br/>';
              if($check < 1){
                // dd($item->basic->status);
                  $id_parent = '';
                    if($item->basic->status==1){
                        // echo 'insert'.$item->basic->name.' '.$id_parent.'<br/>';
                        // insert product
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$item->basic->name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s'),

                        ]);
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insertGetId([
                            'parent_produk_id'=>$insertProduk,
                            'produk_id'=>(string)$item->basic->productID,
                            'stock'=>$item->main_stock,
                            'status'=>'active',
                            'price'=>$item->price->value,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'platform'=>'Tokopedia',
                            'shop_id'=>$shopId,
                            'sku'=>$sku,
                            'page'=>$page,
                            'photo'=>$item->pictures[0]->OriginalURL,
                            'has_variant'=>$has_variant,
                            'link_product'=>$item->other->url,
                            'weight'=>$item->weight->value,
                            'is_must_insurance'=>$mustInsurance,
                            ]);
                        $id_parent = $insertProdukMarketplace;
                        echo 'insert'.$item->basic->name.' '.$id_parent.'<br/>';
                        // dd('satu', $id_parent, $insertProdukMarketplace);
                    }
                    elseif($item->basic->status==2){
                        // insert product
                        // echo 'insert2'.$item->basic->name.'<br/>';
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$item->basic->name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s'),

                        ]);
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insertGetId([
                            'parent_produk_id'=>$insertProduk,
                            'produk_id'=>(string)$item->basic->productID,
                            'stock'=>$item->main_stock,
                            'status'=>'Best (Featured Product)',
                            'price'=>$item->price->value,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'platform'=>'Tokopedia',
                            'sku'=>$sku,
                            'shop_id'=>$shopId,
                            'page'=>$page,
                            'photo'=>$item->pictures[0]->OriginalURL,
                            'has_variant'=>$has_variant,
                            'link_product'=>$item->other->url,
                            'weight'=>$item->weight->value,
                            'is_must_insurance'=>$mustInsurance,
                            ]);
                        $id_parent = $id_parent;
                        echo 'insert2'.$item->basic->name.' '.$id_parent.'<br/>';
                        // dd('dua', $id_parent, $insertProdukMarketplace);
                    }else{
                        echo 'else<br/>';
                    }

                    if($has_variant = 1 && $item->basic->status != 3){
                      echo $id_parent.' has variant<br/>';
                      // dd('tiga', $id_parent);
                        $product_id = $item->basic->productID;
                        $clientVariant = new Client();
                        $resProductVariant = $clientVariant->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/variant/$product_id?", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                        $responses = json_decode($resProductVariant);
                        $variants = $responses->data->children;
                        foreach($variants as $variant){
                            // foreach($responses->data->variant as $variantType){
                            //     foreach($variantType->option as $varianName){
                            //         if($varianName->id == )
                            //     }
                            // }
                            $check = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->count();
                            if($check < 1){
                                $insertVariant = DB::table('produk_marketplace')->insert([
                                    'parent_produk_id'=>$id_parent,
                                    'produk_id'=>$variant->product_id,
                                    'photo'=>$variant->picture->original,
                                    'sku'=>$variant->sku,
                                    'stock'=>$variant->stock,
                                    'status'=>'active',
                                    'price'=>$variant->price,
                                    'platform'=>'Tokopedia',
                                    'created_at'=>date('Y-md H:i:s'),
                                    'shop_id'=>$shopId,
                                    'is_variant'=>1,
                                    'variant_name'=>$variant->name
                                ]);
                            }else{
                                $updateVariant = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->insert([
                                    'parent_produk_id'=>$id_parent,
                                    'produk_id'=>$variant->product_id,
                                    'photo'=>$variant->picture->original,
                                    'sku'=>$variant->sku,
                                    'stock'=>$variant->stock,
                                    'status'=>'active',
                                    'price'=>$variant->price,
                                    'platform'=>'Tokopedia',
                                    'updated_at'=>date('Y-md H:i:s'),
                                    'shop_id'=>$shopId,
                                    'is_variant'=>1,
                                    'variant_name'=>$variant->name
                                ]);
                            }
                        }
                    }

                }else{
                    // dd('masuk else');
                    // echo 'update'.$item->basic->name.'<br/>';
                    $update = DB::table('produk')->where('id', $dataProduk->id)->update([
                        'product_name'=>$item->basic->name,
                        'gudang_id'=>$gudang,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        ]);
                    // Check for updates
                    $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->count();
                    // kalau sku sudah jalan semua

                    $getParentId = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->first();
                    $getVariantParent = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->first();
                    echo $getVariantParent->id.' '.$checkProdukMarketplace.'else <br/>';
                    if($checkProdukMarketplace > 0){
                        if($item->basic->status==1){
                            // echo 'data double'.$item->basic->name.'<br/>';
                            // kalau stock terakhir
                            // bukan insert tp update

                            // $productId =
                            echo $dataProduk->id.' <br/>';
                            $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'active',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL,
                                    'has_variant'=>$has_variant,
                                    'link_product'=>$item->other->url,
                                    'weight'=>$item->weight->value,
                                    'is_must_insurance'=>$mustInsurance,
                                    ]);

                        }elseif($item->basic->status==2){
                            // echo 'data double2'.$item->basic->name.'<br/>';
                           $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'Best (Featured Product)',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL,
                                    'has_variant'=>$has_variant,
                                    'link_product'=>$item->other->url,
                                    'weight'=>$item->weight->value,
                                    'is_must_insurance'=>$mustInsurance,
                                    ]);
                        }elseif($item->basic->status==3){
                            // echo 'data double3'.$item->basic->name.'<br/>';
                           $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'Inactive',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL,
                                    'has_variant'=>$has_variant,
                                    'link_product'=>$item->other->url,
                                    'weight'=>$item->weight->value,
                                    'is_must_insurance'=>$mustInsurance,
                                    ]);
                        }else{
                            // echo 'data double else'.$item->basic->name.'<br/>';
                            $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'inactive',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL,
                                    'has_variant'=>$has_variant,
                                    'link_product'=>$item->other->url,
                                    'weight'=>$item->weight->value,
                                    ]);
                        }

                        if($has_variant == 1){
                          echo $getVariantParent->id.' <br/>';
                            $product_id = $item->basic->productID;
                            $clientVariant = new Client();
                            $resProductVariant = $clientVariant->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/variant/$product_id?", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                            $responses = json_decode($resProductVariant);
                            $variants = $responses->data->children;
                            foreach($variants as $variant){
                                $check = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->count();
                                echo $check.' <br/>';
                                if($check < 1){
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$getVariantParent->id,
                                        'produk_id'=>$variant->product_id,
                                        'photo'=>$variant->picture->original,
                                        'sku'=>$variant->sku,
                                        'stock'=>$variant->stock,
                                        'status'=>'active',
                                        'price'=>$variant->price,
                                        'platform'=>'Tokopedia',
                                        'created_at'=>date('Y-md H:i:s'),
                                        'shop_id'=>$shopId,
                                        'is_variant'=>1,
                                        'variant_name'=>$variant->name
                                    ]);
                                }else{
                                    $updateVariant = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->update([
                                        'parent_produk_id'=>$getVariantParent->id,
                                        'produk_id'=>$variant->product_id,
                                        'photo'=>$variant->picture->original,
                                        'sku'=>$variant->sku,
                                        'stock'=>$variant->stock,
                                        'status'=>'active',
                                        'price'=>$variant->price,
                                        'platform'=>'Tokopedia',
                                        'updated_at'=>date('Y-md H:i:s'),
                                        'shop_id'=>$shopId,
                                        'is_variant'=>1,
                                        'variant_name'=>$variant->name
                                    ]);
                                }
                            }
                        }
                    }else{
                        echo $dataProduk->id.'produk_marketplace<br/>';
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'active',
                                    'price'=>$item->price->value,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'sku'=>$sku,
                                    'shop_id'=>$shopId,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL,
                                    'has_variant'=>$has_variant,
                                    'link_product'=>$item->other->url,
                                    'weight'=>$item->weight->value,
                                    'is_must_insurance'=>$mustInsurance,
                                    ]);

                        if($has_variant == 1){
                            $product_id = $item->basic->productID;
                            $clientVariant = new Client();
                            $resProductVariant = $clientVariant->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/variant/$product_id?", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                            $responses = json_decode($resProductVariant);
                            $variants = $responses->data->children;
                            foreach($variants as $variant){
                                $check = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->count();
                                if($check < 1){
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$getVariantParent->id,
                                        'produk_id'=>$variant->product_id,
                                        'photo'=>$variant->picture->original,
                                        'sku'=>$variant->sku,
                                        'stock'=>$variant->stock,
                                        'status'=>'active',
                                        'price'=>$variant->price,
                                        'platform'=>'Tokopedia',
                                        'created_at'=>date('Y-md H:i:s'),
                                        'shop_id'=>$shopId,
                                        'is_variant'=>1,
                                        'variant_name'=>$variant->name
                                    ]);
                                }else{
                                    $updateVariant = DB::table('produk_marketplace')->where('produk_id', $variant->product_id)->insert([
                                        'parent_produk_id'=>$getVariantParent->id,
                                        'produk_id'=>$variant->product_id,
                                        'photo'=>$variant->picture->original,
                                        'sku'=>$variant->sku,
                                        'stock'=>$variant->stock,
                                        'status'=>'active',
                                        'platform'=>'Tokopedia',
                                        'updated_at'=>date('Y-md H:i:s'),
                                        'shop_id'=>$shopId,
                                        'is_variant'=>1,
                                        'variant_name'=>$variant->name
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $countRow;
    }
    public function getProducts(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        // get shop
        // $getShop = DB::table('Registered_store')->where('platform', 'tokopedia')->where('status', 'active')->whereIn('shop_id',array(9591417,9583529))->get();
        // $getShop = DB::table('Registered_store')->where('platform', 'tokopedia')->where('status', 'active')->where('shop_id',9040202)->get();
        $getShop = DB::table('Registered_store')->where('platform', 'Tokopedia')->where('status', 'active')->get();
        // dd($getShop);
        foreach($getShop as $shop){
            $shopId = $shop->shop_id;
            $gudang = $shop->data_toko;
            $page = 1;
            // $parse = $this->getTheProduct($shopId, $page, $gudang);
                // $page = 0;
                // echo 'shop: '.$shopId.'<br/>';
            do{
                // echo 'page:'.$page.'<br/>';
                $parse = $this->getTheProduct($shopId, $page, $gudang);
                $page = $page + 1;
                // echo 'parse: '.$parse.'<br/>';
            }while($parse>24);
            // echo 'change<br/>';
        }
        echo 'sukses';
    }


    public function chatWebhooks(Request $request){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $date = date('Y-m-d').' 00:00:00';
        $todayTimestamp = strtotime($date);
        $msg_id = $request->msg_id;
        $shop_id = $request->shop_id;
        $nama_toko = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
        // $reschat = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages?shop_id=$shop_id&order=desc&page=1&per_page=15", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $reschat = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages/$msg_id/replies?shop_id=$shop_id&page=1&per_page=15", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $dataChat = json_decode($reschat);
        $chats = collect($dataChat->data)->sortByDesc('reply_time')->values();
        foreach($chats as $chat){
            $chatTime = $chat->reply_time / 1000;
            if($chatTime >= $todayTimestamp && $chat->role == 'User'){
                $check = db::table('message_list')->where('platform', 'Tokopedia')->where('msg_id', $chat->msg_id)->where('shop_id', $shop_id)->where('reply_id', $chat->reply_id)->count();
                if($check < 1){
                    if($chat->msg == 'Uploaded Image' && $chat->attachment->attributes->image_url){
                        $insert = DB::table('message_list')->insert([
                            'shop_id'=>$shop_id,
                            'nama_toko'=>$nama_toko->store_name,
                            'platform'=>'Tokopedia',
                            'sender_name'=>$request->full_name,
                            'msg_id'=>$request->msg_id,
                            'msg'=>$chat->attachment->attributes->image_url,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'buyer_id'=>$request->buyer_id,
                            'reply_id'=>$chat->reply_id
                        ]);
                    }else{
                        $insert = DB::table('message_list')->insert([
                            'shop_id'=>$shop_id,
                            'nama_toko'=>$nama_toko->store_name,
                            'platform'=>'Tokopedia',
                            'sender_name'=>$request->full_name,
                            'msg_id'=>$request->msg_id,
                            'msg'=>$chat->msg,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'buyer_id'=>$request->buyer_id,
                            'reply_id'=>$chat->reply_id
                        ]);
                    }

                }
            }
        }

        // dd($nama_toko);
        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');

        // $config['content'] = $request->full_name." ".$nama_toko->store_name." ".$request->msg_id. " ".$request->message. " ".$request->shop_id. " ". $nama_toko->store_name;
        // $config['content'] = "Chat dari ".$request->full_name."di Tokopedia dari toko ".$nama_toko->store_name." chat id= ".$request->msg_id;
        // $config['to'] = "https://teratur.warisangajahmada.com/admin/balas-page/$insert/Tokopedia";
        // $config['id_cms_users'] = $notifSendTo;
        // \CRUDBooster::sendNotification($config);

    }

    public function campaign(){
        $shops = DB::table('Registered_store')->where('platform', 'Tokopedia')->where('status', 'active')->get();
        // $shops = DB::table('Registered_store')->where('platform', 'Tokopedia')->where('id', 6)->get();
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        // dd($shops);
        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        $token = $token->access_token;
        foreach($shops as $shop){
            $shop_id = (int)$shop->shop_id;
            $page = 1;
            // dd($page);
            do{
                $campign = $this->getCampaign($shop_id, $token, $page, $appID);
                $page = $page + 1;
            }while($campaign > 24);
        }
    }
    public function getCampaign($shop_id, $token, $page, $appID){
        // dd(gettype($shop_id), $token);
        try{
            $client = new Client();
            $resCampaign = $client->get("https://fs.tokopedia.net/v1/slash-price/fs/$appID/view?shop_id=$shop_id&page=5&per_page=25&status=ALL", ['headers'=>["Authorization"=>"Bearer $token"]])->getBody()->getContents();
            // $resCampaign = $client->get("https://fs.tokopedia.net/v1/slash-price/fs/$appID/view?shop_id=$shop_id&product_id=3001870611&status=ALL", ['headers'=>["Authorization"=>"Bearer $token"]])->getBody()->getContents();
            $responseCampaign = json_decode($resCampaign);
            // $count = count($responseCampaign->products);
            dd($responseCampaign);
            // return $count;
        }catch (\GuzzleHttp\Exception\ClientException $e) {
            // dd('masuk sini');
            // dd($e->getResponse());
            dd(json_decode($e->getResponse()->getBody()->getContents()));
        }

    }

    public function label($id){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        try{
            $getQr = DB::table('bookingan')->where('platform', 'Tokopedia')->where('id', $id)->first();
            $orderId = $getQr->orderId;
            $clientShipping = new Client();
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $shippingLabel = json_decode($resShippingLabel);
            $qr = $getQr->invoice_toko;
            // $data = "<embed style='position:absolute;bottom:0;margin-left:150px;'src='https://teratur.warisangajahmada.com/$qr' width='200px' height='200px' />";
            $script = "<div><canvas id='qr_renderer' style='width:200px;height:200px;position:absolute;bottom:0;margin-left:30%;'></canvas></div>
                        <script src='https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js'></script>
                        <script>
                        var myStateTokped = {
                            pdf: null,
                            currentPage: 1,
                            zoom: 1
                        }
                        let buktiStickerTokped = '$qr'
                        pdfjsLib.getDocument('https://teratur.warisangajahmada.com/' + buktiStickerTokped).then((pdf) => {
                        myStateTokped.pdf = pdf;
                            rendererTokped();
                        })
                        function rendererTokped() {
                            myStateTokped.pdf.getPage(myStateTokped.currentPage).then((page) => {
                                var canvas = document.getElementById('qr_renderer');
                                var ctx = canvas.getContext('2d');

                                var viewport = page.getViewport(myStateTokped.zoom);

                                canvas.width = viewport.width;
                                canvas.height = viewport.height;

                                page.render({
                                    canvasContext: ctx,
                                    viewport: viewport
                                });
                            });
                        }
                        </script>";
            $length = strlen($resShippingLabel) - 17;
            $invoiceQr = substr_replace($resShippingLabel, $script, $length, 0);
            // dd($invoiceQr);
            return view('admin.labelPdfNewFlow', compact('invoiceQr'));
            // dd(gettype($resShippingLabel), strlen($resShippingLabel), $newstr, $tes, $resShippingLabel);
            echo 'success<br/>';
        }catch(RequestException $e){
            dd($e);
        }

    }
    public function tokpedLabel(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        try{
            // $getInvoice = DB::table('bookingan')->where('status', 'packing')->where('station', 1)->orderBy('created_at')->first();
            $getInvoice = DB::table('bookingan')->where('kode', 'INV/20211204/MPL/1821867753')->first();
            $orderId = (int) $getInvoice->kode;
            $clientShipping = new Client();
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $shippingLabel = json_decode($resShippingLabel);
            // dd($resShippingLabel);
            $pdf = new \PDF();
            $customPaper = array(0,0,500.28,600.64);
            $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setPaper($customPaper);
            $name = 'TokopediaTes1-'.$orderId.'.pdf';
            dd($pdf->output());
            // \Storage::put('public/label/'.$name, $pdf->output());
            // $content = $pdf->download($name);
            $path = "storage/label/$name";

            return view('admin.labelPdf', compact('resShippingLabel'));
            // dd($resShippingLabel);
            // $updateLinkResi = DB::table('bookingan')->where('id', $insert)->update([
            //     'buktiBayar'=>$path
            // ]);
            $length = strlen($resShippingLabel) - 3;
            $newstr = substr($resShippingLabel, 2, $length);
            dd(gettype($resShippingLabel), strlen($resShippingLabel), $newstr);
            echo 'success<br/>';
        }catch(RequestException $e){
            dd($e);
        }

    }
    public function cek(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        
        // $datas= DB::table('statistik_discount')->where('id', '>', 2899)->get();
        // $datas= DB::table('bookingan')->where('id', 20100)->get();
        // dd($datas);
        // foreach($datas as $data){
            // $nameExplode = explode("storage/label/", $data->link_invoice);
            // $name = $nameExplode[0].$nameExplode[1];
            // // dd($nameExplode, $name);
            // $update = DB::table('statistik_discount')->where('id', $data->id)->update([
            //     'link_invoice'=>$name
            // ]);
            
            // $bookingan = DB::table('bookingan')->where('kode', $data->no_invoice)->first();
            $invoice = 'INV/20220216/MPL/2051013428';
            $buyerSingleOrder = new Client();
            $dataSingleOrder = $buyerSingleOrder->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $orders = json_decode($dataSingleOrder);

            dd($orders, $dataSingleOrder);
            $clientShipping = new Client();
            $orderId = $orders->data->order_id;
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            // dd($resShippingLabel);
            // $shippingLabel = json_decode($resShippingLabel);
            // dd( $orderId);
            
            // \Storage::put('public/label/'.$name, $pdf->output());
            $pdf = new \PDF();
            $customPaper = array(0,0,500.28,600.64);
            $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaper);
            $name = 'https://static-storage-1928.sg-sin1.upcloudobjects.com/gudangin/pdf/buktiBayar/Tokopedia-'.$orderId.'.pdf';
            // dd($pdf->output());
            \Storage::put('public/label/'.$name, $pdf->output());
            \Storage::put('public/label/'.$name, $pdf->output());
            $path = "storage/label/$name";
            
            $update = DB::table('statistik_discount')->where('id', $data->id)->update([
                'link_invoice'=>$name
            ]);
            $clientSendImage = new Client();
            $pdfBuktiBayar = base64_encode($pdf->output());
            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                  'json'=>[
                      
                          'name'=>$name,
                          'contentBuktibayar'=> $pdfBuktiBayar,
                  ]
            ])->getBody()->getContents();
            // dd($bookingan->orderId);
            // dd($orders->data->order_id);
            // dd($orders, $orders->data->encryption->secret);
            // $insert = DB::table('bookingan')->insertGetId([
            //         'nama_pemesan'=>'santy haryanti',
            //         'alamat'=>'Jl. Karanggetas, Kec. Lemahwungkuk, Kota Cirebon, Jawa Barat, 45118 [Tokopedia Note: jln karanggetas 205 , kec lemahwungkuk-panjunan, 45112]
            //         Lemahwungkuk, Kota Cirebon, Jawa Barat 45118',
            //         'phone'=>6287889380333,
            //         'jmlh_item'=>1,
            //         'totalBayar'=>243200,
            //         'kode'=>'INV/20220126/MPL/1984381419',
            //         'platform'=>'Tokopedia',
            //         'kurir'=>'SiCepat',
            //         'toko'=>243,
            //         'status'=>'packing',
            //         'created_at'=>date('Y-m-d H:i:s'),
            //         'logistic_type'=>'pickUp',
            //         'nmToko'=>11466905,
            //         'buktibayar'=>'Tokopedia-'.$orderId.'.pdf'
            //     ]);
            foreach($orders->data->order_info->order_detail as $detail){
                // dd($detail);
                $insert = DB::table('order_from_marketplace_detail')->insert([
                    // 'orderId'=>$data->id,
                    // 'orderId'=>$insert,
                    'product_id'=>$detail->product_id,
                    'item'=>$detail->product_name,
                    'qty'=>$detail->quantity,
                    'hargaSatuan'=>$detail->product_price,
                    'hargaTotal'=>$detail->subtotal_price,
                    'sku'=>$detail->sku
                ]);
            }
        // }
        // $key = file_get_contents(\Storage::path('/uploads/key/private.txt'));
        // $privateKey = PublicKeyLoader::load($key, $password = false)->withHash('sha256')->withMGFHash('sha256');
        
        // $secret = base64_decode($orders->data->encryption->secret);
        //                 $secretKey = $privateKey->decrypt($secret);
        //                 $bcontent = base64_decode($orders->data->encryption->content);
        //                 $bnonce = substr($bcontent, strlen($bcontent) - 12, strlen($bcontent));
        //                 $bcipher = substr($bcontent, 0, strlen($bcontent) - 12);

        //                 // default tag
        //                 $taglength = 16;
        //                 $tag = substr($bcipher, strlen($bcipher) - $taglength, strlen($bcipher));
        //                 $acipher = substr($bcipher, 0, strlen($bcipher) - $taglength);
        //                 $result = openssl_decrypt(
        //                     $acipher,
        //                     "aes-256-gcm",
        //                     $secretKey,
        //                     OPENSSL_RAW_DATA,
        //                     $bnonce,
        //                     $tag);
        //                 $decodeResult = json_decode($result);
        //                 dd($orders, $decodeResult);
                        
                        
    }

    public function isiLabel(){
        $getData = DB::table('bookingan')->where('kode', 'INV/20220128/MPL/1990537889')->get();
        foreach($getData as $data){
            $client = new Client();
            $clientId = env('TOKOPEDIA_CLIENT_ID');
            $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
            $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
            $clientData = base64_encode("{$clientId}:{$clientSecret}");

            $appID = env('TOKOPEDIA_APP_ID');
            try{
            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                'Authorization'=> "Basic $clientData",
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1'
                ]])->getBody()->getContents();
            }catch (RequestException $e) {
                // dd(Psr7\str($e->getRequest()));
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
            $token = json_decode($res);
            $invoice = $data->kode;
             $buyerSingleOrder = new Client();
            $dataSingleOrder = $buyerSingleOrder->get("https://fs.tokopedia.net/v2/fs/$appID/order?invoice_num=$invoice", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $orders = json_decode($dataSingleOrder);

            // $getInvoice = DB::table('bookingan')->where('status', 'packing')->where('station', 2)->orderBy('created_at', 'asc')->first();
            // dd($getInvoice);
            // $orderId = $getInvoice->orderId;
            $orderId = $orders->data->order_id;
            echo $orderId,'<br/>';
            $clientShipping = new Client();
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $shippingLabel = json_decode($resShippingLabel);
            dd($resShippingLabel);
            $pdf = new \PDF();
            $customPaper = array(0,0,800.28,800.64);
            $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setPaper($customPaper);
            $name = 'Tokopedia-'.$orderId.'.pdf';
            \Storage::put('public/label/'.$name, $pdf->output());
            // $content = $pdf->download($name);
            $path = "storage/label/$name";
            dd($pdf->output(), $resShippingLabel);
            $pdfBuktiBayar = base64_encode($pdf->output());
            $clientSendImage = new Client();
            
            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                  'json'=>[
                      
                          'name'=>$name,
                          'contentBuktibayar'=> $pdfBuktiBayar,
                  ]
            ])->getBody()->getContents();
            
            // return response()->json($orderId);
            $update = DB::table('statistik_discount')->where('no_invoice', $data->no_invoice)->update([
                'link_invoice'=>'https://teratur.warisangajahmada.com/'.$path,
            ]);
        }
    }
    public function tokpedLabel2(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        try{
            $getInvoice = DB::table('bookingan')->where('status', 'packing')->where('station', 2)->orderBy('created_at', 'asc')->first();
            // dd($getInvoice);
            $orderId = $getInvoice->orderId;
            // $orderId = 1013202378;
            $clientShipping = new Client();
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $shippingLabel = json_decode($resShippingLabel);
            $pdf = new \PDF();
            $customPaper = array(0,0,700.28,800.64);
            $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'))->setPaper($customPaper);
            $name = 'Tokopedia-'.$orderId.'.pdf';
            \Storage::put('public/label/'.$name, $pdf->output());
            // $content = $pdf->download($name);
            $path = "storage/label/$name";

            return view('admin.labelPdf', compact('resShippingLabel'));
            // dd($resShippingLabel);
            // $updateLinkResi = DB::table('bookingan')->where('id', $insert)->update([
            //     'buktiBayar'=>$path
            // ]);
            $length = strlen($resShippingLabel) - 3;
            $newstr = substr($resShippingLabel, 2, $length);
            dd(gettype($resShippingLabel), strlen($resShippingLabel), $newstr);
            echo 'success<br/>';
        }catch(RequestException $e){
            dd($e);
        }

    }

    public function discount(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        // dd($token);
        // private key
        $key = file_get_contents(\Storage::path('/uploads/key/private.txt'));
        $privateKey = PublicKeyLoader::load($key, $password = false)->withHash('sha256')->withMGFHash('sha256');

        // tokped order api
        date_default_timezone_set("UTC");

        $start_time = 1643475600;
        $current_time = 1643648400;
        // $current_time = $start_time + 172800;

        $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->where('status', 'active')->get();
        // $shopsData = DB::table('Registered_store')->where('platform', "Tokopedia")->where('status', 'active')->whereIn('id', array(4,5,6,7))->get();


         try{
            foreach ($shopsData as $namaToko=> $value) {

                $id_number = $value->shop_id;
                $clientOrder = new Client();
                // ganti dengan yang atas
                $resOrder = $clientOrder->get("https://fs.tokopedia.net/v2/order/list?fs_id=$appID&shop_id=$id_number&from_date=$start_time&to_date=$current_time&page=1&per_page=50&status=700", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                $orders = json_decode($resOrder);
                $ordersData = $orders->data;

                // dd($ordersData);
                if(!empty($ordersData)){
                    foreach($ordersData as $data){
                        if($data->promo_order_detail->summary_promo){
                            foreach($data->promo_order_detail->summary_promo as $promo){
                                if(!empty($promo->discount_details)){
                                    foreach($promo->discount_details as $discount){
                                        if($discount->type =="discount_shipping" ){
                                            // dd('ada', date('d M y', $data->create_time), $discount->amount, $data->amt->shipping_cost);
                                            $getLinkInvoice = DB::table('bookingan')->where('kode', $data->invoice_ref_num)->first();
                                            // dd($getLinkInvoice,$data->invoice_ref_num, $data);
                                            $insert = DB::table('statistik_discount')->insert([
                                            'date'=> date('d M y', $data->create_time),
                                            'no_invoice'=> $data->invoice_ref_num,
                                            'nominal_ongkir_setelah_discount'=> $data->amt->shipping_cost,
                                            'nominal_ongkir_sebelum_discount'=> $data->amt->shipping_cost + $discount->amount,
                                            'nominal_discount'=> $discount->amount,
                                            'toko'=> $data->shop_id,
                                            'platform'=> 'Tokopedia',
                                            'link_invoice'=> 'https://static-storage-1928.sg-sin1.upcloudobjects.com/gudangin/pdf/buktiBayar/'.$getLinkInvoice->buktiBayar,
                                            ]);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            echo 'sukses';
         }catch(\GuzzleHttp\Exception\ClientException $e){
             dd($e);
         }
    }

    public function createPrtoduct(Request $request){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $shop_id= $request->shopId;
        $create = new Client();
        $resCreate = $create->post("https://fs.tokopedia.net/v3/products/fs/$appID/create?shop_id=$shop_id", ['headers'=>[
            'Authorization' => "Bearer $token->access_token",
            'Content-Type' => "application/json"
            ],
            'json'=>
                [
                    "products"=>[
                         "name"=> $request->name,
                         "condition"=>$request->condition,
                         "description"=>$request->descsription,
                         "sku"=>$request->sku,
                         "price"=>$request->price,
                         "status"=>$request->status,
                         "stock"=>$request->stock,
                         "min_order"=>$request->min_order,
                        "category_id"=>562,
                    ]
                ]
            ])->getBody()->getContents();
         $requestPickUp = $client->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/pick-up", ['headers' => [
            'Authorization'=> "Bearer $token->access_token",
            'Content-Type' => "application/json"
            ],
            'json'=>
                [
                    "order_id"=>$orderId,
                    "shop_id"=>$nmToko
                ]

            ])->getBody()->getContents();
    }

    public function chat(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        $date = date('Y-m-d').' 00:00:00';
        $todayTimestamp = strtotime($date);
        $msg_id = 1669096912;
        $shop_id = 8383447;
        // $reschat = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages?shop_id=$shop_id&filter=all&order=desc&page=1&per_page=30", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $reschat = $client->get("https://fs.tokopedia.net/v1/chat/fs/$appID/messages/$msg_id/replies?shop_id=$shop_id&page=1&per_page=15", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $dataChat = json_decode($reschat);
        $chats = collect($dataChat->data)->sortByDesc('reply_time')->values();
        $nama_toko = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
        foreach($chats as $chat){
            $chatTime = $chat->reply_time / 1000;
            if($chatTime >= $todayTimestamp){
                echo $chatTime.'-'.$todayTimestamp.'<br/>';
                echo $chat->reply_id.'<br/>';
                $check = db::table('message_list')->where('platform', 'Tokopedia')->where('msg_id', $chat->msg_id)->where('shop_id', $shop_id)->where('reply_id', $chat->reply_id)->count();
                echo $check.'<br/>';
                if($check < 1){
                   echo 'masuk<br/>';
                }
            }
        }
            dd($chats, $date, $chat->reply_id);
    }

    public function cekSingleProduct(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $shopId = 3897048;
        $product_id = 2281392051;
        $clientProduct = new Client();
        $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?product_id=$product_id", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $response = json_decode($resProduct);
        dd($response);
        $insert = DB::table('produk')->insertGetid([
          'product_name'=>$response->data[0]->basic->name,
          'gudang_id'=>223,
          'created_at'=>date('Y-m-d H:i:s'),
        ]);

        $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
          'parent_produk_id'=>$insert,
          'produk_id'=>$response->data[0]->basic->productID,
          'photo'=>$response->data[0]->pictures[0]->OriginalURL,
          'sku'=>$response->data[0]->other->sku,
          'stock'=>$response->data[0]->main_stock,
          'status'=>'active',
          'price'=>$response->data[0]->value,
          'platform'=>'Tokopedia',
          'created_at'=>date('Y-m-d H:i:s'),
          'shop_id'=>$response->data[0]->basic->shopID,
          'is_variant'=>0,
          'has_variant'=>0
        ]);
        $items = $response->data;
        $is_variant = $items[0]->variant->isVariant == true ? 1 : 0;
        if($is_variant == 1){
            // $product_id = $p;
            $clientVariant = new Client();
            $resProductVariant = $clientVariant->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/variant/$product_id?", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            $response = json_decode($resProductVariant);
            dd($response);
        }
        // dd($items[0], $is_variant);
    }
    
    public function updateStock(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        $product = DB::table('produk_marketplace')->where('id',34749)->first();
        $stock = 258;
        try{
            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
            $shop_id= (int)$shop->shop_id;
            $fsId = (int)$appID;
            // $stock = $item->stock - $produk->qty;
            // if($getOrder->shop_id == 3897048){
            //     $stock = $stockProduct;

            // }
        // dd($token, $shop_id, $appID);
            $data = Array();
            $data[0]['product_id']=(int)$product->produk_id;
            $data[0]['new_stock']=$stock;
            // $shop_id = $shop['shopsId'];
            // dd($shop, $shop_id, $stock, $data, $token);
            $clientStock = new Client();
            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                'Authorization'=> "Bearer $token->access_token",
                'Content-Type'=> 'application/json'
            ],
                'json'=>$data
            ])->getBody()->getContents();
            $responseStock = json_decode($resStock);
            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                'stock'=>$stock
            ]);
        }catch (\GuzzleHttp\Exception\ClientException $ex) {
            // $responseBody = $exception->getResponse()->getBody(true);
            $response = json_decode($ex->getResponse()->getBody()->getContents());
            // dd($response, $e);
        }
    }

}
