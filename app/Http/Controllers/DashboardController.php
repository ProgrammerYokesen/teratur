<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class DashboardController extends Controller
{
    public function dashboardPage()
    {
        
        // cek perhitungannya
        $uniqueSku = DB::table('produk_marketplace')->groupBy('sku')->get();
        $data = [];
        $restock = [];
        foreach($uniqueSku as $index=>$sku){
            $name = DB::table('produk')->where('id', $sku->parent_produk_id)->first();
            $stock = DB::table('produk_marketplace')->where('sku', $sku->sku)->sum('stock');
            // dd($stock, $sku->sku);
            $item = (object)[
                'name'=>$name->product_name,
                'sku'=>$sku->sku,
                'stock'=>$stock,
            ];
            if($stock < 5 && $stock > 0){
                $iteration = count($restock);
                $restock[$iteration] = $item; 
            }else{
                $data[$index] = $item;
            }
        }
        $countRestock = count($restock);
        $countdata = count($data);
        // dd($uniqueSku, $data, $restock);
        return view('dashboard.pages.dashboard', compact('data', 'restock', 'countData', 'countRestock'));
    }

    public function historyPage()
    {
        return view('dashboard.pages.history');
    }
}
