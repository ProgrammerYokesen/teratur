<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;

class SindeHomeDeliveryController extends Controller
{
    public function sindeLogin(){
        $client = new Client();
        $clientLogin = env('SINDE_LOGIN');
        $clientPass = env('SINDE_PASS');
        $resLogin = $client->post("https://sindehomedelivery.com/api/login", [
            'json'=>
            [
                'email'=>$clientLogin,
                'password'=>$clientPass,
            ]
        ])->getBody()->getContents();

        $responseLogin = json_decode($resLogin);
        $insert = DB::table('Registered_store')->where('Platform', "Sinde Home Delivery")->update([
        'access_token'=>$responseLogin->token,
        'access_token_created'=>date('Y-m-d H:i:s')
        ]);
        // dd($responseLogin);
        return redirect()->to('https://teratur.warisangajahmada.com/admin/Registered_store');
    }

    public function getListOrder(){
        // date_default_timezone_set("Asia/Jakarta");
        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
        $access_token = $shop->access_token;
        $client = new Client();
        $startDate = strtotime(date('Y-m-d').'00:00:00');
        // $startDate = 1635720983;1632971248
        $startDate = 1644213273;
        $endDate = strtotime(date('Y-m-d H:i:s'),'+17hours');
        // $endDate = 1635834446;

        $status = 'Paid';
        $resListOrder = $client->get("https://sindehomedelivery.com/api/order/$startDate/$endDate/$status", ['headers'=>
            ['Authorization' => "Bearer $access_token"]
        ])->getBody()->getContents();
        $responseListOrder = json_decode($resListOrder);
        $orders = $responseListOrder->data;
        // dd($orders, $startDate, $endDate);
        if(count($orders) > 0 && !empty($orders[0])){
            // dd(count($orders), $orders);
            foreach($orders as $order){
        // dd($orders, $order->order_id);
                try{
                        $order_id = $order->order_id;
                        $orderDetailClient = new Client();
                        $resDetailOrder = $orderDetailClient->get("https://sindehomedelivery.com/api/order-detail/$order_id",['headers'=>
                            ['Authorization' => "Bearer $access_token"]
                        ])->getBody()->getContents();
                        $responseDetailOrder = json_decode($resDetailOrder);
                        // dd($responseDetailOrder, $orders, $order_id, $resDetailOrder);
                        // get shipping label
                        $labelClient = new Client();
                        $resLabel = $labelClient->get('https://sindehomedelivery.com/api/shipping-label/'.$order_id, ['headers'=>
                            ['Authorization' => "Bearer $access_token"]
                        ])->getBody()->getContents();
                        $responseLabel = json_decode($resLabel);
                        $label = base64_decode($responseLabel->result->model->content);
                        $Invoice = $responseDetailOrder->data[0]->invoice_number;
                        $uniqueFileName = $Invoice.'-shd.pdf';
                        // \Storage::put('public/label/'.$uniqueFileName, $label);
                        $filePath = $uniqueFileName;
                        
                        // dd($resLabel,$label,$responseLabel->result->model->content, 'tes');
                        // assign station
                            $getStation = DB::table('station')->where('first_queue', 1)->first();
                            $nextStation = $getStation->id + 1;
                            if($nextStation > 2){
                                $nextStation = 1;

                            }

                            $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                                'first_queue'=>0,
                            ]);
                            $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                                'first_queue'=>1,
                                'status'=>'empty',
                            ]);
                            $order_sn = $responseDetailOrder->data[0]->invoice_number;
                        $check = DB::table('bookingan')->where('kode', $order->invoice_ref_num)->where('platform', 'Sinde Home Delivery')->count();
                        if($check < 1){
                            $insert = DB::table('bookingan')->insertGetId([
                                'nama_pemesan'=>$order->buyer->name,
                                'phone'=>$responseDetailOrder->data[0]->order_info->destination->receiver_phone,
                                'kode'=>$responseDetailOrder->data[0]->invoice_number,
                                'platform'=>"Sinde Home Delivery",
                                'kurir'=>'sicepat',
                                'toko'=>223,
                                'alamat'=>$responseDetailOrder->data[0]->order_info->destination->address_street,
                                'buktiBayar'=>$filePath,
                                'status'=> "packing",
                                'created_at'=>date('Y-m-d H:i:s'),
                                'nmToko'=>$shop->shop_id,
                                'kota'=>$order->recipient->address->city,
                                'logistic_type'=>'dropoff',
                                'station'=>$getStation->id,
                                'station_queue_time'=>date('Y-m-d H:i:s'),
                                'orderId'=>$order->order_id
                                // bagaimana dengan logistic_type

                            ]);
                            $products = $order->products;
                            $totalQty = 0;
                            $totalPaid = 0;

                            // qr
                                $order_sn = $invoice;
                                $logistic_type = 'dropoff';
                                $pdfQr = new \PDF();
                                $customPaperQr = array(0,0,270.28,270.64);
                                $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                                $nameQr = $order->order_id.'.pdf';
                                // \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                                $content = $pdfQr->download($nameQr);
                                $path = $nameQr;

                            $updateInvoice_toko = DB::table('bookingan')->where('id', $insert)->update([
                                    'invoice_toko'=>$path,
                                ]);
                            foreach($products as $product){
                                $getProduk = DB::table('produk')->where('produk.product_name', $product->name)->join('produk_marketplace', 'produk.id', 'produk_marketplace.parent_produk_id')->first();
                                $totalQty = $totalQty + $product->quantity;
                                $totalPaid = $totalPaid + $product->total_price;
                                $check = DB::table('order_from_marketplace_detail')->where('orderId',$insert)->where('product_id', $getProduk->produk_id)->count();
                                if($check < 1){
                                    $insertItem = DB::table('order_from_marketplace_detail')->insert([
                                        'orderId'=>$insert,
                                        'product_id'=>$getProduk->produk_id,
                                        'item'=>$product->name,
                                        'qty'=>$product->quantity,
                                        'hargaSatuan'=>$product->price,
                                        'hargaTotal'=>$product->total_price,
                                        'kode_item'=>$getProduk->kode_item
                                    ]);
                                }

                            }
                            $updatePriceQty = DB::table('bookingan')->where('id', $insert)->update([
                                'jmlh_item'=>$totalQty,
                                'totalBayar'=>$totalPaid,
                            ]);
                            $pdfQr= base64_encode($pdfQr->output());
                            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                              'json'=>[
                                      'name'=>$uniqueFileName,
                                      'contentBuktibayar'=>$label,
                                      'nameQr'=>$nameQr,
                                      'contentQr'=>$pdfQr
                              ]
                        ])->getBody()->getContents();
                            echo 'sukses';
                        }



                    }catch(\GuzzleHttp\Exception\ClientException $e){
                        dd($e->getResponse()->getBody()->getContents());
                    }


            }
        }
        // dd($responseListOrder);
    }

    public function getProducts(){
        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
        $access_token = $shop->access_token;
        $clientProducts = new Client();
        $resProducts = $clientProducts->get('https://sindehomedelivery.com/api/product', ['headers'=>
                ['Authorization' => "Bearer $access_token"]
            ])->getBody()->getContents();
        $responseProducts = json_decode($resProducts)->data;
        // dd($responseProducts);
        foreach($responseProducts as $product){
            $check = DB::table('produk')->where('product_name', $product->basic->name)->count();
            echo 'produk'.$check.''.$product->basic->name.'<br/>';
            if($check < 1){
                $insertProduk = DB::table('produk')->insertGetId([
                'product_name'=>$product->basic->name,
                'gudang_id'=>$shop->data_toko,
                'created_at'=>date('Y-m-d H:i:s'),
                ]);

                $checkProduk_marketplace = DB::table('produk_marketplace')->where('parent_produk_id', $insertProduk)->count();
                if($checkProduk_marketplace < 1){
                    $insertProduk_marketplace = DB::table('produk_marketplace')->insert([
                     'parent_produk_id'=>$insertProduk,
                     'produk_id'=>$product->basic->productID,
                     'photo'=>$product->pictures->OriginalURL,
                     'stock'=>$product->main_stock,
                     'status'=>'active',
                     'price'=>$product->price->value,
                     'platform'=>'Sinde Home Delivery',
                     'created_at'=>date('Y-m-d H:i:s'),
                     'shop_id'=>$shop->shop_id,
                    //  'sku'=>$product->pictures->OriginalURL,
                    ]);
                }else{
                     $insertProduk_marketplace = DB::table('produk_marketplace')->where('parent_produk_id', $insertProduk)->update([
                     'produk_id'=>$product->basic->productID,
                     'photo'=>$product->pictures->OriginalURL,
                     'stock'=>$product->main_stock,
                     'status'=>'active',
                     'price'=>$product->price->value,
                     'platform'=>'Sinde Home Delivery',
                     'updated_at'=>date('Y-m-d H:i:s'),
                    //  'sku'=>$product->pictures->OriginalURL,
                    ]);
                }


            }else{
                $produk = DB::table('produk')->where('product_name', $product->basic->name)->first();
                $updateProduk = DB::table('produk')->where('id', $produk->id)->update([
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
                $checkProduk_marketplace = DB::table('produk_marketplace')->where('parent_produk_id', $produk->id)->where('Platform', 'Sinde Home Delivery')->count();
                echo $checkProduk_marketplace.' '.$product->basic->name.'<br/>';
                if($checkProduk_marketplace < 1){
                    $getProduk_marketplace = DB::table('produk_marketplace')->where('parent_produk_id', $produk->id)->where('platform', 'Sinde Home Delivery')->first();
                     $insertProduk_marketplace = DB::table('produk_marketplace')->insert([
                     'parent_produk_id'=>$produk->id,
                     'produk_id'=>$product->basic->productID,
                     'photo'=>$product->pictures->OriginalURL,
                     'stock'=>$product->main_stock,
                     'status'=>'active',
                     'price'=>$product->price->value,
                     'platform'=>'Sinde Home Delivery',
                     'created_at'=>date('Y-m-d H:i:s'),
                     'shop_id'=>$shop->shop_id,
                    //  'sku'=>$product->pictures->OriginalURL,
                    ]);
                }else{
                    $updateProduk_marketplace = DB::table('produk_marketplace')->where('platform', 'Sinde Home Delivery')->where('produk_id', $product->productID)->update([
                     'parent_produk_id'=>$produk->id,
                     'produk_id'=>$product->basic->productID,
                     'photo'=>$product->pictures->OriginalURL,
                     'stock'=>$product->main_stock,
                     'status'=>'active',
                     'price'=>$product->price->value,
                     'platform'=>'Sinde Home Delivery',
                     'updated_at'=>date('Y-m-d H:i:s'),
                    //  'sku'=>$product->pictures->OriginalURL,
                    ]);
                }

            }
        }
        // dd($responseProducts);
    }

    public function label(){
        $order_id = '67ea466dd9734c3bba6b2f48eff964ce';
        $labelClient = new Client();
        $resLabel = $labelClient->get('https://sindehomedelivery.com/api/shipping-label/'.$order_id, ['headers'=>
            ['Authorization' => "Bearer $access_token"]
        ])->getBody()->getContents();
        $responseLabel = json_decode($resLabel);
        $label = base64_decode($responseLabel->result->model->content);
        // dd()
        $Invoice = $responseDetailOrder->data[0]->invoice_number;
        $uniqueFileName = $Invoice.'.pdf';
        \Storage::put('public/label/'.$uniqueFileName, $label);
        $filePath = 'storage/label/'.$uniqueFileName;
    }

    public function tesSql(){
        $data = DB::table('message_list')->whereBetween('created_at', ['2021-04-16 23:00:00', '2021-04-17 23:00:00'])->get();
        dd($data);
    }

    public function ranked(){
        $result = DB::select(DB::raw("SELECT *, timestampdiff(second, chat_at, reply_at) as resp_time_sec
        FROM(
        SELECT
        cust.shop_id,
        cust.nama_toko,
        cust.platform,
        cust.sender_name,
        cust.to_id,
        cust.msg_id,
        cust.msg,
        cs.msg AS msg_reply,
        cust.id,
        cs.msg_list_id,
        cust.created_at AS chat_at,
        cs.time AS reply_at
    FROM
        message_list AS cust
            LEFT JOIN
        (
SELECT

  @row_number:=CASE
        WHEN @msg_list_id = msg_list_id THEN @row_number + 1
        ELSE 1
    END AS ranked, #looping untuk kasih rank ke msg_list_id yang sama
    @msg_list_id:=msg_list_id AS msg_list_id,
    msg,
    time

FROM

    #buat derived table yang sudah order by time supaya rank bisa berurutan berdasarkan waktu
    (SELECT
        msg_list_id, msg, time
    FROM
        cs_chat_replied
    ORDER BY msg_list_id , time) AS derived_table,

    #buat cross join untuk set row number default = 0
    (SELECT @row_number:=0) AS default_row_number
    having ranked = 1

) AS cs
        ON cust.id = cs.msg_list_id) as table_with_resp_time
"));

dd($result);
    }

}
