<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Session;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Image;
use Storage;
use App\Http\Controllers\JDID\JdClient;
use App\Http\Controllers\JDID\EpistockUpdateEpiMerchantWareStockRequest;


class DistributionController extends Controller
{
     public function distribute_step_1(){
      $toko = DB::table('data_toko')->get();
      return view('admin.distribute-step-1',compact('toko'));
    }

    public function distribute_step_2(Request $request){
      //dd(Session::all());
      //Session::forget('po.orderan');
      $toko = DB::table('data_toko')->where('id',$request->tokoId)->first();
      $documentNumber = $request->documentNumber;
      Session::put('po.toko',$toko);
      Session::put('po.documentNumber',$documentNumber);
      return view('admin.distribute-step-2');
    }

    public function distribute_step_3(Request $request){
        $product = DB::table('products')->where('products.id',$request->Product)->join('convertion', 'products.id', 'convertion.induk_produk_id')->orderby('convertion.stock_convertion_unit', 'desc')->get();
        // return response()->json($product);
         // $product = DB::table('products')->where('products.id',$request->Product)->join('convertion', 'products.id', 'convertion.induk_produk_id')->get();
        // dd($request, $product);

        $numberofStock= [];
        $qtyUnits = $request->qtyUnit;
        // return response()->json($product[0]);
        // $qtyUnits = [1,2,3];
        foreach($qtyUnits as $index=>$unit){
            $stock = new \stdClass();
            $stock->qty = $unit;
            $stock->satuan = $product[$index]->nama_satuan;

            array_push($numberofStock, $stock);
        }

      $newOrderan=[
        'data'=>$numberofStock,
        'Product' => $product[0],
        'hargaSatuan' => $request->hargaSatuan,
        'hargaTotal' => $request->hargaTotal,
      ];

      $orderan = Session::get('po.orderan');

      if(empty(Session::get('po.orderan'))){
          $orderan[0] = $newOrderan;
      }else{
          array_push($orderan,$newOrderan);
      }

      Session::put('po.orderan',$orderan);
      $toko = Session::get('po.toko');
      $documentNumber = Session::get('po.documentNumber');
      return redirect()->back();
    }

    public function distribute_cancel(){
      Session::forget('po');
      return redirect()->route('distributeStep1');
    }

    public function distribute_submit(){
    //   $produkDikirim = serialize(Session::get('po.orderan'));
    //   dd(Session::get('po.orderan'));
      $documentNumber = Session::get('po.documentNumber');
      $depoId = Session::get('po.toko')->id;
      $insert = DB::table('distribusi')->insertGetId([
        'distid' => "dist-id-".md5($documentNumber.$depoId).time(),
        'documentNumber' => $documentNumber,
        'depoId' => $depoId,
        // 'produkDikirim' => $produkDikirim,
        'tanggal_kirim' => date('Y-m-d H:i:s'),
        'status_kirim' => 'mengirim',
        'status_terima' => 'pending'
      ]);

      foreach(Session::get('po.orderan') as $orderan){
          foreach($orderan['data'] as $eachUnit){
        //   dd($orderan['Product']->induk_produk_id);
              $insertBarang = DB::table('distribusi_product')->insert([
                    'dist_id'=>$insert,
                    'product_id'=>$orderan['Product']->induk_produk_id,
                    'qty'=>$eachUnit->qty,
                    'satuan_unit'=>$eachUnit->satuan,
                    'created_at'=>date('Y-m-d H:i:s'),
                ]);

          }
      }

      Session::forget('po');
      CRUDBooster::redirect('admin/distribusiProduk' ,'Produk berhasil dikirim!', 'success');
    }

    public function distribute_terima($id){
      $terima = DB::table('distribusi')->where('distribusi.distid',$id)->first();
      $depo = DB::table('data_toko')->where('id',$terima->depoId)->first();
    $produkDikirim = DB::table('distribusi_product')->select('products.productName', 'distribusi_product.product_id as ProdukId', 'distribusi_product.dist_id')->where('dist_id', $terima->id)->join('products', 'products.id', 'distribusi_product.product_id')->get()->unique('ProdukId')->values();

      // dd($terima, $depo, $produkDikirim);
      foreach($produkDikirim as $index=>$produk){
          $produkKirimPerProduk = DB::table('distribusi_product')->where('product_id', $produk->ProdukId)->where('dist_id', $produk->dist_id)->orderby('id', 'asc')->get();
          $getKeterangan = DB::table('stock')->where('induk_produk_id', $produk->ProdukId)->orderby('created_at', 'desc')->first();
          foreach($produkKirimPerProduk as $iteration=>$convertion){
            //   $produkDikirim[$index]->stock = new \stdClass();
        //   dd($produkKirimPerProduk, $produk->product_id, $porduk->dist_id, $produk, $produkDikirim, $index);
              $produkDikirim[$index]->stock[$iteration]=[
                  'name'=>$convertion->satuan_unit,
                  'qty'=>$convertion->qty,
                  'qtyReceive'=>$convertion->qtyReceive,
                  'gap'=>$convertion->gapUnit,
                  'keterangan'=>$getKeterangan->keterangan
                  ];
          }
      }
      return view('admin.distribute-terima',compact('terima','depo','produkDikirim'));
    }

    // stockin baru
    public function distribute_verifikasi(Request $request,$id){
      // dd($request);
      $terima = DB::table('distribusi')->where('distid',$id)->first();
      $produkDikirim = DB::table('distribusi_product')->select('products.productName', 'distribusi_product.product_id as ProdukId', 'distribusi_product.dist_id')->where('dist_id', $terima->id)->join('products', 'products.id', 'distribusi_product.product_id')->get()->unique('ProdukId')->values();
      // dd($produkDikirim);
      foreach($produkDikirim as $index=>$produk){
          $produkKirimPerProduk = DB::table('distribusi_product')->where('product_id', $produk->ProdukId)->where('dist_id', $produk->dist_id)->orderby('id', 'asc')->get();
        //   dd($produkKirimPerProduk);
          foreach($produkKirimPerProduk as $iteration=>$convertion){
                $getConversion = DB::table('convertion')->where('induk_produk_id', $convertion->product_id)->where('nama_satuan', $convertion->satuan_unit)->first();
                // angka bon
              $produkDikirim[$index]->stock[$iteration]=[
                  'id'=>$getConversion->id,
                  'name'=>$getConversion->nama_satuan,
                  'qty'=>$convertion->qty,
                  ];
          }
      }
      $numberofStock= [];
      foreach ($produkDikirim as $n => $row) {
        //   dd($row);
        // depo belum jalan

        $laststokInduk = DB::table('stock')->where('data_toko_id',$terima->depoId)->where('induk_produk_id',$row->ProdukId)->orderby('id','desc')->first();
        // tidak ada pengurangan stock karena tidak kirim dari satu depo ke depo lain
        $insertStockTable = DB::table('stock')->insertGetId([
            'data_toko_id'=>$terima->depoId,
            'induk_produk_id'=>$row->ProdukId,
            'tipe'=>'Stock In dari Principal',
            'keterangan'=>$request->notes[$n],
            'created_at'=>date('Y-m-d H:i:s'),
            'documentNumber'=>$id
        ]);
        // dd($request->qtyReceive[$n]);
            $numberofStockPerBarang= [];
            $gap = [];
            $qtyReceive = [];
            $totalStock = 0;
                // dd($row, $request->qtyReceive[$n]);
            foreach($request->qtyReceive[$n] as $i=>$stockNumber){
                $lastStockNumber = DB::table('stock_number')->where('stock_id', $laststokInduk->id)->where('convertion_id', $row->stock[$i]['id'])->first();
                // dd($lastStockNumber, $laststokInduk, $row->stock[$i]['id'], $stockNumber, $row);

                $selisihBarang = $stockNumber - $row->stock[$i]['qty'];
                // echo $selisihBarang.' '.$stockNumber.' '.$row->stock[$i]['qty'].'<br/>';
                array_push($gap, $selisihBarang);

                array_push($qtyReceive, $stockNumber);

                $stock = new \stdClass();
                // $stock[$n] = new \stdClass();
                $stock->qty = $row->stock[$i]['qty'];
                $stock->satuan = $row->stock[$i]['name'];
                $stock->qtyReceive = $stockNumber;
                $stock->productId = $laststokInduk->induk_produk_id;
                // $stock->id =
                if(empty($lastStockNumber)){
                    $lastBookStockNumber = 0;
                }else {
                  $lastBookStockNumber =$lastStockNumber->bookStockUnit_induk;
                }


                $getConversionData = DB::table('convertion')->where('id', $row->stock[$i]['id'])->first();
                $totalStock =$totalStock + (($lastBookStockNumber + $row->stock[$i]['qty']) * $getConversionData->stock_convertion_unit);
                // dd($lastBookStockNumber, $totalStock);
                  // echo $lastStockNumber->bookStockUnit_induk.' '.$getConversionData->stock_convertion_unit.' '.$row->stock[$i]['qty'].' '.$totalStock.' <br/>' ;
                // echo $totalStock.' '.$row->stock[$i]['qty'].' '.$lastStockNumber->bookStockUnit_induk.' '.$getConversionData->stock_convertion_unit.'<br/>';
                array_push($numberofStockPerBarang, $stock);
                $insertNumber = DB::table('stock_number')->insert([
                    'stock_id'=>$insertStockTable,
                    'convertion_id'=>$row->stock[$i]['id'],
                    'unit'=>$stockNumber,
                    'bookStockUnit_induk'=>$lastStockNumber->bookStockUnit_induk + $row->stock[$i]['qty'],
                    'stockOpnameUnit_induk'=>$lastStockNumber->stockOpnameUnit_induk + $stockNumber,
                    'bookStockUnit_depo'=>$lastStockNumber->bookStockUnit_depo + $row->stock[$i]['qty'],
                    'stockOpnameUnit_depo'=>$lastStockNumber->stockOpnameUnit_depo + $stockNumber,
                    'created_at'=>date('Y-m-d H:i:s'),
                ]);
                // dd($row->stock[$i]['id'], $lastStockNumber->bookStockUnit_induk + $row->stock[$i]['qty'], $lastStockNumber->stockOpnameUnit_induk + $stockNumber, $lastStockNumber->bookStockUnit_depo + $row->stock[$i]['qty'], $lastStockNumber->stockOpnameUnit_depo + $stockNumber);
            }
            // dd('stop', $numberofStockPerBarang, $totalStock);
            array_push($numberofStock, $numberofStockPerBarang);
        // dd($numberofStock, $numberofStock[0]->satuan, $numberofStock['satuan'], $gap);
            $newProdukDikirim[] = [
                'data'=>$numberofStock,
                'Product' => $row,
                // 'hargaSatuan' => $request->hargaSatuan,
                // 'hargaTotal' => $request->hargaTotal,
                'gap'=>$gap,
                'qtyReceive'=>$qtyReceive,
                'keterangan' => $request->notes[$n]
              ];
            // dd($newProdukDikirim, $gap, $qtyReceive, $totalStock);

        // update marketplace
        $produk = DB::table('products')->where('id', $row->ProdukId)->first();
        // $kodeBarang = explode('-', $produk->kode_item)[0];
        $idInduk = $produk->id;
        $produk_marketplace = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->get();
        $check = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->count();
        // dd($produk_marketplace, $check);
        if($check > 0) {
            foreach($produk_marketplace as $product){
                $convertId = $product->convertion_id;
    
                $getConvertion = DB::table('convertion')->where('id', $convertId)->first();
                $convertion_number = $getConvertion->stock_convertion_unit;
                $platform = $product->platform;
                // dd($platform,$totalStock , $convertion_number, $getConvertion, $product );
                $stock = floor($totalStock / $convertion_number);
                // dd($stock); 
                if($platform == 'Tokopedia'){
    
                     $client = new Client();
                            $clientId = env('TOKOPEDIA_CLIENT_ID');
                            $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                            $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                            $clientData = base64_encode("{$clientId}:{$clientSecret}");
                            $appID = env('TOKOPEDIA_APP_ID');
                            try{
                                $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                                'Authorization'=> "Basic $clientData",
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1'
                                ]])->getBody()->getContents();
                            }catch (RequestException $e) {
                                // dd(Psr7\str($e->getRequest()));
                                echo Psr7\str($e->getRequest());
                                if ($e->hasResponse()) {
                                    echo Psr7\str($e->getResponse());
                                }
                            }
                            $token = json_decode($res);
                            try{
                                $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('platform', 'Tokopedia')->first();
                                $shop_id= (int)$shop->shop_id;
                                $fsId = (int)$appID;
                                $data = Array();
                                $data[0]['product_id']=(int)$product->produk_id;
                                $data[0]['new_stock']=$stock;
    
                                $clientStock = new Client();
                                $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                    'Authorization'=> "Bearer $token->access_token",
                                    'Content-Type'=> 'application/json'
                                ],
                                    'json'=>$data
                                ])->getBody()->getContents();
                                $responseStock = json_decode($resStock);
                            }catch (\GuzzleHttp\Exception\ClientException $ex) {
                                // $responseBody = $exception->getResponse()->getBody(true);
                                $response = json_decode($ex->getResponse()->getBody()->getContents());
                                // dd($response, $e);
                            }
                }elseif ($platform == 'Shopee'){
                    // yang ditandai adalah induk productsnya;
                     $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$product->shop_id)->first();
                    $shop_id= $shop->shop_id;
                    $access_token = $shop->access_token;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $timestamp = time();
                    $path = "/api/v2/product/update_stock";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    // dd( $shop);
                    if($product->is_variant == 1){
                        $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                        $item_id = $getParent->produk_id;
                        $model_id = $product->produk_id;
                    }else{
                        $item_id = $product->produk_id;
                        $model_id = 0;
                    }
                    // dd($item_id, $model_id, $stock);
                     $data[0]['model_id']=(int)$model_id;
                    $data[0]['normal_stock']=(int)$stock;
                    $clientShopeeStock = new Client();
                    $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type'=>'application/json',
                    ],
                        'json'=>
                            [
                                'item_id'=>(int)$item_id,
                                'stock_list'=>$data
                            ]
                    ])->getBody()->getContents();
                    $responseShopeeStock = json_decode($resShopeeStock);
                    // dd($responseShopeeStock, $data, $item_id);
                }elseif ($platform == 'Blibli'){
                    $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Blibli')->first();
                    $sellerApiKey = $shop->seller_api_key;
                    $storeCode = $shop->shop_id;
                    $client = new Client();
                    $clientId = env('BLIBLI_CLIENT_ID');
                    $clientKey = env('BLIBLI_CLIENT_KEY');
                    $Authorization = base64_encode("{$clientId}:{$clientKey}");
                    $blibliSku = $product->produk_id;
                    // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
                    try{
                         $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $sellerApiKey
                            ],
                                'json'=>
                                    [
                                        'availableStock'=>$stock,
                                    ]
                            ])->getBody();
                        $res = $res->getContents();
                        $response = json_decode($res);
                        $datas = $response->content;
                    }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response );
                    }
                }elseif ($platform == 'JDID') {
                    $jdidProduk = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                    $skuNo = $jdidProduk->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    $skuEncode = json_encode($skuId);
                    // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                }else if($platform == 'Warisan'){
                    // dd($product->sku, $stock);
                   $stockClient = new Client();
                   $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                     'json'=>[
                         'sku'=>$product->sku,
                          'stock'=>$stock
                     ]
                   ])->getBody()->getContents();
                   $responseStock = json_decode($resStock);
                   // dd($stock, $product->sku, $resStock, $responseStock);
                }else {
                    $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                    $access_token = $shop->access_token;
                    $data[0]['product_id']=(string) $product->id;
                    $data[0]['stock_value']=(int) $stock;
                    $stockUpdateClient = new Client();
                    $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                        ['Authorization' => "Bearer $access_token"],
                        'json'=>$data
                    ])->getBody()->getContents();
                    $responseLabel = json_decode($resLabel);
                }
            }
        }
       

      }

      $update = DB::table('distribusi')->where('distid',$id)->update([
        'tanggal_terima' => date('Y-m-d H:i:s'),
        'status_kirim' => "terkirim",
        'status_terima' => "diterima",
        'diterimaOleh' => CRUDBooster::myID(),
        'produkDikirim' => serialize($newProdukDikirim)
      ]);
      // dd($numberofStock, $gap);

      foreach($numberofStock as $i=>$stockNumber){
        // dd($stockNumber);
        // echo $request->qtyReceive[$n][$i].' '.$terima->id.' '.$stockNumber->satuan.' '.$gap[$i].'<br/>';
        // dd($request->qtyReceive[$n][$i]);
        // $getdata = DB::table('distribusi_product')->where('dist_id', $terima->id)->where('satuan_unit', $stockNumber->satuan)->first();
        // dd($request->qtyReceive[$n][$i],$request->qtyReceive[$n][1] , $terima->id, $numberofStock[$i]->satuan, $gap[$i],$stockNumber );
        // dd($stockNumber, $numberofStock[$i]->satuan, $gap[$i], $terima->id);
        foreach($stockNumber as $k=>$dataStock){
          echo $dataStock->qtyReceive.' '.$terima->id.' '.$dataStock->qty.' '.$dataStock->satuan.'<br/>';
          $updateStockNumber = DB::table('distribusi_product')->where('dist_id', $terima->id)->where('satuan_unit', $dataStock->satuan)->where('product_id', $dataStock->productId)->update([
            'qtyReceive'=>(int) $dataStock->qtyReceive,
            'gapUnit'=>(int) $dataStock->qty - (int) $dataStock->qtyReceive,
            'updated_at'=>date('Y-m-d H:i:s')
          ]);
        }
      }
      // dd($numberofStock, $gap);
      return redirect()->back();
    }
    public function stockOpname(){
        // if(CRUDBooster::myPrivilegeId()< '8'){
        $tokoData = DB::table('data_toko')->get();
        $toko = [];
        foreach($tokoData as $value){
          array_push($toko, $value->id);
        }
        // dd($toko);

        // $product = DB::table('produk')->join('produk_marketplace', 'produk.id', 'produk_marketplace.parent_produk_id')->get();
        // $product = DB::table('produk')->join('produk_marketplace', 'produk.id', 'produk_marketplace.parent_produk_id')->get();
        $product = DB::table('products')->get();
        // dd($product, $toko);
    //   }else{
    //     // $toko = DB::table('data_toko')->where('privillegeId',CRUDBooster::myPrivilegeId())->first();
    //     $toko = DB::table('data_toko')->first();
    //     $product = DB::table('products')->get();
    //     // $product = DB::table('produk')->join('produk_marketplace', 'produk.id', 'produk_marketplace.parent_produk_id')->get();


    //   }
    // dd($toko);
      return view('admin.stock-opname',compact('toko','product'));
    }

    public function stockOpnameDoings(Request $request,$id){
      $toko = DB::table('data_toko')->where('privillegeId',CRUDBooster::myPrivilegeId())->first();

      $book = DB::table('stok')->where('tokoId',$toko->id)->where('produkId',$id)->orderby('id','desc')->first();
      $produk = DB::table('products')->where('id',$id)->first();
    //   $produk = DB::table('produk_marketplace')->where('produk_marketplace.id',$id)->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')->first();
        // dd($produk, $produk->stockConversionUnit1);
      $unit1selisih = $request->stock1 - $book->bookStockUnit1;
      $unit2selisih = $request->stock2 - $book->bookStockUnit2;
      $unit3selisih = $request->stock3 - $book->bookStockUnit3;
    //   $unit4selisih = $request->stock4 - $book->bookStockUnit4;

      $totalBook = ($book->bookStockUnit1 *	$produk->stockConversionUnit1) + ($book->bookStockUnit2 *	$produk->stockConversionUnit2) + ($book->bookStockUnit3 *	$produk->stockConversionUnit3);
      $totalOpname = ($request->stock1 *	$produk->stockConversionUnit1) + ($request->stock2 *	$produk->stockConversionUnit2) + ($request->stock3 *	$produk->stockConversionUnit3);
    //   $totalBook = ($book->bookStockUnit1 *	$produk->stockConversionUnit1) + ($book->bookStockUnit2 *	$produk->stockConversionUnit2) + ($book->bookStockUnit3 *	$produk->stockConversionUnit3) + ($book->bookStockUnit4 *	$produk->stockConversionUnit4);
    //   $totalOpname = ($request->stock1 *	$produk->stockConversionUnit1) + ($request->stock2 *	$produk->stockConversionUnit2) + ($request->stock3 *	$produk->stockConversionUnit3) + ($request->stock4 *	$produk->stockConversionUnit4);

      $selisih = $totalOpname - $totalBook;
      $pembagi1 = $produk->stockConversionUnit1;
      $pembagi2 = $produk->stockConversionUnit2;
      $pembagi3 = $produk->stockConversionUnit3;
    //   $pembagi4 = $produk->stockConversionUnit4;
      if($pembagi1 == 0){
          $pembagi1 = 1;
      }
      if($pembagi2 == 0){
          $pembagi2 = 1;
      }
      if($pembagi3 == 0){
          $pembagi3 = 1;
      }
    //   if($pembagi4 == 0){
    //       $pembagi4 = 1;
    //   }
      $modSelisihUnit1 = $selisih % $pembagi1;
      $divSelisihUnit1 = $selisih - $modSelisihUnit1;
      $convertSelisihUnit1 = $divSelisihUnit1 / $pembagi1;

      $modSelisihUnit2 = $modSelisihUnit1 % $pembagi2;
      $divSelisihUnit2 = $modSelisihUnit1 - $modSelisihUnit2;
      $convertSelisihUnit2 = $divSelisihUnit2 / $pembagi2;

      $modSelisihUnit3 = $modSelisihUnit2 % $pembagi3;
      $divSelisihUnit3 = $modSelisihUnit2 - $modSelisihUnit3;
      $convertSelisihUnit3 = $divSelisihUnit3 / $pembagi3;

      // dd("total buku = ".$totalBook,
      // "total opname = ".$totalOpname,
      // "Selisih 1 = ".$unit1selisih,
      // "Selisih 2 = ".$unit2selisih,
      // "Selisih 3 = ".$unit3selisih,
      // "Total Selisih = ".$selisih,
      // "Mod Sisa unit 1 = ".$modSelisihUnit1,
      // "Div Bisa diganti Unit 1 = ".$divSelisihUnit1,
      // "Mod Sisa unit 2 ".$modSelisihUnit2,
      // "Div Bisa diganti Unit 2 = ".$divSelisihUnit2,
      // "Mod Sisa unit 3 = ".$modSelisihUnit3,
      // "Div Bisa diganti Unit 3 = ".$divSelisihUnit3,
      // "Selisih Unit 1 = ".$convertSelisihUnit1,
      // "Selisih Unit 2 = ".$convertSelisihUnit2,
      // "Selisih Unit 3 = ".$convertSelisihUnit3
      // );

    //   $stok = DB::table('stok')->insert([
    //     'distid' => "stock-opname-".md5($id."-".$toko->id).time(),
    //     'tipe' => "stock opname",
    //     'documentNumber' => time(),
    //     'tokoId' => $produk->brand_id,
    //     'produkId' => $produk->id,
    //     'unit_1' => $convertSelisihUnit1,
    //     'unit_2' => $convertSelisihUnit2,
    //     'unit_3' => $convertSelisihUnit3,
    //     'bookStockUnit1' => $request->stock1,
    //     'bookStockUnit2' => $request->stock2,
    //     'bookStockUnit3' => $request->stock3,
    //     'stockOpnameUnit1' => $request->stock1,
    //     'stockOpnameUnit2' => $request->stock2,
    //     'stockOpnameUnit3' => $request->stock3,
    //     'keterangan' => "selisih ".$convertSelisihUnit1." ".$produk->stockNameUnit1." ".$convertSelisihUnit2." ".$produk->stockNameUnit2." ".$convertSelisihUnit3." ".$produk->stockNameUnit3
    //   ]);

      // update marketplace
        $produk = DB::table('products')->where('id', $id)->first();
        // $kodeBarang = explode('-', $produk->kode_item)[0];
        // dd($produk);
        $idInduk = $produk->id;
        $kodeBarang = $produk->id;
        $produk_marketplace = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->get();
        // $produk_marketplace = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->where('platform', 'Shopee')->get();
        foreach($produk_marketplace as $product){
            $unitHitung = $product->stockNameUnit;
            $platform =  $product->platform;
            $stock1 = $request->stock1 * $produk->stockConversionUnit1;
            $stock2 = $request->stock2 * $produk->stockConversionUnit2;
            $stock3 = $request->stock3 * $produk->stockConversionUnit3;

            if($unitHitung == 1){
                    $stock = ($stock1 + $stock2 + $stock3) / $produk->stockConversionUnit1;
            }else if($unitHitung == 2){
                    $stock = ($stock1 + $stock2 + $stock3) / $produk->stockConversionUnit2;
            }else{
                    $stock = ($stock1 + $stock2 + $stock3) / $produk->stockConversionUnit3;
            }
            if($platform == 'Tokopedia'){

                 $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
            }elseif ($platform == 'Shopee'){
                // yang ditandai adalah induk productsnya;
                 $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$product->shop_id)->first();
                $shop_id= $shop->shop_id;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/product/update_stock";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);

                if($product->is_variant == 1){
                    $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                    $item_id = $getParent->produk_id;
                    $model_id = $product->produk_id;
                    // dd($getParent,$item_id, $model_id );
                }else{
                    $item_id = $product->produk_id;
                    $model_id = 0;
                }
                $data[0]['model_id']=(int)$model_id;
                $data[0]['normal_stock']=(int)$stock;
                $clientShopeeStock = new Client();
                $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type'=>'application/json',
                ],
                    'json'=>
                        [
                            'item_id'=>(int)$item_id,
                            'stock_list'=>$data
                        ]
                ])->getBody()->getContents();
                $responseShopeeStock = json_decode($resShopeeStock);
                // dd($responseShopeeStock, $product->produk_id, $data, $item_id);

            }elseif ($platform == 'Blibli'){
                $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Blibli')->first();
                $sellerApiKey = $shop->seller_api_key;
                $storeCode = $shop->shop_id;
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                $blibliSku = $product->produk_id;
                // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
                try{
                     $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $sellerApiKey
                        ],
                            'json'=>
                                [
                                    'availableStock'=>$stock,
                                ]
                        ])->getBody();
                    $res = $res->getContents();
                    $response = json_decode($res);
                    $datas = $response->content;
                }catch (\GuzzleHttp\Exception\ClientException $ex) {
                    // $responseBody = $exception->getResponse()->getBody(true);
                    $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                    dd($response );
                }
            }elseif ($platform == 'JDID') {
                $jdidProduk = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->first();
                $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                $skuNo = $jdidProduk->produk_id;
                $skuId = (object)[
                    'skuId'=>$skuNo,
                ];
                $skuEncode = json_encode($skuId);
                // $c = new JdClient();
                // $c->appKey = env('APP_KEY');
                // $c->appSecret = env('APP_SECRET');
                // $c->accessToken = $shop->access_token;
                // $c->serverUrl = "https://open-api.jd.id/routerjson";
                // $req = new EpistockQueryEpiMerchantWareStockRequest();
                // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                // $respStock = $c->execute($req, $c->accessToken);
                // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                // $currentStock = $stock - $produk->qty;
                // updateStock
                $realNum = (object)[
                    'realNum'=>$stock
                ];
                $c = new JdClient();
                $c->appKey = env('APP_KEY');
                $c->appSecret = env('APP_SECRET');
                $c->accessToken = $shop->access_token;
                $c->serverUrl = "https://open-api.jd.id/routerjson";
                $req = new EpistockUpdateEpiMerchantWareStockRequest();
                $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                $respUpdateStock = $c->execute($req, $c->accessToken);
            }
            else {
                $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                $access_token = $shop->access_token;
                $data[0]['product_id']=(string) $product->id;
                $data[0]['stock_value']=(int) $stock;
                $stockUpdateClient = new Client();
                $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                    ['Authorization' => "Bearer $access_token"],
                    'json'=>$data
                ])->getBody()->getContents();
                $responseLabel = json_decode($resLabel);
            }


        }
      return redirect()->back();
}

    // opname baru
    public function stockOpnameDoing(Request $request,$id){
    $produk = DB::table('products')->where('id',$id)->first();
      $getConvertions = DB::table('convertion')->where('induk_produk_id', $id)->orderby('stock_convertion_unit', 'desc')->get();
    //   dd($getConvertions);
    //   $toko = DB::table('data_toko')->where('privillegeId',CRUDBooster::myPrivilegeId())->first();
      $toko = DB::table('data_toko')->where('id',$produk->brand_id)->first();
      $getStockDepo = DB::table('stock')->where('data_toko_id',$toko->id)->where('induk_produk_id',$id)->orderby('id','desc')->first();
      $inputStocks = $request->stock;
    //   dd($toko, $getConvertions, $getStockDepo, $inputStocks, $id, $produk);
      $hitunganPacker = 0;
      $totalBookInduk = 0;
      $totalBookDepo = 0;
      // dd($inputStocks);
      $allStock = [];
      $insertStock = DB::table('stock')->insertGetId([
            'data_toko_id'=>$toko->id,
            'induk_produk_id'=>$id,
            'tipe'=>'Stock Opname',
            'created_at'=>date('Y-m-d, H:i:s')
         ]);
    // ambil hitungan packer dan convert ke satuan terkecil
    // ambil total book untuk induk dan depo setelah di convert
      foreach($getConvertions as $index=>$convertion){
        $getStock = DB::table('stock_number')->where('stock_id', $getStockDepo->id)->where('convertion_id', $convertion->id)->orderby('created_at', 'desc')->first();
        // dd($getStock);
        // dd($getStock, $convertion, $getConvertions, $getStockDepo->id);
        array_push($allStock, $getStock);
        $convertInput = (int) $inputStocks[$index] * $convertion->stock_convertion_unit;
        // dd($getStock, $convertion, $getConvertions, $getStockDepo->id, $inputStocks[$index] , $convertInput);
        $hitunganPacker = $hitunganPacker + $convertInput;
        $convertBookInduk = $getStock->bookStockUnit_induk * $convertion->stock_convertion_unit;
        $convertBookDepo = $getStock->bookStockUnit_depo * $convertion->stock_convertion_unit;
        $totalBookInduk = $totalBookInduk + $convertBookInduk;
        $totalBookDepo = $totalBookDepo + $convertBookDepo;
        // dd($getStock->bookStockUnit_induk);
        // dd($convertBookInduk, $convertBookDepo, $totalBookInduk, $totalBookDepo);

      }

    // dd($hitunganPacker, $totalBookInduk, $totalBookDepo);

    //   gak bisa jadi satu dengan di atas karena hitungan packer
      $selisih = $hitunganPacker - $totalBookInduk;
      $convertionProduct = [];
      $lastStock = DB::table('stock')->where('induk_produk_id',$id)->orderby('id','desc')->first();
      $lastStockNumber = DB::table('stock_number')->where('stock_id', $lastStock->id)->get();
      $moduloSelisihUnit = 0;
      $bookStockUnit_induk = 0;
      // dd($selisih, $lastStock, $lastStockNumber, $hitunganPacker , $totalBookInduk);
    //   dd($getConvertions);
      foreach($getConvertions as $index=>$convertion){
        // dd($getConvertions);
          if($index == 0){
              $modSelisihUnit = $selisih % $convertion->stock_convertion_unit;
              $divSelisihUnit = $selisih - $modSelisihUnit;
              $convertSelisihUnit = $divSelisihUnit / $convertion->stock_convertion_unit;
              $moduloSelisihUnit = $modSelisihUnit;
            //   dd($selisih , $convertion->stock_convertion_unit, $divSelisihUnit, $modSelisihUnit, $convertSelisihUnit, $moduloSelisihUnit);
          }else{
              $modSelisihUnit = $moduloSelisihUnit % $convertion->stock_convertion_unit;
              $divSelisihUnit = $moduloSelisihUnit - $modSelisihUnit;
              $convertSelisihUnit = $divSelisihUnit / $convertion->stock_convertion_unit;
              $moduloSelisihUnit = $modSelisihUnit;
            //   dd($modSelisihUnit, $moduloSelisihUnit, $convertion->stock_convertion_unit);
          }
          // masukin stock numberofStock
          // echo $convertion->id.' <br/>';
          $insertStockNumber = DB::table('stock_number')->insert([
            'stock_id'=>$insertStock,
            'convertion_id'=>$convertion->id,
            'unit'=>(int) $inputStocks[$index],
            'bookStockUnit_induk'=>$inputStocks[$index],
            'stockOpnameUnit_induk' => $inputStocks[$index],
            'bookStockUnit_depo'=> $inputStocks[$index],
            'stockOpnameUnit_depo'=>$inputStocks[$index],
            'created_at'=>date('Y-m-d H:i:s')
          ]);

        //   untuk keterangan
          array_push($convertionProduct, $convertSelisihUnit);

        //   ambil data terakhir dari depo untuk update induk per convertion
        // cari stock terakhir depo kecuali yang opname
            $product_depos = DB::table('product_depo')->where('induk_produk_id', $id)->where('data_toko_id', '!=', $toko->id)->get();
            $totalStockOpname = 0;
            // get semua depo cek data stock opname terakhir per convertion
            foreach($product_depos as $depo_data){
                $totalStockOpnamePerDepo = 0;
                $getDepoLastStock = DB::table('stock')->where('data_toko_id',$depo_data->data_toko_id)->where('induk_produk_id',$id)->orderby('id','desc')->first();
                $getDepoLastStockNumber = DB::table('stock_number')->where('stock_id', $getDepoLastStock->id)->get();
                foreach($getDepoLastStockNumber as $depoLastStockNumber){

                    $totalStockOpnamePerDepo = $totalStockOpnamePerDepo + $depoLastStockNumber->stockOpnameUnit_depo;
                }
                $totalStockOpname = $totalStockOpname + $totalStockOpnamePerDepo;
            }
            $totalStockOpname = $totalStockOpname + $inputStocks[$index];


            // INCASE DEPO SUDAH AKTIF//


        // data stockopname terakhir di tambah dengan input user overwrite data induk
          // $bookStockUnit_induk = $lastStockNumber->bookStockUnit_induk - $inputStocks[$index] (bookstock depo);
          // $stockOpnameUnit_induk = $lastStockNumber->stockOpnameUnit_induk - $inputStocks[$index] (opname deop);
          // tambah dengan input user
        //   $insertStockNumber = DB::table('stock_number')->insert([
        //     'stock_id'=>$insertStock,
        //     'convertion_id'=>$convertion->id,
        //     'unit'=>$convertSelisihUnit,
        //     'bookStockUnit_depo'=>$inputStocks[$index],
        //     'stockOpnameUnit_depo'=>$inputStocks[$index],
        //     'bookStockUnit_induk'=>$totalStockOpname,
        //     'stockOpnameUnit_induk'=>$totalStockOpname,
        //     'created_at'=>date('Y-m-d H:i:s')
        // ]);
      }
        // dd($convertionProduct);
      $keterangan = 'selisih';
      foreach($convertionProduct as $index=>$convertProduct){
        //   dd($convertProduct, $getConvertions[$index]);
          $keterangan = $keterangan.' '.$convertProduct.' '.$getConvertions[$index]->nama_satuan.' ';
      }
      // dd($keterangan, $selisih, $hitunganPacker,  $totalBookInduk);
      $updateStock = DB::table('stock')->where('id', $insertStock)->update([
         'keterangan'=> $keterangan,
         'updated_at'=>date('Y-m-d H:i:s')
        ]);


      // update marketplace
        // $kodeBarang = explode('-', $produk->kode_item)[0];
        // dd($produk);
        $idInduk = $produk->id;
        // $kodeBarang = $produk->id;
        $produk_marketplace = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->get();
        // dd($produk_marketplace, $produk->id, $getConvertions);
        // $produk_marketplace = DB::table('produk_marketplace')->where('induk_barang_id', $idInduk)->where('platform', 'Shopee')->get();
        foreach($produk_marketplace as $product){
            $unitHitung = $product->stockNameUnit;
            $platform =  $product->platform;
            $convertStockSatuanTerkecil = 0;
            $stockConversionUnit = '';
            foreach($getConvertions as $index=>$convertion){
                $convertStockSatuanTerkecil = $convertStockSatuanTerkecil + ($inputStocks[$index] * $convertion->stock_convertion_unit);
                if($convertion->id == $product->convertion_id){
                    $stockConversionUnit = $convertion->stock_convertion_unit;
                }else{
                    
                }
            }
            // dd($convertStockSatuanTerkecil, $stockConversionUnit, $getConvertions);
            // if($stockConversionUnit == 0){
            //     dd($convertion, $product->convertion_id, $product, $convertion->id, $stockConversionUnit, $produk_marketplace );
            // }
            $stock = floor((int) $convertStockSatuanTerkecil / (int) $stockConversionUnit);
            // dd($stock, $convertStockSatuanTerkecil, $stockConversionUnit);
            if($platform == 'Tokopedia'){

                 $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
            }elseif ($platform == 'Shopee'){
                // yang ditandai adalah induk productsnya;
                 $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$product->shop_id)->first();
                $shop_id= $shop->shop_id;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/product/update_stock";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);

                if($product->is_variant == 1){
                    $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                    $item_id = $getParent->produk_id;
                    $model_id = $product->produk_id;
                    // dd($getParent,$item_id, $model_id );
                }else{
                    $item_id = $product->produk_id;
                    $model_id = 0;
                }
                $data[0]['model_id']=(int)$model_id;
                $data[0]['normal_stock']=(int)$stock;
                $clientShopeeStock = new Client();
                $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type'=>'application/json',
                ],
                    'json'=>
                        [
                            'item_id'=>(int)$item_id,
                            'stock_list'=>$data
                        ]
                ])->getBody()->getContents();
                $responseShopeeStock = json_decode($resShopeeStock);
                // dd($responseShopeeStock, $product->produk_id, $data, $item_id);

            }elseif ($platform == 'Blibli'){
                $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Blibli')->first();
                $sellerApiKey = $shop->seller_api_key;
                $storeCode = $shop->shop_id;
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                $blibliSku = $product->produk_id;
                // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
                try{
                     $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $sellerApiKey
                        ],
                            'json'=>
                                [
                                    'availableStock'=>$stock,
                                ]
                        ])->getBody();
                    $res = $res->getContents();
                    $response = json_decode($res);
                    $datas = $response->content;
                }catch (\GuzzleHttp\Exception\ClientException $ex) {
                    // $responseBody = $exception->getResponse()->getBody(true);
                    $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                    dd($response );
                }
            }elseif ($platform == 'JDID') {
                $jdidProduk = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->first();
                $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                $skuNo = $jdidProduk->produk_id;
                $skuId = (object)[
                    'skuId'=>$skuNo,
                ];
                $skuEncode = json_encode($skuId);
                // $c = new JdClient();
                // $c->appKey = env('APP_KEY');
                // $c->appSecret = env('APP_SECRET');
                // $c->accessToken = $shop->access_token;
                // $c->serverUrl = "https://open-api.jd.id/routerjson";
                // $req = new EpistockQueryEpiMerchantWareStockRequest();
                // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                // $respStock = $c->execute($req, $c->accessToken);
                // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                // $currentStock = $stock - $produk->qty;
                // updateStock
                $realNum = (object)[
                    'realNum'=>$stock
                ];
                $c = new JdClient();
                $c->appKey = env('APP_KEY');
                $c->appSecret = env('APP_SECRET');
                $c->accessToken = $shop->access_token;
                $c->serverUrl = "https://open-api.jd.id/routerjson";
                $req = new EpistockUpdateEpiMerchantWareStockRequest();
                $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                $respUpdateStock = $c->execute($req, $c->accessToken);
            } else if($platform == 'Warisan'){
                $stockClient = new Client();
                $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                  'json'=>[
                      'sku'=>$product->sku,
                       'stock'=>$stock
                  ]
                ])->getBody()->getContents();
                $responseStock = json_decode($resStock);
                // dd($responseStock);
            }else {
                $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                $access_token = $shop->access_token;
                $data[0]['product_id']=(string) $product->id;
                $data[0]['stock_value']=(int) $stock;
                $stockUpdateClient = new Client();
                $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                    ['Authorization' => "Bearer $access_token"],
                    'json'=>$data
                ])->getBody()->getContents();
                $responseLabel = json_decode($resLabel);
            }


        }
      return redirect()->back();
}

    public function productList(Request $request,$id){
      if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('products')->where('productName','LIKE','%'.$cari.'%')->where('brand_id', $id)->get();
            // $data = DB::table('produk')->where('product_name','LIKE','%'.$cari.'%')->where('gudang_id', $id)->get();
            return response()->json($data);
        }
    }

    public function assemblyCreate(){
        $brands = DB::table('data_toko')->get();
      $functions = DB::table('kategori')->get();

      return view('admin.create-assembly',compact('brands','functions'));
    }

    public function productDelete($id){
      $delete = DB::table('products')->where('id', $id)->delete();
      return redirect()->back();
    }
    public function productEdit($id){
      $product = DB::table('products')->where('id',$id)->first();
      $convertions = DB::table('convertion')->where('induk_produk_id', $product->id)->get();
      $brands = DB::table('data_toko')->get();
      $functions = DB::table('kategori')->get();
      $clients = DB::table('clients')->get();
      return view('admin.productEdit',compact('brands','functions','product', 'clients', 'convertions'));
    }
    public function imageDelete($productID,$imageID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productImages);
      array_splice($images, $imageID, 1);
      //dd($product->productImages,$images);
      $image_save = serialize($images);
      $update = DB::table('products')->where('id',$productID)->update([
        'productImages' => $image_save
      ]);

      return redirect()->back();
    }
    public function displayAdd(Request $request,$productID){
    //   $product = DB::table('products')->where('id',$productID)->first();
    //   $images = unserialize($product->productImages	);

    //   if(!empty($request->photos)){
    //     foreach($request->photos as $photo){
    //       $image = $photo;
    //       list($width, $height, $type, $attr) = getimagesize($image);
    //       $time = time();
    //       $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
    //       if($width>$height){
    //         $image_normal = Image::make($image)->widen(1200, function ($constraint)
    //         {
    //           $constraint->upsize();
    //         });
    //       }else{
    //         $image_normal = Image::make($image)->heighten(600, function ($constraint)
    //         {
    //           $constraint->upsize();
    //         });
    //       }

    //       $arrayName[] = 'uploads/'.$imgName;

    //       $image_thumb = Image::make($image)->fit(200, 200);
    //       $image_normal = $image_normal->stream();
    //       $image_thumb = $image_thumb->stream();

    //       Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
    //       Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
    //     }
    //   }

    //   if(!empty($product->productImages)){
    //     foreach($arrayName as $baru){
    //       array_push($images,$baru);
    //     }
    //   }else{
    //     $images = $arrayName;
    //   }

    //   $saveimg = serialize($images);
    //   $product = DB::table('products')->where('id',$productID)->update([
    //     'productImages' => $saveimg
    //   ]);

    //   return redirect()->back();
    $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productImages	);

      if(!empty($request->photos)){
        foreach($request->photos as $photo){
          $image = $photo;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayName[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      if(!empty($product->productImages)){
        foreach($arrayName as $baru){
          array_push($images,$baru);
        }
      }else{
        $images = $arrayName;
      }

      $saveimg = serialize($images);
      $product = DB::table('products')->where('id',$productID)->update([
        'productImages' => $saveimg
      ]);

      return redirect()->back();
    }
    public function detailDelete($productID,$imageID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productVideos	);
      array_splice($images, $imageID, 1);
      $image_save = serialize($images);
      $update = DB::table('products')->where('id',$productID)->update([
        'productVideos' => $image_save
      ]);

      return redirect()->back();
    }
    public function detailAdd(Request $request,$productID){
      $product = DB::table('products')->where('id',$productID)->first();
      $images = unserialize($product->productVideos);
      if(!empty($request->videos)){
        foreach($request->videos as $video){
          $image = $video;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayVideo[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }
      //dd($product->productVideos,$images,$arrayVideo);
      if(!empty($product->productVideos)){
        foreach($arrayVideo as $baru){
          array_push($images,$baru);
        }
      }else{
        $images = $arrayVideo;
      }
      $saveimg = serialize($images);
      $product = DB::table('products')->where('id',$productID)->update([
        'productVideos' => $saveimg
      ]);
      return redirect()->back();
    }

    public function productUpload(){
      $brands = DB::table('data_toko')->get();
      $functions = DB::table('kategori')->get();
      $clients = DB::table('clients')->get();
      return view('admin.productUpload',compact('brands','functions', 'clients'));
    }

     public function productUploadSave(Request $request){
    //   dd($request);
      if(!empty($request->photos)){
        foreach($request->photos as $photo){
          $image = $photo;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayName[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      if(!empty($request->videos)){
        foreach($request->videos as $video){
          $image = $video;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayVideo[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      $videoSave = serialize($arrayVideo);
      $imageSave = serialize($arrayName);
      $slug = str_slug($request->productName);
      $insert = DB::table('products')->insertGetId([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productDescription' => $request->productDescription,
        'productFunction' => $request->ProductFunction,
        'productStatus' => $request->productStatus,
        // 'stockNameUnit1' => $request->stockNameUnit1,
        // 'stockNameUnit2' => $request->stockNameUnit2,
        // 'stockNameUnit3' => $request->stockNameUnit3,
        // 'stockConversionUnit1' => $request->stockConversionUnit1,
        // 'stockConversionUnit2' => $request->stockConversionUnit2,
        // 'stockConversionUnit3' => $request->stockConversionUnit3,
        'productPrice' => $request->productPrice,
        'productPrice1' => $request->productPrice1,
        'productPrice2' => $request->productPrice2,
        'productPrice3' => $request->productPrice3,
        'productImages' => $imageSave,
        'productVideos' => $videoSave,
        'productSlug' => $slug,
        'productNote1' => $request->productNote1,
        'productNote2' => $request->productNote2,
        'productNote3' => $request->productNote3,
        'productNote4' => $request->productNote4,
        'productNote5' => $request->productNote5,
        'productNote6' => $request->productNote6,
        'productNote7' => $request->productNote7,
        'productNote8' => $request->productNote8,
        'productNote9' => $request->productNote9,
        'productNote10' => $request->productNote10,
        'productPrice1' => $request->productPrice1,
        'productPrice2' => $request->productPrice2,
        'productPrice3' => $request->productPrice3,
        'client_id'=>$request->client,
        'kode_item'=>$request->kodeItem,
        'jenis_barang'=>'non-assembly'
      ]);
        $numberOfConvertions = $request->stockNameUnit;
        $insertStock = DB::table('stock')->insertGetId([
            'data_toko_id'=>$request->Brand,
            'induk_produk_id'=>$insert,
            'tipe'=>'Stok Awal',
            'created_at'=>date('Y-m-d H:i:s'),
        ]);
        foreach($numberOfConvertions as $index=>$convertion){
            $insertConvertion = DB::table('convertion')->insertGetId([
             'nama_satuan'=>$convertion,
             'stock_convertion_unit'=>$request->stockConversionUnit[$index],
             'induk_produk_id'=>$insert,
             'created_at'=>date('Y-m-d H:i:s'),
            ]);

            $insertStockNumber = DB::table('stock_number')->insert([
                'stock_id'=>$insertStock,
                'convertion_id'=>$insertConvertion,
                'unit'=>$request->stockAwalUnit[$index],
                'bookStockUnit_induk'=>$request->stockAwalUnit[$index],
                'bookStockUnit_depo'=>$request->stockAwalUnit[$index],
                'created_at'=>date('Y-m-d H:i:s'),
            ]);
        }

        $insertDepoProduct = DB::table('product_depo')->insert([
            'induk_produk_id'=>$insert,
            'data_toko_id'=>$request->Brand,
            'status'=>1,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);
    //   $StockAwal = DB::table('stok')->insert([
    //     'tokoId' => $request->Brand,
    //     'produkId' => $insert,
    //     'tipe' => 'Stok Awal',
    //     'unit_1' => $request->stockAwalUnit1,
    //     'unit_2' => $request->stockAwalUnit2,
    //     'unit_3' => $request->stockAwalUnit3,
    //     'bookStockUnit1' => $request->stockAwalUnit1,
    //     'bookStockUnit2' => $request->stockAwalUnit2,
    //     'bookStockUnit3' => $request->stockAwalUnit3,
    //   ]);

      CRUDBooster::redirect('admin/list_products' ,'Produk berhasil disimpan!', 'success');
    }

    // baru
    public function productUploadSaves(Request $request){
      //dd($request->all());
      if(!empty($request->photos)){
        foreach($request->photos as $photo){
          $image = $photo;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayName[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      if(!empty($request->videos)){
        foreach($request->videos as $video){
          $image = $video;
          list($width, $height, $type, $attr) = getimagesize($image);
          $time = time();
          $imgName  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          if($width>$height){
            $image_normal = Image::make($image)->widen(1200, function ($constraint)
            {
              $constraint->upsize();
            });
          }else{
            $image_normal = Image::make($image)->heighten(600, function ($constraint)
            {
              $constraint->upsize();
            });
          }

          $arrayVideo[] = 'uploads/'.$imgName;

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('uploads/'.$imgName, $image_normal->__toString());
          Storage::disk('local')->put('uploads/thumbnails/'.$imgName, $image_thumb->__toString());
        }
      }

      $videoSave = serialize($arrayVideo);
      $imageSave = serialize($arrayName);
      $slug = str_slug($request->productName);
      $insert = DB::table('products')->insertGetId([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productDescription' => $request->productDescription,
        'productFunction' => $request->ProductFunction,
        'productStatus' => $request->productStatus,
        'productPrice' => $request->productPrice,
        'productPrice1' => $request->productPrice1,
        'productPrice2' => $request->productPrice2,
        'productPrice3' => $request->productPrice3,
        'productImages' => $imageSave,
        'productVideos' => $videoSave,
        'productSlug' => $slug,
        'productNote1' => $request->productNote1,
        'productNote2' => $request->productNote2,
        'productNote3' => $request->productNote3,
        'productNote4' => $request->productNote4,
        'productNote5' => $request->productNote5,
        'productNote6' => $request->productNote6,
        'productNote7' => $request->productNote7,
        'productNote8' => $request->productNote8,
        'productNote9' => $request->productNote9,
        'productNote10' => $request->productNote10,
        'client_id'=>$request->client,
        'kode_item'=>$request->kodeItem,
      ]);

    //   insertDepo
      $insertDepo = DB::table('product_depo')->insert([
         'induk_produk_id'=> $insert,
         'data_toko_id'=> $request->Brand,
         'status'=> $request->status,
         'created_at'=> date('Y-m-d H:i:s'),
        ]);

        $insertStock = DB::table('stock')->insertGetId([
            'data_toko_id' => $request->Brand,
            'induk_produk_id' => $insert,
            'tipe' => 'Stok Awal',
            'created_at'=>date('Y-m-d H:i:s')
          ]);

        $convertions = $request->convertions;
        $satuan = $request->satuan;
        $stockConversionUnit = $request->stockConversionUnit;
        foreach($convertions as $index=>$convertion){
            $insertConvertion = DB::table('convertion')->insertGetId([
                'nama_satuan'=> $satuan[$index],
                'stock_convertion_unit'=> $stockConversionUnit[$index],
                'produk_id'=> $insert,
                'created_at'=> date('Y-m-d H:i:s'),
            ]);
          $insertStockNumber = DB::table('stock_number')->insert([
                'stock_id'=>$insertStock,
                'convertion_id'=> $insertConvertion,
                'unit'=> $request->unit[$index],
                'bookStockUnit_induk'=> $request->stockAwalInduk[$index],
                'bookStockUnit_depo'=> $request->bookStockUnitInduk[$index],
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        }


      CRUDBooster::redirect('admin/list_products' ,'Produk berhasil disimpan!', 'success');
    }

    public function productPaketUpload(Request $request){
    //   $slug = str_slug($request->productName);
    //   $insert = DB::table('products')->insertGetId([
    //     'brand_id' => $request->Brand,
    //     'productName' => $request->productName,
    //     'productDescription' => $request->productDescription,
    //     'kode_item' => $request->kode_item,
    //     'productFunction' => '19',
    //     'productStatus' => 'inactive',
    //     'productSlug' => $slug
    //   ]);

    //   return redirect()->route('productUpdateAssembly',$insert);
    $slug = str_slug($request->productName);
      $insert = DB::table('products')->insertGetId([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productDescription' => $request->productDescription,
        'productFunction' => '19',
        'productStatus' => 'inactive',
        'productSlug' => $slug
      ]);

      return redirect()->route('productUpdateAssembly',$insert);
    }
    public function assemblyUpdate($id){
      // dd($id);
      $product = DB::table('products')->where('id',$id)->first();
      $brands = DB::table('data_toko')->get();
      $clients = DB::table('clients')->get();
      $functions = DB::table('kategori')->get();
      $subproducts = DB::table('assembly_subproduct')->where('assembly_id', $id)->get();
      $countsubproducts = DB::table('assembly_subproduct')->where('assembly_id', $id)->count();
      if($countsubproducts > 0 ){
        foreach($subproducts as $index=>$productSub){
          $getSubproductData = DB::table('products')->where('id', $productSub->product_id)->first();
          // dd($product, $getSubproductData);
          $subproducts[$index]->gambar =  unserialize($getSubproductData->productImages)[0];
          $subproducts[$index]->productName = $getSubproductData->productName;
          $subproducts[$index]->productPrice = $getSubproductData->productPrice;
          $harga += $getSubproductData->productPrice * $productSub->qty;
        }
      }
      // dd($subproducts, $product);
      // if(!empty($product->productNote4)){
      //   $paket = unserialize($product->productNote4);
      //   foreach ($paket as $prd) {
      //     $prdpull = DB::table('products')->where('id',$prd)->first();
      //     $harga += $prdpull->productPrice;
      //   }
      // }

      return view('admin.create-assembly-2',compact('brands','functions','product','harga', 'clients', 'subproducts'));
    }

    public function productPaketTambah(Request $request,$id){
        $pull = DB::table('products')->where('id',$id)->first();
      $productNote4 = $pull->productNote4;
      //dd($pull,$productNote4);
      if(!empty($productNote4)){
          $paket = unserialize($pull->productNote4);
          array_push($paket,$request->newProductPacket);
      }else{
        $paket[] = $request->newProductPacket;
      }
      $new_paket = serialize($paket);
      $update = DB::table('products')->where('id',$id)->update([
        'productNote4' => $new_paket
      ]);
      return redirect()->back();
      // dd($request);
    //   $pull = DB::table('products')->where('id',$id)->first();
    //   $productNote4 = $pull->productNote4;
    //   //dd($pull,$productNote4);

    //   $insertSubProduct = DB::table('assembly_subproduct')->insertGetId([
    //     'product_id'=>$request->newProductPacket,
    //     'assembly_id'=>$id,
    //     'qty'=>$request->subproductQty,
    //     'created_at'=>date('Y-m-d H:i:s')
    //   ]);

    //   $getallSubProduct = DB::table('assembly_subproduct')->where('assembly_id', $id)->count();
      // if(!empty($productNote4)){
      //     $paket = unserialize($pull->productNote4);
      //     array_push($paket,$request->newProductPacket);
      // }else{
      //   // $paket[] = $request->newProductPacket;
      //   $paket[] = ['id_product'=>$request->newProductPacket, 'qty'=>$request->subproduct];
      // }
      // $new_paket = serialize($paket);
      //   // dd($paket, $new_paket, $tes);
      // $update = DB::table('products')->where('id',$id)->update([
      //   'productNote4' => $new_paket
      // ]);
      return redirect()->back();
    }

    public function productUpdating(Request $request,$id){
        // dd($request);
      $slug = str_slug($request->productName);

      $insert = DB::table('products')->where('id',$id)->update([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productRealName' => $request->productRealName,
        'productDescription' => $request->productDescription,
        'productFunction' => $request->ProductFunction,
        'productSlug' => $slug,
        // 'stockNameUnit1'=>$request->stockNameUnit1,
        // 'stockNameUnit2'=>$request->stockNameUnit2,
        // 'stockNameUnit3'=>$request->stockNameUnit3,
        'productStatus' => $request->productStatus,
        'productPrice' => $request->productPrice,
        'productNote1' => $request->productNote1,
        'productNote2' => $request->productNote2,
        'productNote3' => $request->productNote3,
        'productNote4' => $request->productNote4,
        'productNote5' => $request->productNote5,
        'productNote6' => $request->productNote6,
        'productNote7' => $request->productNote7,
        'productNote8' => $request->productNote8,
        'productNote9' => $request->productNote9,
        'productNote10' => $request->productNote10,
        'productPrice1' => $request->productPrice1,
        'productPrice2' => $request->productPrice2,
        'productPrice3' => $request->productPrice3,
        // 'stockConversionUnit1'=>$request->stockConversion1,
        // 'stockConversionUnit2'=>$request->stockConversion2,
        // 'stockConversionUnit3'=>$request->stockConversion3,
        'kode_item'=>$request->kodeItem,
        'client_id'=>$request->clientId,
      ]);
      $names = $request->stockNameUnit;
      // dd($names);
      $convertion_number = $request->stockConversionUnit;
      foreach($names as $index=>$name){
        $cek = DB::table('convertion')->where('induk_produk_id', $id)->where('nama_satuan', $name)->count();
        if($cek < 1){
          // dd('insert');
          $insert = DB::table('convertion')->insert([
             'nama_satuan'=>$name,
             'stock_convertion_unit'=>$convertion_number[$index],
             'induk_produk_id'=>$id,
             'created_at'=>date('Y-m-d H:i:s'),
          ]);
        }else{
          // dd('update');
          $updateConvertion = DB::table('convertion')->where('induk_produk_id', $id)->update([
            'nama_satuan'=>$name,
            'stock_convertion_unit'=>$convertion_number[$index],
            'updated_at'=>date('Y-m-d H:i:s'),
          ]);
        }

      }

      return redirect()->back();
    // $slug = str_slug($request->productName);

    //   $insert = DB::table('products')->where('id',$id)->update([
    //     'brand_id' => $request->Brand,
    //     'productName' => $request->productName,
    //     'productRealName' => $request->productRealName,
    //     'productDescription' => $request->productDescription,
    //     'productFunction' => $request->ProductFunction,
    //     'productSlug' => $slug,
    //     'productStatus' => $request->productStatus,
    //     'productPrice' => $request->productPrice,
    //     'productNote1' => $request->productNote1,
    //     'productNote2' => $request->productNote2,
    //     'productNote3' => $request->productNote3,
    //     'productNote4' => $request->productNote4,
    //     'productNote5' => $request->productNote5,
    //     'productNote6' => $request->productNote6,
    //     'productNote7' => $request->productNote7,
    //     'productNote8' => $request->productNote8,
    //     'productNote9' => $request->productNote9,
    //     'productNote10' => $request->productNote10,
    //     'productPrice1' => $request->productPrice1,
    //     'productPrice2' => $request->productPrice2,
    //     'productPrice3' => $request->productPrice3,
    //   ]);
    //   return redirect()->back();
    }

    public function productAssembly(Request $request, $id){
      // dd($request, $id);
      $slug = str_slug($request->productName);

      $insert = DB::table('products')->where('id',$id)->update([
        'brand_id' => $request->Brand,
        'productName' => $request->productName,
        'productRealName' => $request->productRealName,
        'productDescription' => $request->productDescription,
        'productFunction' => $request->ProductFunction,
        'productSlug' => $slug,
        // 'stockNameUnit1'=>$request->stockNameUnit1,
        // 'stockNameUnit2'=>$request->stockNameUnit2,
        // 'stockNameUnit3'=>$request->stockNameUnit3,
        'productStatus' => $request->productStatus,
        'productPrice' => $request->productPrice,
        'productNote1' => $request->productNote1,
        'productNote2' => $request->productNote2,
        'productNote3' => $request->productNote3,
        'productNote4' => $request->productNote4,
        'productNote5' => $request->productNote5,
        'productNote6' => $request->productNote6,
        'productNote7' => $request->productNote7,
        'productNote8' => $request->productNote8,
        'productNote9' => $request->productNote9,
        'productNote10' => $request->productNote10,
        'productPrice1' => $request->productPrice1,
        'productPrice2' => $request->productPrice2,
        'productPrice3' => $request->productPrice3,
        // 'stockConversionUnit1'=>$request->stockConversion1,
        // 'stockConversionUnit2'=>$request->stockConversion2,
        // 'stockConversionUnit3'=>$request->stockConversion3,
        'kode_item'=>$request->kodeItem,
        'client_id'=>$request->client,
        'jenis_barang'=>'assembly'
      ]);

      $checkConvertion = DB::table('convertion')->where('induk_produk_id', $id)->count();
      if($checkConvertion < 1){
        $insert = DB::table('convertion')->insert([
           'nama_satuan'=>'paket',
           'stock_convertion_unit'=>1,
           'induk_produk_id'=>$id,
           'created_at'=>date('Y-m-d H:i:s'),
         ]);
      }else{
        $update = DB::table('convertion')->where('induk_produk_id', $id)->update([
          'updated_at'=>date('Y-m-d H:i:s'),

        ]);
      }

      // $insertstock = DB::table('stock')->insertGetdId([
      //   'data_toko_id'=>$request->Brand,
      //   'induk_produk_id'=>$id,
      //
      // ])
      // foreach($names as $index=>$name){
      //   $cek = DB::table('convertion')->where('induk_produk_id', $id)->where('nama_satuan', $name)->count();
      //   if($cek < 1){
      //     // dd('insert');
      //      $insert = DB::table('convertion')->insert([
      //        'nama_satuan'=>$name,
      //        'stock_convertion_unit'=>$convertion_number[$index],
      //        'induk_produk_id'=>$id,
      //        'created_at'=>date('Y-m-d H:i:s'),
      //      ]);
      //   }else{
      //     // dd('update');
      //     $updateConvertion = DB::table('convertion')->where('induk_produk_id', $id)->update([
      //       'nama_satuan'=>$name,
      //       'stock_convertion_unit'=>$convertion_number[$index],
      //       'updated_at'=>date('Y-m-d H:i:s'),
      //     ]);
      //   }
      //
      // }

      return redirect()->back();
    }

    public function updatePenjualan(){
        // '3897048', 'sinde-1', '510976397'
        $getDataPenjualan = DB::table('bookingan')->join('order_from_marketplace_detail', 'bookingan.id', 'order_from_marketplace_detail.orderId')->whereIn('order_from_marketplace_detail.product_id', ['856330864', '10637516465', '42', '16','1211239754','13101521526'])->orderby('created_at', 'asc')->get();
        // $getDataPenjualan = DB::table('bookingan')->join('order_from_marketplace_detail', 'bookingan.id', 'order_from_marketplace_detail.orderId')->where('bookingan.kode', 'INV/20211113/MPL/1759379099')->get();

        // $getDataPenjualan = DB::table('bookingan')->whereIn('nmToko', ['3897048', 'sinde-1', '510976397'])->join('order_from_marketplace_detail', 'bookingan.id', 'order_from_marketplace_detail.orderId')->groupby('order_from_marketplace_detail.product_id')->get();
        // dd($getDataPenjualan);
        // ->orderby('created_at', 'asc')
        foreach($getDataPenjualan as $penjualan){

          if($penjualan->model_id){
            $produkMarketplace = DB::table('produk_marketplace')->where('produk_id', $penjualan->model_id)->first();
          }else{
            $produkMarketplace = DB::table('produk_marketplace')->where('produk_id', $penjualan->product_id)->first();
          }
            // dd($produkMarketplace);
            $kodeBarang = $produkMarketplace->induk_barang_id;
            $unitHitung = $produkMarketplace->stockNameUnit;
            $indukProduk = DB::table('products')->where('id', $kodeBarang)->first();

            // lastStockNumber
            $stock = DB::table('stock')->where('induk_produk_id', $kodeBarang)->orderby('id', 'desc')->first();
            // dd($stock);
            $stockNumbers= DB::table('stock_number')->where('stock_number.stock_id', $stock->id)->join('convertion', 'convertion.id', 'stock_number.convertion_id')->get();
            // $stockNumbers= DB::table('convertion')->select('convertion.id')->where('convertion.induk_produk_id', $kodeBarang)->leftjoin('stock_number', 'convertion.id', 'stock_number.convertion_id')->where('stock_number.stock_id', $stock->id)->get();
            $getConversion = DB:: table('convertion')->where('convertion.induk_produk_id', $kodeBarang)->get();
            // dd($stockNumbers, $kodeBarang, $stock, $getConversion);
            $totalLastStock = 0;
            $saleQtyConvert = 0;
            // dd($stockNumbers);
            foreach($getConversion as $index=>$number){
              // dd($number->bookStockUnit_induk, $number->stock_convertion_unit, $convertStock);
                $convertStock = $number->bookStockUnit_induk * $number->stock_convertion_unit;
                $totalLastStock = $totalLastStock + $convertStock;
                // echo $produkMarketplace->convertion_id.' '.$number->convertion_id.'<br/>';
                if($produkMarketplace->convertion_id == $number->convertion_id){
                  // echo $penjualan->qty.' '.$number->stock_convertion_unit.'<br/>';
                  $saleQtyConvert = $penjualan->qty * $number->stock_convertion_unit;
                }
            }
            // dd($totalLastStock, $saleQtyConvert);
            $stockMinusSale = $totalLastStock - $saleQtyConvert;
            $moduloSelisihUnitInduk = 0;
            echo $stockMinusSale.'<br/>';
            $insertNewStock = DB::table('stock')->insertGetId([
              'data_toko_id'=>$penjualan->toko,
              'induk_produk_id'=>$kodeBarang,
              'tipe'=>'penjualan',
              'created_at'=>date('Y-m-d H:i:s'),
              'documentNumber'=>$penjualan->kode
            ]);

            foreach($stockNumbers as $iteration=>$number){
              dd($stockNumbers);
               if($iteration == 0){

                  $modSelisihUnitInduk = $stockMinusSale % $number->stock_convertion_unit;
                  $divSelisihUnitInduk  = $stockMinusSale - $modSelisihUnitInduk;
                  $convertSelisihUnitInduk = $divSelisihUnitInduk  / $number->stock_convertion_unit;
                  $moduloSelisihUnitInduk = $modSelisihUnitInduk ;
                  // dd($stockMinusSale, $number->stock_convertion_unit, $modSelisihUnitInduk);
               }else{
                 $modSelisihUnitInduk = $moduloSelisihUnitInduk % $number->stock_convertion_unit;
                 $divSelisihUnitInduk = $moduloSelisihUnitInduk - $modSelisihUnitInduk;
                 $convertSelisihUnitInduk = $divSelisihUnitInduk / $number->stock_convertion_unit;
                 $moduloSelisihUnitInduk = $modSelisihUnitInduk;
               }
               $check = DB::table('stock_number')->where('stock_id', $insertNewStock)->where('convertion_id', $number->convertion_id)->count();
               if($check < 1){
                 $insertStockNumber = DB::table('stock_number')->insert([
                   'stock_id'=>$insertNewStock,
                   'convertion_id'=>$number->convertion_id,
                   'unit'=>$convertSelisihUnitInduk,
                   'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                   'stockOpnameUnit_induk'=>$number->stockOpnameUnit_induk,
                   'created_at'=>date('Y-m-d H:i:s')
                 ]);
               }else{
                 echo 'sudah ada';
               }


            }
            // dd('STOP');
            $updatePenjualan = DB::table('bookingan')->where('kode', $penjualan->kode)->update([
              'flag'=>1
            ]);
        }

    }
    public function productPaketKurang($paket_id,$prd_id){
      $deleteAssemblyProduct = DB::table('assembly_subproduct')->where('assembly_id', $paket_id)->where('product_id', $prd_id)->delete();
      // $pull = DB::table('products')->where('id',$paket_id)->first();
      // $paketproduk = unserialize($pull->productNote4);
      // array_splice($paketproduk, $prd_id, 1);
      // //dd($paket_id,$prd_id,$pull->productNote4,$paketproduk);
      // $paket_save = serialize($paketproduk);
      // $update = DB::table('products')->where('id',$paket_id)->update([
      //   'productNote4' => $paket_save
      // ]);

      return redirect()->back();
    }

    public function convertionList($id){
        $getConversion = DB::table('convertion')->where('induk_produk_id', $id)->get();
        return response()->json($getConversion);
    }

  public function deleteProduct($index){
    $getSession = Session::get('po.orderan');
    $newOrderan = array_splice($getSession, $index, 1);
    Session::put('po.orderan', $getSession);
    return response()->json('success');
  }
}
