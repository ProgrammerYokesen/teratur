<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use hash_hmac;

class shopeeController extends Controller
{
    public function createAuth($id){
        $timestamp = time();
        $host = "https://partner.shopeemobile.com";
        // $host = "https://partner.test-stable.shopeemobile.com";
        $path = "/api/v2/shop/auth_partner";
        $redirect_url = "https://teratur.gudangin.id/afterRedirect?id=$id";
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        // generate api
        $url = $host.$path.'?partner_id='.$partner_id.'&timestamp='.$timestamp.'&sign='.$sign.'&redirect='.$redirect_url;
        // dd($url, $timestamp);
        return \Redirect::to($url);

    }
     public function createChatAuth($id){
        $timestamp = time();
        $host = "https://partner.shopeemobile.com";
        // $host = "https://partner.test-stable.shopeemobile.com";
        $path = "/api/v2/shop/auth_partner";
        $redirect_url = "https://teratur.gudangin.id/afterRedirectChat?id=$id";
        $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
        $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
        $salt = $partner_id.$path.$timestamp;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        // generate api
        $url = $host.$path.'?partner_id='.$partner_id.'&timestamp='.$timestamp.'&sign='.$sign.'&redirect='.$redirect_url;
        // dd($url, $timestamp);
        return \Redirect::to($url);

    }


    public function afterRedirect(Request $request){
        // create access token
        $code = $request->query('code');
        $shopId= $request->query('shop_id');
        $id= $request->query('id');
        $checkStore = DB::table('Registered_store')->where('id', $id)->where('platform', 'Shopee')->first();
        if(empty($checkStore->shop_id) || $checkStore->shop_id == $shopId){

            $timestamp = time();
            $path = "/api/v2/auth/token/get";
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            $partner_key = env('SHOPEE_PARTNER_KEY');
            $salt = $partner_id.$path.$timestamp;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $expiredDate=date('Y-m-d', strtotime('+1 year'));
            $currentTime = Carbon::now()->format('Y-m-d H:i:s');
            // $indonesiaTimeZone = Carbon::createFromFormat('Y-m-d H:i:s', $currentTime, 'UTC')
            // ->setTimezone('Asia/Jakarta');
            try{
                // create token
                    $client = new Client();
                $res = $client->post("https://partner.shopeemobile.com/api/v2/auth/token/get?sign=$sign&partner_id=$partner_id&timestamp=$timestamp", ['headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json'=>[
                        'code'=>$code,
                        // 'main_account_id'=>2001274,
                        'shop_id'=>(int)$shopId,
                        'partner_id'=>$partner_id
                    ]
                ])->getBody()->getContents();
                $response = json_decode($res);
                $access_token = $response->access_token;
                $refresh_token = $response->refresh_token;
                // dd($response, $code, $shopId, $id, $refresh_token, $access_token, $expiredDate, $indonesiaTimeZone);
                $update = DB::table('Registered_store')->where('id', $id)->update([
                        'shop_id'=>$shopId,
                        'access_token'=>$access_token,
                        'refresh_token'=>$refresh_token,
                        'expired'=>$expiredDate,
                        'access_token_created'=>$currentTime,
                        'expired_in_second'=>$response->expire_in,
                        'status'=>'active'
                    ]);
                return \Redirect::to("https://teratur.gudangin.id/admin/dashboard");
            }catch (RequestException $e){
                dd($e);
            }
        }else{
            echo 'salah login';
        }
    }
    public function afterRedirectChat(Request $request){
        // create access token
        $code = $request->query('code');
        $shopId= $request->query('shop_id');
        $id= $request->query('id');
        $checkStore = DB::table('Registered_store')->where('id', $id)->where('platform', 'Shopee')->first();
        if(empty($checkStore->shop_id) || $checkStore->shop_id == $shopId){
            $timestamp = time();
            $path = "/api/v2/auth/token/get";
            $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
            $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
            $salt = $partner_id.$path.$timestamp;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $expiredDate=date('Y-m-d', strtotime('+1 year'));
            $currentTime = Carbon::now()->format('Y-m-d H:i:s');
            // $indonesiaTimeZone = Carbon::createFromFormat('Y-m-d H:i:s', $currentTime, 'UTC')
            // ->setTimezone('Asia/Jakarta');
            try{
                // create token
                    $client = new Client();
                $res = $client->post("https://partner.shopeemobile.com/api/v2/auth/token/get?sign=$sign&partner_id=$partner_id&timestamp=$timestamp", ['headers' => [
                    'Content-Type' => 'application/json',
                ],
                'json'=>[
                        'code'=>$code,
                        // 'main_account_id'=>2001274,
                        'shop_id'=>(int)$shopId,
                        'partner_id'=>$partner_id
                    ]
                ])->getBody()->getContents();
                $response = json_decode($res);
                $access_token = $response->access_token;
                $refresh_token = $response->refresh_token;
                // dd($response, $code, $shopId, $id, $refresh_token, $access_token, $expiredDate, $indonesiaTimeZone);
                $update = DB::table('Registered_store')->where('id', $id)->update([
                        'shop_id'=>$shopId,
                        'chat_access_token'=>$access_token,
                        'chat_refresh_token'=>$refresh_token,
                        'chat_expired'=>$expiredDate,
                        'chat_access_token_created'=>$currentTime,
                        'chat_expired_in_second'=>$response->expire_in,
                        'status'=>'active'
                    ]);
                return \Redirect::to("https://teratur.gudangin.id/admin/dashboard");
            }catch (RequestException $e){
                dd($e);
            }
        }else{
            echo ' salah login';
        }

    }

    public function getShopeeOrder(){
            date_default_timezone_set("Asia/Jakarta");

            // $shops = DB::table('Registered_store')->where('status', 'active')->where('platform', 'Shopee')->get();
            $shops = DB::table('Registered_store')->whereIn('id', array(1,2,36,49,78))->where('platform', 'Shopee')->get();
            // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
            // $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(1,2,4,5,25))->get();
            // $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(2))->get();

            date_default_timezone_set("Asia/Jakarta");
            $timestamp = time();
            $partner_key = env('SHOPEE_PARTNER_KEY');
            $time_to = Carbon::now()->timestamp + 86400;
            $start_time = Carbon::now()->timestamp - 86400;
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            $path = "/api/v2/order/get_order_list";
            try{
                foreach($shops as $value){
                    $shop_id = $value->shop_id;
                    // dd($value->toko);
                    $access_token = $value->access_token;
                    // dd($access_token, $value);
                    // cek if access token is valid or not
                    $access_expired_time = Carbon::parse($value->access_token_created)->addSecond($value->expired_in_second)->format('Y-m-d H:i:s');
                    $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                    $currentHourTimestamp = strtotime($currentHour);
                    $access_expired_time = strtotime($access_expired_time);
                    // dd($access_expired_time, $currentHourTimestamp > $access_expired_time, $currentHourTimestamp);
                    // jika token expired maka buat baru
                    // if($currentHourTimestamp > $access_expired_time){
                    //     $refresh_token = $value->refresh_token;
                    //     $path_refresh_token = "/api/v2/auth/access_token/get";
                    //     $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    //     $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    //     $clientRefresh = new Client();
                    //     // dd('tes');
                    //     $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                    //             'Content-Type' => 'application/json',
                    //         ],
                    //         'json'=> [
                    //                 'shop_id'=>$shop_id,
                    //                 'refresh_token'=>$refresh_token,
                    //                 'partner_id'=>$partner_id
                    //             ]
                    //         ])->getBody()->getContents();
                    //     $response = json_decode($resRefresh);
                    //     $access_token = $response->access_token;
                    //     $refresh_token = $response->refresh_token;
                    //     $expired_in = $response->expire_in;
                    //     DB::table('Registered_store')->where('shop_id', $shop_id)->update([
                    //             'access_token'=>$access_token,
                    //             'refresh_token'=>$refresh_token,
                    //             'expired_in_second'=>$expired_in,
                    //             'access_token_created'=>$currentHour,
                    //         ]);
                    // }
                    // pakai shipment bukan order list
                    // $path = "/api/v2/order/get_order_list";
                    $path = "/api/v2/order/get_shipment_list";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientGetOrder = new Client();
                    // $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_order_list?time_range_field=create_time&time_from=$start_time&time_to=$time_to&page_size=20&order_status=READY_TO_SHIP&response_optional_fields=order_status&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_shipment_list?page_size=50&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $getOrder = json_decode($resGetOrder);
                    $order = $getOrder->response->order_list;
                    //  dd($getOrder, count($order));
                    if(count($order)>=1){
                        foreach($order as $key=>$values){
                            $order_sn=$values->order_sn;
                             $check = DB::table('bookingan')->where('kode', $order_sn)->first();
                             if($check==null){
                            //  dd($check, $invoice, $order_sn);

                                 $path = "/api/v2/order/get_order_detail";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientOrderList = new Client();
                                $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();

                                $responseList = json_decode($resOrderList);

                                $invoice = $responseList->response->order_list[0]->order_sn;

                                // assign station
                                $getStation = DB::table('station')->where('first_queue', 1)->first();
                                $station = $getStation->id;
                                // dd($getStation);
                                $nextStation = $getStation->id + 1;
                                if($nextStation > 2){
                                    $nextStation = 1;

                                }
                                $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                                    'first_queue'=>0,
                                ]);
                                $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                                    'first_queue'=>1,
                                    'status'=>'empty',
                                ]);

                                // dd($responseList);
                                $package_number = $responseList->response->order_list[0]->package_list[0]->package_number;
                                // $item = $responseList->response->order_list[0]->item_list[$key];
                                $orderItemQty =$responseList->response->order_list[0]->item_list;
                                $insertId = DB::table('bookingan')->insertGetId([
                                    // 'orderId'=>$invoice,
                                    'kode'=>$invoice,
                                    'Platform'=>"Shopee",
                                    'nmToko'=>$shop_id,
                                    'nama_pemesan'=>$responseList->response->order_list[0]->recipient_address->name,
                                    'alamat'=>$responseList->response->order_list[0]->recipient_address->full_address,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'kurir'=>$responseList->response->order_list[0]->shipping_carrier,
                                    'phone'=>$responseList->response->order_list[0]->recipient_address->phone,
                                    'toko'=>$value->data_toko,
                                    // 'toko'=>$value->toko,
                                    'kota'=>strtoupper($responseList->response->order_list[0]->recipient_address->city),
                                    'package_id'=> $package_number,
                                    'status'=>'packing',
                                    // 'station'=>$station,
                                    'payment_method'=>$responseList->response->order_list[0]->payment_method,
                                    'district'=>$responseList->response->order_list[0]->recipient_address->district,
                                    'nation'=>$responseList->response->order_list[0]->recipient_address->state,
                                    'station_queue_time'=>date('Y-m-d H:i:s'),
                                    'postalcode'=>$responseList->response->order_list[0]->recipient_address->zipcode,
                                    // 'province'=>$responseList->response->order_list[0]->recipient_address->district,
                                ]);
                                $qty = 0;
                                $totalPrice = 0;
                                // insert details
                                    foreach($orderItemQty as $index=>$item){
                                        if($item->model_id == 0){
                                            $getProdukPermarketplace = DB::table('produk_marketplace')->where('produk_id', $product->id)->first();
                                        }else{
                                            $getProdukPermarketplace = DB::table('produk_marketplace')->where('produk_id', $item->model_id)->first();
                                        }
                                        $totalPrice = $totalPrice + ($item->model_original_price * $item->model_quantity_purchased);
                                        $insertItem = DB::table('order_from_marketplace_detail')->insert([
                                            'orderId'=>$insertId,
                                            'product_id'=>$item->item_id,
                                            'item'=>$item->item_name,
                                            'qty'=>$item->model_quantity_purchased,
                                            'hargaSatuan'=>$item->model_original_price,
                                            'hargaTotal'=>$totalPrice,
                                            'model_id'=>$item->model_id,
                                            'sku'=>$item->item_sku,
                                            'kode_item'=>$getProdukPermarketplace->kode_item
                                        ]) ;
                                    $qty = $qty + $item->model_quantity_purchased;
                                    // $totalPrice = $totalPrice + $totalPrice;
                                    }
                                // $updateInvoice_toko = DB::table('bookingan')->where('id', $insertId)->update([
                                //     'jmlh_item'=>$qty,
                                //     'totalBayar'=>$totalPrice
                                // ]);
                                // cek logistic type
                                // shipping parameter
                                $path = "/api/v2/logistics/get_shipping_parameter";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientShippingParameter = new Client();
                                $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$invoice")->getBody()->getContents();
                                $responseShippingParameter = json_decode($resShippingParameter);
                                $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                                $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                                $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                                $logistic_type = '';
                                if( $responseShippingParameter->response->info_needed->pickup){
                                    $logistic_type = 'pickUp';
                                    if($responseList->response->order_list[0]->shipping_carrier == 'GrabExpress Instant' || $responseList->response->order_list[0]->shipping_carrier == 'GrabExpress Sameday' || $responseList->response->order_list[0]->shipping_carrier == 'Shopee Express Instant' || $responseList->response->order_list[0]->shipping_carrier == 'GoSend Same Day'){
                                        $station = 6;
                                    }
                                }else{
                                    $logistic_type = 'dropoff';
                                }
                                $pdf = new \PDF();
                                $customPaper = array(0,0,270.28,270.64);
                                $pdf = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaper);
                                $name = $insertId.'.pdf';
                                \Storage::put('public/sticker/'.$name, $pdf->output());
                                $content = $pdf->download($name);
                                // $filePath = 'storage/sticker/'.$name;
                                // dd($responseShcippingParameter,$logistic_type, $filePath);
                                $updateInvoice_toko = DB::table('bookingan')->where('id', $insertId)->update([
                                    'invoice_toko'=>$name,
                                    'logistic_type'=>$logistic_type,
                                    'jmlh_item'=>$qty,
                                    'totalBayar'=>$totalPrice,
                                    'station'=>$station
                                ]);
                                
                                $pdfQr= base64_encode($pdf->output());
                                $clientSendImage = new Client();
                                
                                $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                                      'json'=>[
                                              'nameQr'=>$name,
                                              'contentQr'=>$pdfQr
                                      ]
                                ])->getBody()->getContents();
                             }else {
                                 echo 'data sudah ada';
                             }

                        }
                    }else{
                    // dd('tidak');

                    }

                }
            }catch (RequestException $e){
                echo $e;
            }


          echo 'sukses';

    }

    public function shopeeChat(){
        $shops = DB::table('Registered_store')->where('status', 'active')->where('platform', 'Shopee')->get();
        foreach($shops as $shop){
            $notifSendTo = DB::table('cms_users')->where(function ($query) {
                        $query->where('id_cms_privileges', '=', 1)
                                ->orWhere('id_cms_privileges', '=', 2);
                            })
                                ->pluck('id');
            // unread count
            try{
                $shop_id = $shop->shop_id;
                $access_token = $shop->chat_access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
                $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
            // cek if access token is valid or not
                $access_expired_time = Carbon::parse($shop->chat_access_token_created)->addSecond($shop->chat_expired_in_second)->format('Y-m-d H:i:s');
                $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                $currentHourTimestamp = strtotime($currentHour);
                $access_expired_time = strtotime($access_expired_time);
                // dd($access_expired_time, $currentHour>$access_expired_time, $currentHour);
                if($currentHourTimestamp>$access_expired_time){
                    $refresh_token = $shop->chat_refresh_token;
                    $path_refresh_token = "/api/v2/auth/access_token/get";
                    $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    $clientRefresh = new Client();
                    $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'shop_id'=>$shop_id,
                                'refresh_token'=>$refresh_token,
                                'partner_id'=>$partner_id
                            ]
                        ])->getBody()->getContents();
                    $response = json_decode($resRefresh);
                    $access_token = $response->access_token;
                    $refresh_token = $response->refresh_token;
                    $expired_in = $response->expire_in;
                    DB::table('Registered_store')->where('shop_id', $shop_id)->update([
                            'chat_access_token'=>$access_token,
                            'chat_refresh_token'=>$refresh_token,
                            'chat_expired_in_second'=>$expired_in,
                            'chat_access_token_created'=>$currentHour,
                        ]);
                }

                $timestamp = time();
                $path = "/api/v2/sellerchat/get_unread_conversation_count";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientChat = new Client();
                $resChatCount = $clientChat->get("https://partner.shopeemobile.com/api/v2/sellerchat/get_unread_conversation_count?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                $responseChatCount = json_decode($resChatCount);
                $number = $responseChatCount->response->total_unread_count;
                // dd($number);
                if($number>0){
                    // $config['content'] = "Chat Masuk di $shop->name Shopee";
                    // $config['to'] = "https://shopee.co.id/";
                    // $config['id_cms_users'] = $notifSendTo;
                    // \CRUDBooster::sendNotification($config);
                }
                $path = "/api/v2/sellerchat/get_conversation_list";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
            // message list yang unread
                $resChat = $clientChat->get("https://partner.shopeemobile.com/api/v2/sellerchat/get_conversation_list?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&direction=latest&page_size=60&type=unread")->getBody()->getContents();
                $responseChat = json_decode($resChat);
                $chatList = $responseChat->response->conversations;
                // dd($responseChat, $responseChatCount, $chatList);
                foreach($chatList as $chat){
                    if($chat->to_id == $chat->latest_message_from_id){
                       if($chat->latest_message_type== 'text'){
                            $input = DB::table('message_list')->insert([
                                'shop_id'=>$shop->shop_id,
                                'nama_toko'=>$shop->store_name,
                                'platform'=>'Shopee',
                                'sender_name'=>$chat->to_name,
                                'msg_id'=>$chat->conversation_id,
                                'msg'=>$chat->latest_message_content->text,
                                'to_id'=>$chatList->to_id,
                                "created_at"=>date('Y-m-d H:i:s')
                            ]);
                       }elseif($chat->latest_message_type== 'image'){
                           $input = DB::table('message_list')->insert([
                                'shop_id'=>$shop->shop_id,
                                'nama_toko'=>$shop->store_name,
                                'platform'=>Shopee,
                                'sender_name'=>$chat->to_name,
                                'msg_id'=>$chat->conversation_id,
                                'msg'=>'image',
                                "created_at"=>date('Y-m-d H:i:s'),
                                'to_id'=>$chatList->to_id,
                            ]);
                       }
                    }
                }


            }catch (\GuzzleHttp\Exception\ClientException $e){
                // dd(json_decode($e->getResponse()->getBody()->getContents()));
                // dd($e);
            }
            return 'success';
        }

    }

    public function getShopeeProducts(){
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
        // $shops = DB::table('Registered_store')->where('status', 'active')->get();
        // $shops = DB::table('Registered_store')->where('shop_id', 406576202)->get();
        $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(1,2,4,5,25))->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(4))->get();

        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('id', 5)->get();
        // dd($shops);
        // looping untuk shop
        foreach($shops as $shop){
                // $shop_id= $shop->shop_id;
                // $gudang = $shop->data_toko;
                $shop_id= $shop->shop_id;
                // dd($shop_id);
                $gudang = $shop->toko;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/product/get_item_list";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $page = 0;
                // echo $shop_id.'<br/>';

                // looping untuk page
                do{
                    // echo 'page:'.$page.'<br/>';
                    $next = $this->getAllProducts($shop_id, $access_token, $partner_id, $partner_key, $timestamp, $sign, $page, $gudang);
                    // dd($next, 'luar');
                    // echo $next.'<br/>';
                    $page = $page + 1;
                }while($next == true);
                echo 'sukses';
        }
    }
    public function getAllProducts($shop_id, $access_token, $partner_id, $partner_key, $timestamp, $sign, $page, $gudang){
        try{
            echo $page.$shop_id.'<br/>';
            $clientProductList = new Client();
            $resProductList = $clientProductList->get("https://partner.shopeemobile.com/api/v2/product/get_item_list?offset=$page&page_size=50&item_status=NORMAL&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
            $response = json_decode($resProductList)->response->has_next_page;

            $responseProductList = json_decode($resProductList)->response->item;
            // dd( $gudang, $shop_id, $responseProductList);
            if(!empty($responseProductList)){
                $countProductList = count($responseProductList);
            }else{
                $countProductList = 0;
            }
            echo $countProductList.'<br/>';
            if($countProductList > 0){
                // looping untuk dapat product info
                $count  = 0;
                foreach($responseProductList as $key=>$product){
                    $count = $count + 1;
                    echo 'count:'.$count.'<br/>';
                    // get item info
                    $item_id_list = $product->item_id;
                    // $item_id_list = 5763757658;
                    // dd($item_id_list);
                    $path = "/api/v2/product/get_item_base_info";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientProductInfo = new Client();
                    $resProductInfo = $clientProductInfo->get("https://partner.shopeemobile.com/api/v2/product/get_item_base_info?item_id_list=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                    $responseProductInfo = json_decode($resProductInfo);
                    $productData = $responseProductInfo->response->item_list[0];
                    // dd($productData->item_name, $responseProductInfo->response->item_list[0]->image->image_url_list[0]);
                    echo $productData->item_name.' <br/>';
                    // check
                    $check = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id', $gudang)->count();
                    // dd($check);
                    // get sku di produk marketplace
                    // $produkMarketplace = DB::table('produk_marketplace')->where('sku', $productData->item_sku)->first();
                    // $check = DB::table('produk_marketplace')->where('id', $productMarketplace->parent_produk_id)->count();
                    // echo 'check:'.$check.'<br/>'.'key:'.$key.'<br/>';
                    // dd($check);

                    // product exist
                    if($check > 0){
                        // produk sudah ada
                        // echo $check.'update<br/>'.$productData->item_name.'<br/>';ce
                        echo 'satu<br/>';
                        $updateProduk = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id',$gudang)->update([
                            'product_name'=>$productData->item_name,
                            'gudang_id'=>$gudang,
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);

                        // $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('sku', $productData->item_sku)->count();
                        $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->count();

                        $getParentId = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id', $gudang)->first();
                        if($productData->has_model == true){
                            $variant = 1;
                        }else{
                            $variant = 0;
                        }
                        if($checkProdukMarketplace < 1){
                            // produk_marketplace tidak ada
                            echo 'dua<br/>';
                            $insertProduk = DB::table('produk_marketplace')->insertGetId([
                                'parent_produk_id'=>$getParentId->id,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shop_id,
                                'has_variant'=>$variant,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);
                            echo 'masuk';
                            // get model(variant)
                            if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                // dd($responseModel, $responseProductInfo);
                                foreach($responseModel->model as $key=>$value){
                                    $qty = 0;
                                    $price = $value->price_info[0]->current_price;
                                    foreach($value->stock_info as $stock){
                                        if($stock->stock_type == 2){
                                            $qty = $qty->current_stock;
                                        }
                                    }
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$insertProduk,
                                        'produk_id'=> $value->model_id,
                                        'sku'=>$value->model_sku,
                                        'stock'=>$qty,
                                        'status'=>'active',
                                        'price'=>$price,
                                        'shop_id'=>$shop_id,
                                        'platform'=>'Shopee',
                                        'is_variant'=>1,
                                        // 'sku'=>$value->model_sku,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0],
                                        'variant_name'=>$responseModel->tier_variation[0]->option_list[$key]->option
                                    ]);
                                }
                            }
                        }else{
                            echo 'tiga<br/>';
                            // produk marketplace ada
                            // echo 'masuk';
                            $update = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->update([
                                'parent_produk_id'=>$getParentId->id,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'has_variant'=>$variant,
                                'shop_id'=>$shop_id,
                                // 'sku'=>$productData->item_sku,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);
                            // get model(variant)
                            if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                // dd($responseModel, $responseProductInfo);

                                foreach($responseModel->model as $key=>$value){
                                    // dd($value,$responseModel->tier_variation[0]->option_list[$index]);
                                    $getVariantParent = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->first();
                                    
                                    $checkVariant = DB::table('produk_marketplace')->where('produk_id', $value->model_id)->where('parent_produk_id', $getVariantParent->id)->count();
                                    $qty = 0;
                                        $price = $value->price_info[0]->current_price;
                                        foreach($value->stock_info as $stock){
                                            if($stock->stock_type == 2){
                                                $qty = $qty + $stock->current_stock;
                                                echo 'stock'.$qty;
                                                // dd($qty);
                                            }
                                        }
                            // dd('tes', $value, $getVariantParent->id, $qty);
                                    if($checkVariant < 1){
                                        $insertVariant = DB::table('produk_marketplace')->insert([
                                            'parent_produk_id'=>$getVariantParent->id,
                                            'produk_id'=> $value->model_id,
                                            'sku'=>$value->model_sku,
                                            'stock'=>$qty,
                                            'status'=>'active',
                                            'shop_id'=>$shop_id,
                                            'price'=>$price,
                                            'platform'=>'Shopee',
                                            'is_variant'=>1,
                                            // 'sku'=>$value->model_sku,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0],
                                            'variant_name'=>$responseModel->tier_variation[0]->option_list[$key]->option
                                        ]);
                                    }else{
                                        // dd($getVariantParent, true);
                                        $updateVariant = DB::table('produk_marketplace')->where('produk_id', $value->model_id)->where('platform', 'Shopee')->update([
                                            'parent_produk_id'=>$getVariantParent->id,
                                            'produk_id'=>$value->model_id,
                                            'sku'=>$value->model_sku,
                                            'stock'=>$qty,
                                            'status'=>'active',
                                            'shop_id'=>$shop_id,
                                            'price'=>$price,
                                            'platform'=>'Shopee',
                                            'is_variant'=>1,
                                            // 'sku'=>$value->model_sku,
                                            'updated_at'=>date('Y-m-d H:i:s'),
                                            'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0],
                                            'variant_name'=>$responseModel->tier_variation[0]->option_list[$key]->option
                                        ]);
                                    }
                                }
                            }

                        }

                    // product don't exist
                    }else{
                        // produk not exist
                        echo 'empat<br/>';
                        // echo 'masuk';
                        // echo $check.'baru<br/>'.$productData->item_name.'<br/>';
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$productData->item_name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s')
                        ]);

                        if($productData->has_model == true){
                            $variant = 1;
                        }else{
                            $variant = 0;
                        }
                        $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->count();
                        if($checkProdukMarketplace < 1){
                            $insert = DB::table('produk_marketplace')->insertGetId([
                                'parent_produk_id'=>$insertProduk,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shop_id,
                                'has_variant'=>$variant,
                                // 'sku'=>$value->model_sku,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);

                            if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                // dd($responseModel, $responseProductInfo);
                                foreach($responseModel->model as $key=>$value){
                                    // dd($value, $responseModel->tier_variation[0]->option_list[$index]);
                                    $qty = 0;
                                    $price = $value->price_info[0]->current_price;
                                    foreach($value->stock_info as $stock){
                                        if($stock->stock_type == 2){
                                            $qty = $qty +  $stock->current_stock;
                                        }
                                    }
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$insert,
                                        'produk_id'=> $value->model_id,
                                        'sku'=>$value->model_sku,
                                        'stock'=>$qty,
                                        'status'=>'active',
                                        'shop_id'=>$shop_id,
                                        'price'=>$price,
                                        'platform'=>'Shopee',
                                        // 'sku'=>$value->model_sku,
                                        'is_variant'=>1,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0],
                                        'variant_name'=>$responseModel->tier_variation[0]->option_list[$key]->option
                                    ]);
                                }
                            }
                        }
                        
                    }
                    if($productData->stock_info[0]->current_stock < 5){
                          $notifSendTo = DB::table('cms_users')->where(function ($query) {
                            $query->where('id_cms_privileges', '=', 1)
                                  ->orWhere('id_cms_privileges', '=', 2);
                                })
                                  ->pluck('id');
                            $toko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
                            // $toko = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                            $storeName = $toko->store_name;
                            // $config['content'] = "Stock Product $item->basic->name di Shopee ".$storeName." kurang dari 5";
                            // // $config['to'] = $toko->link_toko;
                            // $config['id_cms_users'] = $notifSendTo;
                            // \CRUDBooster::sendNotification($config);
                       }
                }
            }
            // dd($response);
            return $response;
        }catch (\GuzzleHttp\Exception\ClientException $e) {
            // dd('masuk sini');
            // dd($e->getResponse());
            dd(json_decode($e->getResponse()->getBody()->getContents()));

        }
    }

    public function chatWebhooks(Request $request){
        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');
        // $config['content'] = "Chat dari ".$request->data['content']["from_user_name"]."di Shopee dari toko ".$nama_toko->store_name." chat id= ".$request->data['content']['message_id'];
        // $config['to'] = "https://teratur.warisangajahmada.com/admin/balas-page/$insert/Shopee";
        // $config['id_cms_users'] = $notifSendTo;
        // \CRUDBooster::sendNotification($config);
        $nama_toko = DB::table('Registered_store')->where('shop_id', $request->shop_id)->first();
            $insert = DB::table('message_list')->insertGetId([
                'shop_id'=>$request->shop_id,
                'nama_toko'=>$nama_toko->store_name,
                'platform'=>'Shopee',
                'sender_name'=>$request->data['content']["from_user_name"],
                'msg_id'=>$request->data['content']['message_id'],
                'msg'=>$request->data['content']['content']['text'],
                'created_at'=>date('Y-m-d H:i:s'),
                'to_id'=>$request->data['content']['from_id'],
                'buyer_id'=>$request->data['content']['to_id']
            ]);


        // $config['content'] = "chat shopee".$request->shop_id." ".$request->data['content']['content']['text']. " ".$request->data['content']['message_id']." ".$request->data['content']["from_user_name"];
        // $config['content'] = $request->full_name." ".$nama_toko->store_name." ".$request->msg_id. " ".$request->message. " ".$request->shop_id. " ". $nama_toko->store_name;

        return response()->json();
    }

    public function shopeeWebhooks(Request $request){
        $kode = $request->data['ordersn'];
        $shop_id = $request->shop_id;
        $trackingNo = $request->data['trackingno'];
        $status = $request->data['status'];
        $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');
        // $config['content'] = 'data di shopee webhooks'.$shop_id.' '.$trackingNo.' '.$status;
        //     // $config['to'] = $value->link_toko;
        //     $config['id_cms_users'] = $notifSendTo;
        //     \CRUDBooster::sendNotification($config);

        if($request->data['trackingno']){

            $notifSendTo = DB::table('cms_users')->where(function ($query) {
                $query->where('id_cms_privileges', '=', 1)
                      ->orWhere('id_cms_privileges', '=', 2);
                    })
                      ->pluck('id');
            // $config['content'] = 'data trackingNo dari shopee webhooks'.$kode.' '.$trackingNo;
            //     // $config['to'] = $value->link_toko;
            //     $config['id_cms_users'] = $notifSendTo;
            //     \CRUDBooster::sendNotification($config);

            $awbNo = $request->data['trackingno'];
            $updateTrackingNumber = DB::table('bookingan')->where('platform', 'Shopee')->where('kode', $kode)->update([
            'awbNo'=>$awbNo
            ]);
        }elseif($request->data['status']){
            $notifSendTo = DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');
        // $config['content'] = 'data status dari shopee webhooks'.$kode.' '.$status;
        //     // $config['to'] = $value->link_toko;
        //     $config['id_cms_users'] = $notifSendTo;
        //     \CRUDBooster::sendNotification($config);
            if($request->data['status'] == 'CANCELLED'){
                $status = 'batal';
            }elseif($request->data['status'] == 'PROCESSED'){
                $status = 'packing';
            }elseif($request->data['status'] == 'SHIPPED'){
                $status = 'logistic';
            }elseif($request->data['status'] == 'COMPLETED'){
                $status = 'end order';
            }
            $updateStatus = DB::table('bookingan')->where('platform', 'Shopee')->where('kode', $kode)->update([
            'status_marketplace'=>$status
            ]);
        }

        return response()->json();
    }

    public function voucher(){
        $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->whereIn('id', Array(4,5))->get();
        // dd($shops);
        foreach($shops as $shop){
                $shop_id= $shop->shop_id;
                // dd($shop_id);
                $gudang = $shop->toko;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/voucher/get_voucher_list";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $page = 1;
                $clientVoucher = new Client();
                $resVoucher = $clientVoucher->get("https://partner.shopeemobile.com/api/v2/voucher/get_voucher_list?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign&page_no=$page&page_size=40&status=all", ['headers'=>[
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]])->getBody()->getContents();
                $responseVoucher = json_decode($resVoucher);
                // dd($responseVoucher);
                echo count($responseVoucher->response->voucher_list).'masuk';
                foreach($responseVoucher->response->voucher_list as $voucher){
                    // dd($voucher);
                    $insert = DB::table('shopee_voucher')->insert([
                    'nama'=>$voucher->voucher_name,
                    'code'=>$voucher->voucher_code,
                    'discount'=>$voucher->discount_amount,
                    'toko'=>$shop->name,
                    'start_at'=>$voucher->start_time,
                    'end_at'=>$voucher->end_time,
                    'usage_voucher'=>$voucher->current_usage,
                    // 'promotion_id'=>
                    ]);
                    echo 'masuk';
                }
        }
    }
    public function addon(){
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
        $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->whereIn('id', Array(4,5))->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->where('id','>',12 )->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->where('id',5 )->get();

        foreach($shops as $shop){
                $shop_id= $shop->shop_id;
                // dd($shop_id);
                $gudang = $shop->toko;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/add_on_deal/get_add_on_deal_list";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $page = 1;
                $clientAddon = new Client();
                $resAddon = $clientAddon->get("https://partner.shopeemobile.com/api/v2/add_on_deal/get_add_on_deal_list?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign&page_no=$page&page_size=40&promotion_status=all", ['headers'=>[
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]])->getBody()->getContents();
                $responseAddon = json_decode($resAddon);
                // dd($responseAddon);
                if(count($responseAddon->response->add_on_deal_list) > 0){
                    dd('stop', $shop_id, $responseAddon);
                }
                // foreach($responseVoucher->response->voucher_list as $voucher){
                //     // dd($voucher);
                //     $insert = DB::table('shopee_voucher')->insert([
                //     'nama'=>$voucher->voucher_name,
                //     'code'=>$voucher->voucher_code,
                //     'discount'=>$voucher->discount_amount,
                //     'toko'=>$shop->name
                //     ]);
                //     echo 'masuk';
                // }
        }
    }

     public function discount(){
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->where('id','>',12 )->get();
        $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->where('shop_id',319659503 )->get();
        // $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->whereIn('id', Array(4,5))->get();

        foreach($shops as $shop){
                $shop_id= $shop->shop_id;
                // dd($shop_id);
                $gudang = $shop->toko;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/discount/get_discount_list";
                // $path = "/api/v2/discount/get_discount";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $page = 1;
                // dd($access_token, $sign, $salt, $partner_key);
                $clientDiscount = new Client();
                // $resDiscount = $clientDiscount->get("https://partner.shopeemobile.com/api/v2/discount/get_discount?discount_id=113381&page_no=1&page_size=12&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                //         'Content-Type' => 'application/json',
                //         'Accept' => 'application/json',
                //     ]])->getBody()->getContents();
                $resDiscount = $clientDiscount->get("https://partner.shopeemobile.com/api/v2/discount/get_discount_list?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign&page_no=$page&page_size=40&discount_status=all", ['headers'=>[
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ]])->getBody()->getContents();
                $responseDiscount = json_decode($resDiscount);
                dd($responseDiscount->response->discount_list);
                if(count($responseDiscount->response->discount_list) > 0){
                    dd('stop', $shop_id);
                }
                // foreach($responseDiscount->response->discount_list as $discount){
                //     // dd($discount);
                //     $insert = DB::table('shopee_discount')->insert([
                //     'name'=>$discount->discount_name,
                //     'toko'=>$shop->name
                //     ]);
                //     echo 'masuk';
                // }
        }

    }
    public function oneProduct(){

        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',406576202)->first();
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $item_id = 12926725824;
        $model_id = 0;
        $access_token = $shop->access_token;
        $shop_id = $shop->shop_id;
        $timestamp = time();
        // dd($timestamp);
       if($model_id != 0){
            $path = "/api/v2/product/get_model_list";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientModel = new Client();
            $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
            $responseModel = json_decode($resModel);
            // dd($resModel, $responseModel, 'tes');
            foreach($responseModel->model as $model){
                if($model->model_id == $model_id){
                    $stock = $model->stock_info[0]->current_stock;
                    // dd($model);
                }

            }
       }else{
           $path = "/api/v2/product/get_item_base_info";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientProductInfo = new Client();
            $resProductInfo = $clientProductInfo->get("https://partner.shopeemobile.com/api/v2/product/get_item_base_info?item_id_list=$item_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
            $responseProductInfo = json_decode($resProductInfo);
            $productData = $responseProductInfo->response->item_list[0];
            
            $path = "/api/v2/product/get_model_list";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientModel = new Client();
            $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
            $responseModel = json_decode($resModel);
            dd($productData,$responseModel);
       }

        dd($productData, $responseProductInfo);
    }

    public function getDiscountStatistik(){

            // $shops = DB::table('Registered_store')->where('status', 'active')->where('platform', 'Shopee')->get();
            // $shops = DB::table('Registered_store')->where('id', 36)->where('platform', 'Shopee')->get();
            $shops = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
            // $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(1,2,4,5))->get();
            // $shops = DB::connection('gudang')->table('Shopee_shop')->where('id', 2)->get();

            date_default_timezone_set("Asia/Jakarta");
            $timestamp = time();
            $partner_key = env('SHOPEE_PARTNER_KEY');
            // $start_time = 1633046400;
            // $time_to = 1631145600;
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            try{
                foreach($shops as $value){
                    $shop_id = $value->shop_id;
                    // dd($value->toko);
                    $access_token = $value->access_token;
                    // cek if access token is valid or not
                    $access_expired_time = Carbon::parse($value->access_token_created)->addSecond($value->expired_in_second)->format('Y-m-d H:i:s');
                    $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                    $currentHourTimestamp = strtotime($currentHour);
                    $access_expired_time = strtotime($access_expired_time);
                    $start_time = 1635699600;
                    $time_to = 1636736400;
                    // pakai shipment bukan order list
                    $path = "/api/v2/order/get_order_list";
                    // $path = "/api/v2/order/get_shipment_list";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientGetOrder = new Client();
                    // &cursor=51
                    // dd($start_time, $time_to);
                    $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_order_list?time_range_field=create_time&time_from=$start_time&time_to=$time_to&page_size=20&order_status=COMPLETED&response_optional_fields=order_status&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    // $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_shipment_list?page_size=50&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $getOrder = json_decode($resGetOrder);
                    $order = $getOrder->response->order_list;
                    // dd($getOrder);
                    if(count($getOrder->response->order_list) > 0){
                        $orderArray = array();
                        foreach($order as $eachOrder){
                            array_push($orderArray, $eachOrder->order_sn);
                        }
                        $order_sn = implode(',', $orderArray);
                        // $order_sn = implode(',', $orderArray);
                         $path = "/api/v2/order/get_order_detail";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientOrderList = new Client();
                        $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,payment_method,invoice_data,package_list,estimated_shipping_fee,actual_shipping_fee,fulfillment_flag,actual_shipping_fee_confirmed&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();

                        $responseList = json_decode($resOrderList);
                        // dd($responseList);
                        if(!empty($responseList)){
                            foreach($responseList->response->order_list as $indikator=>$oneList){
                                // dd($oneList->item_list);
                                foreach($oneList->item_list as $index=>$item){
                                    // dd(!empty($item->promotion_type));
                                    // dd($item->model_original_price != $item->model_discounted_price );
                                    // $item->model_original_price != $item->model_discounted_price ||
                                    if( $item->add_on_deal != false || $item->add_on_deal_id != 0 || $item->promotion_id != 0 || $item->promotion_group_id != 0 || !empty($item->promotion_type)){
                                        // || $item->add_on_deal != false || $item->add_on_deal_id != 0 || $item->promotion_id != 0 || $item->promotion_group_id != 0 || !empty($item->promotion_type)
                                        echo $indikator.'  '.$shop_id.' '.$item->promotion_type.' ada<br/>';
                                        // dd('ada');
                                    }else{
                                        echo $indikator.' '.$shop_id.' tidak<br/>';
                                    }
                                }
                            }
                        }
                    }
                    echo 'nothing<br/>';
                }
            }catch (RequestException $e){
                echo $e;
            }
    }

    public function cekShopee(){
        // $datas = DB::table('bookingan')->where('platform', 'Shopee')->where('id', 19122)->get();
        // // dd($datas);
        // foreach($datas as $data){
        //     $shop = DB::table('Registered_store')->where('shop_id', $data->nmToko)->first();
        //     $shop_id = $shop->shop_id;
        //     // $order_sn = $data->kode;
        //     $access_token = $shop->access_token;
        //     $partner_id = (int)env('SHOPEE_PARTNER_ID');
        //     $partner_key = env('SHOPEE_PARTNER_KEY');
        //     $timestamp = time();
        //     $path = "/api/v2/order/get_order_detail";
        //     $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        //     $sign = hash_hmac('sha256', $salt, $partner_key);
        //     $clientOrderList = new Client();
        //     $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
    
        //     $responseList = json_decode($resOrderList);
        //     dd($responseList->response->order_list[0]->item_list, $responseList);
            // foreach($responseList->response->order_list[0]->item_list as $detail){
            //     // dd($detail);
            //     // if($detail->)
            //     $insert = DB::table('order_from_marketplace_detail')->insert([
            //         'orderId'=>$data->id,
            //         'product_id'=>$detail->item_id,
            //         'model_id'=>$detail->model_id,
            //         'item'=>$detail->item_name,
            //         'qty'=>$detail->model_quantity_purchased,
            //         'hargaSatuan'=>$detail->model_original_price,
            //         'hargaTotal'=>$detail->model_original_price * $detail->model_quantity_purchased,
            //         'sku'=>$detail->item_sku
            //     ]);
            // }
        // }
        $shop = DB::table('Registered_store')->where('shop_id', 281934945)->first();
        $shop_id = $shop->shop_id;
        $order_sn = '220216KNYT5E7A';
        $access_token = $shop->access_token;
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $timestamp = time();
        $path = "/api/v2/order/get_order_detail";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientOrderList = new Client();
        $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();

        $responseList = json_decode($resOrderList);
        dd($responseList);
    }

    public function getLabelShopee(Request $request){
        return response()->json();
    }

    public function cekTrackingNumber(){
        $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', 281934945)->first();
        $access_token=$getToko->access_token;
        $shop_id=$getToko->shop_id;
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $order_sn = '220215H1S7QJNH';
        $timestamp = time();
        $path = "/api/v2/logistics/get_tracking_number";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientTrackingNumber = new Client();
        $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
        $responseTrackingNumber = json_decode($resTrackingNumber);
        $trackingNumber = $responseTrackingNumber->response->tracking_number;
        dd($trackingNumber, $responseTrackingNumber);
    }

    public function shopeeLabel(){
        $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', 281934945)->first();
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $timestamp = time();
        $shop_id=$getToko->shop_id;
        $order_sn = '220216KNYT5E7A';
        $package_number="OFG98676451523042";
        // $trackingNumber =
        $access_token=$getToko->access_token;

        // shipping parameter
                $path = "/api/v2/logistics/get_shipping_parameter";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShippingParameter = new Client();
                $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                $responseShippingParameter = json_decode($resShippingParameter);
                $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                // dd($responseShippingParameter);
            // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
            $logistic_type = '';
            if( $responseShippingParameter->response->info_needed->pickup){
                $logistic_type = 'pickUp';
            // echo 'pickup<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        // 'package_number'=>$package_number,
                        'pickup'=>[
                            'address_id'=>$address_id,
                            'pickup_time_id'=>$pickup_time_id,
                            // 'tracking_number'=>$trackingNumber,
                        ],
            
            
                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder);
            
            }elseif($branch_id){
                $logistic_type = 'dropoff';
                // echo 'dropoff<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        'dropoff'=>[
                            'address_id'=>$address_id,
                            'pickup_time_id'=>$pickup_time_id,
                            // 'tracking_number'=>$trackingNumber,
                        ],
            
            
                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder, 'tes');
            }else{
                $logistic_type = 'dropoff';
                // echo 'dropoff<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        'dropoff'=>[
                            'tracking_no'=>'',
                            'branch_id'=>0,
                            'sender_real_name'=>'',
                        ],
            
            
                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder, 'set');
            }
        // get package number
        $path = "/api/v2/order/get_order_detail";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientOrderList = new Client();
        $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
        $responseOrderList = json_decode($resOrderList);
        $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;
        // get Tracking Number
        $path = "/api/v2/logistics/get_tracking_number";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientTrackingNumber = new Client();
        $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
        $responseTrackingNumber = json_decode($resTrackingNumber);
        $trackingNumber = $responseTrackingNumber->response->tracking_number;
        // get shipping document parameter
        $path = "/api/v2/logistics/get_shipping_document_parameter";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $data = Array();
        $data[0]['order_sn']=$order_sn;
        $data[0]['package_number']=$package_number;
        $clientShippingDocumentParameter = new Client();
        $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],

                'json'=>[
                    'order_list'=>$data
                ]
        ])->getBody()->getContents();
        $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
        $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
        // $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', 450864017)->first();
        // create shipping document
        $path = "/api/v2/logistics/create_shipping_document";
        $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $data = array();
        $data[0]['order_sn'] = $order_sn;
        $data[0]['package_number'] = $package_number;
        $data[0]['tracking_number'] = $trackingNumber;
        $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
        $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],

            'json' => [
                'order_list' => $data
            ]
            // 'body'=>$parameter

        ])->getBody()->getContents();
        $resultCreated = json_decode($createdDocument);


    // get shipping document result
    $path = "/api/v2/logistics/get_shipping_document_result";
    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
    $sign = hash_hmac('sha256', $salt, $partner_key);
    $dataDocResult = Array();
    $dataDocResult[0]["order_sn"]=$order_sn;
    $dataDocResult[0]["package_number"]=$package_number;
    $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
    // dd($dataDocResult);
    $clientShippingDocResult = new Client();
    $resDownloadShipDoc = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
        'Content-Type' => 'application/json',
    ],
        'json'=>[
            'order_list'=>$dataDocResult
        ]
    ])->getBody()->getContents();
    $responseShippingDocResult = json_decode($resDownloadShipDoc);
        dd($trackingNumber, $package_number, $resShippingDocumentParameter, $resultCreated,$responseShippingDocResult,$responseTrackingNumber);
    //  dd($resDownloadShipDoc);
        // $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', 281934945)->first();


        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',281934945)->first();
        $id = 20365;
        $shop_id = $shop->shop_id;
        $order_sn = '2201306CHREFT3';
        $access_token = $shop->access_token;
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $timestamp = time();
        $path = "/api/v2/order/get_order_detail";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientOrderList = new Client();
        $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();

        $responseList = json_decode($resOrderList);

        // download shipping document
            $path = "/api/v2/logistics/download_shipping_document";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $dataDownload = Array();
            $dataDownload[0]["order_sn"]=$order_sn;
            $dataDownload[0]["package_number"]=$package_number;
            $clientDownloadShipDoc = new Client();
            $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
                ],
                'json'=>[
                    'shipping_document_type'=>$suggest_shipping_document_type,
                    'order_list'=>$dataDownload,
                    ]
                ])->getBody()->getContents();
            $resShippingLabel = json_decode($resDownloadShipDoc);
            $name = "Shopee-$order_sn.pdf";
            \Storage::put("public/label/$name", $resDownloadShipDoc);
            $path = 'storage/label/'.$name;
            // }catch(\GuzzleHttp\Exception\ClientException $e){
            //     dd($e->getResponse()->getBody()->getContents());
            // }
            // update bukti bayar
            // update logistic_type
            $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                'buktiBayar'=>$path,
                'awbNo'=>$trackingNumber
            ]);
        dd($responseList);
    }

     public function cekorderan(){
         $shops = DB::connection('gudang')->table('Shopee_shop')->whereIn('id', array(1,2,4,5))->get();
        //  $shops =  DB::table('Registered_store')->whereIn('id', array(1,2,36,49,78))->where('platform', 'Shopee')->get();
         foreach($shops as $value){
             $access_token = $value->access_token;
             $shop_id = $value->shop_id;
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            $timestamp = time();
            $partner_key = env('SHOPEE_PARTNER_KEY');
            $start_time = 1630429200;
            $time_to = $start_time + 86400;
            $path = "/api/v2/order/get_order_list";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientGetOrder = new Client();
            $resGetOrder = $clientGetOrder->get("https://partner.shopeemobile.com/api/v2/order/get_order_list?time_range_field=create_time&time_from=$start_time&time_to=$time_to&order_status=COMPLETED&page_size=20&response_optional_fields=order_status&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
            $getOrder = json_decode($resGetOrder);
            $order = $getOrder->response->order_list;
            $order_sn = '21090163YT2570';
            $path = "/api/v2/order/get_order_detail";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientOrderList = new Client();
            $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();

            $responseList = json_decode($resOrderList);
            dd($getOrder,$responseList);
         }


     }
     
     public function shopeeRefreshToken(){
        $shops = DB::table('Registered_store')->where('Platform', 'Shopee')->where('status', 'active')->get();
        foreach($shops as $shop){
            $access_expired_time = Carbon::parse($shop->access_token_created)->addSecond(10800)->format('Y-m-d H:i:s');
            $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
            $currentHourTimestamp = strtotime($currentHour);
            $access_expired_time = strtotime($access_expired_time);
            if($currentHourTimestamp > $access_expired_time){
                $refresh_token = $shop->refresh_token;
                $path_refresh_token = "/api/v2/auth/access_token/get"; 
                $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                $clientRefresh = new Client();
                $shop_id = $shop->shop_id;
                $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'shop_id'=>$shop_id,
                            'refresh_token'=>$refresh_token,
                            'partner_id'=>$partner_id
                        ]
                    ])->getBody()->getContents();
                $response = json_decode($resRefresh);
                // echo $response;
                $access_token = $response->access_token; 
                $refresh_token = $response->refresh_token; 
                $expired_in = $response->expire_in;
                DB::table('Registered_store')->where('shop_id', $shop_id)->where('Platform', 'Shopee')->update([
                    'access_token'=>$access_token,
                    'refresh_token'=>$refresh_token,
                    'expired_in_second'=>$expired_in,
                    'access_token_created'=>$currentHour,
                ]);
            }
        }
    }
    public function shopeeRefreshChatToken(){
        $shops = DB::table('Registered_store')->where('Platform', 'Shopee')->where('status', 'active')->get();
        foreach($shops as $shop){
            $access_expired_time = Carbon::parse($shop->chat_access_token_created)->addSecond(10800)->format('Y-m-d H:i:s');            
            $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
            $currentHourTimestamp = strtotime($currentHour);
            $access_expired_time = strtotime($access_expired_time);
            if($currentHourTimestamp > $access_expired_time){
                $refresh_token = $shop->chat_refresh_token;
                $path_refresh_token = "/api/v2/auth/access_token/get"; 
                $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                $clientRefresh = new Client();
                $shop_id = $shop->shop_id;
                $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'shop_id'=>$shop_id,
                            'refresh_token'=>$refresh_token,
                            'partner_id'=>$partner_id
                        ]
                    ])->getBody()->getContents();
                $response = json_decode($resRefresh);
                // echo $response;
                $access_token = $response->access_token; 
                $refresh_token = $response->refresh_token; 
                $expired_in = $response->expire_in;
                DB::table('Registered_store')->where('shop_id', $shop_id)->where('Platform', 'Shopee')->update([
                    'chat_access_token'=>$access_token,
                    'chat_refresh_token'=>$refresh_token,
                    'chat_expired_in_second'=>$expired_in,
                    'chat_access_token_created'=>$currentHour,
                ]);
            }
        }
    }
    
    public function TeraturShopee(){
        $shopee = DB::connection('gudang')->table('Shopee_shop')->where('shop_status', 'active')->get();
        foreach($shopee as $shop){
            // dd($shop);
            $updateTeratur = DB::table('Registered_store')->where('Platform', 'Shopee')->where('shop_id', $shop->shop_id)->update([
                'chat_access_token'=>$shop->chat_access_token,
                'chat_refresh_token'=>$shop->chat_refresh_token,
                'chat_expired_in_second'=>$shop->chat_expired_in_second,
                'chat_access_token_created'=>$shop->chat_access_token_created,
                'access_token'=>$shop->access_token,
                'refresh_token'=>$shop->refresh_token,
                'expired_in_second'=>$shop->expired_in_second,
                'access_token_created'=>$shop->access_token_created,
            ]);
        }
    }

}
