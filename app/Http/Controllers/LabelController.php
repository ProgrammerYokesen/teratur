<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Exception;
use DB;

class LabelController extends Controller
{
    public function SendImage(){
        $gambar = @file_get_contents('https://teratur.gudangin.id/storage/2021-12/de307fc0345fa9310e661dd131f2334b.png');
        $base64Image = base64_encode($gambar);
        $gambar2 = fopen('https://teratur.gudangin.id/storage/2021-12/de307fc0345fa9310e661dd131f2334b.png', 'r');
        // dd($base64Image);
        // send image
        try{
            $client = new Client();
            $res = $client->post('https://api.gudangin.id/api/label',[
                  'json'=>[
                      
                        //   'Content-type' => 'multipart/form-data',
                          'name'=>'image.png',
                        //   'filename'=>'tes',
                          'contents'=> $base64Image,
                      
                  ]
            ])->getBody()->getContents();
            $response = json_decode($res);
            return response()->json([$response]);
            // dd($res, $response);
        }catch(\GuzzleHttp\Exception\ClientException $e){
                $error = Psr7\Message::toString($e->getResponse());
                // dd($error);
                return response()->json($error);

        }
        
    }
    
    public function fixLabel(){
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        $data = DB::table('bookingan')->where('id', 20213)->first();
        
        // label
        $pdf = new \PDF();
        $orderId = $data->kode;
        $customPaper = array(0,0,500.28,600.64);
        $clientShipping = new Client();
            $orderId = $data->orderId;
            $resShippingLabel = $clientShipping->get("https://fs.tokopedia.net/v1/order/$orderId/fs/$appID/shipping-label?printed=0", ['headers' => ['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
            dd($resShippingLabel);
        $pdf = \PDF::loadView('admin.labelPdf', compact('resShippingLabel'));
        // dd($pdf->output());
        $name = 'Tokopedia-'.$orderId.'.pdf';
        \Storage::put('public/label/'.$name, $pdf->output());
        $path = "storage/label/$name";
        $pdfBuktiBayar = base64_encode($pdf->output());
        $clientSendImage = new Client();
        
        $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
              'json'=>[
                  
                      'name'=>$name,
                      'contentBuktibayar'=> $pdfBuktiBayar,
              ]
        ])->getBody()->getContents();
        $update = DB::table('bookingan')->where('id', 19255)->update([
            'buktiBayar'=>$name
        ]);
    }
    
    public function tesNewFlowTokpedLabel(){
        $invoice = 'INV/20220204/MPL/2012823263';
        $clientCreateInvoice = new Client();
        $resInvoice = $clientCreateInvoice->post('https://api.gudangin.id/api/tokped-label',[
              'json'=>[
                      'invoice'=>$invoice
              ]
        ])->getBody()->getContents();
        
    }
}
