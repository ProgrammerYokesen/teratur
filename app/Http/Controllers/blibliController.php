<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DB;
use CRUDBooster;

use function GuzzleHttp\json_decode;
ini_set('memory_limit', '512M');

class blibliController extends Controller
{
    //
    public function getOrder(){
        $notifSendTo = \DB::table('cms_users')->where(function ($query) {
            $query->where('id_cms_privileges', '=', 1)
                  ->orWhere('id_cms_privileges', '=', 2);
                })
                  ->pluck('id');

        // $shops = \DB::table('Registered_store')->where('platform', 'Blibli')->where('status', 'active')->get();
        $shops = \DB::table('Registered_store')->whereIn('id', array(14,16,17,35))->where('platform', 'Blibli')->where('status', 'active')->get();
        // dd($shops);
        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        try {
            foreach($shops as $shop){
                $shopId = $shop->shop_id;
                $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderList?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$shopId.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&status=FP', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $shop->seller_api_key
                    ]])->getBody();
                $res = $res->getContents();
                $response = json_decode($res);
                $datas = $response->content;
                // dd($datas);
                if(!empty($response->content)) {
                    foreach($datas as $data){
                    //   dd('masuk');
                        //End get each order detail
                        //Cek di db ada atau belum
                        $checkData = \DB::table('bookingan')->where('kode',$data->orderNo)->count();
                        //  dd($checkData);
                        if($checkData < 1 && $data->orderStatus=='FP'){
                             //If belum ada dan pesanan baru
                            $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$data->orderNo.'&orderItemNo='.$data->orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                                'Authorization'=> "Basic $Authorization",
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1',
                                'Api-Seller-Key' => $shop->seller_api_key
                                ]])->getBody();
                            $getDetail = $getDetail->getContents();
                            $responseDetail = json_decode($getDetail);
                            $detailOrder = $responseDetail->value;

                            // dd($detailOrder);
                            if($data->logisticsProductName == "JNE Cashless" || $data->logisticsProductName == "JNE CASHLESS TRUCKING" || $data->logisticsProductName == "JNE_Darat"){
                                // echo 'false<br/>';
                                $logistic_type = 'dropoff';
                            }else{
                                // echo 'true<br/>';
                                if($data->logisticsProductName == 'GRAB Sameday' || $data->logisticsProductName == 'Grab Instant' || $data->logisticsProductName == 'GOSEND Sameday' || $data->logisticsProductName == 'GOSEND Instant'){
                                    $station = 6;
                                }
                                $logistic_type = 'pickUp';
                            }
                            $toko = \DB::table('Registered_store')->where('shop_id', $shopId)->first();

                            // assign station
                            $getStation = DB::table('station')->where('first_queue', 1)->first();
                            $station = $getStation->id;
                            $nextStation = $getStation->id + 1;
                            if($nextStation > 2){
                                $nextStation = 1;

                            }
                            $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                                'first_queue'=>0,
                            ]);
                            $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                                'first_queue'=>1,
                                'status'=>'empty',
                            ]);

                            $insertId = \DB::table('bookingan')->insertGetId([
                                'package_id' => $detailOrder->packageId,
                                'platform' => 'Blibli',
                                'nmToko' => $shopId,
                                'created_at' => date('Y-m-d H:i:s'),
                                'nama_pemesan' => $data->customerFullName,
                                'kota' => strtoupper($detailOrder->shippingCity),
                                'alamat' => $detailOrder->shippingStreetAddress,
                                'kurir' => $data->logisticsProductName,
                                'phone'=>$detailOrder->shippingMobile,
                                // 'item_no'=>$detailOrder->orderItemNo,
                                'logistic_type'=>$logistic_type,
                                'kode'=>$detailOrder->orderNo,
                                'awbNo'=>$detailOrder->awbNumber,
                                'status' => 'packing',
                                'toko'=>$toko->data_toko,
                                'station_queue_time'=>date('Y-m-d H:i:s'),
                                'station'=>$station,
                                'cronjob_hit_time'=>date('H:i')
                                ]);
                            // dd($insertId);
                            $qty = 0;
                            $totalPrice = 0;
                            foreach($datas as $dt){
                                if($data->orderNo == $dt->orderNo){
                                $getProduk = DB::table('produk_marketplace')->where('produk_id', $dt->itemSku)->first();
                                $qty = $qty + $data->qty;
                                $totalPrice = $totalPrice + $data->finalPrice;
                                $insertDetail =   \DB::table('order_from_marketplace_detail')->insert([
                                    'orderId' => $insertId,
                                    'item' => $dt->productName,
                                    'qty' => $dt->qty,
                                    'product_id'=>$dt->itemSku,
                                    'orderItemNo'=>$data->orderItemNo,
                                    'kode_item'=>$getProduk->kode_item
                                    // 'sku'=>$sku,
                                ]);

                                }else{
                                    echo 'tidak sama';
                                }
                            }
                            $order_sn = $detailOrder->orderNo;
                            $pdf = new \PDF();
                                $customPaper = array(0,0,270.28,270.64);
                                $pdf = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaper);
                                $name = $insertId.'.pdf';
                                \Storage::put('public/sticker/'.$name, $pdf->output());
                                $content = $pdf->download($name);
                                $filePath = 'storage/sticker/'.$name;
                            // combine shipping
                            $orderItemNo = $data->orderItemNo;
                            $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/getCombineShipping?businessPartnerCode='.$shopId.'&requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                                'Authorization'=> "Basic $Authorization",
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1',
                                'Api-Seller-Key' => $shop->seller_api_key
                                ]])->getBody();
                            $res = $res->getContents();
                            $response = json_decode($res);
                            $datas = $response->value->combineShipping;
                            $combine = $response->value->alreadyCombined;
                            $itemList = [];
                            // if($combine == false){
                                foreach($datas as $key=>$value){
                                    array_push($itemList,$value->orderItemNo);
                                }
                            // }
                            $update = \DB::table('bookingan')->where('id', $insertId)->where('platform', 'Blibli')->update([
                                'invoice_toko'=>$filePath,
                                'logistic_type'=>$logistic_type,
                                'jmlh_item'=>$qty,
                                'totalBayar'=>$totalPrice,
                            ]);

                            // create package
                            // $res = $client->post('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/createPackage?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&businessPartnerCode='.$shopId.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                            //     'Authorization'=> "Basic $Authorization",
                            //     'Content-Type' => 'application/json',
                            //     'Accept' => 'application/json',
                            //     'Content-Length' => 0,
                            //     'User_Agent' => 'PostmanRuntime/7.17.1',
                            //     'Api-Seller-Key' => $shop->seller_api_key
                            // ],
                            //     'json'=>[
                            //         'orderItemIds'=>$itemList
                            //     ]
                            // ])->getBody();
                            // $res = $res->getContents();
                            // $response = json_decode($res);
                            // $datasCreate = $response->value;
                            // // dd($datas, $res);
                            // // shipping label
                            // $packageId = $detailOrder->packageId;
                            // $storeId = 10001;
                            // $sellerCode = $shopId;
                            // $kode = $detailOrder->orderNo;
                            // // dd($packageId, $sellerCode, $custName, $shop->seller_api_key);
                            // $clientLabel = new Client();
                            // $getShippingLabel = $clientLabel->get("https://api.blibli.com/v2/proxy/seller/v1/orders/$packageId/shippingLabel?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40eb&username=daniel.yokesen@gmail.com&storeId=$storeId&storeCode=$sellerCode&channelId=PT. YOKESEN TEKNOLOGI", ['headers' => [
                            //     'Authorization'=> "Basic $Authorization",
                            //     'Content-Type' => 'application/json',
                            //     'Accept' => 'application/json',
                            //     'Content-Length' => 0,
                            //     'User_Agent' => 'PostmanRuntime/7.17.1',
                            //     'Api-Seller-Key' => $shop->seller_api_key
                            //     ]])->getBody();
                            // $getShippingLabel = $getShippingLabel->getContents();
                            // $responseShippingLabel = json_decode($getShippingLabel);
                            // $shippingLabel = $responseShippingLabel->content->shippingLabel;
                            // date_default_timezone_set("Asia/Jakarta");
                            // $file = base64_decode($shippingLabel);
                            // $uniqueFileName = str_replace(" ","",time()."-".$kode.'.pdf');
                            // \Storage::put('public/label/'.$uniqueFileName, $file);
                            // $filePath = 'storage/label/'.$uniqueFileName;
                            // dd($qty, $shippingLabel, $filePath);
                            // $update = \DB::table('bookingan')->where('id', $insertId)->where('platform', 'Blibli')->update([
                            //     'buktiBayar' => $filePath,
                            // ]);
                            // dd($responseShippingLabel, $datasCreate, $datas);
                            // $config['content'] = "Order Masuk di Blibli ".$shop->store_name;
                            // $config['to'] = '#';
                            // // $config['to'] = $seller['nmToko'];
                            // $config['id_cms_users'] = $notifSendTo;
                            // \CRUDBooster::sendNotification($config);
                            // dd($filePath, $getShippingLabel);
                            // array_push($blibliOrders, $response->content);

                        }else if($checkData!=NULL && $data->orderStatus != 'FP'){
                            // return 'masuk elseif';
                            // dd($data);
                            \DB::table('order_from_marketplace')
                                ->where('orderId', $data->orderNo)
                                ->update(['status' => 'inactive']);
                                // return 'masuk elseif';
                        }else{
                            echo 'sudah ada';
                        }
                        // return 'tidak masuk keduanya';


                    }
                }else{
                    echo 'kosong';
                }

            }
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
                // dd('masuk sini');
                // dd($e->getResponse());
                dd($e->getResponse()->getBody()->getContents());

            }

        return 'Success';
    }

    public function orderStatusBlibli(){
        $sellerCode = [];

        $sellerCode[0]['sellerCode'] = 'BAB-70101';
        $sellerCode[0]['nmToko'] = 'Bardi Banten Official';
        $sellerCode[0]['sellerApiKey'] = '299113531B5CB1B97B71AAA77756947A547F7D15820796843FE904C8BB15460E';
        $sellerCode[1]['sellerCode'] = 'WAM-70000';
        $sellerCode[1]['nmToko'] = 'Warisan Majapahit';
        $sellerCode[1]['sellerApiKey'] = 'C9F7F44E8E5BD1B40E96456DD23F45A792E3EEAE60F12C7A73030ACBDB3A3DA7';
        $sellerCode[2]['sellerCode'] = 'WAG-70030';
        $sellerCode[2]['nmToko'] = 'Warisan Gajahmada Surabaya';
        $sellerCode[2]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //Error "errorCode":"ERR-MA403015 "errorMessage":"Seller's postlive status doesn't fulfill the criteria to access the data"
        $sellerCode[3]['sellerCode'] = 'WAG-70024';
        $sellerCode[3]['nmToko'] = 'Warisan Gajahmada Cirebon';
        $sellerCode[3]['sellerApiKey'] = '06A825311F5952C887427318A3E0AE60101B7F2FA76D3313B0D10E2A655F38ED';
        //EndError
        $sellerCode[10]['sellerCode'] = 'WAG-70023';
        $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Jogjakarta';
        $sellerCode[10]['sellerApiKey'] = '280E783B67C9B1667A3962C4D16FA729F65FE6ADBB455E971B75C56D450B2B65';
        $sellerCode[4]['sellerCode'] = 'WAG-70021';
        $sellerCode[4]['nmToko'] = 'Warisan Gajahmada Bali';
        $sellerCode[4]['sellerApiKey'] = '1DD1633C4262CA0D78735132000ACBD93CDB90AE4DCDDA2DCB90339B1BDD268C';
        $sellerCode[5]['sellerCode'] = 'WAG-60046';
        $sellerCode[5]['nmToko'] = 'Warisan Gajahmada';
        $sellerCode[5]['sellerApiKey'] = '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1';
        //Data baru
        // $sellerCode[6]['nmToko'] = 'Warisan Gajahmada Bogor';
        // $sellerCode[6]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[6]['sellerCode'] = 'WAG-70013';
        // $sellerCode[7]['nmToko'] = 'Warisan Gajahmada Bekasi';
        // $sellerCode[7]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[7]['sellerCode'] = 'WAG-70010';
        // $sellerCode[8]['sellerCode'] = 'WAG-70004';
        // $sellerCode[8]['nmToko'] = 'Warisan Gajahmada JakSel';
        // $sellerCode[8]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[9]['sellerCode'] = 'WAG-70003';
        // $sellerCode[9]['nmToko'] = 'Warisan Gajahmada JakUt';
        // $sellerCode[9]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[10]['sellerCode'] = 'WAG-70011';
        // $sellerCode[10]['nmToko'] = 'Warisan Gajahmada Jakbar';
        // $sellerCode[10]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        // $sellerCode[11]['sellerCode'] = 'WAG-70012';
        // $sellerCode[11]['nmToko'] = 'Warisan Gajahmada Depok';
        // $sellerCode[11]['sellerApiKey'] = '39412629FF252A00D9D1C40F612CAECBD1DD8DD015FB011B815BDD34CFF8AC41';
        //WHW
        $sellerCode[12]['sellerCode'] = 'WAH-60054';
        $sellerCode[12]['nmToko'] = 'Warisan Hayam Wuruk Bali';
        $sellerCode[12]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';

        $sellerCode[13]['sellerCode'] = 'WAH-60053';
        $sellerCode[13]['nmToko'] = 'Warisan Hayam Wuruk Surakarta';
        $sellerCode[13]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';

        $sellerCode[14]['sellerCode'] = 'WAH-60057';
        $sellerCode[14]['nmToko'] = 'Warisan Hayam Wuruk Bandung';
        $sellerCode[14]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[15]['sellerCode'] = 'WAH-60050';
        $sellerCode[15]['nmToko'] = 'Warisan Hayam Wuruk Yogyakarta';
        $sellerCode[15]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[16]['sellerCode'] = 'WAH-60055';
        $sellerCode[16]['nmToko'] = 'Warisan Hayam Wuruk Semarang';
        $sellerCode[16]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[17]['sellerCode'] = 'WAH-60051';
        $sellerCode[17]['nmToko'] = 'Warisan Hayam Wuruk Surabaya';
        $sellerCode[17]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[18]['sellerCode'] = 'WAH-60049';
        $sellerCode[18]['nmToko'] = 'Warisan Hayam Wuruk Pusat';
        $sellerCode[18]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[19]['sellerCode'] = 'WAH-60052';
        $sellerCode[19]['nmToko'] = 'Warisan Hayam Wuruk Malang';
        $sellerCode[19]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[20]['sellerCode'] = 'WAH-60056';
        $sellerCode[20]['nmToko'] = 'Warisan Hayam Wuruk Tasikmalaya';
        $sellerCode[20]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';
        $sellerCode[21]['sellerCode'] = 'WAH-60058';
        $sellerCode[21]['nmToko'] = 'Warisan Hayam Wuruk Bogor';
        $sellerCode[21]['sellerApiKey'] = '565B95D341E4D886AFC3017AEFBB2936A2B98F6A8716BC43509D1CA2F89D7431';

        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");

        $getAllOrders = \DB::table('order_from_marketplace')->where('platform', 'Blibli')->get();
        $allOrders = collect($getAllOrders)->filter(function ($order) {return $order->order_status != 'D';});
        // dd($allOrders);

        foreach ($allOrders as $key => $value) {
            // dd($value);
            $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$value->orderId.'&orderItemNo='.$value->item_no.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $seller['sellerApiKey']
                ]])->getBody();
            // dd($getDetail);
            $getDetail = $getDetail->getContents();
            $responseDetail = json_decode($getDetail);
            $detailOrder = $responseDetail->value;
            $status = $detailOrder->orderStatus;
            // dd($getDetail);
            if($value->order_status != $status){
                $updateOrderStatus = DB::table('order_from_marketplace')->where('Invoice', $invoice)->update([
                    'order_status'=>$status,
                ]);
                if($status == 'PU'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'barang siap dikirim',
                    ]);
                }elseif($status == 'OS'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pesanan dikirim',
                    ]);
                }
                elseif($status == 'PU'){
                    $updateOrderStatusBookingan = DB::table('bookingan')->where('kode', $invoice)->update([
                        'status'=>'pengiriman selesai',
                    ]);
                }

            }
        }
    }

     public function getBlibliProduct(){
        $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->get();
        // $getShops = \DB::table('Registered_store')->where('id','>', 73)->where('platform', 'Blibli')->get();
        foreach($getShops as $shop){
            $shopId= $shop->shop_id;
            $sellerKey = $shop->seller_api_key;
            $toko = $shop->data_toko;
            // dd($shop->seller_api_key);
            $page = 0;

            do{
                // echo $shopId;
                $next = $this->getTheProduct($page, $shopId, $sellerKey, $toko);
                $page = $page + 1;
            }while($next == true);



        }
        echo 'sukses';
    }

    public function getTheProduct($page, $shopId, $sellerKey, $toko){
        try{
            $client = new Client();
            $clientId = env('BLIBLI_CLIENT_ID');
            $clientKey = env('BLIBLI_CLIENT_KEY');
            $Authorization = base64_encode("{$clientId}:{$clientKey}");
            // dd($page, $shopId, $sellerKey, $toko);
            echo 'page: '.$page.' '.$shopId.'<br/>';
             $getProducts = $client->post('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v2/product/getProductList?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$shopId.'&username=daniel.yokesen@gmail.com&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $sellerKey
                    ],
                        'json'=>[
                            'buyable'=>true,
                            'page'=>$page,
                            'size'=>30
                            ]
                    ])->getBody();
                    $getDetail = $getProducts->getContents();
                    $responseDetailProducts = json_decode($getDetail);
                    $sizePerPage = $responseDetailProducts->pageMetaData->pageSize;
                    $totalRecords = $responseDetailProducts->pageMetaData->totalRecords;
                    $pageNumber = $responseDetailProducts->pageMetaData->pageNumber + 1;
                    $products = $responseDetailProducts->content;
                    // dd($responseDetailProducts, $products, $toko);
                    $totalgetProduct = $sizePerPage * $pageNumber;
                    if(count($products) > 0){
                        foreach($products as $key=>$product){
    
                            // echo $product->productName.' '.$toko.'<br/>';
                            $check = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->count();
                            echo $check.' produk<br/>';
    
                            if($check < 1){
                                $insertProduk = \DB::table('produk')->insertGetId([
                                    'product_name'=>$product->productName,
                                    'gudang_id'=>$toko,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    ]);
                                $insert = \DB::table('produk_marketplace')->insert([
                                    'parent_produk_id'=>$insertProduk,
                                    'produk_id'=>(string)$product->gdnSku,
                                    'stock'=>$product->stockAvailableLv2,
                                    'status'=>'active',
                                    'price'=>$product->sellingPrice,
                                    'platform'=>'Blibli',
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'shop_id'=>$shopId,
                                    'sku'=>$product->merchantSku,
                                    'page'=>$page,
                                    'photo'=>$product->image
                                    ]);
                            }else{
                                $updateProduk = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->update([
                                    'product_name'=>$product->productName,
                                    'gudang_id'=>$toko,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    ]);
    
                                $check = \DB::table('produk_marketplace')->where('produk_id', $product->gdnSku)->where('platform', 'Blibli')->count();
    
                                $getParentId = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->first();
                                if($check < 1){
                                    $insert = \DB::table('produk_marketplace')->insert([
                                    'parent_produk_id'=>$getParentId->id,
                                    'produk_id'=>$product->gdnSku,
                                    'stock'=>$product->stockAvailableLv2,
                                    'status'=>'active',
                                    'price'=>$product->sellingPrice,
                                    'platform'=>'Blibli',
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'shop_id'=>$shopId,
                                    'sku'=>$product->merchantSku,
                                    'page'=>$page,
                                    'photo'=>$product->image
                                    ]);
                                }else{
                                   $update = \DB::table('produk_marketplace')->where('produk_id', $product->gdnSku)->where('platform', 'Blibli')->update([
                                    'parent_produk_id'=>$getParentId->id,
                                    'produk_id'=>$product->gdnSku,
                                    'stock'=>$product->stockAvailableLv2,
                                    'status'=>'active',
                                    'price'=>$product->sellingPrice,
                                    'platform'=>'Blibli',
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'shop_id'=>$shopId,
                                    'sku'=>$product->merchantSku,
                                    'page'=>$page,
                                    'photo'=>$product->image
                                    ]);
                                }
    
                            }
                            if($product->stockAvailableLv2 < 5){
                              $notifSendTo = DB::table('cms_users')->where(function ($query) {
                                $query->where('id_cms_privileges', '=', 1)
                                      ->orWhere('id_cms_privileges', '=', 2);
                                    })
                                      ->pluck('id');
                                $tokoNotif = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                                $storeName = $toko->store_name;
                                // $config['content'] = "Stock Product $product->productName di Blibli ".$storeName." kurang dari 5";
                                // // $config['to'] = $tokoNotif->link_toko;
                                // $config['id_cms_users'] = $notifSendTo;
                                // \CRUDBooster::sendNotification($config);
                                // dd($storeName, $product->productName);
                           }
                        }
                    }
                    
                    if($totalgetProduct < $totalRecords){
                        // echo 'lebih';
                        return true;
                    }else{
                        // echo 'sama';
                        return false;
                    }
        }catch(\GuzzleHttp\Exception\ClientException $e){
            dd(json_decode($e->getResponse()->getBody()->getContents()));
        }
    }

    public function tesSku(){
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        try{
                $getProducts = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/product/detailProduct?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode=WAG-60046&gdnSku=WAG-60046-00066-00001&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => '3FE68EF43ED12FF6534024952D1E31E5210A5660989134025241136DA576ABE1'
                ]])->getBody();
                $getDetail = $getProducts->getContents();
                $data = json_decode($getDetail);
                dd($getDetail, $data);
        }catch (\GuzzleHttp\Exception\ClientException $e) {
                // dd('masuk sini');
                // dd($e->getResponse());
                dd(json_decode($e->getResponse()->getBody()->getContents()));

            }

    }

    public function sellerVoucher(){
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");

        //  $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->get();
         $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->where('id','>', 26)->get();
        //  dd($getShops);
         foreach($getShops as $shop){
             try{
                 $storeCode = $shop->shop_id;
                 $sellerKey = $shop->seller_api_key;
                //  dd($sellerKey, $storeCode, $Authorization);
                $getVoucher = $client->post('https://api.blibli.com/v2/proxy/seller/v1/promos/vouchers/filter?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&username=daniel.yokesen@gmail.com&storeId=10001&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $sellerKey
                ],
                    'json'=>[
                        "filter"=>[
                            'voucherStatus'=>'EXPIRED'
                        ]
                    ]
                ])->getBody();
                $getDetail = $getVoucher->getContents();
                $data = json_decode($getDetail);
                // dd($data);
                if($data->paging->totalPage > 2){
                    dd($storeCode);
                }

                if(!empty($data->content)){
                    dd($storeCode);
                    foreach($data->content as $data){
                        $insert = \DB::table('voucher_blibli')->insert([
                        'name'=>$data->voucher->name,
                        'code'=>$data->voucher->code,
                        'toko'=>$shop->store_name
                        ]);
                    }
                }
                echo 'nothing<br/>';
            }catch (\GuzzleHttp\Exception\ClientException $e) {
                    // dd('masuk sini');
                    // dd($e->getResponse());
                    dd($e->getResponse()->getBody()->getContents());

                }
         }
    }
    public function sellerDiscount(){
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");

         $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->get();
        //  $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->where('id',25)->get();
        //  dd($getShops);
         foreach($getShops as $shop){
             try{
                 $storeCode = $shop->shop_id;
                 $sellerKey = $shop->seller_api_key;
                //  dd($sellerKey, $storeCode, $Authorization);
                $getVoucher = $client->post('https://api.blibli.com/v2/proxy/seller/v1/promos/discounts/filter?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&username=daniel.yokesen@gmail.com&storeId=10001&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $sellerKey
                ],
                    'json'=>[
                        "filter"=>[
                            'voucherStatus'=>'ACTIVE'
                        ]
                    ]
                ])->getBody();
                $getDetail = $getVoucher->getContents();
                $data = json_decode($getDetail);
                // dd($data->content);
                if($data->paging->totalPage > 1 ){
                    dd($storeCode);
                }
                if(!empty($data->content)){
                    foreach($data->content as $data){
                        $insert = \DB::table('blibli_promo')->insert([
                        'name'=>$data->name,
                        'code'=>$data->code,
                        'toko'=>$shop->store_name
                        ]);
                    }
                }
            }catch (\GuzzleHttp\Exception\ClientException $e) {
                    // dd('masuk sini');
                    // dd($e->getResponse());
                    dd($e->getResponse()->getBody()->getContents());

                }
         }
    }
    public function blibliDiscount(){
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");

         $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->get();
        //  $getShops = \DB::table('Registered_store')->where('status', 'active')->where('platform', 'Blibli')->where('id',25)->get();
        //  dd($getShops);
         foreach($getShops as $shop){
             try{
                 $storeCode = $shop->shop_id;
                 $sellerKey = $shop->seller_api_key;
                //  dd($sellerKey, $storeCode, $Authorization);
                $getVoucher = $client->post('https://api.blibli.com/v2/proxy/seller/v1/promos/blibli/filter?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&username=daniel.yokesen@gmail.com&storeId=10001&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                'Authorization'=> "Basic $Authorization",
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1',
                'Api-Seller-Key' => $sellerKey
                ],
                    'json'=>[
                        "filter"=>[
                            'voucherStatus'=>'ACTIVE'
                        ],
                        "paging"=>[
                            'size'=>40
                        ]
                    ]
                ])->getBody();
                $getDetail = $getVoucher->getContents();
                $data = json_decode($getDetail);
                // dd($data->content, $data);
                // if($data->paging->totalPage > 1 ){
                //     dd($storeCode);
                // }
                if(!empty($data->content)){
                    foreach($data->content as $data){
                        $insert = \DB::table('blibli_promo')->insert([
                        'name'=>$data->promoName,
                        'code'=>$data->promoCode,
                        'toko'=>$shop->store_name
                        ]);
                    }
                }
            }catch (\GuzzleHttp\Exception\ClientException $e) {
                    // dd('masuk sini');
                    // dd($e->getResponse());
                    dd($e->getResponse()->getBody()->getContents());

                }
         }
    }
    public function getBlibliDiscount(){
        // $shops = \DB::table('Registered_store')->where('platform', 'Blibli')->where('status', 'active')->get();
        $shops = \DB::table('Registered_store')->where('id', 16)->where('platform', 'Blibli')->where('status', 'active')->get();
        // dd($shops);
        $blibliOrders = [];
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        try {
            foreach($shops as $shop){
                $shopId = $shop->shop_id;
                $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderList?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$shopId.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&status=D', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $shop->seller_api_key
                    ]])->getBody();
                $res = $res->getContents();
                $response = json_decode($res);
                $datas = $response->content;
                // dd($datas);
                foreach($datas as $index=>$data){
                         //If belum ada dan pesanan baru
                        // dd($data->customerFullName);
                        $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$data->orderNo.'&orderItemNo='.$data->orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                        // $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo=12078284035&orderItemNo=12108951922&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $shop->seller_api_key
                            ]])->getBody();
                        $getDetail = $getDetail->getContents();
                        $responseDetail = json_decode($getDetail);
                        $detailOrder = $responseDetail->value;
                        // dd($detailOrder);
                        if(!empty($detailOrder->combo) || $detailOrder->financeSettlementInfo->merchantPromo != 0 || !empty($detailOrder->wholesale)){
                            dd($detailOrder, $index, $shopId);

                        }
                        echo 'nothing<br/>';
                }
            }
        }catch(\GuzzleHttp\Exception\ClientException $e){
            dd($e->getResponse()->getBody()->getContents());
        }
    }

    public function cekBlibli(){
        $client = new Client();
        $clientId = env('BLIBLI_CLIENT_ID');
        $clientKey = env('BLIBLI_CLIENT_KEY');
        $Authorization = base64_encode("{$clientId}:{$clientKey}");
        $orderNo =12092027938;
        $orderItemNo=12127987811;
        $shop = \DB::table('Registered_store')->where('platform', 'Blibli')->where('status', 'active')->where('id',35)->first();
        $shopId = $shop->shop_id;
        $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$oprderNo.'&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
            'Authorization'=> "Basic $Authorization",
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1',
            'Api-Seller-Key' => $shop->seller_api_key
            ]])->getBody();
        $getDetail = $getDetail->getContents();
        $responseDetail = json_decode($getDetail);
        $detailOrder = $responseDetail->value;
        dd($detailOrder, $getDetail);
        // combine shipping
        // $orderItemNo = $data->orderItemNo;
        $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/getCombineShipping?businessPartnerCode='.$shopId.'&requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
            'Authorization'=> "Basic $Authorization",
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1',
            'Api-Seller-Key' => $shop->seller_api_key
            ]])->getBody();
        $res = $res->getContents();
        $responseCombine = json_decode($res);
        $datas = $responseCombine->value->combineShipping;
        $combine = $responseCombine->value->alreadyCombined;
        // dd($responseCombine);
        // dd($datas,$detailOrder, $getDetail, $responseCombine);
        $itemList = [];
        if($combine == false){
            foreach($datas as $key=>$value){
                array_push($itemList, (string) $value->orderItemNo);
            }
        }else{
            foreach($datas as $key=>$value){
                array_push($itemList, (string) $value->orderItemNo);
            }
        }
        // dd($itemList);
        // $insertId = 20687;
        // $update = \DB::table('bookingan')->where('id', $insertId)->where('platform', 'Blibli')->update([
        //     'invoice_toko'=>$filePath,
        //     'logistic_type'=>$logistic_type,
        //     'jmlh_item'=>$qty,
        //     'totalBayar'=>$totalPrice,
        // ]);
        // // dd($shopId)
        // // dd($insertId, $itemList, $responseCombine, $responseDetail, $shop, $datas);
        // create package
        $res = $client->post('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/createPackage?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&businessPartnerCode='.$shopId.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
            'Authorization'=> "Basic $Authorization",
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1',
            'Api-Seller-Key' => $shop->seller_api_key
        ],
            'json'=>[
                'orderItemIds'=>$itemList
            ]
        ])->getBody();
        $res = $res->getContents();
        $response = json_decode($res);
        $datasCreate = $response->value;
        dd($response, $res, $responseCombine, $responseDetail,$itemList, $shop, $datas);
        // shipping label
        // $packageId = $detailOrder->packageId;
        $packageId = 2799512470;
        $storeId = 10001;
        $sellerCode = 'BAB-70101';
        $kode = 12091641846;
        // dd($packageId, $sellerCode, $custName, $shop->seller_api_key);
        $clientLabel = new Client();
        $getShippingLabel = $clientLabel->get("https://api.blibli.com/v2/proxy/seller/v1/orders/$packageId/shippingLabel?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40eb&username=daniel.yokesen@gmail.com&storeId=$storeId&storeCode=$sellerCode&channelId=PT. YOKESEN TEKNOLOGI", ['headers' => [
            'Authorization'=> "Basic $Authorization",
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1',
            'Api-Seller-Key' => $shop->seller_api_key
            ]])->getBody();
        $getShippingLabel = $getShippingLabel->getContents();
        $responseShippingLabel = json_decode($getShippingLabel);
        $shippingLabel = $responseShippingLabel->content->shippingLabel;
        date_default_timezone_set("Asia/Jakarta");
        $file = base64_decode($shippingLabel);
        $uniqueFileName = str_replace(" ","",time()."-".$kode.'.pdf');
        \Storage::put('public/label/'.$uniqueFileName, $file);
        $filePath = 'storage/label/'.$uniqueFileName;
        // dd($qty, $shippingLabel, $filePath);
        $update = \DB::table('bookingan')->where('id', 20687)->where('platform', 'Blibli')->update([
            'buktiBayar' => $filePath,
        ]);
        dd($responseShippingLabel, $datasCreate, $datas, $response, $responseCombine);
        // $config['content'] = "Order Masuk di Blibli ".$shop->store_name;
        // $config['to'] = '#';
        // // $config['to'] = $seller['nmToko'];
        // $config['id_cms_users'] = $notifSendTo;
        // \CRUDBooster::sendNotification($config);
    }
}
