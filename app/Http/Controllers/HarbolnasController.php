<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use CRUDBooster;
ini_set('max_execution_time', '500');
set_time_limit(500);

class HarbolnasController extends Controller
{
    public function getData(Request $request){
        $data = $request->json()->all();

        // return $data['nama'];

        $getStation = DB::table('station')->where('first_queue', 1)->first();
        $nextStation = $getStation->id + 1;
        if($nextStation > 2){
            $nextStation = 1;

        }
        $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
            'first_queue'=>0,
        ]);
        $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
            'first_queue'=>1,
            'status'=>'empty',
        ]);
        $orderId = $data['id'];
        $logistic_type = 'dropoff';
        $order_sn = $data['order_id'];
        $pdfQr = new \PDF();
        $customPaperQr = array(0,0,270.28,270.64);
        $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
        $nameQr = $orderId.'.pdf';
        \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
        $content = $pdfQr->download($nameQr);
        $filePath = 'storage/sticker/'.$nameQr;

        // // label
        // $pdf = new \PDF();
        // // $customPaper = array(0,0,500.28,600.64);
        // $pdf = \PDF::loadView('admin.invoicePdf', compact('data'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
        // $name = 'Harbolnas-'.$orderId.'.pdf';
        // \Storage::put('public/label/'.$name, $pdf->output());
        // $path = "storage/label/$name";

        $insertData = DB::table('bookingan')->insertGetId([
            'phone'=>$data['no_wa'],
            'nama_pemesan'=>$data['nama'],
            'jmlh_item'=>1,
            'totalBayar'=>10100,
            'kode'=>$data['order_id'],
            'platform'=>'harbolnas',
            'kurir'=>'ID Express',
            'alamat'=>$data['alamat'],
            'status'=>'logistic',
            'created_at'=>date('Y-m-d H:i:s'),
            'logistic_type'=>'dropoff',
            'status'=>'packing',
            // 'station'=>$getStation->id,
            'station'=>3,
            'invoice_toko'=>$filePath,
            'buktiBayar'=>$path
        ]);

        $insertItem = DB::table('order_from_marketplace_detail')->insert([
            'orderId'=>$insertData,
            'item'=>'1 Bundle Larutan Penyegar Cap Badak',
            'product_id'=>1,
            'qty'=>1,
            'sku'=>'HARBOLNAS-1',
        ]);

        $getLabel = $this->invoiceHarbolnas($insertData);

    }

    public function invoiceHarbolnas(){
    // public function invoiceHarbolnas(){
    //   $data = DB::table('bookingan')->where('id', $id)->first();
      $datas = DB::table('bookingan')->where('platform', 'harbolnas 1111')->get();
    //   dd($datas);
      foreach($datas as $data){
        $id = $data->id;
        $pdf = new \PDF();

        $pdf = \PDF::loadView('admin.invoicePdf', compact('data'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
        $name = 'Harbolnas1111-'.$id.'.pdf';;
        \Storage::put('public/label/'.$name, $pdf->output());
        $path = "storage/label/$name";

        $update = DB::table('bookingan')->where('platform', 'harbolnas 1111')->where('id', $id)->update([
            'buktiBayar'=>$path
        ]);
      }

    }

     public function orderHarbolnas(){
        // $clientGetOrderHarbolnas = new Client();
        // $resOrder = $clientGetOrderHarbolnas->get("https://data.sobatbadak.club/api/harbolnas1111-order-teratur")->getBody();
        // $responseGetOrder = json_decode($resOrder);
        // dd($responseGetOrder, $resOrder);
        // $orderCed = array();
        // $orderCount = 0;
        // unset($value);
        $responseGetOrder = DB::table('bookingan')->where('platform', 'harbolnas 1111')->where('kode', 'ZuW7DFXFjh3Xmcg')->get();

        foreach($responseGetOrder as $key=>$data){
            // if($value->provinsi == 'jabodetabek' && $orderCount < 200){
            // $data = DB::table('bookingan')->where('platform', 'harbolnas 1111')->get();
            // echo $check.' satu<br/>';
            // if($check < 1){
                // dd($value->nama);
                // echo $value->id.'<br/>';
                // $orderCount++;
                // qr
                // $logistic_type = 'dropoff';
                // $order_sn = $value->order_id;
                // $pdfQr = new \PDF();
                // $customPaperQr = array(0,0,270.28,270.64);
                // $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                // $nameQr = $orderId.'.pdf';
                // \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                // $content = $pdfQr->download($nameQr);
                // $filePath = 'storage/sticker/'.$nameQr;
                // //label
                // dd($data);
                $id = $data->id;
                $pdf = new \PDF();
                $customPaper = array(0,0,600.28,620.64);
                $pdf = \PDF::loadView('admin.invoicePdf', compact('data'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true])->setPaper($customPaper);
                $name = 'Harbolnas1111-'.$id.'.pdf';;
                \Storage::put('public/label/'.$name, $pdf->output());
                $path = "storage/label/$name";

                // assign station
                // $getStation = DB::table('station')->where('first_queue', 1)->first();
                // $nextStation = $getStation->id + 1;
                // if($nextStation > 2){
                //     $nextStation = 1;
                //
                // }
                //
                // $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                //     'first_queue'=>0,
                // ]);
                // $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                //     'first_queue'=>1,
                //     'status'=>'empty',
                // ]);
                // $checkOrder = DB::table('bookingan')->where('kode', $value->order_id)->count();
                //     if($checkOrder < 1){
                //       echo $checkOrder.' <br/>';
                //         $insertOrder = DB::table('bookingan')->insertGetId([
                //         'nama_pemesan'=>$value->nama,
                //         'phone'=>$value->no_wa,
                //         'jmlh_item'=>1,
                //         'totalBayar'=>61000,
                //         'kode'=>$value->order_id,
                //         'platform'=>'harbolnas 1111',
                //         'kurir'=>'wahana',
                //         'toko'=>223,
                //         'alamat'=>$value->alamat,
                //         'buktiBayar'=>$path,
                //         'invoice_toko'=>$filePath,
                //         'status'=>'packing',
                //         'created_at'=>date('Y-m-d H:i:s'),
                //         // 'station'=>$getStation->id,
                //          'station'=>3,
                //         'logistic_type'=>$logistic_type,
                //         'kota'=>$value->provinsi,
                //     ]);

                //     $insertItem = DB::table('order_from_marketplace_detail')->insert([
                //         'orderId'=>$insertOrder,
                //         'item'=>'1 Bundle Larutan Penyegar Cap Badak',
                //         'qty'=>1,
                //         'hargaSatuan'=>61000,
                //         'hargaTotal'=>61000,
                //         'sku'=>'HARBOLNAS-1111',
                //     ]);
                //     // array_push($orderTeratur, array_splice($responseGetOrder, $key, 1)[0]);
                //     $orderCount++;
                //     }
                // }else{
                //     // array_push($orderCed, $value);
                // }

            //   dd('tes');

        }

        // $data = $orderCed;
        // $clientHarbolnasCed = new Client();
        // $resHarbolnasCed = $clientHarbolnasCed->post("https://gudang.warisangajahmada.com/ced-harbolnas", [
        //     'json'=>$data
        // ])->getBody()->getContents();
        // dd($orderCed, $responseGetOrder, $orderCount);
    }
    
    public function harbolnas1212(){
        
        $client = new Client();
        $res = $client->get("https://data.sobatbadak.club/api/get-harbolnas1212")->getBody();
        $response = json_decode($res);
        // return response()->json($response);
        foreach($response as $data){
            // return response()->json($data);
            $check = DB::table('bookingan')->where('platform', 'harbolnas 12.12')->where('kode', $data->uuid)->count();
            if($check < 1 ){
                
                // $getStation = DB::table('station')->where('first_queue', 1)->first();
                // $nextStation = $getStation->id + 1;
                // if($nextStation > 2){
                //     $nextStation = 1;
        
                // }
                // $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                //     'first_queue'=>0,
                // ]);
                // $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                //     'first_queue'=>1,
                //     'status'=>'empty',
                // ]);
                
                // invoice
                $pdf = new \PDF();
                // $customPaper = array(0,0,270.28,270.64);
                // dd($data);
                $pdf = \PDF::loadView('invoice', compact('data'));
                $name= 'Harbolnas1212'.$data->uuid.'.pdf';
                // echo 'sebelum';
                \Storage::put('public/label/'.$name, $pdf->output());
                // \Storage::disk('upcloud')->put('pdf/buktiBayar/'.$name, $pdf->output());
                // echo 'masuk';
                $content = $pdf->download($name);
                // $filePath = 'storage/label/'.$name;
                
                // qr
                $pdfQr = new \PDF();
                $customPaperQr = array(0,0,270.28,270.64);
                $order_sn = $data->uuid;
                $logistic_type = 'dropoff';
                $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                $nameQr = $order_sn.'.pdf';
                \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                // \Storage::disk('upcloud')->put('pdf/qr/'.$nameQr, $pdfQr->output());
                $content = $pdfQr->download($nameQr);
                // $filePathQr = 'storage/sticker/'.$nameQr;
                
                $pdfBuktiBayar = base64_encode($pdf->output());
                $pdfQr= base64_encode($pdfQr->output());
                $clientSendImage = new Client();
                
                $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                      'json'=>[
                          
                              'name'=>$name,
                              'contentBuktibayar'=> $pdfBuktiBayar,
                              'nameQr'=>$nameQr,
                              'contentQr'=>$pdfQr
                      ]
                ])->getBody()->getContents();
                $checkBookingan = DB::table('bookingan')->where('kode', $data->uuid)->where('platform', 'harbolnas 12.12')->count();
                if($checkBookingan < 1 ){
                    $insert = DB::table('bookingan')->insertGetId([
                        'phone'=>$data->whatsapp,
                        'nama_pemesan'=>$data->name,
                        'email'=>$data->email,
                        'jmlh_item'=>12,
                        'totalBayar'=>0,
                        'kode'=>$data->uuid,
                        'platform'=>'harbolnas 12.12',
                        // 'kurir'=>'dropoff',
                        'toko'=>223,
                        'alamat'=>$data->alamat_rumah,
                        'status'=>'packing',
                        'created_at'=>date('Y-m-d H:i:s'),
                        'station'=>3,
                        'logistic_type'=>'dropoff',
                        'kota'=>$data->kota,
                        // 'province'=>$data->provinsi,
                        'postalcode'=>$data->kode_pos,
                        'nation'=>'Indonesia',
                        'buktiBayar' => $name,
                        'invoice_toko' =>  $nameQr
                    ]);
                    
                    $chooseProduk = rand(1,2);
                    if($chooseProduk ==1 ){
                        $produk = DB::table('products')->where('id', 606)->first();
                        $products = DB::table('produk_marketplace')->where('platform', 'Tokopedia')->where('induk_barang_id', $produk->id)->first();
                        $stockOpname1= 300;
                        $stockOpname2 = 0;
                    }else{
                        $produk = DB::table('products')->where('id', 605)->first();
                        $products = DB::table('produk_marketplace')->where('platform', 'Tokopedia')->where('induk_barang_id', $produk->id)->first();
                        $stockOpname1= 157;
                        $stockOpname2 = 0;
                    }
                    
                    // check detail
                    $checkDetail = DB::table('order_from_marketplace_detail')->where('orderId', $insert)->count();
                    if($checkDetail < 1){
                        $insertDetail = DB::table('order_from_marketplace_detail')->insert([
                            'orderId'=>$insert,
                            'product_id'=>$products->produk_id,
                            'item'=>'Larutan Penyegar Cap Badak Kaleng Slim 238ml Riki Rhino  Random Rasa',
                            'qty'=>12,
                        ]);
                    }else{
                        echo 'detail sudah ada';
                    }
                    
                }else{
                    echo 'bookingan sudah ada';
                }
                
                // kurangi stock
                
                $getLastStock = DB::table('stock')->where('induk_produk_id', $produk->id)->orderBy('id','desc')->first();
                
                $getLastStockNumbers = DB::table('stock_number')->where('stock_id', $getLastStock->id)->get();
                $totalStock = 0;
                foreach($getLastStockNumbers as $number){
                    $convertion = DB::table('convertion')->where('id', $number->convertion_id)->first();
                    $convert = $number->bookStockUnit_induk * $convertion->stock_convertion_unit;
                    $totalStock = $totalStock + $convert;
                }
                
                // insert New Stock
                $check = DB::table('stock')->where('induk_produk_id', $produk_id)->where('documentNumber', $data->uuid)->count();
                if($check < 1){
                  $insertNewStock = DB::table('stock')->insertGetId([
                        'data_toko_id' => 223,
                        'induk_produk_id'=>$produk->id,
                        'tipe'=>'penjualan',
                        'created_at'=>date('Y-m-d H:i:s'),
                        'documentNumber'=>$data->uuid,
                    ]); 
                }else {
                    echo 'sudah ada';
                }
                
                $currentStock = $totalStock -12;
                $moduloSelisihUnitInduk = 0;
                $getAllConvertions = DB::table('convertion')->where('induk_produk_id', $produk->id)->get();
                foreach($getAllConvertions as $i=>$convertion){
                    // dd($convertion);
                    if($i == 0){
                        $modSelisihUnitInduk = $currentStock % $convertion->stock_convertion_unit;
                        $divSelisihUnitInduk  = $currentStock - $modSelisihUnitInduk;
                        $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertion->stock_convertion_unit;
                        $moduloSelisihUnitInduk = $modSelisihUnitInduk ;
                    }else {
                        $modSelisihUnitInduk = $moduloSelisihUnitInduk % $convertion->stock_convertion_unit;
                        $divSelisihUnitInduk  = $moduloSelisihUnitInduk - $modSelisihUnitInduk;
                        $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertion->stock_convertion_unit;
                        $moduloSelisihUnitInduk = $modSelisihUnitInduk;
                    }
                    
                    $checkNumber = DB::table('stock_number')->where('stock_id', $insertNewStock)->where('convertion_id', $convertion->convertion_id)->count();
                    if($checkNumber < 1){
                        if(count($getAllConvertions)- 1 == $i){
                            $insertNewStockNumber = DB::table('stock_number')->insert([
                                'stock_id'=>$insertNewStock,
                                'convertion_id'=>$convertion->id,
                                'unit'=>12,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpname2,
                                'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                        }else{
                            $insertNewStockNumber = DB::table('stock_number')->insert([
                                'stock_id'=>$insertNewStock,
                                'convertion_id'=>$convertion->id,
                                'unit'=>0,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpname1,
                                'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                        }
                        
                    }else {
                        'echo sudah ada';
                    }
                    
                }
                
            }else {
                echo 'sudah ada';
            }
            
        }
        echo 'sukses';
        // dd($response, $res);
    }
    
    public function changeStation(){
        $datas = DB::table('bookingan')->where('platform', 'Harbolnas 12.12')->where('id', '>', '19002')->where('id', '<', 19110)->get();
        // dd($datas);
        foreach($datas as $data){
            $insertDetail = DB::table('order_from_marketplace_detail')->insert([
                'orderId'=>$data->id,
                'product_id'=>$products->produk_id,
                'item'=>'Larutan Penyegar Cap Badak Kaleng Slim 238ml Riki Rhino  Random Rasa',
                'qty'=>12,
            ]);
        }
        // $getData = DB::table('bookingan')->where('platform', 'harbolnas 12.12')->where('status', 'packing')->limit(20)->get();
        // foreach($getData as $data){
        //     $update = DB::table('bookingan')->where('id', $data->id)->update([
        //         'station'=>4
        //     ]);
        // }
    }
    
    
}
