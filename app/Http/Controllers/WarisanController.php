<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;

class WarisanController extends Controller
{
    public function getOrderWarisan(Request $request){

      $kode = $request->invoice;
      $shop = DB::table('Registered_store')->where('Platform', 'Warisan')->where('status', 'active')->first();
      $logistic_type = 'dropoff';
      $check = DB::table('bookingan')->where('kode', $kode)->count();
        if($request->vendor_id == 18){
            $station = 3;
        }else{
            $station = 4;
        }
        
        $insertOrder = DB::table('bookingan')->insertGetId([
          'nama_pemesan'=> $request->nama_pemesan,
          'phone'=> $request->phone_number,
          'email'=>$request->email,
          'platform'=> 'Warisan',
          'kode'=>$kode,
          'kurir'=> $request->kurir,
          'alamat'=> $request->address,
          'status'=> 'packing',
          'created_at'=> date('Y-m-d H:i:s'),
          'station'=>$station,
          'logistic_type'=> $logistic_type,
          'totalBayar'=>$request->total_price,
          'jmlh_item'=>$request->qtyTotal,
          'toko'=>223,
          'invoice_toko'=>$filePath,
          'status'=>'packing',
          'created_at'=>date('Y-m-d H:i:s'),
          'nmToko'=>'warisan-1',
          'kota'=>$request->city,
          'payment_method'=>$request->payment_method,
          'station_queue_time'=>date('Y-m-d H:i:s'),
          'shippingCost'=>$request->shippingCost
        ]);

        foreach($request->products as $product){
          // return response()->json($product['id']);
          $insertDetail = DB::table('order_from_marketplace_detail')->insert([
            'orderId'=>$insertOrder,
            'product_id'=>$product['id'],
            'item'=>$product['product_name'],
            'qty'=>$product['qty'],
            'hargaSatuan'=>$product['price'],
            'hargaTotal'=>$product['total_price'],
            'sku'=>$product['sku']
          ]);
        }
  
      // qr
      $orderId = $insertOrder;
      $order_sn = $kode;
      $pdfQr = new \PDF();
      $customPaperQr = array(0,0,270.28,270.64);
      $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
      $nameQr = $orderId.'.pdf';
      \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
      $content = $pdfQr->download($nameQr);
      $filePath = 'storage/sticker/'.$nameQr;
      $updateQr = DB::table('bookingan')->where('id', $insertOrder)->update([
        'invoice_toko'=>$nameQr
      ]);
       $this->createInvoiceWarisan($insertOrder);
       
       $pdfQr= base64_encode($pdfQr->output());
        $clientSendImage = new Client();
        
        $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
              'json'=>[
                      'nameQr'=>$nameQr,
                      'contentQr'=>$pdfQr
              ]
        ])->getBody()->getContents(); 
        // $station = 3;
        // foreach($request->products as $product){
        //   $contains = Str::contains($product['product_name'], 'jeruk');
        // //   $contains = str_contains($product['product_name'], 'jeruk');
        //   if($contains == true){
        //       $station = 4;
        //   }
        // }
        // $updateStaion = DB::table('bookingan')->where('id',$insertOrder)->update([
        //     'station'=>$station
        // ]);
        return response()->json($insertOrder, 200);
        
        
    }

    public function getStation(){
      $getStation = DB::table('station')->where('first_queue', 1)->first();
      $nextStation = $getStation->id + 1;
      if($nextStation > 2){
          $nextStation = 1;

      }
      $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
          'first_queue'=>0,
      ]);
      $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
          'first_queue'=>1,
          'status'=>'empty',
      ]);

      return $getStation->id;
    }

    public function getWarisanProduct(){
      $clientProduct = new Client();
      $resProduct = $clientProduct->get("")->getBody();
      $responseProduct = json_decode($resProduct);

      $insert = DB::table('produk')->insertGetId([
        'product_name'=>'',
        'gudang_id'=>223,
        'created_at'=>date('Y-m-d H:i:s')
      ]);

      // $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
      //   'parent_produk_id'=>$insert,
      //   'produk_id'=>,
      //   'photo'=>,
      //   'sku'=>,
      //   'status'=>,
      //   'price'=>,
      //   'platform'=>,
      //   'created_at'=>date('Y-m-d H:i:s'),
      //   'shop_id'=>,
      //   'platform'=>,
      // ])
    }

    public function createHashKey(){
      $clients = DB::table('clients')->get();
      foreach($clients as $client){
        $hashKey = md5($client->name);
        $update = DB::table('clients')->where('id', $client->id)->update([
          'hash_key'=>$hashKey,
          'updated_at'=>date('Y-m-d H:i:s')
        ]);
      }
      echo 'sukses';
    }

    public function searchProductsWarisan(Request $request){
      if ($request->has('q')) {
          $cari = $request->q;
          $client = DB::table('clients')->where('id', 1)->first();
          $data = DB::table('products')->where('productName','LIKE','%'.$cari.'%')->where('brand_id', 223)->where('client_id', 1)->get();
          // $data = DB::table('produk')->where('product_name','LIKE','%'.$cari.'%')->where('gudang_id', $id)->get();
          // $stringToHash = $data[0]->productName.$client;
          // $signature = hash('sha256',$stringToHash);
          // return response()->json([$data, $signature]);\
          return response()->json($data);
      }
    }

    public function createProductToTeratur(Request $request){
      $check = DB::table('produk')->where('produk.product_name', $request->product_name)->join('produk_marketplace', 'produk.id', 'produk_marketplace.parent_produk_id')->count();
      if($request->status == 1){
        $status = 'active';
      }else{
        $status = 'Inactive';
      }
      if($check < 1){
        $insertProduk = DB::table('produk')->insertGetId([
          'product_name'=>$request->product_name,
          'gudang_id'=>223,
          'created_at'=>date('Y-m-d H:i:s')
        ]);
        $insertDetail = DB::table('produk_marketplace')->insert([
          'parent_produk_id'=>$insertProduk,
          'produk_id'=>$request->product_id,
          'photo'=>$request->photo,
          'sku'=>$request->sku,
          'status'=>$status,
          'price'=>$request->price,
          'platform'=>'Warisan',
          'created_at'=>date('Y-m-d H:i:s'),
          'shop_id'=>'warisan-1',
          'product_type'=> $request->product_type,
          'client_id'=>1,
          'description'=>$request->description,
        ]);

        return response()->json(['message'=>'product has been created', 'status'=>'Berhasil'], 200);
      }else{
        return response()->json(['message'=>'product already exist', 'status'=>'Gagal'], 400);
      }
    }

    public function getProduct(){
      $clientProduct = new Client();
      $resProduct = $clientProduct->get("https://api.warisan.co.id/api/v1/products")->getBody();
      $responseProduct = json_decode($resProduct);
    //   dd($responseProduct->data);
      $datas = $responseProduct->data;
      foreach($datas as $data){
        $check = DB::table('produk')->where('product_name', $data->product_name)->count();
        if($data->status ==1){
          $status = 'active';
        }else{
          $status='Inactive';
        }
        if($check < 1){
          $insert = DB::table('produk')->insertGetId([
            'product_name'=>$data->product_name,
            'gudang_id'=>223,
            'created_at'=>date('Y-m-d H:i:s')
          ]);
          $checkProdukMarketplace = DB::table('produk_marketplace')->where('sku', $data->sku)->where('platform', 'Warisan')->count();
          if($checkProdukMarketplace < 1){
            $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
              'parent_produk_id'=>$insert,
              'produk_id'=>$data->id,
              'photo'=>'https://api.warisan.co.id/'.$data->img,
              'sku'=>$data->sku,
              'status'=>$status,
              'price'=>$data->price,
              'platform'=>'Warisan',
              'created_at'=>date('Y-m-d H:i:s'),
              'shop_id'=>'warisan-1',
              'client_id'=>1,
              'product_type'=>$data->product_type,
              'description'=>$request->description,
            ]);
          }else{
            $updateProdukMarketplace = DB::table('produk_marketplace')->where('sku', $getProduk->sku)->where('platform', 'Warisan')->update([
              'parent_produk_id'=>$insert,
              'produk_id'=>$data->id,
              'photo'=>'https://api.warisan.co.id/'.$data->img,
              'sku'=>$data->sku,
              'status'=>$status,
              'price'=>$data->price,
              'platform'=>'Warisan',
              'updated_at'=>date('Y-m-d H:i:s'),
              'shop_id'=>'warisan-1',
              'client_id'=>1,
              'product_type'=>$data->product_type,
              'description'=>$request->description,
            ]);
          }


        }else{
            echo 'update <br/>';
          $update = DB::table('produk')->where('product_name', $data->product_name)->update([
            'updated_at'=>date('Y-m-d H:i:s'),
          ]);
          $getProduk = DB::table('produk')->where('product_name', $data->product_name)->first();
          $checkProdukMarketplace = DB::table('produk_marketplace')->where('sku', $data->sku)->where('platform', 'Warisan')->count();
          if($checkProdukMarketplace < 1){
              echo $data->sku.' insert <br/>';
            $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
              'parent_produk_id'=>$getProduk->id,
              'produk_id'=>$data->id,
              'photo'=>'https://api.warisan.co.id/'.$data->img,
              'sku'=>$data->sku,
              'status'=>$status,
              'price'=>$data->price,
              'platform'=>'Warisan',
              'created_at'=>date('Y-m-d H:i:s'),
              'shop_id'=>'warisan-1',
              'client_id'=>1,
              'product_type'=>$data->product_type,
              'description'=>$request->description,
            ]);
          }else{
              echo $data->sku.' update <br/>';
            $updateProdukMarketplace = DB::table('produk_marketplace')->where('sku', $data->sku)->where('platform', 'Warisan')->update([
              'produk_id'=>$data->id,
              'photo'=>'https://api.warisan.co.id/'.$data->img,
              'sku'=>$data->sku,
              'status'=>$status,
              'price'=>$data->price,
              'platform'=>'Warisan',
              'updated_at'=>date('Y-m-d H:i:s'),
              'shop_id'=>'warisan-1',
              'client_id'=>1,
              'product_type'=>$data->product_type,
              'description'=>$request->description,
            ]);
          }
        }
      }

      return response()->json('success');
    }
    
    public function createInvoiceWarisan($insertOrder){
        // dd($insertOrder);
        $getDatas = DB::table('bookingan')->where('platform', 'Warisan')->where('id',$insertOrder)->get();
        foreach($getDatas as $data){
            $id = $data->id;
            $products = DB::table('order_from_marketplace_detail')->where('orderId', $data->id)->get();
            
            // foreach($products as $index=>$product){
            //   $produk_marketplace = DB::table('produk_marketplace')->where('platform', 'Warisan')->where('produk_id', $product->product_id)->first();
            //   $getInduk = DB::table('products')->where('id', $produk_marketplace->induk_barang_id)->first();
            //   if($getInduk->jenis_barang == 'assembly'){
            //       $getSubproducts = DB::table('assembly_subproduct')->where('assembly_id', $getInduk->id)->get();
            //       foreach($getSubproducts as $iteration=>$subproduct){
            //           $dataProduct = DB::table('products')->where('id', $subproduct->product_id)->first();
            //           $products[$index] = new \stdClass;
            //           $products[$index]->subproducts[$iteration]->productName = $dataProduct->productName;
            //           $products[$index]->subproducts[$iteration]->productQty =(int) $product->qty * (int) $subproduct->qty;
            //       }
                  
            //   }
            // }
            
            // dd($products);
            $pdf = new \PDF();
            $pdf = \PDF::loadView('invoiceWarisan', compact('data', 'products'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
            $name = 'Warisan-'.$id.'.pdf';
            // \Storage::put('public/label/'.$name, $pdf->output());

            $update = DB::table('bookingan')->where('platform', 'Warisan')->where('id', $data->id)->update([
                'buktiBayar'=>$name,
            ]);
            
            $pdfBuktiBayar = base64_encode($pdf->output());
            $clientSendImage = new Client();
            
            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                  'json'=>[
                      
                          'name'=>$name,
                          'contentBuktibayar'=> $pdfBuktiBayar,
                  ]
            ])->getBody()->getContents();
        }
        
        return 'success';
    }
    
    public function getPenjualanWarisan(){
        $getSales = DB::table('bookingan')->where('bookingan.platform', 'Warisan')->join('order_from_marketplace', 'bookingan.id', 'order_from_marketplace.orderId')->get();
        foreach($getSales as $sale){
            $getProductMarketplace = DB::table('produk_marketplace')->where('produk_id', $sale->product_id)->where('paltform', 'Warisan')->first();
            $getIndukBarang= DB::table('products')->where('id', $getProductMarketplace->induk_barang_id)->first();
            
            if($getIndukBarang->jenis_barang == assembly){
                $stockAssembly = 0;
                $getSubProducts = DB::table('assembly_subproduct')->where('assembly_id', $getIndukBarang->id)->get();
                foreach($getSubProducts as $subProduct){
                    $subProductLastStocks = DB::table('stock')->where('induk_produk_id', $subProduct->id)->orderBy('id', 'desc')->first();
                    $stockNumbers = DB::table('stock_number')->where('stock_id', $subProductLastStocks->stock_id)->get();
                    $totalLastStock = 0;
                    foreach($stockNumbers as $number){
                        $conversion = DB::table('convertion')->where('id', $number->convertion_id)->first();
                        $convertStock = $number->bookStockUnit_induk * $conversion->stock_convertion_unit;
                        $totalLastStock = $totalLastStock + $convertStock;
                    }
                    
                    // pengurangan dengan qty sales
                    $qtySale = $sale->qty * $subProduct->qty;
                    $stockAfterSale = $totalLastStock - $qtySale;
                    $moduloSelisihUnitIndukSub = 0;
                    $insertStockAfterMinusSale = DB::table('stock')->insertGetId([
                     'data_toko_id'=>$sale->toko,
                     'induk_produk_id'=>$subProduct->induk_barang_id,
                     'tipe'=>'penjualan',
                     'created_at'=>date('Y-m-d H:i:s'),
                     'documentNumber'=>$sale->kode
                   ]);
                    foreach($stockNumbers as $index=>$angka){
                        $conversion = DB::table('convertion')->where('id', $angka->convertion_id)->first();
                        if($index == 0){
                            $modSelisihUnitInduk = $stockAfterSale % $conversion->stock_convertion_unit;
                            $divSelisihUnitInduk  = $stockAfterSale - $modSelisihUnitInduk;
                            $convertSelisihUnitInduk = $divSelisihUnitInduk  / $conversion->stock_convertion_unit;
                            $moduloSelisihUnitIndukSub = $modSelisihUnitInduk;
                        }else{
                            $modSelisihUnitInduk = $moduloSelisihUnitIndukSub % $conversion->stock_convertion_unit;
                            $divSelisihUnitInduk  = $moduloSelisihUnitIndukSub - $modSelisihUnitInduk;
                            $convertSelisihUnitInduk = $divSelisihUnitInduk  / $conversion->stock_convertion_unit;
                            $moduloSelisihUnitIndukSub = $modSelisihUnitInduk ;
                        }
                        
                        // untuk produk yang masuk assembly, qty yang diambil untuk perhitungan di produk assembly akan mengambil satuan(conversion) terkecil
                        if(count($stockNumbers) - 1 == $index){
                            $insertStockNumber = DB::table('stock_number')->insert([
                              'stock_id'=>$insertStockAfterMinusSale,
                              'convertion_id'=>$angka->convertion_id,
                              'unit'=>$subProduct->qty,
                              'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                              'stockOpnameUnit_induk'=>$angka->stockOpnameUnit_induk,
                              'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                            $convertForAssembly = floor($convertSelisihUnitInduk / $subProduct->qty) ;
                            if($stockAssembly == 0){
                                $stockAssembly = $convertForAssembly;
                            }else if ($stockAssembly > $convertForAssembly){
                                $stockAssembly = $convertForAssembly;
                            }
                        }else {
                            $insertStockNumber = DB::table('stock_number')->insert([
                              'stock_id'=>$insertStockAfterMinusSale,
                              'convertion_id'=>$angka->convertion_id,
                              'unit'=>0,
                              'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                              'stockOpnameUnit_induk'=>$angka->stockOpnameUnit_induk,
                              'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    
                    // update product, foreach untuk penjualan karena satu induk produk bisa lebih dari satu produk marketplace
                    $conversionProduk = DB::table('convertion')->where('id', $getProductMarketplace->convertion_id)->first();
                    $stockToMarketplace = floor($stockAfterSale / $conversionProduk->stock_convertion_unit);
                    $stockClient = new Client();
                    $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                        'json'=>[
                            'sku'=>$getProductMarketplace->sku,
                            'stock'=>$stockToMarketplace
                        ]
                    ])->getBody()->getContents();
                    $responseStock = json_decode($resStock);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $getProductMarketplace->produk_id)->where('platform', 'Warisan')->update([
                        'stock'=>$stockToMarketplace
                    ]);
                }
                
                // update assembly product, foreach untuk penjualan karena satu induk produk bisa lebih dari satu produk marketplace
                $stockClient = new Client();
                $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                    'json'=>[
                        'sku'=>$getProductMarketplace->sku,
                        'stock'=>$stockAssembly
                    ]
                ])->getBody()->getContents();
                $responseStock = json_decode($resStock);
                $updateStock = DB::table('produk_marketplace')->where('produk_id', $getProductMarketplace->produk_id)->where('platform', 'Warisan')->update([
                    'stock'=>$stockToMarketplace
                ]);
                
            }else{
                $productLastStocks = DB::table('stock')->where('induk_produk_id', $getIndukBarang->id)->orderBy('id', 'desc')->first();
                $stockNumbers = DB::table('stock_number')->where('stock_id', $productLastStocks->stock_id)->get();
                
                $totalLastStock = 0;
                $convertSale = 0;
                $insertStockAfterMinusSale = DB::table('stock')->insertGetId([
                 'data_toko_id'=>$sale->toko,
                 'induk_produk_id'=>$getIndukBarang->id,
                 'tipe'=>'penjualan',
                 'created_at'=>date('Y-m-d H:i:s'),
                 'documentNumber'=>$sale->kode
               ]);
                foreach($stockNumbers as $number){
                    $conversion = DB::table('convertion')->where('id', $number->convertion_id)->first();
                    $convertStock = $number->bookStockUnit_induk * $conversion->stock_convertion_unit;
                    $totalLastStock = $totalLastStock + $convertStock;
                    if($number->convertion_id == $getProductMarketplace->convertion_id){
                        $convertSale = $sale->qty + $conversion->stock_convertion_unit;
                    }
                }
                
                $stockAfterSale = $totalLastStock - $convertSale;
                $moduloSelisihUnitIndukSub = 0;
                // update stock number
                foreach($stockNumbers as $index=>$angka){
                    $conversion = DB::table('convertion')->where('id', $angka->convertion_id)->first();
                    if($index == 0){
                        $modSelisihUnitInduk = $stockAfterSale % $conversion->stock_convertion_unit;
                        $divSelisihUnitInduk  = $stockAfterSale - $modSelisihUnitInduk;
                        $convertSelisihUnitInduk = $divSelisihUnitInduk  / $conversion->stock_convertion_unit;
                        $moduloSelisihUnitIndukSub = $modSelisihUnitInduk;
                    }else{
                        $modSelisihUnitInduk = $moduloSelisihUnitIndukSub % $conversion->stock_convertion_unit;
                        $divSelisihUnitInduk  = $moduloSelisihUnitIndukSub - $modSelisihUnitInduk;
                        $convertSelisihUnitInduk = $divSelisihUnitInduk  / $conversion->stock_convertion_unit;
                        $moduloSelisihUnitIndukSub = $modSelisihUnitInduk ;
                    }
                    
                    if($getProductMarketplace->convertion_id == $angka->convertion_id ){
                        $insertStockNumber = DB::table('stock_number')->insert([
                          'stock_id'=>$insertStockAfterMinusSale,
                          'convertion_id'=>$angka->convertion_id,
                          'unit'=>$sale->qty,
                          'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                          'stockOpnameUnit_induk'=>$angka->stockOpnameUnit_induk,
                          'created_at'=>date('Y-m-d H:i:s'),
                        ]);
                    }else {
                        $insertStockNumber = DB::table('stock_number')->insert([
                          'stock_id'=>$insertStockAfterMinusSale,
                          'convertion_id'=>$angka->convertion_id,
                          'unit'=>0,
                          'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                          'stockOpnameUnit_induk'=>$angka->stockOpnameUnit_induk,
                          'created_at'=>date('Y-m-d H:i:s'),
                        ]);
                    }
                    
                }
                
                // update product, foreach untuk penjualan karena satu induk produk bisa lebih dari satu produk marketplace
                $conversionProduk = DB::table('convertion')->where('id', $getProductMarketplace->convertion_id)->first();
                $stockToMarketplace = floor($stockAfterSale / $conversionProduk->stock_convertion_unit);
                $stockClient = new Client();
                $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                    'json'=>[
                        'sku'=>$getProductMarketplace->sku,
                        'stock'=>$stockToMarketplace
                    ]
                ])->getBody()->getContents();
                $responseStock = json_decode($resStock);
                $updateStock = DB::table('produk_marketplace')->where('produk_id', $getProductMarketplace->produk_id)->where('platform', 'Warisan')->update([
                    'stock'=>$stockToMarketplace
                ]);
            }
        }
    }
    
    public function changeInvoice(){
        // $cari = '+ kalender';
        // $cari = '+ totebag';
        // $cari = '+ Kotak Makan';
        // $cari = '+ cutlery';
        // $cari = 'Larutan Penyegar Cap Badak Rasa Jeruk + Paket A';
        // $cari = 'Larutan Penyegar Cap Badak Rasa Jeruk + Paket B';
        // $cari = 'Larutan Penyegar Cap Badak Rasa Jeruk + Paket C';
        // $cari = 'Larutan Penyegar Cap Badak Rasa Jeruk + Paket D';
        $cari = 'Larutan Penyegar Cap Badak Rasa Jeruk + Paket E';
        // $orders = DB::table('bookingan')->where('status', 'packing')->where('station', 3)->get();
        // $getOrders = DB::table('order_from_marketplace_detail')->join('bookingan', 'bookingan.id', 'order_from_marketplace_detail.orderId')->where('order_from_marketplace_detail.item', 'LIKE', '%'.$cari.'%')->get()->unique('orderId')->values();
        $getOrders = DB::table('order_from_marketplace_detail')->where('orderId', '>', 19933)->where('order_from_marketplace_detail.item', 'LIKE', '%'.$cari.'%')->get();
        // where('orderId', '>', 19912)->
        // dd($getOrders, 'tes');
       foreach($getOrders as $detail){
           $orders = DB::table('bookingan')->where('status', 'packing')->where('station', 3)->where('id', $detail->orderId)->get();
           foreach($orders as $data){
            //   dd($data);
               $id = $data->id;
              $products = DB::table('order_from_marketplace_detail')->where('orderId', $data->id)->get();
              $pdf = new \PDF();
                $pdf = \PDF::loadView('invoice', compact('data', 'products'));
                $name = 'Warisan-'.$id.'.pdf';;
                \Storage::put('public/label/E/'.$name, $pdf->output());
                // $path = "storage/label/$name";
                $update = DB::table('bookingan')->where('platform', 'Warisan')->where('id', $data->id)->update([
                    'buktiBayar'=>$name,
                ]);
                
                $pdfBuktiBayar = base64_encode($pdf->output());
                $clientSendImage = new Client();
                
                $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                      'json'=>[
                          
                              'name'=>$name,
                              'contentBuktibayar'=> $pdfBuktiBayar,
                      ]
                ])->getBody()->getContents();
           }
           
       } 
       dd('selesai');
    }
}
