<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;

class StatisticController extends Controller
{
    // catatan untuk statistic sebaiknya di update perhari
    // buat customer Data karena data akan dibedakan berdasarkan customernya
    public function iteration(){
        // untuk generate data pertama kali
        // codingan untuk geenrate data per hari
        // default codingan untuk nampilin di frontnya
        // filter
        $totalOrder = $this->totalOrder('all');
        // $totalOrderToday
        $totalOrderTokped = $this->totalOrder('Tokopedia');
        $totalOrderShopee = $this->totalOrder('Shopee');
        $totalOrderJDID = $this->totalOrder('JDID');
        $totalOrderBlibli = $this->totalOrder('Blibli');
        // $orderInMenu = DB::table('bookingan')->where('status', "accept order")->count();
        $date = date('Y-m-d');
        $orderPacking = DB::conenction('statistics')->table('cycle_time')->where('cycle_name', "packing time")->whereDate('created_at', '=', $date)->count();
        // $orderPacking1 = $this->orderPacking(1);
        // $orderPacking2 = $this->orderPacking(2);
        $orderDropPick = DB::table('bookingan')->where('status', 'logistic')->count();
        $orderDone = DB::table('bookingan')->where('status', "end order")->count();

        $insertIteration = DB::conenction('statistics')->table('iteration')->insert([
            'total_order'=>$totalOrder,
            'total_tokped'=>$totalOrderTokped,
            'total_shopee'=>$totalOrderShopee,
            'total_blibli'=>$totalOrderBlibli,
            'total_jdid'=>$totalOrderJDID,
            'total_packed_order'=>$orderPacking,
            'total_drop_pick'=>$orderDropPick,
            'order_done'=>$orderDone,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);
        // dd($orderInMenu, $orderPacking, $orderPacking1, $orderPacking2, $orderDropPick);

    }

    public function orderPacking($station){
        if($station == 'all'){
            $orderPacking = DB::table('bookingan')->where('status', "packing")->count();
        }else{
            $orderPacking = DB::table('bookingan')->where('status', "packing")->where('station', $station)->count();
        }
    }

   public function productStatistic(){
        $getProducts = DB::table('produk_marketplace')->get()->unique('sku')->values();

        foreach($getProducts as $product){
            $getProudctsBasedOnSku = DB::table('produk_marketplace')->where('sku', $product->sku)->get();
            $totalPenjualanPerSku = 0;

            foreach($getProudctsBasedOnSku as $item){
                $getSale = DB::table('bookingan')
                            ->join('order_from_marketplace_detail', 'order_from_marketplace_detail.orderId', 'bookingan.id')
                            ->where('order_from_marketplace_detail.product_id', $item->produk_id)
                            ->where('bookingan.status', 'end order')
                            // ->limit(5)
                            ->get();

                $totalPenjualan = 0;
                if (count($getSale) > 0) {
                    foreach($getSale as $sale){
                        $totalPenjualan = $totalPenjualan + $sale->qty;
                    }
                }

                if ($item->platform == 'Shopee' && $item->hasVarian == 0){
                    $getProduct = DB::table('produk_marketplace')
                                    ->where('produk_id', $item->produk_id)
                                    ->where('platform', $item->platform)
                                    ->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')
                                    ->first();
                }

                if ($item->platform == 'Blibli' || $item->platform == 'Tokopedia' || $item->platform == 'JDID') {
                    $getProduct = DB::table('produk_marketplace')
                                    ->where('produk_id', $item->produk_id)
                                    ->where('platform', $item->platform)
                                    ->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')
                                    ->first();
                }

                // Pick name
                $name = DB::table('produk_marketplace')->where('platform', 'Tokopedia')->where('sku', $product->sku)->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')->first();

                if (empty($name)) {
                    $name = DB::table('produk_marketplace')->where('platform', 'Shopee')->where('sku', $product->sku)->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')->first();

                    if (empty($name)) {
                        $name = DB::table('produk_marketplace')->where('platform', 'Blibli')->where('sku', $product->sku)->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')->first();

                        if (empty($name)) {
                            $name = DB::table('produk_marketplace')->where('platform', 'JDID')->where('sku', $product->sku)->join('produk', 'produk.id', 'produk_marketplace.parent_produk_id')->first();
                        }
                    }
                }

                $codeGroup = explode('-', $product->sku);
                $brand = DB::table('kategori')->where('code_name', $codeGroup[0])->first();

                //   nampung total penjualan
                $totalSale = $getProduct->total_sale + $totalPenjualan;

                DB::table('product_statistics')->insert([
                    'product_name' => $name->product_name,
                    'brand' => $brand->nama_kategori,
                    'product_id' => $item->produk_id,
                    'product_sold' =>$totalSale,
                    'marketplace' => $item->platform,
                    'created_at' => now()
                ]);

                // $updateSale = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', $item->platform)->update([
                //     'total_sale'=>$totalSale
                // ]);

                $totalPerjualanPerSku = $totalPenjualanPerSku + $totalSale;
            }

        }
        echo 'sukes';
    }

  public function topCityBasedOnTotalOrder(){
      $getAllCity = DB::table('bookingan')->get('kota')->unique('kota')->values();
      $getAllCityBlibli = DB::table('bookingan')->where('platform', 'Blibli')->get()->unique('kota')->values();
      $getAllCityShopee = DB::table('bookingan')->where('platform', 'Shopee')->get()->unique('kota')->values();
    //   dd($getAllCity);

    foreach($getAllCity as $city){
        // dd($city);
        $cekCity = DB::connection('statistics')->table('city_from_marketplace')->where('city', $city->kota)->count();
        if($cekCity < 1){
            $insertCity = DB::connection('statistics')->table('city_from_marketplace')->insert([
                'city'=>$city->kota,
                'created_at'=>date('Y-m-d H:i:s'),
            ]);
        }else{
             $insertCity = DB::connection('statistics')->table('city_from_marketplace')->where('city', $city->kota)->update([
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        // $salePerCity = DB::table('bookingan')->where('kota', $city->kota)->where('status', "end order")->count();
        // // $salePerCity = DB::table('bookingan')->where('kota', "kota jakarta pusat")->where('status', "end order")->count();
        // $order = DB::table('bookingan')->where('kota', $city->kota)->where('status', "end order")->sum('jmlh_item');
        // // $order = DB::table('bookingan')->where('kota', "kota jakarta pusat")->where('status', "end order")->sum('jmlh_item');
        // // dd($order, $salePerCity, $city);

        // $insert = DB::connection('statistics')->table('top_city_totalorder')->insert([
        //     'total_sale'=>$salePerCity,
        //     'city'=>$city->kota,
        //     'qty'=>$order,
        //     'created_at'=>date('Y-m-d H:i:s'),
        // ]);
        // // dd($salePerCity, $city, qty product);
    }
    echo 'sukses';
  }

  public function totalOrderByMarketplace(){
      $marketPlaces = DB::table('bookingan')->get()->unique('platform')->values();
      foreach($marketPlaces as $marketplace){
          $totalOrder = DB::table('bookingan')->where('platform', $marketplace->platform)->where('status', "end order")->count();
          $order = DB::table('bookingan')->where('platform', $marketplace->platform)->where('status', "end order")->sum('jmlh_item');
        //   dd($totalOrder,$marketplace->platform, $order );
          $insert = DB::connection('statistics')->table('total_order_bymarketplace')->insert([
            'total_order'=>$totalOrder,
            'marketplace'=>$marketplace->platform,
            'qty'=>$order,
            'created_at'=>date('Y-m-d H:i:s'),
          ]);
      }
      echo 'sukses';
  }

  public function topBuyer(){
    //   on hold
      $buyers = DB::table('bookingan')->get()->unique('nama_pemesan')->values();
      foreach($buyers as $buyer){
          $totalBuy = DB::table('bookingan')->where('nama_pemesan', $buyer->nama_peemsan)->where('status', "end order")->count();
          dd($totalBuy);
      }
  }

  public function cancellationRate(){
      $marketplaces = DB::table('bookingan')->get()->unique('platform')->values();
      foreach($marketplaces as $marketplace){
          $cancelOrder = DB::table('bookingan')->where('status', 'batal')->where('platform', $marketplace->platform)->count();
          $totalOrder = $this->totalOrder($marketplace->platform);
          $cancellationRate = ($cancelOrder / $totalOrder) * 100;

          $insert = DB::connection('statistics')->table('cancellation_rate')->insert([
            'marketplace'=>$marketplace->platform,
            'total_order'=>$totalOrder,
            'rate'=>$cancellationRate,
            'total_cancel_order'=>$cancelOrder,
            'created_at'=>date('Y-m-d H:i:s'),
          ]);
      }
      echo 'sukses';
  }

  public function totalOrder($marketplace){
      if($marketplace == 'all'){
        $totalOrder = DB::table('bookingan')->count();
      }else{
          $totalOrder = DB::table('bookingan')->where('platform', $marketplace)->count();
      }
      return $totalOrder;
  }

  public function demographicTokped(){
      $shops = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('status', 'active')->get();
      $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);
        foreach($shops as $shop){
        // dd('tes');
            // $shop_id = $shop->shop_id;
            $shop_id = 11466905;
            $startDate = 20210801;
            $endDate = 20210805;
            $demogrpahicClient = new Client();
            $resDemographic = $demogrpahicClient->get("https://fs.tokopedia.net/v1/statistic/buyer-statistics/fs/$appID/$shop_id?start_date=$startDate&end_date=$endDate&page_size=25&sort_by=3&sort_type=4", ['headers' => [
            'Authorization'=> "Bearer $token->access_token",
            ]])->getBody()->getContents();
            $responseDemographic = json_decode($resDemographic);
            dd($responseDemographic);
        }

  }

  public function tokpedShopPerformance(){
        $shops = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('status', 'active')->get();
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");

        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        foreach($shops as $shop){

        }
  }


}
