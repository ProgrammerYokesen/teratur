<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
// use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\ClientErrorResponseException;

use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
use CRUDBooster;
use Session;
use Mike42\Escpos\Printer;
use Mike42\Escpos\ImagickEscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use App\Http\Controllers\JDID\SellerOrderSendGoodsOpenApiRequest;
use App\Http\Controllers\JDID\JdClient;
use App\Http\Controllers\JDID\EpistockQueryEpiMerchantWareStockRequest;
use App\Http\Controllers\JDID\EpistockUpdateEpiMerchantWareStockRequest;
use App\Http\Controllers\JDID\SellerProductGetWareInfoListByVendorIdRequest;
use App\Http\Controllers\JDID\SellerProductGetSkuInfoBySpuIdAndVenderIdRequest;
use App\Http\Controllers\stdClass;

class ProcessController extends Controller
{
    // public function balasPage(Request $request, $id , $platform){
    //     return view('admin.balas', compact('id', 'platform'));
    // }
    public function balas(Request $request, $id , $platform){
            // dd('masuk', $id , $platform);
        if($platform == 'Tokopedia'){
            $client = new Client();
            $clientId = env('TOKOPEDIA_CLIENT_ID');
            $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
            $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
            $clientData = base64_encode("{$clientId}:{$clientSecret}");

            $appID = env('TOKOPEDIA_APP_ID');
            try{
            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                'Authorization'=> "Basic $clientData",
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1'
                ]])->getBody()->getContents();
            }catch (RequestException $e) {
                // dd(Psr7\str($e->getRequest()));
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
            $token = json_decode($res);

            $data = DB::table('message_list')->where('id', $id)->first();
            $msgId = (int)$data->msg_id;
            $shopId = (int)$data->shop_id;
            // dd($msgId, $data, $request->balasan);
            $clientSendChat = new Client();
            $resSendChat = $clientSendChat->post("https://fs.tokopedia.net/v1/chat/fs/$appID/messages/$msgId/reply", ['headers'=>[
                'Authorization' => "Bearer $token->access_token",
                ],
                'json'=>[
                    'shop_id'=>$shopId,
                    'message'=>$request->balasan,
                    ]
                ])->getBody()->getContents();
            $responseSendChat = json_decode($resSendChat);
            $insertReplied = DB::table('cs_chat_replied')->insert([
                'name_id'=>CRUDBooster::myID(),
                'platform'=>'Tokopedia',
                'msg'=>$request->balasan,
                'time'=>date('Y-m-d H:i:s'),
                'msg_list_id'=>$id,
            ]);
            
            $insertLog = DB::table('chat_log')->insert([
                'msg_id'=>$responseSendChat->data->msg_id,
                'sender_id'=>$responseSendChat->data->sender_id,
                'role'=>$responseSendChat->data->role,
                'sender_id'=>$responseSendChat->data->sender_id,
                'msg'=>$responseSendChat->data->msg,
                'reply_time'=>$responseSendChat->data->reply_time,
                'from'=>$responseSendChat->data->from,
            ]);
            CRUDBooster::redirect('admin/message_list', 'pesan telah dibalas', 'success');
            // return redirect->back()

        }elseif($platform == 'Shopee'){
            $data = DB::table('message_list')->where('id', $id)->first();
            // $shop = DB::table('Registered_store')->where('shop_id', $data->shop_id)->first();
            $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $data->shop_id)->first();
            $access_token = $shop->chat_access_token;
                try{
                    $shop_id = $shop->shop_id;
                    $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
                    $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
                // cek if access token is valid or not
                    $access_expired_time = Carbon::parse($shop->chat_access_token_created)->addSecond($shop->chat_expired_in_second)->format('Y-m-d H:i:s');
                    $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                    $currentHourTimestamp = strtotime($currentHour);
                    $access_expired_time = strtotime($access_expired_time);
                    // dd($access_expired_time, $currentHour>$access_expired_time, $currentHour);
                    // if($currentHourTimestamp>$access_expired_time){
                    //     $refresh_token = $shop->chat_refresh_token;
                    //     $path_refresh_token = "/api/v2/auth/access_token/get";
                    //     $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    //     $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    //     $clientRefresh = new Client();
                    //     $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                    //             'Content-Type' => 'application/json',
                    //         ],
                    //         'json'=> [
                    //                 'shop_id'=>$shop_id,
                    //                 'refresh_token'=>$refresh_token,
                    //                 'partner_id'=>$partner_id
                    //             ]
                    //         ])->getBody()->getContents();
                    //     $response = json_decode($resRefresh);
                    //     $access_token = $response->access_token;
                    //     $refresh_token = $response->refresh_token;
                    //     $expired_in = $response->expire_in;
                    //     DB::table('Shopee_shop')->where('shop_id', $shop_id)->update([
                    //             'chat_access_token'=>$access_token,
                    //             'chat_refresh_token'=>$refresh_token,
                    //             'chat_expired_in_second'=>$expired_in,
                    //             'chat_access_token_created'=>$currentHour,
                    //         ]);
                    // }

                    //    sendchat
                    $timestamp = time();
                    $path = "/api/v2/sellerchat/send_message";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientChat = new Client();
                    // dd($data->msg_id, $request->balasan, $partner_id, $partner_key, $shop_id);
                    $resSendChat = $clientChat->post("https://partner.shopeemobile.com/api/v2/sellerchat/send_message?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=> [
                        'Content-Type' => 'application/json',
                    ],
                        'json'=> [
                            'to_id'=>(int)$data->to_id,
                            'message_type'=>'text',
                            'content'=>[
                               'text'=> $request->balasan,
                            ]
                        ]
                    ])->getBody()->getContents();
                    $responseSendChat = json_decode($resSendChat);
                    // dd($responseSendChat, $resSendChat);
                    $insertReplied = DB::table('cs_chat_replied')->insert([
                        'name_id'=>CRUDBooster::myID(),
                        'platform'=>'Shopee',
                        'msg'=>$request->balasan,
                        'time'=>date('Y-m-d H:i:s'),
                        'msg_list_id'=>$id,
                    ]);

                    CRUDBooster::redirect('admin/message_list', 'pesan telah dibalas', 'success');
                }catch (\GuzzleHttp\Exception\ClientException $e){
                    dd(json_decode($e->getResponse()->getBody()->getContents()));
                    // dd($e);
                }
                // 71309198

        }
                return 'success';
    }

    public function cekInvoice($id){
        $order = DB::table('bookingan')->where('bookingan.id', $id)->join('order_from_marketplace_detail', 'bookingan.id', 'order_from_marketplace_detail.orderId')->select('bookingan.nmToko','bookingan.id AS bookinganId', 'bookingan.nama_pemesan', 'bookingan.jmlh_item', 'bookingan.toko', 'bookingan.alamat', 'bookingan.phone', 'bookingan.kode', 'order_from_marketplace_detail.item', 'order_from_marketplace_detail.qty', 'bookingan.platform', 'bookingan.buktiBayar', 'bookingan.kurir', 'bookingan.status', 'bookingan.logistic_type', 'bookingan.package_id', 'order_from_marketplace_detail.orderItemNo', 'bookingan.awbNo')->get();
        // dd($order);
        $logistic_type = $order[0]->logistic_type;
        $toko = DB::table('data_toko')->where('id', $order[0]->toko)->first();
        if($order[0]->platform == 'Shopee'){
            try{
                $shop_id = (int)$order[0]->nmToko;
                // $store = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                $store = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
                // dd($store, $shop_id, $order[0]);
                $access_token = $store->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $order_sn = $order[0]->kode;
                // shipping parameter
                $path = "/api/v2/logistics/get_shipping_parameter";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShippingParameter = new Client();
                $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                $responseShippingParameter = json_decode($resShippingParameter);
                $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                // dd($responseShippingParameter);
            // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
            $logistic_type = '';
            if( $responseShippingParameter->response->info_needed->pickup){
                $logistic_type = 'pickUp';
            // echo 'pickup<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        'package_number'=>$package_number,
                        'pickup'=>[
                            'address_id'=>$address_id,
                            'pickup_time_id'=>$pickup_time_id,
                            // 'tracking_number'=>$trackingNumber,
                        ],


                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder);

            }elseif($branch_id){
                $logistic_type = 'dropoff';
                // echo 'dropoff<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        'dropoff'=>[
                            'address_id'=>$address_id,
                            'pickup_time_id'=>$pickup_time_id,
                            // 'tracking_number'=>$trackingNumber,
                        ],


                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder, 'tes');
            }else{
                $logistic_type = 'dropoff';
                // echo 'dropoff<br/>';
                // ship order
                $path = "/api/v2/logistics/ship_order";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientShipOrder = new Client();
                $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                    'Content-Type' => 'application/json',
                ],
                'json'=> [
                        'order_sn'=>$order_sn,
                        'dropoff'=>[
                            'tracking_no'=>'',
                            'branch_id'=>0,
                            'sender_real_name'=>'',
                        ],


                    ]
                ])->getBody()->getContents();
                $responseShipOrder = json_decode($resShipOrder);
                // dd($responseShipOrder);
            }
            // dd($logistic_type);
            $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                'logistic_type'=>$logistic_type,
            ]);
            // get package number
            $path = "/api/v2/order/get_order_detail";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientOrderList = new Client();
            $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
            $responseOrderList = json_decode($resOrderList);
            $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;
            // dd($package_number);

            // get Tracking Number
            $path = "/api/v2/logistics/get_tracking_number";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientTrackingNumber = new Client();
            $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
            $responseTrackingNumber = json_decode($resTrackingNumber);
            $trackingNumber = $responseTrackingNumber->response->tracking_number;
            // $trackingNumber = $this->trackingNumber($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn);

            // get shipping document parameter
            $path = "/api/v2/logistics/get_shipping_document_parameter";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $data = Array();
            $data[0]['order_sn']=$order_sn;
            $data[0]['package_number']=$package_number;
            $clientShippingDocumentParameter = new Client();
            $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],

                    'json'=>[
                        'order_list'=>$data
                    ]
            ])->getBody()->getContents();
            $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
            $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;

            // CREATE SHIPPING DOCUMENT
            $path = "/api/v2/logistics/create_shipping_document";
            $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $data = array();
            $data[0]['order_sn'] = $order_sn;
            $data[0]['package_number'] = $package_number;
            $data[0]['tracking_number'] = $trackingNumber;
            $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
            $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],

                'json' => [
                    'order_list' => $data
                ]
                // 'body'=>$parameter

            ])->getBody()->getContents();
            $resultCreated = json_decode($createdDocument);

            // get shipping document result
            $path = "/api/v2/logistics/get_shipping_document_result";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $dataDocResult = Array();
            $dataDocResult[0]["order_sn"]=$order_sn;
            $dataDocResult[0]["package_number"]=$package_number;
            $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
            // dd($dataDocResult);
            $clientShippingDocResult = new Client();
            $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
            ],
                'json'=>[
                    'order_list'=>$dataDocResult
                ]
            ])->getBody()->getContents();
            $responseShippingDocResult = json_decode($resShippingDocResult);
            // $responseShippingDocResult = $this->shippingDocumentResult($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn, $package_number, $suggest_shipping_document_type);

            // dd($responseShippingDocResult);
            // download shipping document
            $path = "/api/v2/logistics/download_shipping_document";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $dataDownload = Array();
            $dataDownload[0]["order_sn"]=$order_sn;
            $dataDownload[0]["package_number"]=$package_number;
            $clientDownloadShipDoc = new Client();
            $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
                ],
                'json'=>[
                    'shipping_document_type'=>$suggest_shipping_document_type,
                    'order_list'=>$dataDownload,
                    ]
                ])->getBody()->getContents();
            $resShippingLabel = json_decode($resDownloadShipDoc);
            $name = "Shopee-$order_sn.pdf";
            \Storage::put("public/label/$name", $resDownloadShipDoc);
            $path = 'storage/label/'.$name;
            }catch(\GuzzleHttp\Exception\ClientException $e){
                dd($e->getResponse()->getBody()->getContents());
            }
            // update bukti bayar
            // update logistic_type
            $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                'buktiBayar'=>$path,
                'awbNo'=>$trackingNumber
            ]);

        }elseif($order[0]->platform == 'Blibli'){
             try{
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                $orderNo = $order[0]->kode;
                $orderItemNo = $order[0]->orderItemNo;
                $shopId = $order[0]->nmToko;
                $sellerCode = $order[0]->nmToko;
                $shop = DB::table('Registered_store')->where('Platform', 'Blibli')->where('shop_id', $sellerCode)->first();
                $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$orderNo.'&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $shop->seller_api_key
                    ]])->getBody();
                $getDetail = $getDetail->getContents();
                $responseDetail = json_decode($getDetail);
                $detailOrder = $responseDetail->value;

                 // shipping label
                $packageId = $detailOrder->packageId;
                $storeId = 10001;
                $kode = $order[0]->kode ;
                // dd($order[0], $sellerCode, $kode, $shop->seller_api_key, $packageId);
                $client = new Client();
                $getShippingLabel = $client->get("https://api.blibli.com/v2/proxy/seller/v1/orders/$packageId/shippingLabel?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40eb&username=daniel.yokesen@gmail.com&storeId=$storeId&storeCode=$sellerCode&channelId=PT. YOKESEN TEKNOLOGI", ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $shop->seller_api_key
                    ]])->getBody();
                $getShippingLabel = $getShippingLabel->getContents();
                $responseShippingLabel = json_decode($getShippingLabel);
                $shippingLabel = $responseShippingLabel->content->shippingLabel;
                date_default_timezone_set("Asia/Jakarta");
                $file = base64_decode($shippingLabel);
                // dd($file, $shippingLabel, $responseShippingLabel, $getShippingLabel);
                $uniqueFileName = str_replace(" ","",time()."-".$kode.'.pdf');
                \Storage::put('public/label/'.$uniqueFileName, $file);
                $filePath = 'storage/label/'.$uniqueFileName;
                // dd($qty, $shippingLabel, $filePath);
                $update = \DB::table('bookingan')->where('id', $id)->where('platform', 'Blibli')->update([
                    'buktiBayar' => $filePath,
                    'package_id'=>$packageId
                ]);
                // dd($id, $order);
             }catch(\GuzzleHttp\Exception\ClientException $ex){
                 $response = json_decode($ex->getResponse()->getBody()->getContents());
                 dd($response);
             }
        }
        // dd($order);
        $pdf = new \PDF();
        $customPaper = array(0,0,270.28,270.64);
        $pdf = \PDF::loadView('admin.sticker', compact('order','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaper);
        // $pdf = \PDF::loadView('admin.sticker', compact('order','logistic_type'))->setOptions(['defaultFont' => 'sans-serif']);
        // $name = $order[0]->platform.'-'.$order[0]->kode.'.pdf';
        $name = $order[0]->bookinganId.'.pdf';
        \Storage::put('public/sticker/'.$name, $pdf->output());
        $content = $pdf->download($name);
        $filePath = 'storage/sticker/'.$name;
        $updateInvoice_toko = DB::table('bookingan')->where('id', $id)->update([
            'invoice_toko'=>$filePath
        ]);
        return view('admin.invoice', compact('order', 'toko', 'id'));
    }

    // public function shippingDocumentResult($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn, $package_number, $suggest_shipping_document_type){
    //     $path = "/api/v2/logistics/get_shipping_document_result";
    //     $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
    //     $sign = hash_hmac('sha256', $salt, $partner_key);
    //     $dataDocResult = Array();
    //     $dataDocResult[0]["order_sn"]=$order_sn;
    //     $dataDocResult[0]["package_number"]=$package_number;
    //     $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
    //     // dd($dataDocResult);
    //     $clientShippingDocResult = new Client();
    //     $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
    //         'Content-Type' => 'application/json',
    //     ],
    //         'json'=>[
    //             'order_list'=>$dataDocResult
    //         ]
    //     ])->getBody()->getContents();
    //     $responseShippingDocResult = json_decode($resShippingDocResult);
    //     // dd($responseShippingDocResult->response->result_list[0]->status);
    //     // echo $responseShippingDocResult->response->result_list[0]->status.'<br/>';
    //     if($responseShippingDocResult->response->result_list[0]->status == 'PROCESSED' || $responseShippingDocResult->response->result_list[0]->status == 'READY'){
    //         return $responseShippingDocResult;
    //     }else {
    //         $this->shippingDocumentResult($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn, $package_number, $suggest_shipping_document_type);
    //     }
    // }

    // public function trackingNumber($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn){
    //     $path = "/api/v2/logistics/get_tracking_number";
    //     $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
    //     $sign = hash_hmac('sha256', $salt, $partner_key);
    //     $clientTrackingNumber = new Client();
    //     $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
    //     $responseTrackingNumber = json_decode($resTrackingNumber);
    //     $trackingNumber = $responseTrackingNumber->response->tracking_number;
    //     if(!empty($trackingNumber)){
    //         // echo $trackingNumber.'<br/>';
    //         return $trackingNumber;
    //     }else{
    //         // echo 'no<br/>';
    //         $this->trackingNumber($shop_id, $access_token, $timestamp, $partner_id, $partner_key, $order_sn);
    //     }
    // }

     public function prosesPengemasan($id){
        $getOrders = DB::table('bookingan')->where('id', $id)->first();

        $getStation = DB::table('station')->where('first_queue', 1)->first();
        // dd($getStation);
        $nextStation = $getStation->id + 1;
        if($nextStation > 2){
            $nextStation = 1;

        }
        $updateOrder = DB::table('bookingan')->where('id', $id)->update([
            'status'=>'packing',
            'station'=>$getStation->id
        ]);

        $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
            'first_queue'=>0,
        ]);
        $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
            'first_queue'=>1,
            'status'=>'empty',
        ]);


        // dd('tes');
        $getDetailProduk = DB::table('order_from_marketplace_detail')->where('orderId', $id)->get();
        foreach($getDetailProduk as $produk){
            if($getOrders->platform == 'Tokopedia'){
                // $productInOtherMarketplace = DB::table('produk_marketplace')->where('produk_id', $getProdukParentId->product_id)->where('platform', '!=', 'Tokopedia')->get();
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Tokopedia')->get();
                // dd($productInOtherMarketplace, $getProdukSku->sku, $produk);
                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Shopee'){
                        // dd('shopee');
                        // $shop = DB::table('Registered_store')->where('data_toko',$getOrders->toko)->where('platform', 'Shopee')->first();
                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko',$getOrders->toko)->first();
                        $shop_id= $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        $stock = $item->stock - $produk->qty;
                         // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            $stock = $getModelId->stock - $produk->qty;
                        }
                        // dd($item->produk_id, $model_id, getty$stock);

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // dd($responseShopeeStock->message, $responseShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                                // kalau sku sudah jalan
                                // $updateStock = DB::table('produk_marketplace')->where('sku', $item->sku)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                            }else{
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }

                    }elseif($item->platform == 'Blibli'){
                        // dd('blibli');
                        // check if variant? blibli belum ada
                        $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;
                        // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
                        try{
                             $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                                'Authorization'=> "Basic $Authorization",
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1',
                                'Api-Seller-Key' => $sellerApiKey
                                ],
                                    'json'=>
                                        [
                                            'availableStock'=>$stock,
                                        ]
                                ])->getBody();
                            $res = $res->getContents();
                            $response = json_decode($res);
                            $datas = $response->content;
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Blibli')->update([
                                'stock'=>$stock
                            ]);
                            // dd($datas, $res);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $getProdukSku->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }

            }elseif($getOrders->platform == 'Shopee'){
                    // dd('shopee');
                    // $productInOtherMarketplace = DB::table('produk_marketplace')->where('parent_produk_id', $getProdukParentId->parent_produk_id)->where('platform', '!=', 'Shopee')->get();
                    $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Shopee')->get();
                    
                    // $getToko = DB::table('Registered_store')->where('shop_id', $getOrders->nmToko)->where('Platform', 'Shopee')->first();
                    $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $getOrders->nmToko)->first();
                    $access_token=$getToko->access_token;
                    $shop_id=$getToko->shop_id;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $order_sn = $getOrders->kode;
                    // $package_number = $getToko->package_id;
                    $timestamp = time();

                    // get package number
                    $path = "/api/v2/order/get_order_detail";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientOrderList = new Client();
                    $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $responseOrderList = json_decode($resOrderList);
                    $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;

                    // get Tracking Number
                    $path = "/api/v2/logistics/get_tracking_number";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientTrackingNumber = new Client();
                    $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                    $responseTrackingNumber = json_decode($resTrackingNumber);
                    $trackingNumber = $responseTrackingNumber->response->tracking_number;

                    // get shipping document parameter
                    $path = "/api/v2/logistics/get_shipping_document_parameter";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $data = Array();
                    $data[0]['order_sn']=$order_sn;
                    $data[0]['package_number']=$package_number;
                    $clientShippingDocumentParameter = new Client();
                    $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],

                            'json'=>[
                                'order_list'=>$data
                            ]
                    ])->getBody()->getContents();
                    $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                    $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                    // CREATE SHIPPING DOCUMENT
                        $path = "/api/v2/logistics/create_shipping_document";
                        $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $data = array();
                        $data[0]['order_sn'] = $order_sn;
                        $data[0]['package_number'] = $package_number;
                        $data[0]['tracking_number'] = $trackingNumber;
                        $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                        $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                            'headers' => [
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ],

                            'json' => [
                                'order_list' => $data
                            ]
                            // 'body'=>$parameter

                        ])->getBody()->getContents();
                        $resultCreated = json_decode($createdDocument);

                    // get shipping document result
                    $path = "/api/v2/logistics/get_shipping_document_result";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDocResult = Array();
                    $dataDocResult[0]["order_sn"]=$order_sn;
                    $dataDocResult[0]["package_number"]=$package_number;
                    $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                    // dd($dataDocResult);
                    $clientShippingDocResult = new Client();
                    $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                    ],
                        'json'=>[
                            'order_list'=>$dataDocResult
                        ]
                    ])->getBody()->getContents();
                    $responseShippingDocResult = json_decode($resShippingDocResult);
                    // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
                // download shipping document
                    $path = "/api/v2/logistics/download_shipping_document";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDownload = Array();
                    $dataDownload[0]["order_sn"]=$order_sn;
                    $dataDownload[0]["package_number"]=$package_number;
                    $clientDownloadShipDoc = new Client();
                    $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                        ],
                        'json'=>[
                            'shipping_document_type'=>$suggest_shipping_document_type,
                            'order_list'=>$dataDownload,
                            ]
                        ])->getBody()->getContents();
                    $resShippingLabel = json_decode($resDownloadShipDoc);
                    $name = "Shopee-$order_sn.pdf";
                    \Storage::put("public/label/$name", $resDownloadShipDoc);
                    $path = 'storage/label/'.$name;
                    // update bukti bayar
                    $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                        'buktiBayar'=>$path,
                        'awbNo'=>$trackingNumber
                    ]);
                    // dd($responseTrackingNumber,$responseShippingDocResult, $resultCreated, $resShippingDocumentParameter, $resDownloadShipDoc );
                foreach($productInOtherMarketplace as $item){
                    // dd('blibli');
                    if($item->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;
                        // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item, $stock, $item->stock, $produk->qty);
                        try{
                             $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                                'Authorization'=> "Basic $Authorization",
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1',
                                'Api-Seller-Key' => $sellerApiKey
                                ],
                                    'json'=>
                                        [
                                            'availableStock'=>$stock,
                                        ]
                                ])->getBody();
                            $res = $res->getContents();
                            $response = json_decode($res);
                            $datas = $response->content;
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Blibli')->update([
                                'stock'=>$stock
                            ]);
                            // dd($datas, $res);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response );
                        }

                    }elseif($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('data_toko',$getOrders->toko)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $stock = $item->stock - $produk->qty;
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$item->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                            // dd($responseStock);
                        // }catch(\GuzzleHttp\Exception\ClientException $e){
                        //     dd(json_decode($e->getResponse()->getBody()->getContents()));
                        //     // dd($e);

                        // }
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $getProdukSku->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // dd($respStock, $skuId, $currentStock, $stock, $produk->qty);
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        // dd($respUpdateStock, $currentStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }
            }elseif($getOrders->platform == 'Blibli'){
                // $productInOtherMarketplace = DB::table('produk_marketplace')->where('parent_produk_id', $getProdukParentId->parent_produk_id)->where('platform', '!=', 'Blibli')->get();
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Blibli')->get();
                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('data_toko',$getOrders->toko)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $stock = $item->stock - $produk->qty;
                            $data = Array();
                            $fsId = (int)$appID;
                            $data[0]['product_id']=(int)$item->produk_id;
                            $data[0]['new_stock']=$stock;
                            // dd($data,$fsId, $shop_id, $token->access_token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch(\GuzzleHttp\Exception\ClientException $e){
                            dd($e);
                        }
                    }elseif($getOrders->platform == 'Shopee'){
                        // $shop = DB::table('Registered_store')->where('shop_id',$getOrders->nmToko)->first();
                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko',$getOrders->toko)->first();
                        $shop_id= $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        $stock = $item->stock - $produk->qty;
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->first();
                            $model_id = $getModelId->produk_id;
                            $stock = $getModelId->stock - $produk->qty;
                        }
                        $clientShopeeStock = new Client();
                        $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type'=>'application/json',
                        ],
                            'json'=>
                                [
                                    'item_id'=>$item->produk_id,
                                    'stock_list'=>
                                        [
                                            'model_id'=>$model_id,
                                            'normal_stock'=>$stock
                                        ]
                                ]
                        ])->getBody()->getContents();
                        $responseShopeeStock = json_decode($resShopeeStock);

                        // updateStock
                        if($item->has_variant == 1){
                            $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                        }else{
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                        }
                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $getProdukSku->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }
            }else{
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'JDID')->get();
                // echo 'JDID';
                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);

                        $shopId = $item->shop_id;
                        $product_id = $item->produk_id;
                        $clientProduct = new Client();
                        $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?product_id=$produk_id", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                        $response = json_decode($resProduct);
                        $items = $response->data;
                        // dd($items, $response);
                    }
                }
            }
        }
        // cycletime untuk all order

        // $orderInTime = strtotime($getOrders->created_at);
        // $orderPackStart = strtotime(date('Y-m-d H:i:s'));
        // $time = $orderPackStart-$orderInTime
        // do update the databse

        // order activities

        // $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
        // $name = $person->name;
        // $orderInvoice = $getOrders->kode;
        // $activities = $name.' memencet tombol proses pengemasan untuk order '.$orderInvoice;
        // timeActivities = Date('M d, Y - H:i:s');
        // update ke db
        CRUDBooster::redirect('admin/order', 'order telah masuk dalam antrian untuk di proses', 'success');
    }

    public function editInvoice($id){
        $order = DB::table('bookingan')->where('bookingan.id', $id)->get();

        $toko = DB::table('data_toko')->get();

        return view('admin.edit-invoice', compact('order', 'toko', 'id'));
    }

    public function checkStation($id, $nextStation){
        $station = DB::table('station')->where('id', $nextStation)->first();
            if($station->status == 'empty'){
                 $updateOrder = DB::table('bookingan')->where('id', $id)->update([
                    'order_status'=>'packing',
                    'station'=>$nextStation->id
                ]);

                $updateFirstQueue = DB::table('station')->where('first_queue', 1)->update([
                    'first_queue'=>0,
                    'status'=>filled,
                ]);

                $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                    'first_queue'=>1,
                    'status'=>'empty',
                ]);

                return 'success';
            }else{
                $nextStation = $nextStation + 1;
                $this->checkStation($id, $nextStation);
            }

    }

    public function pdfSticker($id){
        $order = DB::table('bookingan')->where('bookingan.id', $id)->join('order_from_marketplace_detail', 'bookingan.id', 'order_from_marketplace_detail.orderId')->select('bookingan.id AS bookinganId', 'bookingan.nama_pemesan', 'bookingan.jmlh_item', 'bookingan.toko', 'bookingan.alamat', 'bookingan.phone', 'bookingan.kode', 'order_from_marketplace_detail.item', 'order_from_marketplace_detail.qty', 'bookingan.platform', 'bookingan.buktiBayar', 'bookingan.kurir', 'bookingan.status')->get();
        // dd($order);
        $toko = DB::table('data_toko')->where('id', $order[0]->toko)->first();

        $pdf = new \PDF();
        $pdf = \PDF::loadView('admin.sticker', compact('order', 'toko'))->setOptions(['defaultFont' => 'sans-serif']);
        // $name = $order[0]->platform.'-'.$order[0]->kode.'.pdf';
        // $name = $order[0]->bookinganId.'.pdf';
        // Storage::put('files/sticker/'.$name, $pdf->output());
        $content = $pdf->download($name);
        // return view('admin.sticker', compact('order', 'toko'));
    }

    public function updateLogistic(Request $request){
       foreach($request->invoices as $invoice){

        $getOrder = DB::table('bookingan')->where('kode', $invoice)->first();

        // cycletime untuk pick/drop into done
        $getData = DB::connection('statistics')->table('cycle_time')->where('bookingan_id', $getOrder->id)->first();
        $orderPackEnd = strtotime($getData->created_at);
        $orderDone = strtotime(date('Y-m-d H:i:s'));
        $time = $orderDone - $orderPackEnd;
        $insertData = DB::connection('statistics')->table('cycle_time')->insert([
            'cycle_name'=>'logistic time',
            'time'=>$time,
            'bookingan_id'=>$getOrder->id,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        // total cycle time

        $totalCycle = $orderDone - strtotime($getOrder->created_at);
        $insertTotalCycle = DB::connection('statistics')->table('cycle_time')->insert([
            'cycle_name'=>"cycle time",
            'time'=>$totalCycle,
            'bookingan_id'=>$getOrder->id,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

      // order activities

        $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
        $name = $person->name;
        $orderInvoice = $invoice;
        $activities = $name.' memencet tombol sudah dibawa oleh kurir untuk order '.$invoice;
        // $timeActivities = Date('M d, Y - H:i:s');
        $timeActivities = Date('Y-m-d H:i:s');
        $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
            'user_id'=>CRUDBooster::myID(),
            'bookingan_id'=>$getOrder->id,
            'time'=>$timeActivities,
            'created_at'=>$timeActivities,
            'description'=>$activities,
        ]);
        if($request->resi){
          $check = DB::table('bookingan')->where('kode', $invoice)->count();
          $updateLogistic = DB::table('bookingan')->where('kode', $invoice)->update([
              'status'=>'end order',
              'awbNo'=>$request->resi
          ]);
          // return response()->json(['order punya resi', $invoice, $check]);
        }else{
          $updateLogistic = DB::table('bookingan')->where('kode', $invoice)->update([
              'status'=>'end order'
          ]);
          // return response()->json('order tidak punya resi');
        }

        if($getOrder->platform == 'Warisan'){
          $warisanUpdateClient = new Client();
          $resWarisanOrderUpdate = $warisanUpdateClient->post("https://api.warisan.co.id/api/v1/product-edit",[
            'json'=>[
                'invoice'=>$getOrder->kode,
                'status'=>'dikirim',
                'resi'=>$request->resi,
            ]
          ])->getBody();
          $response = json_decode($resWarisanOrderUpdate);
          return response()->json($response);
        }
       }
        return response()->json('order has been process successfully');
    }

    public function updatePacking(Request $request){
        $updateOrder = DB::table('bookingan')->where('id', $request->orderId)->update([
            'status'=>'logistic',
            'status_proses'=> 'siap dikirim',
            'time_status_proses'=> date('Y-m-d H:i:s'),
            'packing_time'=>$request->time,
            'packed_by'=>CRUDBooster::myID(),
            'cronjob_hit_time'=>date('H:i')
        ]);

        $order = DB::table('bookingan')->where('id', $request->orderId)->first();
        $logistic_type = $order->logistic_type;
        // cycletime untuk packing
        $orderPackStart = strtotime($order->created_at);
        $orderPackEnd = strtotime(date('Y-m-d H:i:s'));
        $time = $orderPackEnd - $orderPackStart;
        $updateData = DB::connection('statistics')->table('cycle_time')->insert([
            'cycle_name'=>'packing time',
            'time'=>$time,
            'bookingan_id'=>$request->orderId,
            'created_at'=>date('Y-m-d H:i:s'),
        ]);

        // order activities

        $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
        $name = $person->name;
        $orderInvoice = $getData->kode;
        $activities = $name.' memencet tombol sudah selesai pada proses packing untuk orderan '.$invoice;
        // $timeActivities = Date('M d, Y - H:i:s');
        $timeActivities = Date('Y-m-d H:i:s');
        $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
            'user_id'=>CRUDBooster::myID(),
            'bookingan_id'=>$order->id,
            'time'=>$timeActivities,
            'created_at'=>$timeActivities,
            'description'=>$activities,
        ]);

        // $updateOrder = DB::table('bookingan')->where('id', $request->orderId)->update([
        //     'status'=>'logistic',
        //     'status_proses'=> 'siap dikirim',
        //     'time_status_proses'=> date('Y-m-d H:i:s'),
        //     'packing_time'=>$request->time,
        //     'packed_by'=>CRUDBooster::myID()
        // ]);

        return response()->json($logistic_type);
    }

    public function logistic($id){
        // dd($id);
        $getProduct = DB::table('bookingan')->where('id', $id)->first();
        $store = DB::table('Registered_store')->where('shop_id', $getProduct->nmToko)->first();
        // dd($id, $getProduct);
        // return redirect()->route('pack', ['id'=>$getProduct->station])->with('success', 'tes');
        // dd($getProduct);
        if($getProduct->platform == 'Tokopedia'){

                $orderId = (int)$getProduct->orderId;
                // dd($orderId);
                $nmToko = (int)$getProduct->nmToko;
                $client = new Client();
                $clientId = env('TOKOPEDIA_CLIENT_ID');
                $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                $clientData = base64_encode("{$clientId}:{$clientSecret}");

                $appID = env('TOKOPEDIA_APP_ID');
                try{
                $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                    'Authorization'=> "Basic $clientData",
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1'
                    ]])->getBody()->getContents();
                }catch (RequestException $e) {
                    echo Psr7\str($e->getRequest());
                    if ($e->hasResponse()) {
                        echo Psr7\str($e->getResponse());
                    }
                }
                $token = json_decode($res);
                // dd($orderId,$nmToko);
                
                $currentTime = date('Y-m-d H:i:s');
                $startTimeLimit = date('Y-m-d').' 15:00:00';
                $startTimeLimitInstant = date('Y-m-d').' 20:00:00';
                $updateData = DB::table('bookingan')->where('id', $id)->update([
                        'scan'=>'1',
                        'time_req_pickup'=>$currentTime
                    ]);
                // return response()->json([$orderId, $nmToko,$token->access_token, $appID]);
                if(($currentTime < $startTimeLimit && ($getProduct->logistic_service != 'Instant Courier' || $getProduct->logistic_service != 'Instant' || empty($getProduct->logistic_service))) || ($currentTime < $startTimeLimitInstant && ($getProduct->logistic_service == 'Instant Courier' || $getProduct->logistic_service == 'Instant'))){
                    
                    try{
                        $requestPickUp = $client->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/pick-up", ['headers' => [
                        'Authorization'=> "Bearer $token->access_token",
                        'Content-Type' => "application/json"
                        ],
                        'json'=>
                            [
                                "order_id"=>$orderId,
                                "shop_id"=>$nmToko
                            ]
    
                        ])->getBody()->getContents();
    
                        $decodeRequestPickUp = json_decode($requestPickUp);
                        
                        $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
                        $name = $person->name;
                        $orderInvoice = $getProduct->kode;
                        $activities = $name.' memanggil kurir untuk orderan '.$orderInvoice;
                        $timeActivities = Date('Y-m-d H:i:s');
                        $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
                            'user_id'=>CRUDBooster::myID(),
                            'bookingan_id'=>$id,
                            'time'=>$timeActivities,
                            'created_at'=>$timeActivities,
                            'description'=>$activities,
                        ]);
                        return response()->json('success');
                        
                        // dd($decodeRequestPickUp);
                    }catch (RequestException $e){
                        $error = $e->getResponse()->getBody()->getContents();
                        dd('tes');
                        // dd(Psr7\str($e->getRequest()));
                        return response()->json(['gagal', $error]);
    
                    }
                }elseif ($currentTime < $startTimeLimit){
                    try{
                        $requestPickUp = $client->post("https://fs.tokopedia.net/inventory/v1/fs/$appID/pick-up", ['headers' => [
                        'Authorization'=> "Bearer $token->access_token",
                        'Content-Type' => "application/json"
                        ],
                        'json'=>
                            [
                                "order_id"=>$orderId,
                                "shop_id"=>$nmToko
                            ]
    
                        ])->getBody()->getContents();
    
                        $decodeRequestPickUp = json_decode($requestPickUp);
                        
                        $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
                        $name = $person->name;
                        $orderInvoice = $getProduct->kode;
                        $activities = $name.' memanggil kurir untuk orderan '.$orderInvoice;
                        $timeActivities = Date('Y-m-d H:i:s');
                        $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
                            'user_id'=>CRUDBooster::myID(),
                            'bookingan_id'=>$id,
                            'time'=>$timeActivities,
                            'created_at'=>$timeActivities,
                            'description'=>$activities,
                        ]);
                        return response()->json('success');
                        
                        // dd($decodeRequestPickUp);
                    }catch (RequestException $e){
                        $error = $e->getResponse()->getBody()->getContents();
                        dd('tes');
                        // dd(Psr7\str($e->getRequest()));
                        return response()->json(['gagal', $error]);
    
                    }
                }

        }elseif($getProduct->platform == 'JDID'){
            try{
                $accessToken = $store->access_token;
                $expressNo = $getProduct->awbNo;
                $orderId = $getProduct->kode;
                $c = new JdClient();
                $c->appKey = env('APP_KEY');
                $c->appSecret = env('APP_SECRET');
                $c->accessToken = $accessToken;
                $c->serverUrl = "https://open-api.jd.id/routerjson";
                $req = new SellerOrderSendGoodsOpenApiRequest();
                $req->setOrderId($orderId);
                $req->setExpressNo($expressNo);
                // $req->setExpressCompany( "abc" );
                $resp = $c->execute($req, $c->accessToken);
                // dd($resp);
                $updateData = DB::table('bookingan')->where('id', $id)->update([
                        'scan'=>'1'
                    ]);
                    
                $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
                $name = $person->name;
                $orderInvoice = $getProduct->kode;
                $activities = $name.' memanggil kurir untuk orderan '.$orderInvoice;
                $timeActivities = Date('Y-m-d H:i:s');
                $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
                    'user_id'=>CRUDBooster::myID(),
                    'bookingan_id'=>$id,
                    'time'=>$timeActivities,
                    'created_at'=>$timeActivities,
                    'description'=>$activities,
                ]);
                return response()->json('success');
            }catch(\GuzzleHttp\Exception\ClientException $e){
                return response()->json('gagal');
                // dd($e->getResponse()->getBody()->getContents());
            }
        }else{
            if($getProduct->platform == 'Blibli'){
                $orderDetail = DB::table('order_from_marketplace_detail')->where('orderId', $id)->first();
                // dd('tes');
                // dd($getProduct);
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                try{
                    $sellerKey = "";
                    $packageId = $getProduct->package_id;
                    // $packageId = 2328552384;
                    // foreach($sellerCode as $key=>$value){
                    //     if($value['sellerCode'] == $product->nmToko){
                    //         $sellerKey = $value['sellerApiKey'];
                    //     }
                    // }
                    // dd($sellerKey);
                    $shopId = $getProduct->nmToko;
                    // $shopId = 'BAB-70101';
                    $orderItemNo= $orderDetail->orderItemNo;
                    $sellerKey = $store->seller_api_key;
                    // // $orderItemNo= 12107647659;
                    // $res = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/getCombineShipping?businessPartnerCode='.$shopId.'&requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    //     'Authorization'=> "Basic $Authorization",
                    //     'Content-Type' => 'application/json',
                    //     'Accept' => 'application/json',
                    //     'Content-Length' => 0,
                    //     'User_Agent' => 'PostmanRuntime/7.17.1',
                    //     'Api-Seller-Key' => $sellerKey,
                    //     ]])->getBody();
                    // $res = $res->getContents();
                    // $responseCombine = json_decode($res);
                    // // dd($responseCombine);
                    // $datas = $responseCombine->value->combineShipping;
                    // $combine = $responseCombine->value->alreadyCombined;
                    // $itemList = [];
                    // // if($combine == false){
                    //     // foreach($datas as $key=>$value){
                    //     //     array_push($itemList,$value->orderItemNo);
                    //     // }
                    // // }
                    // // dd($datas, $res, $itemList);

                    // // create package
                    // $res = $client->post('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/createPackage?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&businessPartnerCode='.$shopId.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    //     'Authorization'=> "Basic $Authorization",
                    //     'Content-Type' => 'application/json',
                    //     'Accept' => 'application/json',
                    //     'Content-Length' => 0,
                    //     'User_Agent' => 'PostmanRuntime/7.17.1',
                    //     'Api-Seller-Key' => $sellerKey
                    // ],
                    //     'json'=>[
                    //         'orderItemIds'=>$itemList
                    //     ]
                    //     ])->getBody();
                    // $res = $res->getContents();
                    // $response = json_decode($res);
                    // $datas = $response->value;
                    // dd($datas, $response);
                    // // harus ambil detail order untuk ambil awb no
                    // $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$getProduct->kode.'&orderItemNo='.$orderDetail->orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    //         'Authorization'=> "Basic $Authorization",
                    //         'Content-Type' => 'application/json',
                    //         'Accept' => 'application/json',
                    //         'Content-Length' => 0,
                    //         'User_Agent' => 'PostmanRuntime/7.17.1',
                    //         'Api-Seller-Key' => $sellerKey
                    //         ]])->getBody();
                    //     $getDetail = $getDetail->getContents();
                    //     $responseDetail = json_decode($getDetail);
                    //     $detailOrder = $responseDetail->value;
                    //      $awbNo = $detailOrder->awbNumber;
                         $awbNo = 'BLIGO2814978370-1';
                        //  dd($awbNo, $packageId, $shopId);
                    // fulfill regular order
                    $packageId = 2814921198;
                    $resfulfill = $client->post('https://api.blibli.com/v2/proxy/seller/v1/orders/regular/'.$packageId.'/fulfill?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&channelId=PT. YOKESEN TEKNOLOGI&username=daniel.yokesen@gmail.com&storeCode='.$shopId, ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $sellerKey
                    ],
                        'json'=>[
                            "awbNo"=>$awbNo
                        ]
                    ])->getBody();
                    $res = $resfulfill->getContents();
                    dd($res, $resfulfill);
                    $responseFullfill = json_decode($res);
                    $updateData = DB::table('bookingan')->where('id', $id)->update([
                        'scan'=>'1'
                    ]);
                    
                    $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
                    $name = $person->name;
                    $orderInvoice = $getProduct->kode;
                    $activities = $name.' memanggil kurir untuk orderan '.$orderInvoice;
                    $timeActivities = Date('Y-m-d H:i:s');
                    $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
                        'user_id'=>CRUDBooster::myID(),
                        'bookingan_id'=>$id,
                        'time'=>$timeActivities,
                        'created_at'=>$timeActivities,
                        'description'=>$activities,
                    ]);
                    return response()->json('success');
                    // hasil kosong
                    // $updateData = DB::table('bookingan')->where('id', $id)->update([
                    //     'status'=>'end order'
                    // ]);
                    // return redirect()->route('pick');
                }catch(\GuzzleHttp\Exception\ClientException $e){
                    // return response()->json($e);
                    dd($e->getResponse()->getBody()->getContents());
                }
            }else{
                // $updateData = DB::table('bookingan')->where('id', $id)->update([
                //     'status'=>'end order'
                // ]);
            //   return redirect()->route('drop');

            }
        }

    }


    public function chatToko($toko='chat' ){
        if($toko == 'whw'){
            $chatData = DB::table('message_list')->where('nama_toko', 'LIKE', '%Wuruk%')->orWhere('nama_toko', 'LIKE', '%whw%')->orderBy('created_at', 'DESC')->paginate(20);
            // dd($chatData);
        }elseif($toko == 'wgm'){
            $chatData = DB::table('message_list')->where('nama_toko', 'LIKE', '%Gajahmada%')->orWhere('nama_toko', 'LIKE', '%wgm%')->orderBy('created_at', 'DESC')->paginate(20);

        }elseif($toko == 'bardi'){
            $chatData = DB::table('message_list')->where('nama_toko', 'LIKE', '%bardi%')->orderBy('created_at', 'DESC')->get();

        }elseif($toko == 'sriwijaya'){
            $chatData = DB::table('message_list')->where('nama_toko', 'LIKE', '%sriwijaya%')->orWhere('nama_toko', 'LIKE', '%majapahit%')->orderBy('created_at', 'DESC')->paginate(20);

        }else{
            $chatData = DB::table('message_list')->orderBy('created_at', 'DESC')->paginate(20);
            // dd($chatData);
        }

        return view("admin.chat", compact('chatData'));
    }

    public function updateOrder($station, $bookinganId, $count){
        if($station == 5){
            $getOrder = DB::table('bookingan')->where('status', 'packing')->get();
            $countData = count($getOrder);

            if($countData > 0){
                foreach($getOrder as $key=>$value){
                    $getDetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->get();
                    if(count($getDetail) > 0) {
                         foreach($getDetail as $index=>$detail){
                           if($value->platform == 'harbolnas 12.12'){
                                // dd($value);
                                $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Tokopedia')->first();
                                // dd($product);
                                $getDetail[$index]->photo = $product->photo;
                                $getDetail[$index]->initial = $product->initial;
                            }else{
                                $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', $value->platform)->first();
                                $getDetail[$index]->photo = $product->photo;
                                $getDetail[$index]->initial = $product->initial;
                            }
                            if($value->platform == 'Warisan'){
                            $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Warisan')->first();
                            // cekinduk apakah assembly atau tidak
                                $induk = DB::table('products')->where('id',$product->induk_barang_id)->first();
                                if($induk->jenis_barang == 'assembly'){
                                   $getsubproducts = DB::table('assembly_subproduct')->where('assembly_id', $induk->id)->get();
                                   foreach($getsubproducts as $iteration=>$product){
                                       $getproduct = DB::table('products')->where('id', $product->id)->first();
                                       $getDetail[$index]->sub[$iteration] = $getproduct->productName;
                                   }
                                }
                                
                            }

                        }
                        $getOrder[$key]->order = $getDetail;
                    }
                }
                $getOrder = $getOrder->filter(function ($item){
                    return count($item->order) > 0;
                })->values();
            }
        }else{
            $getOrder = DB::table('bookingan')->where('status', 'packing')->where('station', $station)->orderBy('station_queue_time', 'asc')->get();
            // dd($getOrder)
            $countData = count($getOrder);
            if($countData > 0){
                foreach($getOrder as $key=>$value){
                    $getDetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->get();
                    if(count($getDetail) > 0) {
                        foreach($getDetail as $index=>$detail){
                            if($value->platform == 'harbolnas 12.12'){
                                // dd($value);
                                $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Tokopedia')->first();
                                // dd($product);
                                $getDetail[$index]->photo = $product->photo;
                                $getDetail[$index]->initial = $product->initial;
                            }else{
                                $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', $value->platform)->first();
                                $getDetail[$index]->photo = $product->photo;
                                $getDetail[$index]->initial = $product->initial;
                            }
                            if($value->platform == 'Warisan'){
                                $product = DB::table('produk_marketplace')->where('produk_id', $detail->product_id)->where('platform', 'Warisan')->first();
                                // cekinduk apakah assembly atau tidak
                                $induk = DB::table('products')->where('id',$product->induk_barang_id)->first();
                                if($induk->jenis_barang == 'assembly'){
                                   $getsubproducts = DB::table('assembly_subproduct')->where('assembly_id', $induk->id)->get();
                                   foreach($getsubproducts as $iteration=>$product){
                                       $getproduct = DB::table('products')->where('id', $product->id)->first();
                                       $getDetail[$index]->sub[$iteration] = $getproduct->productName;
                                   }
                                }
                                
                            }

                        }
                        $getOrder[$key]->order = $getDetail;
                    }
                }
                $getOrder = $getOrder->filter(function ($item){
                    return count($item->order) > 0;
                })->values();
            }
        }
        if(count($getOrder) > 0 && $getOrder[0]->platform == "Shopee"){
            // return response()->json('jalan');
            $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $getOrder[0]->nmToko)->first();
            // $getToko = DB::table('Shopee_shop')->where('shop_id', $getOrder[0]->nmToko)->where('Platform', 'Shopee')->first();
            $access_token=$getToko->access_token;
            $shop_id=$getToko->shop_id;
            $partner_id = (int)env('SHOPEE_PARTNER_ID');
            $partner_key = env('SHOPEE_PARTNER_KEY');
            $order_sn = $getOrder[0]->kode;
            // $package_number = $getToko->package_id;
            $timestamp = time();

            // get package number
            $path = "/api/v2/order/get_order_detail";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientOrderList = new Client();
            $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
            $responseOrderList = json_decode($resOrderList);
            $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;
            // return response()->json($responseOrderList);
            // get Tracking Number
            $path = "/api/v2/logistics/get_tracking_number";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $clientTrackingNumber = new Client();
            $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
            $responseTrackingNumber = json_decode($resTrackingNumber);
            $trackingNumber = $responseTrackingNumber->response->tracking_number;
            // return response()->json([$responseTrackingNumber, $order_sn]);

            // get shipping document parameter
            $path = "/api/v2/logistics/get_shipping_document_parameter";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $data = Array();
            $data[0]['order_sn']=$order_sn;
            $data[0]['package_number']=$package_number;
            $clientShippingDocumentParameter = new Client();
            $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],

                    'json'=>[
                        'order_list'=>$data
                    ]
            ])->getBody()->getContents();
            $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
            $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
            // CREATE SHIPPING DOCUMENT
                $path = "/api/v2/logistics/create_shipping_document";
                $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $data = array();
                $data[0]['order_sn'] = $order_sn;
                $data[0]['package_number'] = $package_number;
                $data[0]['tracking_number'] = $trackingNumber;
                $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],

                    'json' => [
                        'order_list' => $data
                    ]
                    // 'body'=>$parameter

                ])->getBody()->getContents();
                $resultCreated = json_decode($createdDocument);
// return response()->json([$responseTrackingNumber, $resultCreated]);
            // get shipping document result
            $path = "/api/v2/logistics/get_shipping_document_result";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $dataDocResult = Array();
            $dataDocResult[0]["order_sn"]=$order_sn;
            $dataDocResult[0]["package_number"]=$package_number;
            $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
            // dd($dataDocResult);
            $clientShippingDocResult = new Client();
            $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
            ],
                'json'=>[
                    'order_list'=>$dataDocResult
                ]
            ])->getBody()->getContents();
            $responseShippingDocResult = json_decode($resShippingDocResult);
            // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
        // download shipping document
            $path = "/api/v2/logistics/download_shipping_document";
            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
            $sign = hash_hmac('sha256', $salt, $partner_key);
            $dataDownload = Array();
            $dataDownload[0]["order_sn"]=$order_sn;
            $dataDownload[0]["package_number"]=$package_number;
            $clientDownloadShipDoc = new Client();
            $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
                ],
                'json'=>[
                    'shipping_document_type'=>$suggest_shipping_document_type,
                    'order_list'=>$dataDownload,
                    ]
                ])->getBody()->getContents();
            $resShippingLabel = json_decode($resDownloadShipDoc);
            $name = "Shopee-$order_sn.pdf";
            \Storage::put("public/label/$name", $resDownloadShipDoc);
            // $path = 'storage/label/'.$name;
            // update bukti bayar
            $updatebooking = DB::table('bookingan')->where('id', $getOrder[0]->id)->update([
                'buktiBayar'=>$name,
                'awbNo'=>$trackingNumber
            ]);
            $pdfBuktiBayar = base64_encode($resDownloadShipDoc);
            $clientSendImage = new Client();
            
            $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                  'json'=>[
                      
                          'name'=>$name,
                          'contentBuktibayar'=> $pdfBuktiBayar,
                  ]
            ])->getBody()->getContents();
            // return response()->json([$responseTrackingNumber, $resultCreated, $responseShippingDocResult, $resShippingDocumentParameter, $resDownloadShipDoc]);

            // dd($responseTrackingNumber,$responseShippingDocResult, $resultCreated, $resShippingDocumentParameter, $resDownloadShipDoc );
        }elseif(count($getOrder) > 0 && $getOrder[0]->platform == "Blibli" && $count <= 3){
            // try{
            //     $shop = DB::table('Registered_store')->where('shop_id', $getOrder[0]->nmToko)->first();
            //     $client = new Client();
            //     $clientId = env('BLIBLI_CLIENT_ID');
            //     $clientKey = env('BLIBLI_CLIENT_KEY');
            //     $Authorization = base64_encode("{$clientId}:{$clientKey}");
            //     $orderNo = $getOrder[0]->kode;
            //     $orderItemNo = $getOrder[0]->orderItemNo;
            //     $shopId = $getOrder[0]->nmToko;
            //     $sellerCode = $getOrder[0]->nmToko;
            //     $getDetail = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/order/orderDetail?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeId=10001&orderNo='.$orderNo.'&orderItemNo='.$orderItemNo.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
            //         'Authorization'=> "Basic $Authorization",
            //         'Content-Type' => 'application/json',
            //         'Accept' => 'application/json',
            //         'Content-Length' => 0,
            //         'User_Agent' => 'PostmanRuntime/7.17.1',
            //         'Api-Seller-Key' => $shop->seller_api_key
            //         ]])->getBody();
            //     $getDetail = $getDetail->getContents();
            //     $responseDetail = json_decode($getDetail);
            //     $detailOrder = $responseDetail->value;

            //      // shipping label
            //     $packageId = $detailOrder->packageId;
            //     $storeId = 10001;
            //     $kode = $order[0]->kode ;
            //     $client = new Client();
            //     $getShippingLabel = $client->get("https://api.blibli.com/v2/proxy/seller/v1/orders/$packageId/shippingLabel?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40eb&username=daniel.yokesen@gmail.com&storeId=$storeId&storeCode=$sellerCode&channelId=PT. YOKESEN TEKNOLOGI", ['headers' => [
            //         'Authorization'=> "Basic $Authorization",
            //         'Content-Type' => 'application/json',
            //         'Accept' => 'application/json',
            //         'Content-Length' => 0,
            //         'User_Agent' => 'PostmanRuntime/7.17.1',
            //         'Api-Seller-Key' => $shop->seller_api_key
            //         ]])->getBody();
            //     $getShippingLabel = $getShippingLabel->getContents();
            //     $responseShippingLabel = json_decode($getShippingLabel);
            //     $shippingLabel = $responseShippingLabel->content->shippingLabel;
            //     date_default_timezone_set("Asia/Jakarta");
            //     $file = base64_decode($shippingLabel);
            //     // dd($file, $shippingLabel, $responseShippingLabel, $getShippingLabel);
            //     $uniqueFileName = str_replace(" ","",time()."-".$kode.'.pdf');
            //     \Storage::put('public/label/'.$uniqueFileName, $file);
            //     $filePath = 'storage/label/'.$uniqueFileName;
            //     // dd($qty, $shippingLabel, $filePath);
            //     $update = \DB::table('bookingan')->where('id', $getOrder[0]->id)->where('platform', 'Blibli')->update([
            //         'buktiBayar' => $filePath,
            //         'package_id'=>$packageId
            //     ]);
            //     // dd($id, $order);
            //  }catch(\GuzzleHttp\Exception\ClientException $ex){
            //      $response = json_decode($ex->getResponse()->getBody()->getContents());
            //     //  dd($response);
            //     // return response()->json($response);
            //  }
        }

        return response()->json($getOrder);
    }

    public function searchOrder(Request $request, $type){
        // dd($request);
        if($type == 'dropoff'){
            // tokped
            $getTokpedDrop = DB::table('bookingan');
            $getTokpedDrop->where('status', 'logistic');
            $getTokpedDrop->where('platform', 'Tokopedia');
            $getTokpedDrop->where('logistic_type', 'dropoff');
            if($request->toko){
                $toko = $request->toko;
                $getTokpedDrop->where(function ($getTokpedDrop) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                            $getShop = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('store_name', $eachToko)->first();

                        $getTokpedDrop->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                $getShop = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('store_name', $request->toko)->first();
                $getTokpedDrop->where('nmToko',$getShop->shop_id);
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getTokpedDrop->where(function ($getTokpedDrop) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getTokpedDrop->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });

                // foreach($request->kurir as $eachKurir){
                //     $get->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                // }
            }

            if($request->start_date){
                // dd($startDate);
                $getTokpedDrop->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getTokpedDrop->whereDate('created_at', '<=', $request->end_date);
            }
            $datasTokpedDrop = $getTokpedDrop->get();

            foreach($datasTokpedDrop as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('shop_id', $data->nmToko)->first();
                $datasTokpedDrop[$key]->store_name = $nmToko->store_name;
            }

            // shopee
            $getShopeeDrop = DB::table('bookingan');
            $getShopeeDrop->where('status', 'logistic');
            $getShopeeDrop->where('platform', 'Shopee');
            $getShopeeDrop->where('logistic_type', 'dropoff');
            if($request->toko){
                $toko = $request->toko;
                $getShopeeDrop->where(function ($getShopeeDrop) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Shopee')->where('store_name', $eachToko)->first();

                        $getShopeeDrop->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getShopeeDrop->where(function ($getShopeeDrop) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getShopeeDrop->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                // foreach($request->kurir as $eachKurir){
                //     $get->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                // }
            }

            if($request->start_date){
                // dd($startDate);
                $geShopeeDropt->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getShopeeDrop->whereDate('created_at', '<=', $request->end_date);
            }
            $datasShopeeDrop = $getShopeeDrop->get();
            foreach($datasShopeeDrop as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Shopee')->where('shop_id', $data->nmToko)->first();
                $datasShopeeDrop[$key]->store_name = $nmToko->store_name;
            }
            // blibli
            $getBlibliDrop = DB::table('bookingan');
            $getBlibliDrop->where('status', 'logistic');
            $getBlibliDrop->where('platform', 'Blibli');
            $getBlibliDrop->where('logistic_type', 'dropoff');
            if($request->toko){
                $toko = $request->toko;
                $getBlibliDrop->where(function ($getBlibliDrop) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Blibli')->where('store_name', $eachToko)->first();

                        $getShopeeDrop->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getBlibliDrop->where(function ($getBlibliDrop) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getBlibliDrop->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                // foreach($request->kurir as $eachKurir){
                //     $get->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                // }
            }

            if($request->start_date){
                // dd($startDate);
                $getBlibliDrop->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getBlibliDrop->whereDate('created_at', '<=', $request->end_date);
            }
            $datasBlibliDrop = $getBlibliDrop->get();
            foreach($datasBlibliDrop as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Blibli')->where('shop_id', $data->nmToko)->first();
                $datasBlibliDrop[$key]->store_name = $nmToko->store_name;
            }

            $countTokpedDrop = count($datasTokpedDrop);
            $countBlibliDrop = count($datasBlibliDrop);
            $countShopeeDrop = count($datasShopeeDrop);
            $countAllDrop = $countTokpedDrop + $countBlibliDrop + $countShopeeDrop;
            $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();
            // pick
            $datasTokped = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasBlibli = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasShopee = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

            $countAllPick = $datasTokped + $datasBlibli + $datasShopee;
            $id = 5;

            $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();
            return view('station.pages.drop', compact('datasTokpedDrop', 'datasBlibliDrop', 'datasShopeeDrop', 'countTokpedDrop', 'countBlibliDrop', 'countShopeeDrop', 'countAllDrop', 'countAllPack', 'countAllPick', 'id', 'allStore'));
        }elseif($type == 'pick'){
            // tokped
            $getTokped = DB::table('bookingan');
            $getTokped->where('status', 'logistic');
            $getTokped->where('platform', 'Tokopedia');
            $getTokped->where('logistic_type', 'pickUp');
            if($request->toko){
                $toko = $request->toko;
                $getTokped->where(function ($getTokped) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('store_name', $eachToko)->first();

                        $getTokped->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getTokped->where(function ($getTokped) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getTokped->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });

            }

            if($request->start_date){
                $getTokped->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getTokped->whereDate('created_at', '<=', $request->end_date);
            }
            $datasTokped = $getTokped->get();
             foreach($datasTokped as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('shop_id', $data->nmToko)->first();
                $datasTokped[$key]->store_name = $nmToko->store_name;
            }

            // shopee
            $getShopee = DB::table('bookingan');
            $getShopee->where('status', 'logistic');
            $getShopee->where('platform', 'Shopee');
            $getShopee->where('logistic_type', 'pickUp');
            if($request->toko){
                $getShopee->where(function ($getShopee) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Shopee')->where('store_name', $eachToko)->first();

                        $getShopee->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getShopee->where(function ($getShopee) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                         $eachKurir = $kurir[$i];
                         if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getShopee->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                foreach($request->kurir as $eachKurir){
                    $getShopee->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                }
            }

            if($request->start_date){
                // dd($startDate);
                $getShopee->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getShopee->whereDate('created_at', '<=', $request->end_date);
            }
            $datasShopee = $getShopee->get();
            foreach($datasShopee as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Shopee')->where('shop_id', $data->nmToko)->first();
                $datasShopee[$key]->store_name = $nmToko->store_name;
                // dd($nmToko);
            }
            // blibli
            $getBlibli = DB::table('bookingan');
            $getBlibli->where('status', 'logistic');
            $getBlibli->where('platform', 'Blibli');
            $getBlibli->where('logistic_type', 'pickUp');
            if($request->toko){
                $kurir = $request->kurir;
                $getBlibli->where(function ($getBlibli) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getBlibli->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getBlibli->where(function ($getBlibli) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getBlibli->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                foreach($request->kurir as $eachKurir){
                    $getBlibli->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                }
            }

            if($request->start_date){
                // dd($startDate);
                $getBlibli->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getBlibli->whereDate('created_at', '<=', $request->end_date);
            }
            $datasBlibli = $getBlibli->get();
            foreach($datasBlibli as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Blibli')->where('shop_id', $data->nmToko)->first();
                $datasBlibli[$key]->store_name = $nmToko->store_name;
            }

            $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

            $countAllDrop = $datasTokpedDrop + $datasBlibliDrop + $datasShopeeDrop;
            $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();
            $countTokped = count($datasTokped);
            $countBlibli = count($datasBlibli);
            $countShopee = count($datasShopee);
            $countAllPick = $countTokped + $countBlibli + $countShopee;
            $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();
            $id = 5;
            return view('station.pages.pick', compact('datasTokped', 'datasBlibli', 'datasShopee', 'countTokped', 'countBlibli', 'countShopee', 'countAllPick', 'countAllDrop', 'countAllPack', 'id', 'allStore'));
        }else{
            // tokped
            //  dd($request);
            $getTokped = DB::table('bookingan');
            $getTokped->where('status', 'end order');
            $getTokped->where('platform', 'Tokopedia');
            if($request->toko){

                $toko = $request->toko;
                $getTokped->where(function ($getTokped) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('store_name', $eachToko)->first();

                        $getTokped->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getTokped->where(function ($getTokped) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getTokped->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });

            }

            if($request->start_date){
                $getTokped->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getTokped->whereDate('created_at', '<=', $request->end_date);
            }
            $datasTokped = $getTokped->get();
             foreach($datasTokped as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('shop_id', $data->nmToko)->first();
                $datasTokped[$key]->store_name = $nmToko->store_name;
            }

            // shopee
            $getShopee = DB::table('bookingan');
            $getShopee->where('status', 'end order');
            $getShopee->where('platform', 'Shopee');
            if($request->toko){
                $getShopee->where(function ($getShopee) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Shopee')->where('store_name', $eachToko)->first();

                        $getShopee->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getShopee->where(function ($getShopee) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                         $eachKurir = $kurir[$i];
                         if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getShopee->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                foreach($request->kurir as $eachKurir){
                    $getShopee->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                }
            }

            if($request->start_date){
                // dd($startDate);
                $getShopee->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getShopee->whereDate('created_at', '<=', $request->end_date);
            }
            $datasShopee = $getShopee->get();
            foreach($datasShopee as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Shopee')->where('shop_id', $data->nmToko)->first();
                $datasShopee[$key]->store_name = $nmToko->store_name;
                // dd($nmToko);
            }
            // blibli
            $getBlibli = DB::table('bookingan');
            $getBlibli->where('status', 'end order');
            $getBlibli->where('platform', 'Blibli');
            if($request->toko){
                $toko = $request->toko;
                $getBlibli->where(function ($getBlibli) use($toko) {
                    for($i = 0; $i < count($toko); $i++){
                        $eachToko = $toko[$i];
                        $getShop = DB::table('Registered_store')->where('Platform', 'Blibli')->where('store_name', $eachToko)->first();

                        $getBlibli->orWhere('nmToko', $getShop->shop_id);
                    }
                });
            }
            if($request->kurir){
                $kurir = $request->kurir;
                $getBlibli->where(function ($getBlibli) use($kurir) {
                    for($i = 0; $i < count($kurir); $i++){
                        $eachKurir = $kurir[$i];
                        if($eachKurir == 'Gojek'){
                            $eachKurir = 'Go';
                        }
                        $getBlibli->orWhere('kurir', 'LIKE','%'.$eachKurir.'%');
                    }
                });
                foreach($request->kurir as $eachKurir){
                    $getBlibli->where('kurir', 'LIKE', '%'.$eachKurir.'%');
                }
            }

            if($request->start_date){
                // dd($startDate);
                $getBlibli->whereDate('created_at', '>=', $request->start_date);
            }
            if($request->end_date){
                $getBlibli->whereDate('created_at', '<=', $request->end_date);
            }
            $datasBlibli = $getBlibli->get();
            foreach($datasBlibli as $key=>$data){
                $nmToko = DB::table('Registered_store')->where('Platform', 'Blibli')->where('shop_id', $data->nmToko)->first();
                $datasBlibli[$key]->store_name = $nmToko->store_name;
            }

            $datasTokpedPick = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasBlibliPick = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasShopeePick = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'pickUp')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

            $datasTokpedDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Tokopedia')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasBlibliDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Blibli')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();
            $datasShopeeDrop = DB::table('bookingan')->where('bookingan.status', 'logistic')->where('bookingan.logistic_type', 'dropoff')->where('bookingan.platform', 'Shopee')->join('Registered_store', 'bookingan.nmToko', 'Registered_store.shop_id')->count();

            $countAllDrop = $datasTokpedDrop + $datasBlibliDrop + $datasShopeeDrop;
            $countAllPack = DB::table('bookingan')->where('status', 'packing')->count();
            $countTokped = count($datasTokped);
            $countBlibli = count($datasBlibli);
            $countShopee = count($datasShopee);
            $countAllPick = $datasTokpedPick + $datasBlibliPick + $datasShopeePick;
            $allStore = DB::table('Registered_store')->where('status', 'active')->groupBy('store_name')->get();
            $id = 5;
            return view('station.pages.pick', compact('datasTokped', 'datasBlibli', 'datasShopee', 'countTokped', 'countBlibli', 'countShopee', 'countAllPick', 'countAllDrop', 'countAllPack', 'id', 'allStore'));
        }

    }

   public function balasChat(Request $request){
      if($request->platform == 'Tokopedia' || $request->platform == 'tokopedia'){
            $client = new Client();
            $clientId = env('TOKOPEDIA_CLIENT_ID');
            $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
            $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
            $clientData = base64_encode("{$clientId}:{$clientSecret}");

            $appID = env('TOKOPEDIA_APP_ID');
            try{
            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                'Authorization'=> "Basic $clientData",
                'Content-Length' => 0,
                'User_Agent' => 'PostmanRuntime/7.17.1'
                ]])->getBody()->getContents();
            }catch (RequestException $e) {
                // dd(Psr7\str($e->getRequest()));
                echo Psr7\str($e->getRequest());
                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }
            }
            $token = json_decode($res);

            $data = DB::table('message_list')->where('id', $request->id)->first();
            $msgId = (int)$data->msg_id;
            $shopId = (int)$data->shop_id;
            // dd($msgId, $data, $request->balasan);
            $clientSendChat = new Client();
            $resSendChat = $clientSendChat->post("https://fs.tokopedia.net/v1/chat/fs/$appID/messages/$msgId/reply", ['headers'=>[
                'Authorization' => "Bearer $token->access_token",
                ],
                'json'=>[
                    'shop_id'=>$shopId,
                    'message'=>$request->message,
                    ]
                ])->getBody()->getContents();
            $responseSendChat = json_decode($resSendChat);
            $insert = DB::table('cs_chat_replied')->insert([
                'name_id'=>CRUDBooster::myID(),
                'msg'=>$request->message,
                'time'=>$request->fullDate,
                'platform'=>'Tokopedia',
                'msg_list_id'=>$request->id
            ]);
            $insertLog = DB::table('chat_log')->insert([
                'msg_id'=>$responseSendChat->data->msg_id,
                'sender_id'=>$responseSendChat->data->sender_id,
                'role'=>$responseSendChat->data->role,
                'sender_id'=>$responseSendChat->data->sender_id,
                'msg'=>$responseSendChat->data->msg,
                'reply_time'=>$responseSendChat->data->reply_time,
                'from'=>$responseSendChat->data->from,
            ]);
            return response()->json('success');

            // CRUDBooster::redirect('admin/message_list', 'pesan telah dibalas', 'success');
            // return redirect->back()

        }elseif($request->platform == 'Shopee'){
            //   return response()->json($request->message);
            $data = DB::table('message_list')->where('id', $request->id)->first();
            // $shop = DB::table('Registered_store')->where('shop_id', $data->shop_id)->where('Platform', 'Shopee')->first();
            $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $data->shop_id)->first();
            $access_token = $shop->chat_access_token;
                try{
                    $shop_id = $shop->shop_id;
                    $partner_id = (int)env('SHOPEE_PARTNER_CHAT_ID');
                    $partner_key = env('SHOPEE_PARTNER_CHAT_KEY');
                // cek if access token is valid or not
                    $access_expired_time = Carbon::parse($shop->chat_access_token_created)->addSecond($shop->chat_expired_in_second)->format('Y-m-d H:i:s');
                    $currentHour =  Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s');
                    $currentHourTimestamp = strtotime($currentHour);
                    $access_expired_time = strtotime($access_expired_time);
                    // dd($access_expired_time, $currentHour>$access_expired_time, $currentHour);
                    // if($currentHourTimestamp>$access_expired_time){
                    //     $refresh_token = $shop->chat_refresh_token;
                    //     $path_refresh_token = "/api/v2/auth/access_token/get";
                    //     $salt_refresh = $partner_id.$path_refresh_token.$timestamp;
                    //     $sign_refresh = hash_hmac('sha256', $salt_refresh, $partner_key);
                    //     $clientRefresh = new Client();
                    //     $resRefresh = $clientRefresh->post("https://partner.shopeemobile.com/api/v2/auth/access_token/get?partner_id=$partner_id&timestamp=$timestamp&sign=$sign_refresh", ["headers"=>[
                    //             'Content-Type' => 'application/json',
                    //         ],
                    //         'json'=> [
                    //                 'shop_id'=>$shop_id,
                    //                 'refresh_token'=>$refresh_token,
                    //                 'partner_id'=>$partner_id
                    //             ]
                    //         ])->getBody()->getContents();
                    //     $response = json_decode($resRefresh);
                    //     $access_token = $response->access_token;
                    //     $refresh_token = $response->refresh_token;
                    //     $expired_in = $response->expire_in;
                    //     DB::table('Shopee_shop')->where('shop_id', $shop_id)->update([
                    //             'chat_access_token'=>$access_token,
                    //             'chat_refresh_token'=>$refresh_token,
                    //             'chat_expired_in_second'=>$expired_in,
                    //             'chat_access_token_created'=>$currentHour,
                    //         ]);
                    // }

                    //    sendchat
                    $timestamp = time();
                    $path = "/api/v2/sellerchat/send_message";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientChat = new Client();
                    // dd($data->msg_id, $request->balasan, $partner_id, $partner_key, $shop_id);
                    $resSendChat = $clientChat->post("https://partner.shopeemobile.com/api/v2/sellerchat/send_message?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=> [
                        'Content-Type' => 'application/json',
                    ],
                        'json'=> [
                            'to_id'=>(int)$data->to_id,
                            'message_type'=>'text',
                            'content'=>[
                              'text'=> $request->message,
                            ]
                        ]
                    ])->getBody()->getContents();
                    $responseSendChat = json_decode($resSendChat);

                    // dd($responseSendChat, $resSendChat);
                    $insert = DB::table('cs_chat_replied')->insert([
                        'name_id'=>CRUDBooster::myID(),
                        'msg'=>$request->message,
                        'time'=>$request->fullDate,
                        'platform'=>'Shopee',
                        'msg_list_id'=>$request->id
                    ]);
                    return response()->json('success');

                    // CRUDBooster::redirect('admin/message_list', 'pesan telah dibalas', 'success');
                }catch (\GuzzleHttp\Exception\ClientException $e){
                    // dd(json_decode($e->getResponse()->getBody()->getContents()));
                    $tes = json_decode($e->getResponse()->getBody()->getContents());
                    // dd($e);
                    return response()->json($tes);
                }

        }


    //   return response()->json('success');
   }

  public function print($path){
     $pdf = decrypt($path);
     dd($pdf);
    //  return response()->json($pdf);
    // $connector = new FilePrintConnector("4BARCODE_4B-2054L");
    $connector = new WindowsPrintConnector("4BARCODE_4B-2054L");
    $printer = new Printer($connector);
    try {
        echo 'masuk';
        $pages = ImagickEscposImage::loadPdf($pdf);
        dd($pages);
        foreach ($pages as $page) {
            $printer -> graphics($page);
        }
        $printer -> cut();
        echo 'tes';
    } catch (Exception $e) {
        /*
    	 * loadPdf() throws exceptions if files or not found, or you don't have the
    	 * imagick extension to read PDF's
    	 */
    // 	 return response()->json('failed');
        echo $e -> getMessage() . "\n";
    } finally {
        $printer -> close();
        return response()->json('success');
    }
  }

  public function filterDate(Request $request){
    //   dd($request);
      return redirect()->back()->with('date', $request->filterDate);
  }

  public function updateOrderCekInvoice($bookinganId){

      $checkTrackingNumber = DB::table('bookingan')->where('id', $id)->first();
      $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $checkTrackingNumber->nmToko)->first();
    //   $getToko = DB::table('Registered_store')->where('shop_id', $checkTrackingNumber->nmToko)->where('Platform', 'Shopee')->first();
        $access_token=$getToko->access_token;
        $shop_id=$getToko->shop_id;
        $partner_id = (int)env('SHOPEE_PARTNER_ID');
        $partner_key = env('SHOPEE_PARTNER_KEY');
        $order_sn = $checkTrackingNumber->kode;
        $timestamp = time();

        // get shipping document parameter
        $path = "/api/v2/logistics/get_shipping_document_parameter";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $data = Array();
        $data[0]['order_sn']=$order_sn;
        $data[0]['package_number']=$package_number;
        $clientShippingDocumentParameter = new Client();
        $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],

                'json'=>[
                    'order_list'=>$data
                ]
        ])->getBody()->getContents();
        $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
        $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;

        // get shipping document result
        $path = "/api/v2/logistics/get_shipping_document_result";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $dataDocResult = Array();
        $dataDocResult[0]["order_sn"]=$order_sn;
        $dataDocResult[0]["package_number"]=$package_number;
        $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
        // dd($dataDocResult);
        $clientShippingDocResult = new Client();
        $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
            'Content-Type' => 'application/json',
        ],
            'json'=>[
                'order_list'=>$dataDocResult
            ]
        ])->getBody()->getContents();
        $responseShippingDocResult = json_decode($resShippingDocResult);
        $status = $responseShippingDocResult->response->result_list[0]->status;
        // dd($status, $responseShippingDocResult);
        $path = "/api/v2/logistics/get_tracking_number";
        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
        $sign = hash_hmac('sha256', $salt, $partner_key);
        $clientTrackingNumber = new Client();
        $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
        $responseTrackingNumber = json_decode($resTrackingNumber);
        $trackingNumberResponse = $responseTrackingNumber->response->tracking_number;
        $update = DB::table('bookingan')->where('id', $id)->update([
            'awbNo'=>$trackingNumberResponse
        ]);
      $trackingNumber = $trackingNumberResponse;
      if(empty($trackingNumber) && $status != 'READY'){
         $trackingNumber ='tidak ada';
        return response()->json($trackingNumber);
      }elseif(!empty($trackingNumber) && $status == 'READY'){
          return response()->json($trackingNumber);
      }
  }
  public function filterMarketplace($marketplace){
      if($marketplace == 'Shopee'){
        $getStore = DB::connection('gudang')->table('Shopee_shop')->get();
      }else{
        $getStore = DB::table('Registered_store')->where('Platform', $marketplace)->get();
      }

      return response()->json($getStore);
  }

  public function updateProductPerStore($store, $marketplace){
      $shop_id = (int)$store;
      if($marketplace == 'Shopee'){
          $page = 0;
          do{
            // echo 'page:'.$page.'<br/>';
            $next = $this->updateProductShopee($shop_id, $page);
            // echo $next.'<br/>';
            $page = $page + 1;
          }while($next == true);
          echo 'sukses';
      }elseif($marketplace == 'Blibli'){
          $page = 0;

            do{
                // echo $shopId;
                $next = $this->updateProductBlibli( $shop_id, $page);
                $page = $page + 1;
            }while($next == true);
            echo 'sukses';
      }elseif($marketplace == 'Tokopedia'){
          $page = 0;
          do{
                // echo 'page:'.$page.'<br/>';
                $parse = $this->updateProductTokopedia($shop_id, $page);
                $page = $page + 1;
                // echo 'parse: '.$parse.'<br/>';
            }while($parse>24);
      }else{
          $page = 1;

            do{
                $spuShop = $this->getUpdateSpu($shop_id, $page);
                if($spuShop == 20){
                    $more = true;
                }else{
                    $more = false;
                }
                $page = $page + 1;
            }while($more == true);

        }

        echo 'finish';
  }
  public function updateProductShopee($shop_id, $page){
      try{
          $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
        //   $shop = DB::table('Registered_store')->where('shop_id', $shop_id)->where('Platform', 'Shopee')->first();
          $gudang = $shop->toko;
          $access_token = $shop->access_token;
          $partner_id = (int)env('SHOPEE_PARTNER_ID');
          $partner_key = env('SHOPEE_PARTNER_KEY');
          $timestamp = time();
          $path = "/api/v2/product/get_item_list";
          $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
          $sign = hash_hmac('sha256', $salt, $partner_key);

          $clientProductList = new Client();
          $resProductList = $clientProductList->get("https://partner.shopeemobile.com/api/v2/product/get_item_list?offset=$page&page_size=50&item_status=NORMAL&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
          $response = json_decode($resProductList)->response->has_next_page;

          $responseProductList = json_decode($resProductList)->response->item;

          if(!empty($responseProductList)){
                $countProductList = count($responseProductList);
            }else{
                $countProductList = 0;
            }
            echo $countProductList.'<br/>';
            if($countProductList > 0){
                // looping untuk dapat product info
                $count  = 0;
                foreach($responseProductList as $key=>$product){
                    $count = $count + 1;
                    echo 'count:'.$count.'<br/>';
                    // get item info
                    $item_id_list = $product->item_id;
                    $path = "/api/v2/product/get_item_base_info";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientProductInfo = new Client();
                    $resProductInfo = $clientProductInfo->get("https://partner.shopeemobile.com/api/v2/product/get_item_base_info?item_id_list=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                    $responseProductInfo = json_decode($resProductInfo);
                    $productData = $responseProductInfo->response->item_list[0];

                    // check
                    $check = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id', $gudang)->count();

                    // product exist
                    if($check > 0){

                        echo $check.'update<br/>'.$productData->item_name.'<br/>';
                        $updateProduk = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id',$gudang)->update([
                            'product_name'=>$productData->item_name,
                            'gudang_id'=>$gudang,
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);

                        // $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('sku', $productData->item_sku)->count();
                        $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->count();

                        $getParentId = DB::table('produk')->where('product_name', $productData->item_name)->where('gudang_id', $gudang)->first();
                        if($productData->has_model == true){
                            $variant = 1;
                        }else{
                            $variant = 0;
                        }
                        if($checkProdukMarketplace < 1){
                            $insertProduk = DB::table('produk_marketplace')->insertGetId([
                                'parent_produk_id'=>$getParentId->id,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shop_id,
                                'has_variant'=>$variant,
                                'sku'=>$productData->item_sku,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);
                            echo 'masuk';
                            // get model(variant)
                            if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                // dd($responseModel, $responseProductInfo);
                                foreach($responseModel->model as $key=>$value){
                                    $qty = 0;
                                    $price = $value->price_info[0]->current_price;
                                    foreach($value->stock_info as $stock){
                                        if($stock->stock_type == 2){
                                            $qty = $qty->current_stock;
                                        }
                                    }
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$insertProduk,
                                        'produk_id'=> $value->model_id,
                                        'sku'=>$value->model_sku,
                                        'stock'=>$qty,
                                        'status'=>'active',
                                        'price'=>$price,
                                        'shop_id'=>$shop_id,
                                        'platform'=>'Shopee',
                                        'is_variant'=>1,
                                        'sku'=>$productData->item_sku,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                                    ]);
                                }
                            }
                        }else{
                            // echo 'masuk';
                            $update = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->update([
                                'parent_produk_id'=>$getParentId->id,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'has_variant'=>$variant,
                                'shop_id'=>$shop_id,
                                'sku'=>$productData->item_sku,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);
                            // get model(variant)
                            if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                // dd($responseModel, $responseProductInfo);

                                foreach($responseModel->model as $key=>$value){
                                    $getVariantParent = DB::table('produk_marketplace')->where('produk_id', $productData->item_id)->where('platform', 'Shopee')->first();
                                    $checkVariant = DB::table('produk_marketplace')->where('produk_id', $value->model_id)->where('parent_produk_id', $getVariantParent->id)->count();
                                    $qty = 0;
                                        $price = $value->price_info[0]->current_price;
                                        foreach($value->stock_info as $stock){
                                            if($stock->stock_type == 2){
                                                $qty = $qty + $stock->current_stock;
                                                echo 'stock'.$qty;
                                                // dd($qty);
                                            }
                                        }
                            // dd('tes', $value, $getVariantParent->id, $qty);
                                    if($checkVariant < 1){
                                        $insertVariant = DB::table('produk_marketplace')->insert([
                                            'parent_produk_id'=>$getVariantParent->id,
                                            'produk_id'=> $value->model_id,
                                            'sku'=>$value->model_sku,
                                            'stock'=>$qty,
                                            'status'=>'active',
                                            'shop_id'=>$shop_id,
                                            'price'=>$price,
                                            'platform'=>'Shopee',
                                            'is_variant'=>1,
                                            'sku'=>$productData->item_sku,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                                        ]);
                                    }else{
                                        $updateVariant = DB::table('produk_marketplace')->where('produk_id', $value->model_id)->where('parent_produk_id', $getVariantParent->id)->where('platform', 'Shopee')->update([
                                            'produk_id'=>$value->model_id,
                                            'sku'=>$value->model_sku,
                                            'stock'=>$qty,
                                            'status'=>'active',
                                            'shop_id'=>$shop_id,
                                            'price'=>$price,
                                            'platform'=>'Shopee',
                                            'is_variant'=>1,
                                            'sku'=>$productData->item_sku,
                                            'updated_at'=>date('Y-m-d H:i:s'),
                                            'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                                        ]);
                                    }
                                }
                            }

                        }

                    // product don't exist
                    }else{
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$productData->item_name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s')
                        ]);

                        if($productData->has_model == true){
                            $variant = 1;
                        }else{
                            $variant = 0;
                        }
                        $insert = DB::table('produk_marketplace')->insertGetId([
                                'parent_produk_id'=>$insertProduk,
                                'produk_id'=>$productData->item_id,
                                'sku'=>$productData->item_sku,
                                'stock'=>$productData->stock_info[0]->current_stock,
                                'status'=>'active',
                                'price'=>$productData->price_info[0]->current_price,
                                'platform'=>'Shopee',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shop_id,
                                'has_variant'=>$variant,
                                'sku'=>$productData->item_sku,
                                'page'=>$page,
                                'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                            ]);

                        if($productData->has_model){
                                // check model kalau dia punya model
                                // $item_id = $productData->item_id;
                                $path = "/api/v2/product/get_model_list";
                                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                                $sign = hash_hmac('sha256', $salt, $partner_key);
                                $clientModel = new Client();
                                $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                                $responseModel = json_decode($resModel)->response;
                                foreach($responseModel->model as $key=>$value){
                                    $qty = 0;
                                    $price = $value->price_info[0]->current_price;
                                    foreach($value->stock_info as $stock){
                                        if($stock->stock_type == 2){
                                            $qty = $qty +  $stock->current_stock;
                                        }
                                    }
                                    $insertVariant = DB::table('produk_marketplace')->insert([
                                        'parent_produk_id'=>$insert,
                                        'produk_id'=> $value->model_id,
                                        'sku'=>$value->model_sku,
                                        'stock'=>$qty,
                                        'status'=>'active',
                                        'shop_id'=>$shop_id,
                                        'price'=>$price,
                                        'platform'=>'Shopee',
                                        'sku'=>$productData->item_sku,
                                        'is_variant'=>1,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'photo'=>$responseProductInfo->response->item_list[0]->image->image_url_list[0]
                                    ]);
                                }
                            }
                    }
                    if($productData->stock_info[0]->current_stock < 5){
                          $notifSendTo = DB::table('cms_users')->where(function ($query) {
                            $query->where('id_cms_privileges', '=', 1)
                                  ->orWhere('id_cms_privileges', '=', 2);
                                })
                                  ->pluck('id');
                            $toko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
                            // $toko = DB::table('Registered_store)->where('shop_id', $shop_id)->where('Platform', 'Shopee')->first();
                            $storeName = $toko->store_name;
                            $config['content'] = "Stock Product $item->basic->name di Shopee ".$storeName." kurang dari 5";
                            // $config['to'] = $toko->link_toko;
                            $config['id_cms_users'] = $notifSendTo;
                            \CRUDBooster::sendNotification($config);
                       }
                }
            }
            // dd($response);
            return $response;
        }catch (\GuzzleHttp\Exception\ClientException $e) {
            dd(json_decode($e->getResponse()->getBody()->getContents()));

        }
  }
  public function updateProductBlibli($shop_id, $page){
    try{
            $shop = DB::table('Registered_store')->where('Platform', 'Blibli')->where('shop_id', $shop_id)->first();
            $sellerKey = $shop->seller_api_key;
            $toko = $shop->data_toko;
            $client = new Client();
            $clientId = env('BLIBLI_CLIENT_ID');
            $clientKey = env('BLIBLI_CLIENT_KEY');
            $Authorization = base64_encode("{$clientId}:{$clientKey}");
            // dd($page, $shopId, $sellerKey, $toko);
            // echo 'page: '.$page.' '.$shopId.'<br/>';
             $getProducts = $client->post('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v2/product/getProductList?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$shop_id.'&username=daniel.yokesen@gmail.com&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                    'Authorization'=> "Basic $Authorization",
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1',
                    'Api-Seller-Key' => $sellerKey
                    ],
                        'json'=>[
                            'buyable'=>true,
                            'page'=>$page,
                            'size'=>30
                            ]
                    ])->getBody();
                    $getDetail = $getProducts->getContents();
                    $responseDetailProducts = json_decode($getDetail);
                    $sizePerPage = $responseDetailProducts->pageMetaData->pageSize;
                    $totalRecords = $responseDetailProducts->pageMetaData->totalRecords;
                    $pageNumber = $responseDetailProducts->pageMetaData->pageNumber + 1;
                    $products = $responseDetailProducts->content;
                    $totalgetProduct = $sizePerPage * $pageNumber;

                    foreach($products as $key=>$product){

                        // echo $product->productName.' '.$toko.'<br/>';
                        $check = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->count();
                        echo $check.' produk<br/>';

                        if($check < 1){
                            $insertProduk = \DB::table('produk')->insertGetId([
                                'product_name'=>$product->productName,
                                'gudang_id'=>$toko,
                                'created_at'=>date('Y-m-d H:i:s'),
                                ]);
                            $insert = \DB::table('produk_marketplace')->insert([
                                'parent_produk_id'=>$insertProduk,
                                'produk_id'=>(string)$product->gdnSku,
                                'stock'=>$product->stockAvailableLv2,
                                'status'=>'active',
                                'price'=>$product->sellingPrice,
                                'platform'=>'Blibli',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shopId,
                                'sku'=>$product->merchantSku,
                                'page'=>$page,
                                'photo'=>$product->image
                                ]);
                        }else{
                            $updateProduk = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->update([
                                'product_name'=>$product->productName,
                                'gudang_id'=>$toko,
                                'updated_at'=>date('Y-m-d H:i:s'),
                                ]);

                            $check = \DB::table('produk_marketplace')->where('produk_id', $product->gdnSku)->where('platform', 'Blibli')->count();

                            $getParentId = \DB::table('produk')->where('product_name', $product->productName)->where('gudang_id', $toko)->count();
                            if($check < 1){
                                $insert = \DB::table('produk_marketplace')->insert([
                                'parent_produk_id'=>$getParentId->id,
                                'produk_id'=>$product->gdnSku,
                                'stock'=>$product->stockAvailableLv2,
                                'status'=>'active',
                                'price'=>$product->sellingPrice,
                                'platform'=>'Blibli',
                                'created_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shopId,
                                'sku'=>$product->merchantSku,
                                'page'=>$page,
                                'photo'=>$product->image
                                ]);
                            }else{
                               $update = \DB::table('produk_marketplace')->where('produk_id', $product->gdnSku)->where('platform', 'Blibli')->update([
                                // 'parent_produk_id'=>$insertProduk,
                                'produk_id'=>$product->gdnSku,
                                'stock'=>$product->stockAvailableLv2,
                                'status'=>'active',
                                'price'=>$product->sellingPrice,
                                'platform'=>'Blibli',
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'shop_id'=>$shopId,
                                'sku'=>$product->merchantSku,
                                'page'=>$page,
                                'photo'=>$product->image
                                ]);
                            }

                        }
                        if($product->stockAvailableLv2 < 5){
                          $notifSendTo = DB::table('cms_users')->where(function ($query) {
                            $query->where('id_cms_privileges', '=', 1)
                                  ->orWhere('id_cms_privileges', '=', 2);
                                })
                                  ->pluck('id');
                            $tokoNotif = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                            $storeName = $toko->store_name;
                            $config['content'] = "Stock Product $product->productName di Blibli ".$storeName." kurang dari 5";
                            // $config['to'] = $tokoNotif->link_toko;
                            $config['id_cms_users'] = $notifSendTo;
                            \CRUDBooster::sendNotification($config);
                            // dd($storeName, $product->productName);
                       }
                    }
                    if($totalgetProduct < $totalRecords){
                        // echo 'lebih';
                        return true;
                    }else{
                        // echo 'sama';
                        return false;
                    }
        }catch(\GuzzleHttp\Exception\ClientException $e){
            dd(json_decode($e->getResponse()->getBody()->getContents()));
        }
  }
  public function updateProductTokopedia($shop_id, $page){
        $shop = DB::table('Registered_store')->where('Platform', 'Tokopedia')->where('shop_id', $shop_id)->first();
        $gudang = $shop->data_toko;
        $client = new Client();
        $clientId = env('TOKOPEDIA_CLIENT_ID');
        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
        $clientData = base64_encode("{$clientId}:{$clientSecret}");
        echo $shopId.'<br/>';
        $appID = env('TOKOPEDIA_APP_ID');
        try{
        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
            'Authorization'=> "Basic $clientData",
            'Content-Length' => 0,
            'User_Agent' => 'PostmanRuntime/7.17.1'
            ]])->getBody()->getContents();
        }catch (RequestException $e) {
            // dd(Psr7\str($e->getRequest()));
            echo Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                echo Psr7\str($e->getResponse());
            }
        }
        $token = json_decode($res);

        $clientProduct = new Client();
        $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?shop_id=$shopId&page=$page&per_page=50&sort=7", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        // $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?product_id=1988194709", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
        $response = json_decode($resProduct);
        $items = $response->data;
        // dd($items, $response);
        if(!empty($items)){
            $countRow = count($items);
        }else{
            $countRow = 0;

        }
        if($countRow > 0){
            foreach($items as $item){
              $check = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->count();
              $dataProduk = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->first();
                echo 'check: '.$check.'<br/>';
                if($item->other->sku){
                    echo 'SKU<br/>';
                    $sku = $item->other->sku;
                }else{
                    echo 'no SKU<br/>';
                    $sku = null;
                }
              if($item->main_stock < 5){
                  $notifSendTo = DB::table('cms_users')->where(function ($query) {
                    $query->where('id_cms_privileges', '=', 1)
                          ->orWhere('id_cms_privileges', '=', 2);
                        })
                          ->pluck('id');
                    $toko = DB::table('Registered_store')->where('shop_id', $shop_id)->first();
                    $storeName = $toko->store_name;
                    $config['content'] = "Stock Product $item->basic->name di Tokopedia ".$storeName." kurang dari 5";
                    $config['to'] = $toko->link_toko;
                    $config['id_cms_users'] = $notifSendTo;
                    \CRUDBooster::sendNotification($config);
              }
              if($check < 1){
                    if($item->basic->status==1){
                        echo 'insert'.$item->basic->name.'<br/>';
                        // insert product
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$item->basic->name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s'),

                        ]);
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
                            'parent_produk_id'=>$insertProduk,
                            'produk_id'=>(string)$item->basic->productID,
                            'stock'=>$item->main_stock,
                            'status'=>'active',
                            'price'=>$item->price->value,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'platform'=>'Tokopedia',
                            'shop_id'=>$shopId,
                            'sku'=>$sku,
                            'page'=>$page,
                            'photo'=>$item->pictures[0]->OriginalURL
                            ]);
                    }
                    elseif($item->basic->status==2){
                        // insert product
                        echo 'insert2'.$item->basic->name.'<br/>';
                        $insertProduk = DB::table('produk')->insertGetId([
                            'product_name'=>$item->basic->name,
                            'gudang_id'=>$gudang,
                            'created_at'=>date('Y-m-d H:i:s'),

                        ]);
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
                            'parent_produk_id'=>$insertProduk,
                            'produk_id'=>(string)$item->basic->productID,
                            'stock'=>$item->main_stock,
                            'status'=>'Best (Featured Product)',
                            'price'=>$item->price->value,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'platform'=>'Tokopedia',
                            'sku'=>$sku,
                            'shop_id'=>$shopId,
                            'page'=>$page,
                            'photo'=>$item->pictures[0]->OriginalURL
                            ]);
                    }else{
                        echo 'else<br/>';
                    }

                }else{
                    // dd('masuk else');
                    // echo 'update'.$item->basic->name.'<br/>';
                    $update = DB::table('produk')->where('id', $dataProduk->id)->update([
                        'product_name'=>$item->basic->name,
                        'gudang_id'=>$gudang,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        ]);
                    // Check for updates
                    $checkProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->count();
                    // kalau sku sudah jalan semua
                    // $checkProdukMarketplace = DB::table('produk_marketplace')->where('sku', $sku)->where('platform', 'Tokopedia')->count();
                    echo $checkProdukMarketplace.'<br/>';
                    $getParentId = DB::table('produk')->where('product_name', $item->basic->name)->where('gudang_id', $gudang)->first();
                    if($checkProdukMarketplace > 0){
                        if($item->basic->status==1){
                            // echo 'data double'.$item->basic->name.'<br/>';
                            // kalau stock terakhir
                            // bukan insert tp update

                            // $productId =
                            $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'active',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL
                                    ]);

                        }elseif($item->basic->status==2){
                            // echo 'data double2'.$item->basic->name.'<br/>';
                           $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'Best (Featured Product)',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL
                                    ]);
                        }elseif($item->basic->status==3){
                            // echo 'data double3'.$item->basic->name.'<br/>';
                           $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'Inactive',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL
                                    ]);
                        }else{
                            // echo 'data double else'.$item->basic->name.'<br/>';
                            $updateProdukMarketplace = DB::table('produk_marketplace')->where('produk_id', $item->basic->productID)->where('platform', 'Tokopedia')->update([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'inactive',
                                    'price'=>$item->price->value,
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'shop_id'=>$shopId,
                                    'sku'=>$sku,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL
                                    ]);
                        }
                    }else{
                        echo 'produk_marketplace<br/>';
                        $insertProdukMarketplace = DB::table('produk_marketplace')->insert([
                                    'parent_produk_id'=>$dataProduk->id,
                                    'produk_id'=>(string)$item->basic->productID,
                                    'stock'=>$item->main_stock,
                                    'status'=>'inactive',
                                    'price'=>$item->price->value,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'platform'=>'Tokopedia',
                                    'sku'=>$sku,
                                    'shop_id'=>$shopId,
                                    'page'=>$page,
                                    'photo'=>$item->pictures[0]->OriginalURL
                                    ]);
                    }
                }
            }
        }

        return $countRow;
  }
  public function getUpdateSpu($page, $shop){
        $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";
        $req = new SellerProductGetWareInfoListByVendorIdRequest();
        $req->setPage($page);
        $req->setSize( 20 );

        $respSpuShop = $c->execute($req, $c->accessToken);

        // dd($respSpuShop);
        foreach($respSpuShop->jingdong_seller_product_getWareInfoListByVendorId_response->returnType->model->spuInfoVoList as $spu){
            $sku = $this->getsku($page, $shop, $spu);

        }
        $count = count($respSpuShop->jingdong_seller_product_getWareInfoListByVendorId_response->returnType->model->spuInfoVoList);
        return $count;
    }
  public function updateProductJDID($shop_id, $page){
      $c = new JdClient();
        $c->appKey = env('APP_KEY');
        $c->appSecret = env('APP_SECRET');
        $c->accessToken = $shop->access_token;
        $c->serverUrl = "https://open-api.jd.id/routerjson";
        $req = new SellerProductGetSkuInfoBySpuIdAndVenderIdRequest();
        $req->setSpuId( $spu->spuId );

        $respSku = $c->execute($req, $c->accessToken);
        // dd($respSku, $shop);
        foreach($respSku->jingdong_seller_product_getSkuInfoBySpuIdAndVenderId_response->returnType->model as $sku){
            $skuId = (object)[
                    'skuId'=>$sku->skuId
                ];
            $skuEncode = json_encode($skuId);
            // dd($skuEncode);
            $skuNo = $sku->skuId;
            $c = new JdClient();
            $c->appKey = env('APP_KEY');
            $c->appSecret = env('APP_SECRET');
            $c->accessToken = $shop->access_token;
            $c->serverUrl = "https://open-api.jd.id/routerjson";
            $req = new EpistockQueryEpiMerchantWareStockRequest();
            // $req->setWareStockQueryListStr([{'sku':$sku->skuId}]);
            $req->setWareStockQueryListStr("[{'skuId':$sku->skuId}]");

            $respStock = $c->execute($req, $c->accessToken);
            // dd($respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1], $sku);
            $check = DB::table('produk')->where('product_name', $sku->skuName)->where('gudang_id', $shop->data_toko)->count();
            $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
            if($stock < 5){
              $notifSendTo = DB::table('cms_users')->where(function ($query) {
                $query->where('id_cms_privileges', '=', 1)
                      ->orWhere('id_cms_privileges', '=', 2);
                    })
                      ->pluck('id');
                $toko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $shop_id)->first();
                // $toko = DB::table('Shopee_shop')->where('shop_id', $shop_id)->where('Platform', 'Shopee')->first();
                $storeName = $toko->store_name;
                $config['content'] = "Stock Product $item->basic->name di JDID ".$storeName." kurang dari 5";
                // $config['to'] = $toko->link_toko;
                $config['id_cms_users'] = $notifSendTo;
                \CRUDBooster::sendNotification($config);
           }
            echo 'check: '.$check.'<br/>';

            $gambar = DB::table('produk_marketplace')->where('sku', $sku->sellerSkuId)->where('platform', 'Tokopedia')->first();
            if($check < 1){
                $insertProduk = DB::table('produk')->insertGetId([
                'product_name'=>$sku->skuName,
                'gudang_id'=>$shop->data_toko,
                'created_at'=>date('Y-m-d H:i:s'),
                ]);

                $checkDetail = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->count();
                echo $checkDetail.'<br/>';
                if($checkDetail < 1){
                    $insert = DB::table('produk_marketplace')->insert([
                    'parent_produk_id'=>$insertProduk,
                    'produk_id'=>$sku->skuId,
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock
                    ]);
                }else{
                    $update = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->update([
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock
                    ]);
                }
            }else{
                $updateProduk = DB::table('produk')->where('product_name', $sku->skuName)->update([
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);

                $checkDetail = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->count();

                echo $checkDetail.'<br/>';
                if($checkDetail < 1){
                    $getProduk = DB::table('produk')->where('product_name', $sku->skuName)->first();
                    $insert = DB::table('produk_marketplace')->insert([
                    'parent_produk_id'=>$getProduk->id,
                    'produk_id'=>$sku->skuId,
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock
                    ]);
                }else{
                   $update = DB::table('produk_marketplace')->where('platform', 'JDID')->where('produk_id', $sku->skuId)->update([
                    'photo'=>$gambar->photo,
                    'sku'=>$sku->sellerSkuId,
                    'status'=>'active',
                    'price'=>$sku->jdPrice,
                    'platform'=>'JDID',
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'shop_id'=>$shop->shop_id,
                    'stock'=>$stock
                    ]);
                }
            }
        }
  }

  public function submitEditInvoice(Request $request, $id){
      $getOrder = DB::table('bookingan')->where('id', $id)->first();

      $edit = DB::table('bookingan')->where('id', $id)->update([
         'toko'=>$request->toko,
         'nama_pemesan'=>$request->nama,
         'phone'=>$request->phone,
         'alamat'=>$request->alamat,
         'platform'=>$request->platform,
         'buktiBayar'=>$request->invoice,
         'kurir'=>$request->kurir,
         ]);
  }

  public function prosesShopeeBlibli($bookinganId){

    $getOrder = DB::table('bookingan')->where('id', $bookinganId)->first();
    $getDetailProduk = DB::table('order_from_marketplace_detail')->where('orderId', $bookinganId)->get();
    foreach($getDetailProduk as $produk){
        // if(($getOrder->shop_id == '3897048' && $getOrder->platform == 'Tokopedia') || ($getOrder->shop_id == '510976397' && $getOrder->platform == 'Shopee') || ($getOrder->platform == 'Sinde Home Delivery') ){
        if(($getOrder->nmToko == 'warisan-1' && $getOrder->platform == 'Warisan') || ($getOrder->nmToko == '3897048' && $getOrder->platform == 'Tokopedia') || ($getOrder->nmToko == '510976397' && $getOrder->platform == 'Shopee') || ($getOrder->platform == 'Sinde Home Delivery') || ($getOrder->nmToko == '406576202' && $getOrder->platform == 'Shopee') || ($getOrder->nmToko == '10696883' && $getOrder->platform == 'Tokopedia') || ($getOrder->nmToko == '9040202' && $getOrder->platform == 'Tokopedia') || ($getOrder->nmToko== '8383447' && $getOrder->platform == 'Tokopedia') || ($getOrder->nmToko == 'WAM-70000' && $getOrder->platform == 'Blibli') || ($getOrder->nmToko == 'WAG-60046' && $getOrder->platform == 'Blibli') || ($getOrder->nmToko == 'WAH-60049' && $getOrder->platform == 'Blibli') || ($getOrder->nmToko == '265342933' && $getOrder->platform == 'Shopee') || ($getOrder->nmToko == '281934945' && $getOrder->platform == 'Shopee')){
            // untuk shopee cek apakah variant atau bukan
            // dd('masuk');
                if($getOrder->platform == 'Shopee'){
                    if(!empty($produk->model_id)){
                        $produkTerjual= DB::table('produk_marketplace')->where('produk_id', $produk->model_id)->where('platform', $getOrder->platform)->first();
                    }else{
                        $produkTerjual= DB::table('produk_marketplace')->where('produk_id', $produk->product_id)->where('platform', $getOrder->platform)->first();
                    }
                }else{
                    $produkTerjual= DB::table('produk_marketplace')->where('produk_id', $produk->product_id)->where('platform', $getOrder->platform)->first();
                }
            if(empty($produkTerjual)){
              return response()->json(['status'=>'failed', 'msg'=>'barang belum ada di teratur'],200);
            }
            elseif(empty($produkTerjual->induk_barang_id)){
                return response()->json(['status'=>'failed', 'msg'=>'barang belum memiliki induk produk'],200);
            }elseif(empty($produkTerjual->convertion_id)){
                return response()->json(['status'=>'failed', 'msg'=>'barang belum memiliki konversi'],200);
            }
            $indukProdukId = $produkTerjual->induk_barang_id;
            $produk_induk = DB::table('products')->where('id', $indukProdukId)->first();
            if($produk_induk->jenis_barang == 'assembly'){
                // dd('assembly');
            //   $getSubproducts = DB::table('assembly_subproduct')->where('assembly_subproduct.assembly_id', $produk_induk->id)->join('produk_marketplace', 'assembly_subproduct.product_id', 'produk_marketplace.induk_barang_id')->where('produk_marketplace.platform', $produkTerjual->platform)->get();
              $getSubproducts = DB::table('assembly_subproduct')->where('assembly_id', $produk_induk->id)->get();
              $currentStockAssembly = 0;
            //   dd($getSubproducts, $produk_induk);
              foreach($getSubproducts as $k=>$produkSub){
                // if($produkSub->product_type == 'ready stock'){
                // ambil last stock subproduct
                  $getLastStock = DB::table('stock')->where('induk_produk_id', $produkSub->product_id)->orderby('id','desc')->first();
                  $getLastStockNumbers = DB::table('stock_number')->where('stock_number.stock_id', $getLastStock->id)->join('convertion', 'convertion.id', 'stock_number.convertion_id')->orderBy('convertion.stock_convertion_unit', 'desc')->get();
                  
                  $currentStock = 0;
                //   per subproduct
                  $qtyTerjualdariAssembly = ($produk->qty * $produkSub->qty) * $produkTerjual->stock_convertion_unit;
                    
                  foreach($getLastStockNumbers as $index=>$numberOfStock){
                    // last stock perconvertion
                    $angkaPerConvertion = $numberOfStock->bookStockUnit_induk * $numberOfStock->stock_convertion_unit;
                    $currentStock = $currentStock + $angkaPerConvertion;
                  }
                  $stockMinusAssemblyTerjual = $currentStock - $qtyTerjualdariAssembly;
                  $stockConvertToAssembly = $stockMinusAssemblyTerjual / $produkSub->qty;
                  if($k == 0){
                    $currentStockAssembly = $stockConvertToAssembly;
                  }else if($currentStockAssembly > $stockConvertToAssembly){
                    $currentStockAssembly = $stockConvertToAssembly;
                  }
                  // return response()->json([$stockMinusAssemblyTerjual, $produk, $getLastStockNumber, $qtyTerjualdariAssembly, $currentStock, $produkTerjual, $produkSub]);
    
                    
                    // cek
                    $cek = DB::table('stock')->where('data_toko_id', $getLastStock->data_toko_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $produkSub->product_id)->count();
                  // update stock per subproduct
                  if($cek < 1){
                      $insertStockAfterMinusSale = DB::table('stock')->insertGetId([
                         'data_toko_id'=>$getOrder->toko,
                         'induk_produk_id'=>$produkSub->product_id,
                         'tipe'=>'penjualan',
                         'created_at'=>date('Y-m-d H:i:s'),
                         'documentNumber'=>$getOrder->kode
                      ]);
                  }else{
                      $data = DB::table('stock')->where('data_toko_id', $getLastStock->data_toko_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $produkSub->product_id)->first();
                  
                      $insertStockAfterMinusSale = $data->id;
                  }
                  

                  $moduloSelisihUnitIndukSub = 0;
                  $getConvertionsSub = DB::table('convertion')->where('induk_produk_id',$produkSub->product_id)->orderby('stock_convertion_unit', 'desc')->get();
                //   dd($getConvertionsSub);
                //  dd($getSubproducts, $produk_induk, $getLastStock, $getLastStockNumbers, $qtyTerjualdariAssembly, $produk, $produkSub);
                  foreach($getConvertionsSub as $iteration=>$convertionSub){
                    if($iteration == 0){
                      $modSelisihUnitInduk = $stockMinusAssemblyTerjual % $convertionSub->stock_convertion_unit;
                      $divSelisihUnitInduk  = $stockMinusAssemblyTerjual - $modSelisihUnitInduk;
                      $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertionSub->stock_convertion_unit;
                      $moduloSelisihUnitIndukSub = $modSelisihUnitInduk ;
                    }else{
                      $modSelisihUnitInduk = $moduloSelisihUnitIndukSub % $convertionSub->stock_convertion_unit;
                      $divSelisihUnitInduk  = $moduloSelisihUnitIndukSub - $modSelisihUnitInduk;
                      $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertionSub->stock_convertion_unit;
                      $moduloSelisihUnitIndukSub = $modSelisihUnitInduk;
                    }
                    // cek
                        $check = DB::table('stock_number')->where('stock_id', $insertStockAfterMinusSale)->where('convertion_id', $convertionSub->id)->count();
                    // dibagi mana yang sataunnya
                    if($check < 1){
                        if(count($getConvertionsSub) - 1 == $iteration){
                            $insertStockNumber = DB::table('stock_number')->insert([
                              'stock_id'=>$insertStockAfterMinusSale,
                              'convertion_id'=>$convertionSub->id,
                              'unit'=>$qtyTerjualdariAssembly,
                              'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                              'stockOpnameUnit_induk'=>$getLastStockNumbers[$iteration]->stockOpnameUnit_induk,
                              'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                        }else{
                            $insertStockNumber = DB::table('stock_number')->insert([
                              'stock_id'=>$insertStockAfterMinusSale,
                              'convertion_id'=>$convertionSub->id,
                              'unit'=>0,
                              'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                              'stockOpnameUnit_induk'=>$getLastStockNumbers[$iteration]->stockOpnameUnit_induk,
                              'created_at'=>date('Y-m-d H:i:s'),
                            ]);
                        }
                    }
                    
                    
                  }
                  // return response()->json([$stockMinusAssemblyTerjual, $produk, $getLastStockNumber, $qtyTerjualdariAssembly, $currentStock, $produkTerjual]);
                  // update marketplaceStock for subproduct
                  $getAllSubproductProductBranchs = DB::table('produk_marketplace')->where('induk_barang_id', $produkSub->product_id)->get();
                  foreach($getAllSubproductProductBranchs as $branchProduct){
                    $marketplaceProdukConvertion = DB::table('convertion')->where('id', $branchProduct->convertion_id)->first();
                    
                    // dd($marketplaceProdukConvertion, $branchProduct->convertion_id, $produkSub->product_id);
                    $stockToMarketplace = floor($stockMinusAssemblyTerjual / $marketplaceProdukConvertion->stock_convertion_unit);
                    
                    // fullfillment for shopee
                    if($getOrder->platform == 'Shopee'){
                        $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $getOrder->nmToko)->first();
                        // $getToko = DB::table('Registered_store')->where('shop_id', $getOrder->nmToko)->where('Platform', 'Shopee')->first();
                        $access_token=$getToko->access_token;
                        $shop_id= (int) $getToko->shop_id;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $order_sn = $getOrder->kode;
                        // $package_number = $getToko->package_id;
                        $timestamp = time();
    
                        // shipping parameter
                            $path = "/api/v2/logistics/get_shipping_parameter";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientShippingParameter = new Client();
                            $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                            $responseShippingParameter = json_decode($resShippingParameter);
                            $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                            $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                            $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                            // return response()->json([$responseShippingParameter, 'tes']);
                        // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
                        $logistic_type = '';
                        if( $responseShippingParameter->response->info_needed->pickup){
                            $logistic_type = 'pickUp';
                            // ship order
                            $path = "/api/v2/logistics/ship_order";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientShipOrder = new Client();
                            $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                                'Content-Type' => 'application/json',
                            ],
                            'json'=> [
                                    'order_sn'=>$order_sn,
                                    // 'package_number'=>$package_number,
                                    'pickup'=>[
                                        'address_id'=>$address_id,
                                        'pickup_time_id'=>$pickup_time_id,
                                        // 'tracking_number'=>$trackingNumber,
                                    ],
    
    
                                ]
                            ])->getBody()->getContents();
                            $responseShipOrder = json_decode($resShipOrder);
                            // dd($responseShipOrder);
    
                        }elseif($branch_id){
                            $logistic_type = 'dropoff';
                            // echo 'dropoff<br/>';
                            // ship order
                            $path = "/api/v2/logistics/ship_order";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientShipOrder = new Client();
                            $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                                'Content-Type' => 'application/json',
                            ],
                            'json'=> [
                                    'order_sn'=>$order_sn,
                                    'dropoff'=>[
                                        'address_id'=>$address_id,
                                        'pickup_time_id'=>$pickup_time_id,
                                        // 'tracking_number'=>$trackingNumber,
                                    ],
    
    
                                ]
                            ])->getBody()->getContents();
                            $responseShipOrder = json_decode($resShipOrder);
                            // dd($responseShipOrder, 'tes');
                        }else{
                            $logistic_type = 'dropoff';
                            // echo 'dropoff<br/>';
                            // ship order
                            $path = "/api/v2/logistics/ship_order";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientShipOrder = new Client();
                            $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                                'Content-Type' => 'application/json',
                            ],
                            'json'=> [
                                    'order_sn'=>$order_sn,
                                    'dropoff'=>[
                                        'tracking_no'=>'',
                                        'branch_id'=>0,
                                        'sender_real_name'=>'',
                                    ],
    
    
                                ]
                            ])->getBody()->getContents();
                            $responseShipOrder = json_decode($resShipOrder);
                            // dd($responseShipOrder, 'set');
                        }
    
                        // get package number
                        $path = "/api/v2/order/get_order_detail";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientOrderList = new Client();
                        $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                        $responseOrderList = json_decode($resOrderList);
                        $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;
    
                        // get Tracking Number
                        $path = "/api/v2/logistics/get_tracking_number";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientTrackingNumber = new Client();
                        $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                        $responseTrackingNumber = json_decode($resTrackingNumber);
                        $trackingNumber = $responseTrackingNumber->response->tracking_number;
    
                        // get shipping document parameter
                        $path = "/api/v2/logistics/get_shipping_document_parameter";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $data = Array();
                        $data[0]['order_sn']=$order_sn;
                        $data[0]['package_number']=$package_number;
                        $clientShippingDocumentParameter = new Client();
                        $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ],
    
                                'json'=>[
                                    'order_list'=>$data
                                ]
                        ])->getBody()->getContents();
                        $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                        $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                        // CREATE SHIPPING DOCUMENT
                            $path = "/api/v2/logistics/create_shipping_document";
                            $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $data = array();
                            $data[0]['order_sn'] = $order_sn;
                            $data[0]['package_number'] = $package_number;
                            $data[0]['tracking_number'] = $trackingNumber;
                            $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                            $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                                'headers' => [
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                ],
    
                                'json' => [
                                    'order_list' => $data
                                ]
                                // 'body'=>$parameter
    
                            ])->getBody()->getContents();
                            $resultCreated = json_decode($createdDocument);
    
                        // get shipping document result
                        $path = "/api/v2/logistics/get_shipping_document_result";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $dataDocResult = Array();
                        $dataDocResult[0]["order_sn"]=$order_sn;
                        $dataDocResult[0]["package_number"]=$package_number;
                        $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                        // dd($dataDocResult);
                        $clientShippingDocResult = new Client();
                        $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type' => 'application/json',
                        ],
                            'json'=>[
                                'order_list'=>$dataDocResult
                            ]
                        ])->getBody()->getContents();
                        $responseShippingDocResult = json_decode($resShippingDocResult);
                        // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
                    // download shipping document
                        $path = "/api/v2/logistics/download_shipping_document";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $dataDownload = Array();
                        $dataDownload[0]["order_sn"]=$order_sn;
                        $dataDownload[0]["package_number"]=$package_number;
                        $clientDownloadShipDoc = new Client();
                        $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type' => 'application/json',
                            ],
                            'json'=>[
                                'shipping_document_type'=>$suggest_shipping_document_type,
                                'order_list'=>$dataDownload,
                                ]
                            ])->getBody()->getContents();
                        $resShippingLabel = json_decode($resDownloadShipDoc);
                        $name = "Shopee-$order_sn.pdf";
                        \Storage::put("public/label/$name", $resDownloadShipDoc);
                        // $path = 'storage/label/'.$name;
                        // update bukti bayar
                        $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                            'buktiBayar'=>$name,
                            'awbNo'=>$trackingNumber
                        ]);
                        $pdfBuktiBayar = base64_encode($resDownloadShipDoc);
                        $clientSendImage = new Client();
                        
                        $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                              'json'=>[
                                  
                                      'name'=>$name,
                                      'contentBuktibayar'=> $pdfBuktiBayar,
                              ]
                        ])->getBody()->getContents();
                    }
                    
                    if($branchProduct->platform == 'Tokopedia'){
                          $client = new Client();
                         $clientId = env('TOKOPEDIA_CLIENT_ID');
                         $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                         $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                         $clientData = base64_encode("{$clientId}:{$clientSecret}");
                         $appID = env('TOKOPEDIA_APP_ID');
                         try{
                             $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                             'Authorization'=> "Basic $clientData",
                             'Content-Length' => 0,
                             'User_Agent' => 'PostmanRuntime/7.17.1'
                             ]])->getBody()->getContents();
                         }catch (RequestException $e) {
                             // dd(Psr7\str($e->getRequest()));
                             echo Psr7\str($e->getRequest());
                             if ($e->hasResponse()) {
                                 echo Psr7\str($e->getResponse());
                             }
                         }
                         $token = json_decode($res);
                         try{
                             $shop = DB::table('Registered_store')->where('shop_id',$branchProduct->shop_id)->where('platform', 'Tokopedia')->first();
                             $shop_id= (int)$shop->shop_id;
                             $fsId = (int)$appID;
                             // $stock = $item->stock - $produk->qty;
                             // if($getOrder->shop_id == 3897048){
                             //     $stock = $stockProduct;

                             // }
                         // dd($token, $shop_id, $appID);
                             $data = Array();
                             $data[0]['product_id']=(int)$branchProduct->produk_id;
                             $data[0]['new_stock']=$stockToMarketplace;
                             // $shop_id = $shop['shopsId'];
                             // dd($shop, $shop_id, $stock, $data, $token);
                             $clientStock = new Client();
                             $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                 'Authorization'=> "Bearer $token->access_token",
                                 'Content-Type'=> 'application/json'
                             ],
                                 'json'=>$data
                             ])->getBody()->getContents();
                             $responseStock = json_decode($resStock);
                             $updateStock = DB::table('produk_marketplace')->where('produk_id', $branchProduct->produk_id)->where('platform', 'Tokopedia')->update([
                                 'stock'=>$stockToMarketplace
                             ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                             // $responseBody = $exception->getResponse()->getBody(true);
                             $response = json_decode($ex->getResponse()->getBody()->getContents());
                             // dd($response, $e);
                         }

                   }else if($branchProduct->platform == 'Shopee'){
                     $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $branchProduct->shop_id)->first();
                    //  $shop = DB::table('Registered_store')->where('data_toko', $branchProduct->shop_id)->first();
                     $shop_id= (int) $shop->shop_id;
                     $access_token = $shop->access_token;
                     $partner_id = (int)env('SHOPEE_PARTNER_ID');
                     $partner_key = env('SHOPEE_PARTNER_KEY');
                     $timestamp = time();
                     $path = "/api/v2/product/update_stock";
                     $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                     $sign = hash_hmac('sha256', $salt, $partner_key);
                     $model_id = 0;
                     // $stock = $item->stock - $produk->qty;
                         // cek apakah model perlu atau tidak(variant)?
                     if($branchProduct->is_variant == 1){
                         $getParent = DB::table('produk_marketplace')->where('id', $branchProduct->parent_produk_id)->first();
                         $item_id = $getParent->produk_id;
                         $model_id = $branchProduct->produk_id;
                         // dd($getParent,$item_id, $model_id );
                     }else{
                         $item_id = $branchProduct->produk_id;
                         $model_id = 0;
                     }

                     try{
                         $data = Array();
                         $data[0]['model_id']=(int)$model_id;
                         $data[0]['normal_stock']=(int)$stockToMarketplace;
                         $clientShopeeStock = new Client();
                         $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                             'Content-Type'=>'application/json',
                         ],
                             'json'=>
                                 [
                                     'item_id'=>(int)$item_id,
                                     'stock_list'=>$data
                                 ]
                         ])->getBody()->getContents();
                         $responseShopeeStock = json_decode($resShopeeStock);
                         // updateStock
                         if($branchProduct->has_variant == 1){
                             $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $branchProduct->produk_id)->where('platform', 'Shopee')->update([
                                 'stock'=>$stockToMarketplace
                                 ]);

                         }else{
                             // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                             //     'stock'=>$stock
                             //     ]);
                             $updateStock = DB::table('produk_marketplace')->where('produk_id', $branchProduct->produk_id)->where('platform', 'Shopee')->update([
                             'stock'=>$stockToMarketplace
                             ]);
                         }
                     }catch(\GuzzleHttp\Exception\ClientException $ex){
                         $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                         dd($response->message );
                     }
                   }else if($branchProduct->platform == 'Blibli'){
                     $shop = DB::table('Registered_store')->where('shop_id',$branchProduct->shop_id)->where('Platform', 'Blibli')->first();
                     $sellerApiKey = $shop->seller_api_key;
                     $storeCode = $shop->shop_id;
                     // $stock = $item->stock - $produk->qty;
                     $client = new Client();
                     $clientId = env('BLIBLI_CLIENT_ID');
                     $clientKey = env('BLIBLI_CLIENT_KEY');
                     $Authorization = base64_encode("{$clientId}:{$clientKey}");
                     $blibliSku = $branchProduct->produk_id;

                     try{
                         $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                           'Authorization'=> "Basic $Authorization",
                           'Content-Type' => 'application/json',
                           'Accept' => 'application/json',
                           'Content-Length' => 0,
                           'User_Agent' => 'PostmanRuntime/7.17.1',
                           'Api-Seller-Key' => $sellerApiKey
                           ],
                               'json'=>
                                   [
                                       'availableStock'=>$stockToMarketplace,
                                   ]
                           ])->getBody();
                       $res = $res->getContents();
                       $response = json_decode($res);
                       $datas = $response->content;
                       $updateStock = DB::table('produk_marketplace')->where('produk_id', $branchProduct->produk_id)->where('platform', 'Blibli')->update([
                           'stock'=>$stockToMarketplace
                       ]);
                      }catch (\GuzzleHttp\Exception\ClientException $ex) {
                         // $responseBody = $exception->getResponse()->getBody(true);
                         $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                         dd($response );
                     }
                   } else if($branchProduct->platform == 'JDID'){
                       $shop = DB::table('Registered_store')->where('shop_id', $branchProduct->shop_id)->where('Platform', 'JDID')->first();
                       $skuNo = $branchProduct->produk_id;
                       $skuId = (object)[
                           'skuId'=>$skuNo,
                       ];
                       // updateStock
                       $realNum = (object)[
                           'realNum'=>$stockToMarketplace
                       ];
                       $c = new JdClient();
                       $c->appKey = env('APP_KEY');
                       $c->appSecret = env('APP_SECRET');
                       $c->accessToken = $shop->access_token;
                       $c->serverUrl = "https://open-api.jd.id/routerjson";
                       $req = new EpistockUpdateEpiMerchantWareStockRequest();
                       $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                       $respUpdateStock = $c->execute($req, $c->accessToken);
                       $updateStock = DB::table('produk_marketplace')->where('produk_id', $branchProduct->produk_id)->where('platform', 'JDID')->update([
                           'stock'=>$stockToMarketplace
                       ]);
                   } else if($branchProduct->platform == 'Warisan'){
                       $stockClient = new Client();
                       $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                         'json'=>[
                             'sku'=>$branchProduct->sku,
                              'stock'=>$stockToMarketplace
                         ]
                       ])->getBody()->getContents();
                       $responseStock = json_decode($resStock);
                       $updateStock = DB::table('produk_marketplace')->where('produk_id', $branchProduct->produk_id)->where('platform', 'Warisan')->update([
                           'stock'=>$stockToMarketplace
                       ]);
                   } else{
                       $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->where('produk_id', $branchProduct->produk_id)->first();
                       $access_token = $shop->access_token;
                       $data[0]['product_id']=(string) $branchProduct->produk_id;
                       $data[0]['stock_value']=(int) $stockToMarketplace;
                       $stockUpdateClient = new Client();
                       $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                           ['Authorization' => "Bearer $access_token"],
                           'json'=>$data
                       ])->getBody()->getContents();
                       $responseLabel = json_decode($resLabel);
                   }
                  }
                // }else{
                //       echo 'made by order <br/>';
                // }

              }
              // update assembly
              // ambil produkmarketplace untuk si assembly dan update data di marketplaceny
            //   $getAssemblyProdukMarketplaces = DB::table('produk_marketplace')->where('induk_barang_id', $produk_induk->id)->where('id', '!=' ,$produk->product_id)->get();
              $getAssemblyProdukMarketplaces = DB::table('produk_marketplace')->where('induk_barang_id', $produk_induk->id)->get();
            //   dd($getAssemblyProdukMarketplaces, $currentStockAssembly);
              foreach($getAssemblyProdukMarketplaces as $assemblyProdukMarketplace){
                if($assemblyProdukMarketplace->platform == 'Tokopedia'){
                      $client = new Client();
                     $clientId = env('TOKOPEDIA_CLIENT_ID');
                     $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                     $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                     $clientData = base64_encode("{$clientId}:{$clientSecret}");
                     $appID = env('TOKOPEDIA_APP_ID');
                     try{
                         $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                         'Authorization'=> "Basic $clientData",
                         'Content-Length' => 0,
                         'User_Agent' => 'PostmanRuntime/7.17.1'
                         ]])->getBody()->getContents();
                     }catch (RequestException $e) {
                         // dd(Psr7\str($e->getRequest()));
                         echo Psr7\str($e->getRequest());
                         if ($e->hasResponse()) {
                             echo Psr7\str($e->getResponse());
                         }
                     }
                     $token = json_decode($res);
                     try{
                         $shop = DB::table('Registered_store')->where('shop_id',$assemblyProdukMarketplace->shop_id)->where('platform', 'Tokopedia')->first();
                         $shop_id= (int)$shop->shop_id;
                         $fsId = (int)$appID;
                         // $stock = $item->stock - $produk->qty;
                         // if($getOrder->shop_id == 3897048){
                         //     $stock = $stockProduct;

                         // }
                     // dd($token, $shop_id, $appID);
                         $data = Array();
                         $data[0]['product_id']=(int)$assemblyProdukMarketplace->produk_id;
                         $data[0]['new_stock']=$currentStockAssembly;
                         // $shop_id = $shop['shopsId'];
                         // dd($shop, $shop_id, $stock, $data, $token);
                         $clientStock = new Client();
                         $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                             'Authorization'=> "Bearer $token->access_token",
                             'Content-Type'=> 'application/json'
                         ],
                             'json'=>$data
                         ])->getBody()->getContents();
                         $responseStock = json_decode($resStock);
                         $updateStock = DB::table('produk_marketplace')->where('produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'Tokopedia')->update([
                             'stock'=>$currentStockAssembly
                         ]);
                     }catch (\GuzzleHttp\Exception\ClientException $ex) {
                         // $responseBody = $exception->getResponse()->getBody(true);
                         $response = json_decode($ex->getResponse()->getBody()->getContents());
                         // dd($response, $e);
                     }
                }else if($assemblyProdukMarketplace->platform == 'Shopee'){
                    $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko', $assemblyProdukMarketplace->shop_id)->first();
                    // $shop = DB::table('Registered_store')->where('data_toko', $assemblyProdukMarketplace->shop_id)->first();
                    $shop_id= $shop->shop_id;
                    $access_token = $shop->access_token;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $timestamp = time();
                    $path = "/api/v2/product/update_stock";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $model_id = 0;
                    // $stock = $item->stock - $produk->qty;
                        // cek apakah model perlu atau tidak(variant)?
                    if($branchProduct->is_variant == 1){
                        $getParent = DB::table('produk_marketplace')->where('id', $assemblyProdukMarketplace->parent_produk_id)->first();
                        $item_id = $getParent->produk_id;
                        $model_id = $assemblyProdukMarketplace->produk_id;
                        // dd($getParent,$item_id, $model_id );
                    }else{
                        $item_id = $assemblyProdukMarketplace->produk_id;
                        $model_id = 0;
                    }

                    try{
                        $data[0]['model_id']=(int)$model_id;
                        $data[0]['normal_stock']=(int)$currentStockAssembly;
                        $clientShopeeStock = new Client();
                        $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type'=>'application/json',
                        ],
                            'json'=>
                                [
                                    'item_id'=>(int)$item_id,
                                    'stock_list'=>$data
                                ]
                        ])->getBody()->getContents();
                        $responseShopeeStock = json_decode($resShopeeStock);
                        // updateStock
                        if($branchProduct->has_variant == 1){
                            $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$currentStockAssembly
                                ]);

                        }else{
                            // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                            //     'stock'=>$stock
                            //     ]);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'Shopee')->update([
                            'stock'=>$currentStockAssembly
                            ]);
                        }
                    }catch(\GuzzleHttp\Exception\ClientException $ex){
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response->message );
                    }
                } else if($assemblyProdukMarketplace->platform == 'Blibli'){
                      $shop = DB::table('Registered_store')->where('shop_id',$assemblyProdukMarketplace->shop_id)->first();
                      $sellerApiKey = $shop->seller_api_key;
                      $storeCode = $shop->shop_id;
                      // $stock = $item->stock - $produk->qty;
                      $client = new Client();
                      $clientId = env('BLIBLI_CLIENT_ID');
                      $clientKey = env('BLIBLI_CLIENT_KEY');
                      $Authorization = base64_encode("{$clientId}:{$clientKey}");
                      $blibliSku = $assemblyProdukMarketplace->produk_id;

                      try{
                          $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $sellerApiKey
                            ],
                                'json'=>
                                    [
                                        'availableStock'=>$currentStockAssembly,
                                    ]
                            ])->getBody();
                        $res = $res->getContents();
                        $response = json_decode($res);
                        $datas = $response->content;
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'Blibli')->update([
                            'stock'=>$currentStockAssembly
                        ]);
                       }catch (\GuzzleHttp\Exception\ClientException $ex) {
                          // $responseBody = $exception->getResponse()->getBody(true);
                          $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                          dd($response );
                      }
                } else if($assemblyProdukMarketplace->platform == 'JDID'){
                      $shop = DB::table('Registered_store')->where('shop_id', $assemblyProdukMarketplace->shop_id)->first();
                      $skuNo = $assemblyProdukMarketplace->produk_id;
                      $skuId = (object)[
                          'skuId'=>$skuNo,
                      ];
                      // updateStock
                      $realNum = (object)[
                          'realNum'=>$currentStockAssembly
                      ];
                      $c = new JdClient();
                      $c->appKey = env('APP_KEY');
                      $c->appSecret = env('APP_SECRET');
                      $c->accessToken = $shop->access_token;
                      $c->serverUrl = "https://open-api.jd.id/routerjson";
                      $req = new EpistockUpdateEpiMerchantWareStockRequest();
                      $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                      $respUpdateStock = $c->execute($req, $c->accessToken);
                      $updateStock = DB::table('produk_marketplace')->where('produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'JDID')->update([
                          'stock'=>$currentStockAssembly
                      ]);
                } else if($assemblyProdukMarketplace->platform == 'Warisan'){
                    // return response()->json('masuk');
                    $stockClient = new Client();
                    $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                      'json'=>[
                          'sku'=>$assemblyProdukMarketplace->sku,
                           'stock'=>$currentStockAssembly
                      ]
                    ])->getBody()->getContents();
                    $responseStock = json_decode($resStock);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $assemblyProdukMarketplace->produk_id)->where('platform', 'Warisan')->update([
                        'stock'=>$currentStockAssembly
                    ]);
                    return response()->json(['status'=>'success', 'msg'=>'warisan success'],200);
                } else{
                    $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                    $access_token = $shop->access_token;
                    $data[0]['product_id']=(string) $assemblyProdukMarketplace->id;
                    $data[0]['stock_value']=(int) $currentStockAssembly;
                    $stockUpdateClient = new Client();
                    $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                        ['Authorization' => "Bearer $access_token"],
                        'json'=>$data
                    ])->getBody()->getContents();
                    $responseLabel = json_decode($resLabel);
                }
              }
                echo 'sukses';
                // return response()->json('done');
              // not assembly
            }else{

              // update satuan
                // dd('satuan');
              $converterNumber = DB::table('convertion')->where('id', $produkTerjual->convertion_id)->first();
              $convertProdukTerbeli = $produk->qty * $converterNumber->stock_convertion_unit;

              // ambil stock
              $stok_induk = DB::table('stock')->where('induk_produk_id',$produk_induk->id)->orderby('id','desc')->first();
              $stok_depo = DB::table('stock')->where('data_toko_id',$produk_induk->brand_id)->where('induk_produk_id',$produk_induk->id)->orderby('id','desc')->first();
            
              // ambil stock number
              $stock_induk_numbers = DB::table('stock_number')->where('stock_id', $stok_induk->id)->get();
            //   return response()->json($stock_induk_numbers);
              $stock_depo_number = DB::table('stock_number')->where('stock_id', $stok_induk->id)->get();
              $convertions = DB::table('convertion')->where('induk_produk_id',$indukProdukId)->get();
            //   dd($stock_induk_numbers);
              $stockIndukConverted = 0;
              $stockDepoConverted = 0;
              foreach($stock_induk_numbers as $index=>$stockNumber){
                  $convertion = DB::table('convertion')->where('id', $stockNumber->convertion_id)->first();
                  $stockIndukConverted = $stockIndukConverted + ($stockNumber->bookStockUnit_induk * $convertion->stock_convertion_unit);
                  $stockDepoConverted = $stockIndukConverted + ($stockNumber->bookStockUnit_induk * $convertion->stock_convertion_unit);
              }
            //   return response()->json($stockIndukConverted);
                  $currentStockInduk = $stockIndukConverted - $convertProdukTerbeli;
                //   dd($currentStockInduk, $stockIndukConverted, $convertProdukTerbeli);
                  $currentStockDepo = $stockDepoConverted - $convertProdukTerbeli;
                
                  $cek = DB::table('stock')->where('data_toko_id', $produk_induk->brand_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $indukProdukId)->count();

                  if($cek < 1){
                    //   return response()->json($produk_induk);
                      $insertStockPenjualan = DB::table('stock')->insertGetId([
                          'data_toko_id'=>$getOrder->toko,
                          'induk_produk_id'=>$indukProdukId,
                          'tipe'=>'penjualan',
                          'created_at'=>date('Y-m-d H:i:s'),
                          'documentNumber'=>$getOrder->kode
                      ]);
                  }else {
                      $lastData = DB::table('stock')->where('data_toko_id', $produk_induk->brand_id)->where('documentNumber', $getOrder->kode)->where('tipe', 'penjualan')->where('induk_produk_id', $indukProdukId)->first();
                      $insertStockPenjualan = $lastData->id;
                      
                  }
                  
                  $moduloSelisihUnitInduk = 0;
                  $moduloSelisihUnitDepo = 0;
                //   dd($convertions, $currentStockInduk);
                  // dalam codingan coach tidak diperhitungkan apakah bundle atau satuan lain yang lebih kecil dapat dijadikan satuan atasnya
                  foreach($convertions as $index=>$convertion){
                      if($index == 0){
                          $modSelisihUnitInduk = $currentStockInduk % $convertion->stock_convertion_unit;
                          $divSelisihUnitInduk  = $currentStockInduk - $modSelisihUnitInduk;
                          $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitInduk = $modSelisihUnitInduk;
                        //   dd($modSelisihUnitInduk, $currentStockInduk, $convertion->stock_convertion_unit, $moduloSelisihUnitInduk);
                          $modSelisihUnitDepo = $currentStockDepo % $convertion->stock_convertion_unit;
                          $divSelisihUnitDepo = $currentStockDepo - $modSelisihUnitDepo;
                          $convertSelisihUnitDepo = $divSelisihUnitDepo / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitDepo = $modSelisihUnitDepo;
                      }else{
                          $modSelisihUnitInduk = $moduloSelisihUnitInduk % $convertion->stock_convertion_unit;
                          $divSelisihUnitInduk = $moduloSelisihUnitInduk - $modSelisihUnitInduk;
                          $convertSelisihUnitInduk = $divSelisihUnitInduk / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitInduk = $modSelisihUnitInduk;
                            // dd($moduloSelisihUnitInduk , $modSelisihUnitInduk, $convertSelisihUnitInduk, $divSelisihUnitInduk, $convertion);
                          $modSelisihUnitDepo = $moduloSelisihUnitIndukDepo % $convertion->stock_convertion_unit;
                          $divSelisihUnitDepo = $moduloSelisihUnitIndukDepo - $modSelisihUnitDepo;
                          $convertSelisihUnitDepo = $divSelisihUnitDepo / $convertion->stock_convertion_unit;
                          $moduloSelisihUnitDepo = $modSelisihUnitDepo;
                      }
                      
                      $check = DB::table('stock_number')->where('stock_id', $insertStockPenjualan)->where('convertion_id', $convertion->id)->count();
                    //   dd($check);
                    if($stock_induk_numbers[$index]->stockOpnameUnit_induk == null){
                        $stockOpnameUnit_induk = 0;
                    }else {
                        $stockOpnameUnit_induk = $stock_induk_numbers[$index]->stockOpnameUnit_induk;
                    }
                      if($check < 1){
                        //   dd($stock_induk_numbers, $index, $convertions);
                          if($convertion->id == $produkTerjual->convertion_id){
                              $insertNumberOfStock = DB::table('stock_number')->insert([
                                'stock_id'=>$insertStockPenjualan,
                                'convertion_id'=>$convertion->id,
                                'unit'=>$produk->qty,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'bookStockUnit_depo'=>$convertSelisihUnit,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'created_at'=>date('Y-m-d H:i:s')
                              ]);
                          }else{
                              $insertNumberOfStock = DB::table('stock_number')->insert([
                                'stock_id'=>$insertStockPenjualan,
                                'convertion_id'=>$convertion->id,
                                'unit'=>0,
                                'bookStockUnit_induk'=>$convertSelisihUnitInduk,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'bookStockUnit_depo'=>$convertSelisihUnit,
                                'stockOpnameUnit_induk'=>$stockOpnameUnit_induk,
                                'created_at'=>date('Y-m-d H:i:s')
                              ]);
                          }
                      }
                      
                      
                       

                  }
            }
            // dd($stok_induk, $convertProdukTerbeli, $stock_induk_numbers, $convertions);

            // update to marketplace
            // $otherProducts = DB::table('produk_marketplace')->where('induk_barang_id', $indukProdukId)->where('produk_id', '!=', $produkTerjual->produk_id)->get();
            $otherProducts = DB::table('produk_marketplace')->where('induk_barang_id', $indukProdukId)->get();
            // dd($currentStockInduk, $otherProducts);
            // dd($otherProducts);
            foreach($otherProducts as $product){
               $getConversionProduk = DB::table('convertion')->where('id',$product->convertion_id)->first();
            //   return response()->json( $getConversionProduk);
            if(empty($getConversionProduk)){
                    // return response()->json($product);
                }elseif($getConversionProduk->stock_convertion_unit == 0){
                    // return response()->json($product);
                }
                $stock = floor($currentStockInduk / $getConversionProduk->stock_convertion_unit);
                
                // return response()->json( [$getConversionProduk, $currentStockInduk, $stock]);
                // dd($getConversionProduk, $stock);
                if($getOrder->platform == 'Tokopedia'){
                    // cek kode hitung
                        // cek marketplace
                    if($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){

                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        // $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        // dd($access_token, $shop);
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($product->is_variant == 1){
                            $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                            $item_id = $getParent->produk_id;
                            $model_id = $product->produk_id;
                            // dd($getParent,$item_id, $model_id );
                        }else{
                            $item_id = $product->produk_id;
                            $model_id = 0;
                        }

                        try{
                            $data = Array();
                            $data[0]['model_id']=(int)$model_id;
                            $data[0]['normal_stock']=(int)$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // dd($responseShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message, $ex );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } else if($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                    }else{
                      $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                      $access_token = $shop->access_token;
                      $data[0]['product_id']=(string) $product->produk_id;
                      $data[0]['stock_value']=(int) $stock;
                      $stockUpdateClient = new Client();
                      $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                          ['Authorization' => "Bearer $access_token"],
                          'json'=>$data
                      ])->getBody()->getContents();
                      $responseLabel = json_decode($resLabel);
                    }

                }elseif($getOrder->platform == 'Shopee'){
                    // ambil data bookingan
                    $getToko = DB::table('Registered_store')->where('shop_id', $getOrder->nmToko)->first();
                    // $getToko = DB::table('Registered_store')->where('shop_id', $getOrder->nmToko)->where('Platform', 'Shopee')->first();
                    $access_token=$getToko->access_token;
                    $shop_id=$getToko->shop_id;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $order_sn = $getOrder->kode;
                    // $package_number = $getToko->package_id;
                    $timestamp = time();

                    // shipping parameter
                        $path = "/api/v2/logistics/get_shipping_parameter";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShippingParameter = new Client();
                        $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                        $responseShippingParameter = json_decode($resShippingParameter);
                        $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                        $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                        $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                        // dd($responseShippingParameter);
                    // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
                    $logistic_type = '';
                    if( $responseShippingParameter->response->info_needed->pickup){
                        $logistic_type = 'pickUp';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                // 'package_number'=>$package_number,
                                'pickup'=>[
                                    'address_id'=>$address_id,
                                    'pickup_time_id'=>$pickup_time_id,
                                    // 'tracking_number'=>$trackingNumber,
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder);

                    }elseif($branch_id){
                        $logistic_type = 'dropoff';
                        // echo 'dropoff<br/>';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                'dropoff'=>[
                                    'address_id'=>$address_id,
                                    'pickup_time_id'=>$pickup_time_id,
                                    // 'tracking_number'=>$trackingNumber,
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder, 'tes');
                    }else{
                        $logistic_type = 'dropoff';
                        // echo 'dropoff<br/>';
                        // ship order
                        $path = "/api/v2/logistics/ship_order";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $clientShipOrder = new Client();
                        $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                            'Content-Type' => 'application/json',
                        ],
                        'json'=> [
                                'order_sn'=>$order_sn,
                                'dropoff'=>[
                                    'tracking_no'=>'',
                                    'branch_id'=>0,
                                    'sender_real_name'=>'',
                                ],


                            ]
                        ])->getBody()->getContents();
                        $responseShipOrder = json_decode($resShipOrder);
                        // dd($responseShipOrder, 'set');
                    }

                    // get package number
                    $path = "/api/v2/order/get_order_detail";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientOrderList = new Client();
                    $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                    $responseOrderList = json_decode($resOrderList);
                    $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;

                    // get Tracking Number
                    $path = "/api/v2/logistics/get_tracking_number";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientTrackingNumber = new Client();
                    $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                    $responseTrackingNumber = json_decode($resTrackingNumber);
                    $trackingNumber = $responseTrackingNumber->response->tracking_number;

                    // get shipping document parameter
                    $path = "/api/v2/logistics/get_shipping_document_parameter";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $data = Array();
                    $data[0]['order_sn']=$order_sn;
                    $data[0]['package_number']=$package_number;
                    $clientShippingDocumentParameter = new Client();
                    $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],

                            'json'=>[
                                'order_list'=>$data
                            ]
                    ])->getBody()->getContents();
                    $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                    $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                    // CREATE SHIPPING DOCUMENT
                        $path = "/api/v2/logistics/create_shipping_document";
                        $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $data = array();
                        $data[0]['order_sn'] = $order_sn;
                        $data[0]['package_number'] = $package_number;
                        $data[0]['tracking_number'] = $trackingNumber;
                        $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                        $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                            'headers' => [
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ],

                            'json' => [
                                'order_list' => $data
                            ]
                            // 'body'=>$parameter

                        ])->getBody()->getContents();
                        $resultCreated = json_decode($createdDocument);

                    // get shipping document result
                    $path = "/api/v2/logistics/get_shipping_document_result";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDocResult = Array();
                    $dataDocResult[0]["order_sn"]=$order_sn;
                    $dataDocResult[0]["package_number"]=$package_number;
                    $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                    // dd($dataDocResult);
                    $clientShippingDocResult = new Client();
                    $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                    ],
                        'json'=>[
                            'order_list'=>$dataDocResult
                        ]
                    ])->getBody()->getContents();
                    $responseShippingDocResult = json_decode($resShippingDocResult);
                    // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
                // download shipping document
                    $path = "/api/v2/logistics/download_shipping_document";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $dataDownload = Array();
                    $dataDownload[0]["order_sn"]=$order_sn;
                    $dataDownload[0]["package_number"]=$package_number;
                    $clientDownloadShipDoc = new Client();
                    $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                        ],
                        'json'=>[
                            'shipping_document_type'=>$suggest_shipping_document_type,
                            'order_list'=>$dataDownload,
                            ]
                        ])->getBody()->getContents();
                    $resShippingLabel = json_decode($resDownloadShipDoc);
                    $name = "Shopee-$order_sn.pdf";
                    \Storage::put("public/label/$name", $resDownloadShipDoc);
                    // $path = 'storage/label/'.$name;
                    // update bukti bayar
                    $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                        'buktiBayar'=>$name,
                        'awbNo'=>$trackingNumber
                    ]);
                    $pdfBuktiBayar = base64_encode($resDownloadShipDoc);
                    $clientSendImage = new Client();
                    
                    $resImage = $clientSendImage->post('https://api.gudangin.id/api/label',[
                          'json'=>[
                              
                                  'name'=>$name,
                                  'contentBuktibayar'=> $pdfBuktiBayar,
                          ]
                    ])->getBody()->getContents();
                    
                    if($product->platform == 'Shopee'){

                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        // $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        // dd($access_token, $shop);
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($product->is_variant == 1){
                            $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                            $item_id = $getParent->produk_id;
                            $model_id = $product->produk_id;
                            // dd($getParent,$item_id, $model_id );
                        }else{
                            $item_id = $product->produk_id;
                            $model_id = 0;
                        }

                        try{
                            $data[0]['model_id']=(int)$model_id;
                            $data[0]['normal_stock']=(int)$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // dd($responseShopeeStock);
                            // updateStock
                            if($product->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message, $ex );
                        }
                    }elseif($product->platform == 'Blibli'){
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }

                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                      }else{
                            $client = new Client();
                            $data[0]['product_id']=(string) $product->produk_id;
                            $data[0]['stock_value']=(int) $stock;
                            $resUpdateStock = $client->post("https://sindehomedelivery.com/api/stock/overwrite",['headers'=>
                                ['Authorization' => "Bearer $access_token"],
                                'json'=>$data
                            ])->getBody()->getContents();
                      }
                }elseif($getOrder->platform == 'Blibli'){
                    
                    if($product->platform == 'Blibli'){
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $product->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }

                    }elseif($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id',  $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);

                      }else{
                          $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                          $access_token = $shop->access_token;
                          $data[0]['product_id']=(string) $product->produk_id;
                          $data[0]['stock_value']=(int) $stock;
                          $stockUpdateClient = new Client();
                          $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                              ['Authorization' => "Bearer $access_token"],
                              'json'=>$data
                          ])->getBody()->getContents();
                          $responseLabel = json_decode($resLabel);
                      }
                }elseif($getOrder->platform == 'JDID'){
                    
                    if($product->platform == 'JDID'){
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store)->where('shop_id', $product->shop_id)->where('Platform', 'Shopee')->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    } elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);

                    }else{
                        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                        $access_token = $shop->access_token;
                        $data[0]['product_id']=(string) $product->produk_id;
                        $data[0]['stock_value']=(int) $stock;
                        $stockUpdateClient = new Client();
                        $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                            ['Authorization' => "Bearer $access_token"],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseLabel = json_decode($resLabel);
                      }
                }elseif($getOrder->platform == 'Sinde Home Delivey'){

                    if($product->platform == 'Sinde Home Delivery'){
                        $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                        $access_token = $shop->access_token;
                        $data[0]['product_id']=(string) $product->produk_id;
                        $data[0]['stock_value']=(int) $stock;
                        $stockUpdateClient = new Client();
                        $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                            ['Authorization' => "Bearer $access_token"],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseLabel = json_decode($resLabel);
                    
                    }elseif($product->platform == 'Tokopedia'){
                         $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            // $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$product->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($product->platform == 'Shopee'){
                         $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                        //  $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        // $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            // $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($product->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        // $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                    }else{
                        // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                        $skuNo = $product->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        // $skuEncode = json_encode($skuId);
                        // $c = new JdClient();
                        // $c->appKey = env('APP_KEY');
                        // $c->appSecret = env('APP_SECRET');
                        // $c->accessToken = $shop->access_token;
                        // $c->serverUrl = "https://open-api.jd.id/routerjson";
                        // $req = new EpistockQueryEpiMerchantWareStockRequest();
                        // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        // $respStock = $c->execute($req, $c->accessToken);
                        // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        // // $currentStock = $stock - $produk->qty;
                        // // updateStock
                        $realNum = (object)[
                            'realNum'=>$stock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$stock
                        ]);
                    }
                    // warisan
                }else{
                    
                  if($product->platform == 'Warisan'){
                        $stockClient = new Client();
                        $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                          'json'=>[
                              'sku'=>$product->sku,
                               'stock'=>$stock
                          ]
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Warisan')->update([
                            'stock'=>$stock
                        ]);
                  }elseif($product->platform == 'Tokopedia'){
                       $client = new Client();
                      $clientId = env('TOKOPEDIA_CLIENT_ID');
                      $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                      $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                      $clientData = base64_encode("{$clientId}:{$clientSecret}");
                      $appID = env('TOKOPEDIA_APP_ID');
                      try{
                          $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                          'Authorization'=> "Basic $clientData",
                          'Content-Length' => 0,
                          'User_Agent' => 'PostmanRuntime/7.17.1'
                          ]])->getBody()->getContents();
                      }catch (RequestException $e) {
                          // dd(Psr7\str($e->getRequest()));
                          echo Psr7\str($e->getRequest());
                          if ($e->hasResponse()) {
                              echo Psr7\str($e->getResponse());
                          }
                      }
                      $token = json_decode($res);
                      try{
                          $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                          $shop_id= (int)$shop->shop_id;
                          $fsId = (int)$appID;
                          // $stock = $item->stock - $produk->qty;
                          // if($getOrder->shop_id == 3897048){
                          //     $stock = $stockProduct;

                          // }
                      // dd($token, $shop_id, $appID);
                          $data = Array();
                          $data[0]['product_id']=(int)$product->produk_id;
                          $data[0]['new_stock']=$stock;
                          // $shop_id = $shop['shopsId'];
                          // dd($shop, $shop_id, $stock, $data, $token);
                          $clientStock = new Client();
                          $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                              'Authorization'=> "Bearer $token->access_token",
                              'Content-Type'=> 'application/json'
                          ],
                              'json'=>$data
                          ])->getBody()->getContents();
                          $responseStock = json_decode($resStock);
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                              'stock'=>$stock
                          ]);
                      }catch (\GuzzleHttp\Exception\ClientException $ex) {
                          // $responseBody = $exception->getResponse()->getBody(true);
                          $response = json_decode($ex->getResponse()->getBody()->getContents());
                          // dd($response, $e);
                      }
                  }elseif($product->platform == 'Shopee'){
                       $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $product->shop_id)->first();
                      $shop_id= (int) $shop->shop_id;
                      $access_token = $shop->access_token;
                      $partner_id = (int)env('SHOPEE_PARTNER_ID');
                      $partner_key = env('SHOPEE_PARTNER_KEY');
                      $timestamp = time();
                      $path = "/api/v2/product/update_stock";
                      $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                      $sign = hash_hmac('sha256', $salt, $partner_key);
                      $model_id = 0;
                      // $stock = $item->stock - $produk->qty;
                          // cek apakah model perlu atau tidak(variant)?
                      if($item->has_variant == 1){
                          $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                          $model_id = $getModelId->produk_id;
                          // $stock = $getModelId->stock - $produk->qty;
                      }

                      try{
                          $data[0]['model_id']=$model_id;
                          $data[0]['normal_stock']=$stock;
                          $clientShopeeStock = new Client();
                          $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                              'Content-Type'=>'application/json',
                          ],
                              'json'=>
                                  [
                                      'item_id'=>(int)$item->produk_id,
                                      'stock_list'=>$data
                                  ]
                          ])->getBody()->getContents();
                          $responseShopeeStock = json_decode($resShopeeStock);
                          // updateStock
                          if($item->has_variant == 1){
                              $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                  'stock'=>$stock
                                  ]);

                          }else{
                              // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                              //     'stock'=>$stock
                              //     ]);
                              $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                              'stock'=>$stock
                              ]);
                          }
                      }catch(\GuzzleHttp\Exception\ClientException $ex){
                          $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                          dd($response->message );
                      }
                  }elseif($product->platform == 'Blibli'){
                      $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                      $sellerApiKey = $shop->seller_api_key;
                      $storeCode = $shop->shop_id;
                      // $stock = $item->stock - $produk->qty;
                      $client = new Client();
                      $clientId = env('BLIBLI_CLIENT_ID');
                      $clientKey = env('BLIBLI_CLIENT_KEY');
                      $Authorization = base64_encode("{$clientId}:{$clientKey}");
                      $blibliSku = $item->produk_id;

                      try{
                          $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $sellerApiKey
                            ],
                                'json'=>
                                    [
                                        'availableStock'=>$stock,
                                    ]
                            ])->getBody();
                        $res = $res->getContents();
                        $response = json_decode($res);
                        $datas = $response->content;
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                            'stock'=>$stock
                        ]);
                       }catch (\GuzzleHttp\Exception\ClientException $ex) {
                          // $responseBody = $exception->getResponse()->getBody(true);
                          $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                          dd($response );
                      }
                  }elseif($product->platform == 'JDID'){
                    // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                    $skuNo = $product->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    // $skuEncode = json_encode($skuId);
                    // $c = new JdClient();
                    // $c->appKey = env('APP_KEY');
                    // $c->appSecret = env('APP_SECRET');
                    // $c->accessToken = $shop->access_token;
                    // $c->serverUrl = "https://open-api.jd.id/routerjson";
                    // $req = new EpistockQueryEpiMerchantWareStockRequest();
                    // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                    // $respStock = $c->execute($req, $c->accessToken);
                    // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                    // // $currentStock = $stock - $produk->qty;
                    // // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                        'stock'=>$stock
                    ]);
                  } else{
                      $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                      $access_token = $shop->access_token;
                      $data[0]['product_id']=(string) $product->produk_id;
                      $data[0]['stock_value']=(int) $stock;
                      $stockUpdateClient = new Client();
                      $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                          ['Authorization' => "Bearer $access_token"],
                          'json'=>$data
                      ])->getBody()->getContents();
                      $responseLabel = json_decode($resLabel);
                  }
                }
            }

            // di luar warisan dan shinde
        }else{
            if($getOrder->platform == 'Tokopedia'){
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Tokopedia')->get();
                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Shopee'){
                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$item->shop_id)->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('sku', $item->sku)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($item->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            // dd($response );
                        }

                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $item->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }
                $message = 'Tokopedia';
            }elseif($getOrder->platform == 'Shopee'){
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Shopee')->get();

                $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $getOrder->nmToko)->first();
                $access_token=$getToko->access_token;
                $shop_id=$getToko->shop_id;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $order_sn = $getOrder->kode;
                // $package_number = $getToko->package_id;
                $timestamp = time();

                // shipping parameter
                    $path = "/api/v2/logistics/get_shipping_parameter";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShippingParameter = new Client();
                    $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                    $responseShippingParameter = json_decode($resShippingParameter);
                    $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                    $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                    $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                    // dd($responseShippingParameter);
                    // return response()->json($responseShippingParameter);
                // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
                $logistic_type = '';
                if( $responseShippingParameter->response->info_needed->pickup){
                    $logistic_type = 'pickUp';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            'package_number'=>$package_number,
                            'pickup'=>[
                                'address_id'=>$address_id,
                                'pickup_time_id'=>$pickup_time_id,
                                'tracking_number'=>$trackingNumber,
                            ],


                        ]
                    ])->getBody()->getContents();
                    $responseShipOrder = json_decode($resShipOrder);
                    // return response()->json($responseShipOrder);
                    // dd($responseShipOrder);

                }elseif($branch_id){
                    $logistic_type = 'dropoff';
                    // echo 'dropoff<br/>';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            'dropoff'=>[
                                'address_id'=>$address_id,
                                'pickup_time_id'=>$pickup_time_id,
                                // 'tracking_number'=>$trackingNumber,
                            ],


                        ]
                    ])->getBody()->getContents();
                    // $responseShipOrder = json_decode($resShipOrder);
                    // dd($responseShipOrder, 'tes');
                }else{
                    $logistic_type = 'dropoff';
                    // echo 'dropoff<br/>';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            'dropoff'=>[
                                'tracking_no'=>'',
                                'branch_id'=>0,
                                'sender_real_name'=>'',
                            ],


                        ]
                    ])->getBody()->getContents();
                    $responseShipOrder = json_decode($resShipOrder);
                    // dd($responseShipOrder, 'set');
                }

                // get package number
                $path = "/api/v2/order/get_order_detail";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientOrderList = new Client();
                $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                $responseOrderList = json_decode($resOrderList);
                $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;

                // get Tracking Number
                $path = "/api/v2/logistics/get_tracking_number";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientTrackingNumber = new Client();
                $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                $responseTrackingNumber = json_decode($resTrackingNumber);
                $trackingNumber = $responseTrackingNumber->response->tracking_number;

                // get shipping document parameter
                $path = "/api/v2/logistics/get_shipping_document_parameter";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $data = Array();
                $data[0]['order_sn']=$order_sn;
                $data[0]['package_number']=$package_number;
                $clientShippingDocumentParameter = new Client();
                $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],

                        'json'=>[
                            'order_list'=>$data
                        ]
                ])->getBody()->getContents();
                $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                // CREATE SHIPPING DOCUMENT
                    $path = "/api/v2/logistics/create_shipping_document";
                    $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $data = array();
                    $data[0]['order_sn'] = $order_sn;
                    $data[0]['package_number'] = $package_number;
                    $data[0]['tracking_number'] = $trackingNumber;
                    $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                    $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],

                        'json' => [
                            'order_list' => $data
                        ]
                        // 'body'=>$parameter

                    ])->getBody()->getContents();
                    $resultCreated = json_decode($createdDocument);

                // get shipping document result
                $path = "/api/v2/logistics/get_shipping_document_result";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $dataDocResult = Array();
                $dataDocResult[0]["order_sn"]=$order_sn;
                $dataDocResult[0]["package_number"]=$package_number;
                $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                // dd($dataDocResult);
                $clientShippingDocResult = new Client();
                $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                ],
                    'json'=>[
                        'order_list'=>$dataDocResult
                    ]
                ])->getBody()->getContents();
                $responseShippingDocResult = json_decode($resShippingDocResult);
                // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
            // download shipping document
                $path = "/api/v2/logistics/download_shipping_document";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $dataDownload = Array();
                $dataDownload[0]["order_sn"]=$order_sn;
                $dataDownload[0]["package_number"]=$package_number;
                $clientDownloadShipDoc = new Client();
                $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                    ],
                    'json'=>[
                        'shipping_document_type'=>$suggest_shipping_document_type,
                        'order_list'=>$dataDownload,
                        ]
                    ])->getBody()->getContents();
                $resShippingLabel = json_decode($resDownloadShipDoc);
                $name = "Shopee-$order_sn.pdf";
                \Storage::put("public/label/$name", $resDownloadShipDoc);
                $path = 'storage/label/'.$name;
                // update bukti bayar
                $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                    'buktiBayar'=>$path,
                    'awbNo'=>$trackingNumber
                ]);
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Shopee')->get();

                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->first();
                        $sellerApiKey = $shop->seller_api_key;
                        $storeCode = $shop->shop_id;
                        $stock = $item->stock - $produk->qty;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $blibliSku = $item->produk_id;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                              'Authorization'=> "Basic $Authorization",
                              'Content-Type' => 'application/json',
                              'Accept' => 'application/json',
                              'Content-Length' => 0,
                              'User_Agent' => 'PostmanRuntime/7.17.1',
                              'Api-Seller-Key' => $sellerApiKey
                              ],
                                  'json'=>
                                      [
                                          'availableStock'=>$stock,
                                      ]
                              ])->getBody();
                          $res = $res->getContents();
                          $response = json_decode($res);
                          $datas = $response->content;
                          $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Blibli')->update([
                              'stock'=>$stock
                          ]);
                         }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }elseif($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$item->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $getProdukSku->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // dd($respStock, $skuId, $currentStock, $stock, $produk->qty);
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        // dd($respUpdateStock, $currentStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }
                $message = 'Shopee';
            }elseif($getOrder->platform == 'Blibli'){
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'Blibli')->get();

                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Shopee'){
                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$item->shop_id)->first();
                        $shop_id= (int) $shop->shop_id;
                        $access_token = $shop->access_token;
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $timestamp = time();
                        $path = "/api/v2/product/update_stock";
                        $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                        $sign = hash_hmac('sha256', $salt, $partner_key);
                        $model_id = 0;
                        $stock = $item->stock - $produk->qty;
                            // cek apakah model perlu atau tidak(variant)?
                        if($item->has_variant == 1){
                            $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->first();
                            $model_id = $getModelId->produk_id;
                            $stock = $getModelId->stock - $produk->qty;
                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>(int)$item->produk_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            if($item->has_variant == 1){
                                $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                    'stock'=>$stock
                                    ]);

                            }else{
                                // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                                //     'stock'=>$stock
                                //     ]);
                                $updateStock = DB::table('produk_marketplace')->where('sku', $item->sku)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);
                            }
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }
                    }elseif($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $stock = $item->stock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=(int)$item->produk_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }else{
                        $jdidProduk = DB::table('produk_marketplace')->where('sku', $getProdukSku->sku)->where('platform', 'JDID')->first();
                        $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                        $skuNo = $jdidProduk->produk_id;
                        $skuId = (object)[
                            'skuId'=>$skuNo,
                        ];
                        $skuEncode = json_encode($skuId);
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockQueryEpiMerchantWareStockRequest();
                        $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                        $respStock = $c->execute($req, $c->accessToken);
                        $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                        $currentStock = $stock - $produk->qty;
                        // dd($respStock, $skuId, $currentStock, $stock, $produk->qty);
                        // updateStock
                        $realNum = (object)[
                            'realNum'=>$currentStock
                        ];
                        $c = new JdClient();
                        $c->appKey = env('APP_KEY');
                        $c->appSecret = env('APP_SECRET');
                        $c->accessToken = $shop->access_token;
                        $c->serverUrl = "https://open-api.jd.id/routerjson";
                        $req = new EpistockUpdateEpiMerchantWareStockRequest();
                        $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                        $respUpdateStock = $c->execute($req, $c->accessToken);
                        // dd($respUpdateStock, $currentStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'JDID')->update([
                            'stock'=>$currentStock
                        ]);
                    }
                }

              $message = 'Blibli';
            }else{
                $productInOtherMarketplace = DB::table('produk_marketplace')->where('sku', $produk->sku)->where('platform', '!=', 'JDID')->get();
                foreach($productInOtherMarketplace as $item){
                    if($item->platform == 'Tokopedia'){
                        $client = new Client();
                        $clientId = env('TOKOPEDIA_CLIENT_ID');
                        $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                        $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                        $clientData = base64_encode("{$clientId}:{$clientSecret}");
                        $appID = env('TOKOPEDIA_APP_ID');
                        try{
                            $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                            'Authorization'=> "Basic $clientData",
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1'
                            ]])->getBody()->getContents();
                        }catch (RequestException $e) {
                            // dd(Psr7\str($e->getRequest()));
                            echo Psr7\str($e->getRequest());
                            if ($e->hasResponse()) {
                                echo Psr7\str($e->getResponse());
                            }
                        }
                        $token = json_decode($res);
                        try{
                            $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->where('platform', 'Tokopedia')->first();
                            $shop_id= (int)$shop->shop_id;
                            $fsId = (int)$appID;
                            $product_id = (int)$item->produk_id;
                            $clientProduct = new Client();
                            $resProduct = $clientProduct->get("https://fs.tokopedia.net/inventory/v1/fs/$appID/product/info?product_id=$produk_id", ['headers'=>['Authorization' => "Bearer $token->access_token"]])->getBody()->getContents();
                            $response = json_decode($resProduct);
                            $items = $response->data;
                            $currentStock = $items[0]->stock->value;
                            $stock = $currentStock - $produk->qty;
                            // if($getOrder->shop_id == 3897048){
                            //     $stock = $stockProduct;

                            // }
                        // dd($token, $shop_id, $appID);
                            $data = Array();
                            $data[0]['product_id']=$product_id;
                            $data[0]['new_stock']=$stock;
                            // $shop_id = $shop['shopsId'];
                            // dd($shop, $shop_id, $stock, $data, $token);
                            $clientStock = new Client();
                            $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                                'Authorization'=> "Bearer $token->access_token",
                                'Content-Type'=> 'application/json'
                            ],
                                'json'=>$data
                            ])->getBody()->getContents();
                            $responseStock = json_decode($resStock);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Tokopedia')->update([
                                'stock'=>$stock
                            ]);
                        }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents());
                            // dd($response, $e);
                        }
                    }elseif($item->platform == 'Shopee'){
                        $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$item->shop_id)->first();
                        $partner_id = (int)env('SHOPEE_PARTNER_ID');
                        $partner_key = env('SHOPEE_PARTNER_KEY');
                        $access_token = $shop->access_token;
                        $shop_id = (int) $shop->shop_id;
                        $timestamp = time();

                        if($item->is_variant == 1){
                            $getParent = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->first();
                            $item_id = (int)$getParent->produk_id;
                            $model_id = $item->produk_id;
                            $path = "/api/v2/product/get_model_list";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientModel = new Client();
                            $resModel = $clientModel->get("https://partner.shopeemobile.com/api/v2/product/get_model_list?item_id=$item_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                            $responseModel = json_decode($resModel)->response;
                            foreach($responseModel->model as $model){
                                if($model->model_id == $model_id){
                                    $stock = $model->stock_info[0]->current_stock - $produk->qty;
                                }
                            }

                        }else{
                            $item_id = (int)$item->produk_id;
                            $model_id = 0;
                            $path = "/api/v2/product/get_item_base_info";
                            $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                            $sign = hash_hmac('sha256', $salt, $partner_key);
                            $clientProductInfo = new Client();
                            $resProductInfo = $clientProductInfo->get("https://partner.shopeemobile.com/api/v2/product/get_item_base_info?item_id_list=$item_id_list&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign")->getBody()->getContents();
                            $responseProductInfo = json_decode($resProductInfo);
                            $productData = $responseProductInfo->response->item_list[0];
                            $stock = $productData->stock_info[0]->current_stock - $produk->qty;

                        }

                        try{
                            $data[0]['model_id']=$model_id;
                            $data[0]['normal_stock']=$stock;
                            $clientShopeeStock = new Client();
                            $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                                'Content-Type'=>'application/json',
                            ],
                                'json'=>
                                    [
                                        'item_id'=>$item_id,
                                        'stock_list'=>$data
                                    ]
                            ])->getBody()->getContents();
                            $responseShopeeStock = json_decode($resShopeeStock);
                            // updateStock
                            $updateStock = DB::table('produk_marketplace')->where('sku', $item->sku)->where('platform', 'Shopee')->update([
                            'stock'=>$stock
                            ]);
                        }catch(\GuzzleHttp\Exception\ClientException $ex){
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response->message );
                        }

                    }elseif($item->platform == 'Blibli'){
                        $shop = DB::table('Registered_store')->where('shop_id',$item->shop_id)->where('platform', 'Tokopedia')->first();
                        $shopId = $shop->shop_id;
                        $sku = $item->produk_id;
                        $sellerKey = $shop->seller_api_key;
                        $client = new Client();
                        $clientId = env('BLIBLI_CLIENT_ID');
                        $clientKey = env('BLIBLI_CLIENT_KEY');
                        $Authorization = base64_encode("{$clientId}:{$clientKey}");
                        $getProducts = $client->get('https://api.blibli.com/v2/proxy/mta/api/businesspartner/v1/product/detailProduct?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&businessPartnerCode='.$shopId.'gdnSku='.$sku.'&channelId=PT. YOKESEN TEKNOLOGI', ['headers' => [
                            'Authorization'=> "Basic $Authorization",
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'Content-Length' => 0,
                            'User_Agent' => 'PostmanRuntime/7.17.1',
                            'Api-Seller-Key' => $sellerKey
                            ]])->getBody();
                            $getDetail = $getProducts->getContents();
                            $currentStock = $getDetail->value->items[0]->availableStockLevel2;
                            $stock = $currentStock - $produk->qty;

                        try{
                            $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$sku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                                'Authorization'=> "Basic $Authorization",
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                                'Content-Length' => 0,
                                'User_Agent' => 'PostmanRuntime/7.17.1',
                                'Api-Seller-Key' => $sellerApiKey
                                ],
                                    'json'=>
                                        [
                                            'availableStock'=>$stock,
                                        ]
                                ])->getBody();
                            $res = $res->getContents();
                            $response = json_decode($res);
                            $datas = $response->content;
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Blibli')->update([
                                'stock'=>$stock
                            ]);

                            }catch (\GuzzleHttp\Exception\ClientException $ex) {
                            // $responseBody = $exception->getResponse()->getBody(true);
                            $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                            dd($response );
                        }
                    }
                }
                $message = 'JDID';

            }
        }

    //   cek marketplace->cek satuan unit terkecil->cek satuan unit barangtersebut

    }
        // order activities

        $person = DB::table('cms_users')->where('id', CRUDBooster::myID())->first();
        $name = $person->name;
        $orderInvoice = $getOrder->kode;
        $activities = $name.' memencet tombol start untuk packing order '.$orderInvoice;
        // $timeActivities = Date('M d, Y - H:i:s');
        $timeActivities = Date('Y-m-d H:i:s');
        $updateActivities = DB::connection('statistics')->table('order_activities')->insert([
            'user_id'=>CRUDBooster::myID(),
            'bookingan_id'=>$bookinganId,
            'time'=>$timeActivities,
            'created_at'=>$timeActivities,
            'description'=>$activities,
        ]);

    return response()->json(['status'=>'success', 'msg'=>'success'],200);
  }

  public function totalOrder(){
      $stores = DB::table('Registered_store')->get();
    //   $data = array();
      foreach($stores as $index=>$store){
          $getOrder = DB::table('bookingan')->whereBetween('created_at', ['2021-09-01', '2021-09-31'])->where('nmToko', $store->shop_id)->count();
        //   dd($getOrder);
        $insert = DB::table('total_order_september')->insert([
            'store_name'=>$store->store_name,
            'total_amount'=>$getOrder,
            'marketplace'=>$store->Platform
        ]);
        // array_push($data, ['store'=>$store->store_name,'total_order'=>$getOrder]);

        // $data[$index]->store = $store->store_name;
        // $data[$index]->totalOrder = $getOrder;
      }
    //   $data = collect($data);
    //   dd($data);
  }

    public function paginate(){
      $currentPage = LengthAwarePaginator::resolveCurrentPage();
      $currentResults = $items->slice(($currentPage - 1) * $perPage, $perPage)->all();
      $results = new LengthAwarePaginator($currentResults, $items->count(), $perPage);
      $results->withPath('station')->links();

      return $results;
    }

    public function penjualanDummy($kode, $qty, $toko){
        //   check stock terakhir
        $kodeBarang = explode('-', $kode)[0];
        $produk_induk = DB::table('products')->where('kode_item', $kodeBarang)->first();
        $stok = DB::table('stok')->where('tokoId',$toko)->where('produkId',$produk_induk->id)->orderby('id','desc')->first();
        $unitHitung = explode('-', $kode)[1][0];
        // $stockProduct1 = 0;
        // $stockProduct2 = 0;
        $stockProduct = 0;
        $satuanTerkecil = $produk_induk->productNote1;
        // dd($satuanTerkecil, $produk_induk, $kodeBarang);
        $name = "";
        if($satuanTerkecil == 3 || $satuanTerkecil == $produk_induk->stockNameUnit3){
            $name = $produk_induk->stockNameUnit3;
            if($unitHitung == 'x'){
              $unit_1 = $qty;
              $unit_2 = 0;
              $unit_3 = 0;
              $convertKarton = $stok->bookStockUnit1 * $produk_induk->stockConversionUnit1;
              $convertBundle = $stok->bookStockUnit2 * $produk_induk->stockConversionUnit2;
              $convertPcs = $stok->bookStockUnit3;
              $currentStock = $convertKarton + $convertBundle + $convertPcs;
              $stockProduct = $currentStock - ($unit_1 * $produk_induk->stockConversionUnit1);
            }elseif($unitHitung == 'y'){
              $unit_1 = 0;
              $unit_2 = $qty;
              $unit_3 = 0;
              $convertKarton =  $stok->bookStockUnit1 * $produk_induk->stockConversionUnit1;
              $bundle =  $stok->bookStockUnit2;
              $convertPcs =  $stok->bookStockUnit3;
              $currentStock = $convertKarton + $bundle + $convertPcs;
              $stockProduct = $currentStock - ($unit_2 * $produk_induk->stockConversionUnit2);
            }else{
              $unit_1 = 0;
              $unit_2 = 0;
              $unit_3 = $qty;
              $convertKarton =  $stok->bookStockUnit1 * $produk_induk->stockConversionUnit1;
              $bundle =  $stok->bookStockUnit2 * $produk_induk->stockConversionUnit2;
              $pcs = $stok->bookStockUnit3;
              $currentStock = $convertKarton + $bundle + $pcs;
              $stockProduct = $currentStock - $unit_3;
            }
        }elseif($satuanTerkecil == 2 || $satuanTerkecil == $produk_induk->stockNameUnit2){
            $name = $produk_induk->stockNameUnit2;
            if($unitHitung == 'x'){
              $unit_1 = $qty;
              $unit_2 = 0;
              $unit_3 = 0;
              $convertKarton = $stok->bookStockUnit1 * $produk_induk->stockConversionUnit1;
              $convertBundle = $stok->bookStockUnit2 * $produk_induk->stockConversionUnit2;
            //   $convertPcs = $stok->bookStockUnit3;
              $currentStock = $convertKarton + $convertBundle;
              $stockProduct = $currentStock - ($unit_1 * $produk_induk->stockConversionUnit1);
            }elseif($unitHitung == 'y'){
                // dd('tes');
              $unit_1 = 0;
              $unit_2 = $qty;
              $unit_3 = 0;
              $convertKarton =  $stok->bookStockUnit1 * $produk_induk->stockConversionUnit1;
              $bundle =  $stok->bookStockUnit2;
            //   $convertPcs =  $stok->bookStockUnit3;
              $currentStock = $convertKarton + $bundle;
              $stockProduct = $currentStock - ($unit_2 * $produk_induk->stockConversionUnit2);
            }
        }elseif($satuanTerkecil == 1|| $satuanTerkecil == $produk_induk->stockNameUnit1){
            $name = $produk_induk->stockNameUnit1;
              $unit_1 = $qty;
              $unit_2 = 0;
              $unit_3 = 0;
              $convertKarton = $stok->bookStockUnit1 ;
            //   $convertBundle = $stok->bookStockUnit2 * $produk_induk->stockConversionUnit2;
            //   $convertPcs = $stok->bookStockUnit3;
            //   $currentStock = $convertKarton + $convertBundle;
              $stockProduct = $convertKarton - $unit_1;
        }
        echo $stockProduct.' '.$name;
        // $updateStock = DB::table('stock')->where('tokoId',$toko)->where('produkId',$produk_induk->id)->orderby('id','desc')->update([
        //       'distid'=>"sales-id-".md5('tes123'.$toko).time(),
        //       'tipe' => "tes penjualan",
        //       'documentNumber' => 'tes123',
        //       'tokoId' => $toko,
        //       'produkId' => $produk_induk->id,
        //       'unit_1' => $unit_1,
        //       'unit_2' => $unit_2,
        //       'unit_3' => $unit_3,
        //       'bookStockUnit1' => $stockProduct1,
        //       'bookStockUnit2' => $stockProduct2,
        //       'bookStockUnit3' => $stockProduct3,
        //       'stockOpnameUnit1' => $stok->stockOpnameUnit1,
        //       'stockOpnameUnit2' => $stok->stockOpnameUnit2,
        //       'stockOpnameUnit3' => $stok->stockOpnameUnit3,
        //       'stockNameUnit1' => $produk_induk->stockNameUnit1,
        //       'stockNameUnit2' => $produk_induk->stockNameUnit2,
        //       'stockNameUnit3' => $produk_induk->stockNameUnit3
        //  ]);

        $platform = 'Tokopedia';
        $otherProducts = DB::table('products')->where('kode_item', 'LIKE', '%'.$kodeBarang.'%')->where('kode_item', '!=', $kode)->get();
        foreach($otherProducts as $product){
            if($getOrder->platform == 'Tokopedia'){
                // cek kode hitung
                $unitHitungMarketplace = explode("-", $product)[1][0];
                    if($unitHitungMarketplace == 'x'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit1);
                    }elseif($unitHitungMarketplace == 'y'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit2);
                    }else{
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit3);
                    }
                    // cek marketplace
                if($product->platform == 'Shopee'){

                    $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko', $product->shop_id)->first();
                    $shop_id= $shop->shop_id;
                    $access_token = $shop->access_token;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $timestamp = time();
                    $path = "/api/v2/product/update_stock";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $model_id = 0;
                    // $stock = $item->stock - $produk->qty;
                        // cek apakah model perlu atau tidak(variant)?
                    if($item->has_variant == 1){
                        $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                        $model_id = $getModelId->produk_id;
                        // $stock = $getModelId->stock - $produk->qty;
                    }

                    try{
                        $data[0]['model_id']=$model_id;
                        $data[0]['normal_stock']=$stock;
                        $clientShopeeStock = new Client();
                        $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type'=>'application/json',
                        ],
                            'json'=>
                                [
                                    'item_id'=>(int)$item->produk_id,
                                    'stock_list'=>$data
                                ]
                        ])->getBody()->getContents();
                        $responseShopeeStock = json_decode($resShopeeStock);
                        // updateStock
                        if($item->has_variant == 1){
                            $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);

                        }else{
                            // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                            //     'stock'=>$stock
                            //     ]);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                            'stock'=>$stock
                            ]);
                        }
                    }catch(\GuzzleHttp\Exception\ClientException $ex){
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response->message );
                    }
                }elseif($product->platform == 'Blibli'){
                    $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                    $sellerApiKey = $shop->seller_api_key;
                    $storeCode = $shop->shop_id;
                    // $stock = $item->stock - $produk->qty;
                    $client = new Client();
                    $clientId = env('BLIBLI_CLIENT_ID');
                    $clientKey = env('BLIBLI_CLIENT_KEY');
                    $Authorization = base64_encode("{$clientId}:{$clientKey}");
                    $blibliSku = $item->produk_id;

                    try{
                        $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                          'Authorization'=> "Basic $Authorization",
                          'Content-Type' => 'application/json',
                          'Accept' => 'application/json',
                          'Content-Length' => 0,
                          'User_Agent' => 'PostmanRuntime/7.17.1',
                          'Api-Seller-Key' => $sellerApiKey
                          ],
                              'json'=>
                                  [
                                      'availableStock'=>$stock,
                                  ]
                          ])->getBody();
                      $res = $res->getContents();
                      $response = json_decode($res);
                      $datas = $response->content;
                      $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                          'stock'=>$stock
                      ]);
                     }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response );
                    }
                }else{
                    // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                    $skuNo = $product->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    // $skuEncode = json_encode($skuId);
                    // $c = new JdClient();
                    // $c->appKey = env('APP_KEY');
                    // $c->appSecret = env('APP_SECRET');
                    // $c->accessToken = $shop->access_token;
                    // $c->serverUrl = "https://open-api.jd.id/routerjson";
                    // $req = new EpistockQueryEpiMerchantWareStockRequest();
                    // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                    // $respStock = $c->execute($req, $c->accessToken);
                    // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                    // // $currentStock = $stock - $produk->qty;
                    // // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                        'stock'=>$stock
                    ]);
                }

            }elseif($getOrder->platform == 'Shopee'){
                // ambil data bookingan
                $getToko = DB::connection('gudang')->table('Shopee_shop')->where('shop_id', $getOrder->nmToko)->first();
                $access_token=$getToko->access_token;
                $shop_id=$getToko->shop_id;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $order_sn = $getOrder->kode;
                // $package_number = $getToko->package_id;
                $timestamp = time();

                // shipping parameter
                    $path = "/api/v2/logistics/get_shipping_parameter";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShippingParameter = new Client();
                    $resShippingParameter = $clientShippingParameter->get("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_parameter?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign&order_sn=$order_sn")->getBody()->getContents();
                    $responseShippingParameter = json_decode($resShippingParameter);
                    $address_id = $responseShippingParameter->response->pickup->address_list[0]->address_id;
                    $pickup_time_id = $responseShippingParameter->response->pickup->address_list[0]->time_slot_list[0]->pickup_time_id;
                    $branch_id = $responseShippingParameter->response->dropoff->branch_list[0]->branch_id;
                    // dd($responseShippingParameter);
                // dd($responseShippingParameter->response->info_needed->pickup, $responseShippingParameter);
                $logistic_type = '';
                if( $responseShippingParameter->response->info_needed->pickup){
                    $logistic_type = 'pickUp';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            // 'package_number'=>$package_number,
                            'pickup'=>[
                                'address_id'=>$address_id,
                                'pickup_time_id'=>$pickup_time_id,
                                // 'tracking_number'=>$trackingNumber,
                            ],


                        ]
                    ])->getBody()->getContents();
                    $responseShipOrder = json_decode($resShipOrder);
                    // dd($responseShipOrder);

                }elseif($branch_id){
                    $logistic_type = 'dropoff';
                    // echo 'dropoff<br/>';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            'dropoff'=>[
                                'address_id'=>$address_id,
                                'pickup_time_id'=>$pickup_time_id,
                                // 'tracking_number'=>$trackingNumber,
                            ],


                        ]
                    ])->getBody()->getContents();
                    $responseShipOrder = json_decode($resShipOrder);
                    // dd($responseShipOrder, 'tes');
                }else{
                    $logistic_type = 'dropoff';
                    // echo 'dropoff<br/>';
                    // ship order
                    $path = "/api/v2/logistics/ship_order";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $clientShipOrder = new Client();
                    $resShipOrder = $clientShipOrder->post("https://partner.shopeemobile.com/api/v2/logistics/ship_order?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ["headers"=> [
                        'Content-Type' => 'application/json',
                    ],
                    'json'=> [
                            'order_sn'=>$order_sn,
                            'dropoff'=>[
                                'tracking_no'=>'',
                                'branch_id'=>0,
                                'sender_real_name'=>'',
                            ],


                        ]
                    ])->getBody()->getContents();
                    $responseShipOrder = json_decode($resShipOrder);
                    // dd($responseShipOrder, 'set');
                }

                // get package number
                $path = "/api/v2/order/get_order_detail";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientOrderList = new Client();
                $resOrderList = $clientOrderList->get("https://partner.shopeemobile.com/api/v2/order/get_order_detail?order_sn_list=$order_sn&response_optional_fields=total_amount,buyer_username,recipient_address,item_list,shipping_carrier,dropshipper,dropshipper_phone,invoice_data,package_list&shop_id=$shop_id&partner_id=$partner_id&access_token=$access_token&sign=$sign&timestamp=$timestamp")->getBody()->getContents();
                $responseOrderList = json_decode($resOrderList);
                $package_number = $responseOrderList->response->order_list[0]->package_list[0]->package_number;

                // get Tracking Number
                $path = "/api/v2/logistics/get_tracking_number";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $clientTrackingNumber = new Client();
                $resTrackingNumber = $clientTrackingNumber->get("https://partner.shopeemobile.com/api/v2/logistics/get_tracking_number?order_sn=$order_sn&response_optional_fields=first_mile_tracking_number&response_optional_fields=first_mile_tracking_number&shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign")->getBody()->getContents();
                $responseTrackingNumber = json_decode($resTrackingNumber);
                $trackingNumber = $responseTrackingNumber->response->tracking_number;

                // get shipping document parameter
                $path = "/api/v2/logistics/get_shipping_document_parameter";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $data = Array();
                $data[0]['order_sn']=$order_sn;
                $data[0]['package_number']=$package_number;
                $clientShippingDocumentParameter = new Client();
                $resShippingDocumentParameter = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_parameter?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", ['headers'=>[
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],

                        'json'=>[
                            'order_list'=>$data
                        ]
                ])->getBody()->getContents();
                $resShippingDocumentParameter = json_decode($resShippingDocumentParameter);
                $suggest_shipping_document_type = $resShippingDocumentParameter->response->result_list[0]->suggest_shipping_document_type;
                // CREATE SHIPPING DOCUMENT
                    $path = "/api/v2/logistics/create_shipping_document";
                    $salt = $partner_id . $path . $timestamp . $access_token . $shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $data = array();
                    $data[0]['order_sn'] = $order_sn;
                    $data[0]['package_number'] = $package_number;
                    $data[0]['tracking_number'] = $trackingNumber;
                    $data[0]['shipping_document_type'] = $suggest_shipping_document_type;
                    $createdDocument = $clientShippingDocumentParameter->post("https://partner.shopeemobile.com/api/v2/logistics/create_shipping_document?shop_id=$shop_id&partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&sign=$sign", [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],

                        'json' => [
                            'order_list' => $data
                        ]
                        // 'body'=>$parameter

                    ])->getBody()->getContents();
                    $resultCreated = json_decode($createdDocument);

                // get shipping document result
                $path = "/api/v2/logistics/get_shipping_document_result";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $dataDocResult = Array();
                $dataDocResult[0]["order_sn"]=$order_sn;
                $dataDocResult[0]["package_number"]=$package_number;
                $dataDocResult[0]["shipping_document_type"]=$suggest_shipping_document_type;
                // dd($dataDocResult);
                $clientShippingDocResult = new Client();
                $resShippingDocResult = $clientShippingDocResult->post("https://partner.shopeemobile.com/api/v2/logistics/get_shipping_document_result?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                ],
                    'json'=>[
                        'order_list'=>$dataDocResult
                    ]
                ])->getBody()->getContents();
                $responseShippingDocResult = json_decode($resShippingDocResult);
                // dd($suggest_shipping_document_type, $resShippingDocumentParameter);
            // download shipping document
                $path = "/api/v2/logistics/download_shipping_document";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                $dataDownload = Array();
                $dataDownload[0]["order_sn"]=$order_sn;
                $dataDownload[0]["package_number"]=$package_number;
                $clientDownloadShipDoc = new Client();
                $resDownloadShipDoc = $clientDownloadShipDoc->post("https://partner.shopeemobile.com/api/v2/logistics/download_shipping_document?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type' => 'application/json',
                    ],
                    'json'=>[
                        'shipping_document_type'=>$suggest_shipping_document_type,
                        'order_list'=>$dataDownload,
                        ]
                    ])->getBody()->getContents();
                $resShippingLabel = json_decode($resDownloadShipDoc);
                $name = "Shopee-$order_sn.pdf";
                \Storage::put("public/label/$name", $resDownloadShipDoc);
                $path = 'storage/label/'.$name;
                // update bukti bayar
                $updatebooking = DB::table('bookingan')->where('id', $id)->update([
                    'buktiBayar'=>$path,
                    'awbNo'=>$trackingNumber
                ]);

                $unitHitungMarketplace = explode("-", $product)[1][0];
                // cek kode hitung
                    if($unitHitungMarketplace == 'x'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit1);
                    }elseif($unitHitungMarketplace == 'y'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit2);
                    }else{
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit3);
                    }

                if($product->platform == 'Blibli'){
                      $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                    $sellerApiKey = $shop->seller_api_key;
                    $storeCode = $shop->shop_id;
                    // $stock = $item->stock - $produk->qty;
                    $client = new Client();
                    $clientId = env('BLIBLI_CLIENT_ID');
                    $clientKey = env('BLIBLI_CLIENT_KEY');
                    $Authorization = base64_encode("{$clientId}:{$clientKey}");
                    $blibliSku = $produk->produk_id;

                    try{
                        $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                          'Authorization'=> "Basic $Authorization",
                          'Content-Type' => 'application/json',
                          'Accept' => 'application/json',
                          'Content-Length' => 0,
                          'User_Agent' => 'PostmanRuntime/7.17.1',
                          'Api-Seller-Key' => $sellerApiKey
                          ],
                              'json'=>
                                  [
                                      'availableStock'=>$stock,
                                  ]
                          ])->getBody();
                      $res = $res->getContents();
                      $response = json_decode($res);
                      $datas = $response->content;
                      $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                          'stock'=>$stock
                      ]);
                     }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response );
                    }
                }elseif($product->platform == 'Tokopedia'){
                     $client = new Client();
                    $clientId = env('TOKOPEDIA_CLIENT_ID');
                    $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                    $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                    $clientData = base64_encode("{$clientId}:{$clientSecret}");
                    $appID = env('TOKOPEDIA_APP_ID');
                    try{
                        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                        'Authorization'=> "Basic $clientData",
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1'
                        ]])->getBody()->getContents();
                    }catch (RequestException $e) {
                        // dd(Psr7\str($e->getRequest()));
                        echo Psr7\str($e->getRequest());
                        if ($e->hasResponse()) {
                            echo Psr7\str($e->getResponse());
                        }
                    }
                    $token = json_decode($res);
                    try{
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                        $shop_id= (int)$shop->shop_id;
                        $fsId = (int)$appID;
                        // $stock = $item->stock - $produk->qty;
                        // if($getOrder->shop_id == 3897048){
                        //     $stock = $stockProduct;

                        // }
                    // dd($token, $shop_id, $appID);
                        $data = Array();
                        $data[0]['product_id']=(int)$product->produk_id;
                        $data[0]['new_stock']=$stock;
                        // $shop_id = $shop['shopsId'];
                        // dd($shop, $shop_id, $stock, $data, $token);
                        $clientStock = new Client();
                        $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                            'Authorization'=> "Bearer $token->access_token",
                            'Content-Type'=> 'application/json'
                        ],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                            'stock'=>$stock
                        ]);
                    }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents());
                        // dd($response, $e);
                    }

                }else{
                    // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                    $skuNo = $product->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    // $skuEncode = json_encode($skuId);
                    // $c = new JdClient();
                    // $c->appKey = env('APP_KEY');
                    // $c->appSecret = env('APP_SECRET');
                    // $c->accessToken = $shop->access_token;
                    // $c->serverUrl = "https://open-api.jd.id/routerjson";
                    // $req = new EpistockQueryEpiMerchantWareStockRequest();
                    // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                    // $respStock = $c->execute($req, $c->accessToken);
                    // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                    // // $currentStock = $stock - $produk->qty;
                    // // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                        'stock'=>$stock
                    ]);
                }
            }elseif($getOrder->platform == 'Blibli'){
                $unitHitungMarketplace = explode("-", $product)[1][0];
                // cek kode hitung
                    if($unitHitungMarketplace == 'x'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit1);
                    }elseif($unitHitungMarketplace == 'y'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit2);
                    }else{
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit3);
                    }
                if($product->platform == 'Shopee'){
                     $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko', $product->shop_id)->first();
                    $shop_id= $shop->shop_id;
                    $access_token = $shop->access_token;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $timestamp = time();
                    $path = "/api/v2/product/update_stock";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $model_id = 0;
                    // $stock = $item->stock - $produk->qty;
                        // cek apakah model perlu atau tidak(variant)?
                    if($item->has_variant == 1){
                        $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                        $model_id = $getModelId->produk_id;
                        // $stock = $getModelId->stock - $produk->qty;
                    }

                    try{
                        $data[0]['model_id']=$model_id;
                        $data[0]['normal_stock']=$stock;
                        $clientShopeeStock = new Client();
                        $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type'=>'application/json',
                        ],
                            'json'=>
                                [
                                    'item_id'=>(int)$item->produk_id,
                                    'stock_list'=>$data
                                ]
                        ])->getBody()->getContents();
                        $responseShopeeStock = json_decode($resShopeeStock);
                        // updateStock
                        if($item->has_variant == 1){
                            $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);

                        }else{
                            // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                            //     'stock'=>$stock
                            //     ]);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                            'stock'=>$stock
                            ]);
                        }
                    }catch(\GuzzleHttp\Exception\ClientException $ex){
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response->message );
                    }
                }elseif($product->platform == 'Tokopedia'){
                     $client = new Client();
                    $clientId = env('TOKOPEDIA_CLIENT_ID');
                    $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                    $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                    $clientData = base64_encode("{$clientId}:{$clientSecret}");
                    $appID = env('TOKOPEDIA_APP_ID');
                    try{
                        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                        'Authorization'=> "Basic $clientData",
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1'
                        ]])->getBody()->getContents();
                    }catch (RequestException $e) {
                        // dd(Psr7\str($e->getRequest()));
                        echo Psr7\str($e->getRequest());
                        if ($e->hasResponse()) {
                            echo Psr7\str($e->getResponse());
                        }
                    }
                    $token = json_decode($res);
                    try{
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                        $shop_id= (int)$shop->shop_id;
                        $fsId = (int)$appID;
                        // $stock = $item->stock - $produk->qty;
                        // if($getOrder->shop_id == 3897048){
                        //     $stock = $stockProduct;

                        // }
                    // dd($token, $shop_id, $appID);
                        $data = Array();
                        $data[0]['product_id']=(int)$product->produk_id;
                        $data[0]['new_stock']=$stock;
                        // $shop_id = $shop['shopsId'];
                        // dd($shop, $shop_id, $stock, $data, $token);
                        $clientStock = new Client();
                        $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                            'Authorization'=> "Bearer $token->access_token",
                            'Content-Type'=> 'application/json'
                        ],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                            'stock'=>$stock
                        ]);
                    }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents());
                        // dd($response, $e);
                    }

                }else{
                    // $jdidProduk = DB::table('produk_marketplace')->where('kode_item', $produk->sku)->where('platform', 'JDID')->first();
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->first();
                    $skuNo = $product->produk_id;
                    $skuId = (object)[
                        'skuId'=>$skuNo,
                    ];
                    // $skuEncode = json_encode($skuId);
                    // $c = new JdClient();
                    // $c->appKey = env('APP_KEY');
                    // $c->appSecret = env('APP_SECRET');
                    // $c->accessToken = $shop->access_token;
                    // $c->serverUrl = "https://open-api.jd.id/routerjson";
                    // $req = new EpistockQueryEpiMerchantWareStockRequest();
                    // $req->setWareStockQueryListStr("[{'skuId':$skuId->skuId}]");
                    // $respStock = $c->execute($req, $c->accessToken);
                    // $stock = $respStock->jingdong_epistock_queryEpiMerchantWareStock_response->EptRemoteResult->model->$skuNo[1];
                    // // $currentStock = $stock - $produk->qty;
                    // // updateStock
                    $realNum = (object)[
                        'realNum'=>$stock
                    ];
                    $c = new JdClient();
                    $c->appKey = env('APP_KEY');
                    $c->appSecret = env('APP_SECRET');
                    $c->accessToken = $shop->access_token;
                    $c->serverUrl = "https://open-api.jd.id/routerjson";
                    $req = new EpistockUpdateEpiMerchantWareStockRequest();
                    $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                    $respUpdateStock = $c->execute($req, $c->accessToken);
                    $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->update([
                        'stock'=>$stock
                    ]);
                }
            }else{
                $unitHitungMarketplace = explode("-", $product)[1][0];
                // cek kode hitung
                    if($unitHitungMarketplace == 'x'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit1);
                    }elseif($unitHitungMarketplace == 'y'){
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit2);
                    }else{
                        $stock = floor($stockProduct / $produk_induk->stockConversionUnit3);
                    }
                if($product->platform == 'Tokopedia'){
                     $client = new Client();
                    $clientId = env('TOKOPEDIA_CLIENT_ID');
                    $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                    $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                    $clientData = base64_encode("{$clientId}:{$clientSecret}");
                    $appID = env('TOKOPEDIA_APP_ID');
                    try{
                        $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                        'Authorization'=> "Basic $clientData",
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1'
                        ]])->getBody()->getContents();
                    }catch (RequestException $e) {
                        // dd(Psr7\str($e->getRequest()));
                        echo Psr7\str($e->getRequest());
                        if ($e->hasResponse()) {
                            echo Psr7\str($e->getResponse());
                        }
                    }
                    $token = json_decode($res);
                    try{
                        $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Tokopedia')->first();
                        $shop_id= (int)$shop->shop_id;
                        $fsId = (int)$appID;
                        // $stock = $item->stock - $produk->qty;
                        // if($getOrder->shop_id == 3897048){
                        //     $stock = $stockProduct;

                        // }
                    // dd($token, $shop_id, $appID);
                        $data = Array();
                        $data[0]['product_id']=(int)$product->produk_id;
                        $data[0]['new_stock']=$stock;
                        // $shop_id = $shop['shopsId'];
                        // dd($shop, $shop_id, $stock, $data, $token);
                        $clientStock = new Client();
                        $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                            'Authorization'=> "Bearer $token->access_token",
                            'Content-Type'=> 'application/json'
                        ],
                            'json'=>$data
                        ])->getBody()->getContents();
                        $responseStock = json_decode($resStock);
                        $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Tokopedia')->update([
                            'stock'=>$stock
                        ]);
                    }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents());
                        // dd($response, $e);
                    }
                }elseif($product->platform == 'Shopee'){
                     $shop = DB::connection('gudang')->table('Shopee_shop')->where('toko', $product->shop_id)->first();
                    $shop_id= $shop->shop_id;
                    $access_token = $shop->access_token;
                    $partner_id = (int)env('SHOPEE_PARTNER_ID');
                    $partner_key = env('SHOPEE_PARTNER_KEY');
                    $timestamp = time();
                    $path = "/api/v2/product/update_stock";
                    $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                    $sign = hash_hmac('sha256', $salt, $partner_key);
                    $model_id = 0;
                    // $stock = $item->stock - $produk->qty;
                        // cek apakah model perlu atau tidak(variant)?
                    if($item->has_variant == 1){
                        $getModelId = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->first();
                        $model_id = $getModelId->produk_id;
                        // $stock = $getModelId->stock - $produk->qty;
                    }

                    try{
                        $data[0]['model_id']=$model_id;
                        $data[0]['normal_stock']=$stock;
                        $clientShopeeStock = new Client();
                        $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                            'Content-Type'=>'application/json',
                        ],
                            'json'=>
                                [
                                    'item_id'=>(int)$item->produk_id,
                                    'stock_list'=>$data
                                ]
                        ])->getBody()->getContents();
                        $responseShopeeStock = json_decode($resShopeeStock);
                        // updateStock
                        if($item->has_variant == 1){
                            $updateStock = DB::table('produk_marketplace')->where('parent_produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                                'stock'=>$stock
                                ]);

                        }else{
                            // $updateStock = DB::table('produk_marketplace')->where('produk_id', $item->produk_id)->where('platform', 'Shopee')->update([
                            //     'stock'=>$stock
                            //     ]);
                            $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Shopee')->update([
                            'stock'=>$stock
                            ]);
                        }
                    }catch(\GuzzleHttp\Exception\ClientException $ex){
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response->message );
                    }
                }else{
                    $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->first();
                    $sellerApiKey = $shop->seller_api_key;
                    $storeCode = $shop->shop_id;
                    // $stock = $item->stock - $produk->qty;
                    $client = new Client();
                    $clientId = env('BLIBLI_CLIENT_ID');
                    $clientKey = env('BLIBLI_CLIENT_KEY');
                    $Authorization = base64_encode("{$clientId}:{$clientKey}");
                    $blibliSku = $item->produk_id;

                    try{
                        $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                          'Authorization'=> "Basic $Authorization",
                          'Content-Type' => 'application/json',
                          'Accept' => 'application/json',
                          'Content-Length' => 0,
                          'User_Agent' => 'PostmanRuntime/7.17.1',
                          'Api-Seller-Key' => $sellerApiKey
                          ],
                              'json'=>
                                  [
                                      'availableStock'=>$stock,
                                  ]
                          ])->getBody();
                      $res = $res->getContents();
                      $response = json_decode($res);
                      $datas = $response->content;
                      $updateStock = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'Blibli')->update([
                          'stock'=>$stock
                      ]);
                     }catch (\GuzzleHttp\Exception\ClientException $ex) {
                        // $responseBody = $exception->getResponse()->getBody(true);
                        $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                        dd($response );
                    }
                }
            }
        }
    }

    public function cariIndukProduk(Request $request, $toko){
        if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('products')->where('productName','LIKE','%'.$cari.'%')->where('brand_id', $toko)->get();
            // $data = DB::table('produk')->where('product_name','LIKE','%'.$cari.'%')->where('gudang_id', $id)->get();
            return response()->json($data);
        }
    }

    public function editProduk(Request $request, $id){
        // dd($request);
        $updateProduk = DB::table('produk_marketplace')->where('id', $id)->update([
            'initial'=>$request->initial,
            'kode_item'=>$request->kode_item,
            'jenis_barang'=>$request->jenis_barang,
            'convertion_id'=>$request->stockName,
            'stock_convertion_unit'=>$request->stockUnit,
            'updated_at'=>date('Y-m-d H:i:s'),
            'induk_barang_id'=>$request->indukProduk,
        ]);

       return redirect()->to('https://teratur.gudangin.id/admin/list_products');
    }

    public function detailIndukEdit($id){
        $getIndukProduk = DB::table('products')->where('id', $id)->first();
        $unserializeImage = unserialize($getIndukProduk->productImages);
        $getIndukProduk->productImages = $unserializeImage[0];
        // dd($unserializeImage[0], $getIndukProduk);
        return response()->json($getIndukProduk);
    }

    public function getOngkir(){
        $order = DB::table('bookingan')->whereIn('nmToko', ['3897048', 'sinde-1', '510976397'])->get();
        dd($order);
    }

    public function createOrderanStep1(){
        $toko = DB::table('data_toko')->get();
        return view('admin.bookingan',compact('toko'));
    }

    public function creatingOrderStep1(Request $request){
        // dd('tes');
      // date_default_timezone_set("Asia/Jakarta");
      // $file = $request->file('invoice');
      // $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());
      // Storage::disk('local')->put('uploads/'.$uniqueFileName, $file);
      //
      // $files = Storage::allFiles('uploads/'.$uniqueFileName);

      $insert = DB::table('bookingan')->insertGetId([
        'phone' => $request->phone,
        'nama_pemesan' => $request->nama,
        'alamat' => $request->alamat,
        'platform' => $request->platform,
        'kode' => $request->noinvoice,
        'kurir' => $request->kurir,
        'toko' => $request->toko,
        // 'buktiBayar' => $files[0],
        'time_status_keranjang' => date('Y-m-d H:i:s'),
        'time_status_bayar' => date('Y-m-d H:i:s'),
      ]);

      return redirect()->route('createOrderanStep2',$insert);
    }
    public function createOrderanStep2($id){
      $pesanan = DB::table('bookingan')->where('id',$id)->first();
      $toko = DB::table('data_toko')->where('id',$pesanan->toko)->first();
      return view('admin.bookingan-step-2',compact('pesanan','id','toko'));
    }

    public function creatingOrderStep2(Request $request,$id){
     //dd($request->all());
    // dd($request, $id, $request->nameUnit);
     $product = DB::table('products')->where('id',$request->Product)->first();
     $convertion = DB::table('convertion')->where('id', $request->nameUnitId)->first();
     $newOrderan = [
       'qty' => $request->qty,
       'satuanUnit' => $convertion->nama_satuan,
       'Product' => $product,
       'id'=>$request->nameUnitId
     ];

     $orderan = Session::get('po.orderan');

     if(empty(Session::get('po.orderan'))){
         $orderan[0] = $newOrderan;
     }else{
         array_push($orderan,$newOrderan);
     }


     //dd($orderan,$newOrderan);
     Session::put('po.orderan',$orderan);
     return redirect()->back();
   }

   public function orderan_submit($id){
      $bookingan = DB::table('bookingan')->where('bookingan.id',$id)->first();

      $totalBayar = 0;
      $jmlh_item = 0;

      // dd(Session::get('po.orderan'));
      foreach(Session::get('po.orderan') as $index=>$produk){
        $bookingan->order[$index]= new \stdClass();
        $bookingan->order[$index]->product=$produk['Product']->productName;
        $bookingan->order[$index]->qty= $produk['qty'];
        $products = DB::table('products')->where('products.id', $produk['Product']->id)->first();
        $insertOrderDetail = DB::table('order_from_marketplace_detail')->insert([
          'orderId'=>$id,
          'product_id'=>$produk['Product']->id,
          'item'=>$products->productName,
          'qty'=>$produk['qty'],
          'hargaSatuan'=>$produk['Product']->productPrice,
          'hargaTotal'=>$produk['Product']->productPrice * $produk['qty'],
          'convertion_id'=>$produk['id']
        ]);
        $jmlh_item += $produk['qty'];
        $totalBayar +=  $produk['Product']->productPrice * $produk['qty'];

        // kurangin stock
        // get last stock1
        $lastStock = DB::table('stock')->where('induk_produk_id', $produk['Product']->id)->orderby('id', 'desc')->first();
        $lastStockNumber = DB::table('stock_number')->select('convertion.id as convertionId', 'stock_number.bookStockUnit_induk', 'convertion.stock_convertion_unit', 'stock_number.stockOpnameUnit_induk')->where('stock_id', $lastStock->id)->join('convertion', 'convertion.id', 'stock_number.convertion_id')->orderby('convertion.stock_convertion_unit', 'desc')->get();
        $totalLastStock = 0;
        $convertSale = 0;
        $lastStockOpnameUnit = [];
        // dd($lastStockNumber, $lastStock->id);
        foreach($lastStockNumber as $index=>$number){
            $convertStock = $number->bookStockUnit_induk * $number->stock_convertion_unit;
            $totalLastStock = $totalLastStock + $convertStock;
            array_push($lastStockOpnameUnit, $number->stockOpnameUnit_induk);
            if($number->convertionId == $produk['id']){
               $convertSale = $produk['qty'] * $number->stock_convertion_unit;
            }
        }
        $stockMinusSale = $totalLastStock - $convertSale;

        $insertNewStock = DB::table('stock')->insertGetId([
          'data_toko_id'=>$produk['Product']->brand_id,
          'induk_produk_id'=>$produk['Product']->id,
          'tipe'=>'penjualan offline',
          'created_at'=>date('Y-m-d H:i:s')
        ]);
        // dd($lastStockOpnameUnit);
        $getConvertions = DB::table('convertion')->where('induk_produk_id', $produk['Product']->id)->orderby('stock_convertion_unit', 'desc')->get();
        $moduloSelisihUnitInduk = 0;
        foreach ($getConvertions as $index=>$convertion) {
          if($index == 0){

             $modSelisihUnitInduk = $stockMinusSale % $convertion->stock_convertion_unit;
             $divSelisihUnitInduk  = $stockMinusSale - $modSelisihUnitInduk;
             $convertSelisihUnitInduk = $divSelisihUnitInduk  / $convertion->stock_convertion_unit;
             $moduloSelisihUnitInduk = $modSelisihUnitInduk;
             // dd($stockMinusSale, $convertion->stock_convertion_unit,$modSelisihUnitInduk, $divSelisihUnitInduk, $convertSelisihUnitInduk);
          }else{
            // dd($moduloSelisihUnitInduk);
            $modSelisihUnitInduk = $moduloSelisihUnitInduk % $convertion->stock_convertion_unit;
            $divSelisihUnitInduk = $moduloSelisihUnitInduk - $modSelisihUnitInduk;
            $convertSelisihUnitInduk = $divSelisihUnitInduk / $convertion->stock_convertion_unit;
            $moduloSelisihUnitInduk = $modSelisihUnitInduk;

            // dd($moduloSelisihUnitInduk, $convertion->stock_convertion_unit,$modSelisihUnitInduk, $divSelisihUnitInduk, $convertSelisihUnitInduk);
          }
          // dd($convertion);
          $insertStockNumber = DB::table('stock_number')->insert([
            'stock_id'=>$insertNewStock,
            'convertion_id'=>$convertion->id,
            'unit'=>$convertSelisihUnitInduk,
            'bookStockUnit_induk'=>$convertSelisihUnitInduk,
            'stockOpnameUnit_induk'=>$lastStockOpnameUnit[$index],
            'created_at'=>date('Y-m-d H:i:s')
          ]);
        }
          //cek products marketplaceStock
          $getProductMarketplace = DB::table('produk_marketplace')->where('induk_barang_id', $produk['Product']->id)->get();
          foreach($getProductMarketplace as $product){
            $produkMarketplaceConvertion = DB::table('convertion')->where('id', $product->convertion_id)->first();
            $stockMarketplace = $stockMinusSale / $produkMarketplaceConvertion->stock_convertion_unit;

            if($product->platform == 'Tokopedia'){
              $client = new Client();
              $clientId = env('TOKOPEDIA_CLIENT_ID');
              $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
              $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
              $clientData = base64_encode("{$clientId}:{$clientSecret}");
              $appID = env('TOKOPEDIA_APP_ID');
              try{
                  $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                  'Authorization'=> "Basic $clientData",
                  'Content-Length' => 0,
                  'User_Agent' => 'PostmanRuntime/7.17.1'
                  ]])->getBody()->getContents();
              }catch (RequestException $e) {
                  // dd(Psr7\str($e->getRequest()));
                  echo Psr7\str($e->getRequest());
                  if ($e->hasResponse()) {
                      echo Psr7\str($e->getResponse());
                  }
              }
              $token = json_decode($res);
              try{
                  $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('platform', 'Tokopedia')->first();
                  $shop_id= (int)$shop->shop_id;
                  $fsId = (int)$appID;
                  $data = Array();
                  $data[0]['product_id']=(int)$product->produk_id;
                  $data[0]['new_stock']=$stockMarketplace;

                  $clientStock = new Client();
                  $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                      'Authorization'=> "Bearer $token->access_token",
                      'Content-Type'=> 'application/json'
                  ],
                      'json'=>$data
                  ])->getBody()->getContents();
                  $responseStock = json_decode($resStock);
              }catch (\GuzzleHttp\Exception\ClientException $ex) {
                  // $responseBody = $exception->getResponse()->getBody(true);
                  $response = json_decode($ex->getResponse()->getBody()->getContents());
                  // dd($response, $e);
              }
            }elseif ($platform == 'Shopee'){
                // yang ditandai adalah induk productsnya;
                 $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$product->shop_id)->first();
                $shop_id= $shop->shop_id;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/product/update_stock";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);

                if($product->is_variant == 1){
                    $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                    $item_id = $getParent->produk_id;
                    $model_id = $product->produk_id;
                }else{
                    $item_id = $product->produk_id;
                    $model_id = 0;
                }
                $data[0]['model_id']=(int)$model_id;
                $data[0]['normal_stock']=(int)$stockMarketplace;
                $clientShopeeStock = new Client();
                $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type'=>'application/json',
                ],
                    'json'=>
                        [
                            'item_id'=>(int) $item_id,
                            'stock_list'=>$data
                        ]
                ])->getBody()->getContents();
                $responseShopeeStock = json_decode($resShopeeStock);
          }elseif ($platform == 'Blibli'){
              $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Blibli')->first();
              $sellerApiKey = $shop->seller_api_key;
              $storeCode = $shop->shop_id;
              $client = new Client();
              $clientId = env('BLIBLI_CLIENT_ID');
              $clientKey = env('BLIBLI_CLIENT_KEY');
              $Authorization = base64_encode("{$clientId}:{$clientKey}");
              $blibliSku = $product->produk_id;
              // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
              try{
                   $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                      'Authorization'=> "Basic $Authorization",
                      'Content-Type' => 'application/json',
                      'Accept' => 'application/json',
                      'Content-Length' => 0,
                      'User_Agent' => 'PostmanRuntime/7.17.1',
                      'Api-Seller-Key' => $sellerApiKey
                      ],
                          'json'=>
                              [
                                  'availableStock'=>$stockMarketplace
                              ]
                      ])->getBody();
                  $res = $res->getContents();
                  $response = json_decode($res);
                  $datas = $response->content;
              }catch (\GuzzleHttp\Exception\ClientException $ex) {
                  // $responseBody = $exception->getResponse()->getBody(true);
                  $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                  dd($response );
              }
      }elseif ($platform == 'JDID') {
          $jdidProduk = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->first();
          $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
          $skuNo = $jdidProduk->produk_id;
          $skuId = (object)[
              'skuId'=>$skuNo,
          ];
          $skuEncode = json_encode($skuId);
          // updateStock
          $realNum = (object)[
              'realNum'=>$stockMarketplace
          ];
          $c = new JdClient();
          $c->appKey = env('APP_KEY');
          $c->appSecret = env('APP_SECRET');
          $c->accessToken = $shop->access_token;
          $c->serverUrl = "https://open-api.jd.id/routerjson";
          $req = new EpistockUpdateEpiMerchantWareStockRequest();
          $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
          $respUpdateStock = $c->execute($req, $c->accessToken);
      }
      else {
          $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
          $access_token = $shop->access_token;
          $stockUpdateClient = new Client();
          $data[0]['product_id']=$product->id;
          $data[0]['stock_value']=$stockMarketplace;
          $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
              ['Authorization' => "Bearer $access_token"],
              'json'=>$data
          ])->getBody()->getContents();
          $responseLabel = json_decode($resLabel);
        }
      }
    }
      // dd($bookingan);
      $bookinganId = $bookingan->id;
      $logistic_type = 'dropoff';
      $order_sn = $bookingan->kode;
      $pdfQr = new \PDF();
      $customPaperQr = array(0,0,270.28,270.64);
      $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
      $nameQr = $bookinganId.'.pdf';
      \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
      $content = $pdfQr->download($nameQr);
      $filePath = 'storage/sticker/'.$nameQr;

      $pdf = new \PDF();


      $pdf = \PDF::loadView('admin.invoicePdf', compact('bookingan'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
      $name = 'offline-'.$id.'.pdf';
      \Storage::put('public/label/'.$name, $pdf->output());
      $path = "storage/label/$name";

      $getStation = DB::table('station')->where('first_queue', 1)->first();
      $nextStation = $getStation->id + 1;
      if($nextStation > 2){
          $nextStation = 1;

      }
      $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
          'first_queue'=>0,
      ]);
      $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
          'first_queue'=>1,
          'status'=>'empty',
      ]);

      $update = DB::table('bookingan')->where('id',$id)->update([
        'jmlh_item' => $jmlh_item,
        'totalBayar' => $totalBayar,
        'status_keranjang' => 'keranjang_berisi',
        'status_bayar' => 'pembayaran diterima',
        // 'status' => 'packing',
        'station'=>$getStation->id,
        'logistic_type'=>'dropoff',
        'buktiBayar'=>$path,
        'invoice_toko'=>$filePath,
        'updated_at'=>date('Y-m-d H:i:s'),
      ]);

      $sku = Session::get('sku');
      // $updateOrder = DB::table('order_from_marketplace')->where('Invoice', $bookingan->kode)->update([
      //       'status'=>'inactive',
      //       'pengemasan'=>'true',
      //       ]);

      Session::forget('po');
    //   Session::forget('sku');
      CRUDBooster::redirect('admin/bookingan77' ,'Orderan berhasil dibuat!', 'success');

    }

    public function orderan_cancel(){
      Session::forget('po');
      CRUDBooster::redirect('admin/bookingan77' ,'Orderan dicancel!', 'danger');
    }

    public function substractOrderan($id){
      $orderan = Session::get('po.orderan');
      array_splice($orderan, $id, 1);
      Session::put('po.orderan',$orderan);
      return redirect()->back();
    }

    public function createStockAwal(){
      $products = DB::table('products')->where('client_id', 2)->get();
      foreach($products as $product){
        $insertStock = DB::table('stock')->insertGetId([
          'data_toko_id'=>223,
          'induk_produk_id'=>$product->id,
          'tipe'=>'Stock Awal',
          'created_at'=>date('Y-m-d H:i:s'),
        ]);
        $getConversions = DB::table('convertion')->where('induk_produk_id', $product->id)->get();
        foreach($getConversions as $convertion){
          $insertStockNumber = DB::table('stock_number')->insert([
            'stock_id'=>$insertStock,
            'convertion_id'=>$convertion->id,
            'unit'=>0,
            'bookStockUnit_induk'=>0,
            'stockOpnameUnit_induk'=>0,
            'bookStockUnit_depo'=>0,
            'stockOpnameUnit_depo'=>0,
            'created_at'=>date('Y-m-d H:i:s'),
          ]);
        }
      }
      echo 'sukses';
    }
    
    // public function sendPicture(){
        
    //     try{
    //         $client = new Client();
    //         $resUploadImage = $client->post("https://api.gudangin.id/api/v2/media_space/upload_image?partner_id=$partner_id&timestamp=$timestamp&sign=$sign", [
    //           'headers'=> [
    //               'Content-Type' => 'application/json',
    //           ],
    //           'multipart'=>[
    //               [
    //                   'Content-type' => 'multipart/form-data',
    //                   'name'=>'image',
    //                   'contents'=> fopen($file_path, 'r'),
    //               ],
    //           ]
    //       ])->getBody()->getContents();
    //     }catch(\GuzzleHttp\Exception\ClientException $e){
    //       $response = json_decode($e->getResponse()->getBody()->getContents());
    //       dd($response);
    //     }
    // }
    
    public function doubleProduct(){
        // $getShops = DB::table('Registered_store')->where('shop_id', 3897048)->get();
        // // dd($getShop);
        // foreach($getShops as $shop){
            $getProducts = DB::table('produk_marketplace')->where('shop_id', 510976397)->whereNull('induk_barang_id')->get();
            dd($getProducts);
            // foreach($getProducts as $product){
            //     $countProduct = DB::table('produk_marketplace')->where('shop_id', $product->shop_id)->where('produk_id', $product->produk_id)->count();
            //     if($countProduct > 1){
            //         dd($product);
            //     }
            // }
        // }
        echo 'done';
        // $getData = DB::table('produk_marketplace')->where('shop')
    }
    
    public function updateBarang(){
        // $getAllSindeProduct = DB::table('products')->where('brand_id', 223)->where('client_id')->get();
        $getAllSindeProduct = DB::table('products')->where('id', 577)->first();
        $getAllMarketplaceProducts = DB::table('produk_marketplace')->select('produk_marketplace.platform','produk_marketplace.sku','produk_marketplace.is_variant','produk_marketplace.shop_id', 'produk_marketplace.induk_barang_id', 'convertion.stock_convertion_unit','produk_marketplace.produk_id')->where('produk_marketplace.induk_barang_id', $getAllSindeProduct->id)->join('convertion', 'convertion.id', 'produk_marketplace.convertion_id')->get();
        $getLastStock = DB::table('stock')->where('induk_produk_id', $getAllSindeProduct->id)->orderBy('id', 'desc')->first();
        // dd($getLastStock, $getAllMarketplaceProducts);
        $getLastStockNumbers = DB::table('stock_number')->where('stock_number.stock_id', $getLastStock->id)->join('convertion', 'convertion.id', 'stock_number.convertion_id')->get();
        // dd($getLastStockNumbers);
        $totalStock = 0;
        foreach($getLastStockNumbers as $number){
            $convertStock = $number->bookStockUnit_induk * $number->stock_convertion_unit;
            $totalStock = $totalStock + $convertStock;
        }
        foreach($getAllMarketplaceProducts as $product){
            // $stock = floor($totalStock / $product->stock_convertion_unit);
            $stock = 28;
            if($product->platform == 'Tokopedia'){
                
                $client = new Client();
                $clientId = env('TOKOPEDIA_CLIENT_ID');
                $clientSecret = env('TOKOPEDIA_CLIENT_SECRET');
                $myData = array('client-id'=>$clientId, 'client-secret'=>$clientSecret);
                $clientData = base64_encode("{$clientId}:{$clientSecret}");
                $appID = env('TOKOPEDIA_APP_ID');
                try{
                    $res = $client->post('https://accounts.tokopedia.com/token?grant_type=client_credentials', ['headers' => [
                    'Authorization'=> "Basic $clientData",
                    'Content-Length' => 0,
                    'User_Agent' => 'PostmanRuntime/7.17.1'
                    ]])->getBody()->getContents();
                }catch (RequestException $e) {
                    echo Psr7\str($e->getRequest());
                    if ($e->hasResponse()) {
                        echo Psr7\str($e->getResponse());
                    }
                }
                $token = json_decode($res);
                try{
                    $shop = DB::table('Registered_store')->where('shop_id', $product->shop_id)->where('platform', 'Tokopedia')->first();
                    $shop_id= (int)$shop->shop_id;
                    $fsId = (int)$appID;
                    $data = Array();
                    $data[0]['product_id']=(int)$product->produk_id;
                    $data[0]['new_stock']=$stock;

                    $clientStock = new Client();
                    $resStock = $clientStock->post("https://fs.tokopedia.net/inventory/v1/fs/$fsId/stock/update?shop_id=$shop_id", ['headers'=>[
                        'Authorization'=> "Bearer $token->access_token",
                        'Content-Type'=> 'application/json'
                    ],
                        'json'=>$data
                    ])->getBody()->getContents();
                    $responseStock = json_decode($resStock);
                }catch (\GuzzleHttp\Exception\ClientException $ex) {
                    // $responseBody = $exception->getResponse()->getBody(true);
                    $response = json_decode($ex->getResponse()->getBody()->getContents());
                    // dd($response, $e);
                }
            }elseif($product->platform == 'Shopee'){
                // yang ditandai adalah induk productsnya;
                 $shop = DB::connection('gudang')->table('Shopee_shop')->where('shop_id',$product->shop_id)->first();
                $shop_id= $shop->shop_id;
                $access_token = $shop->access_token;
                $partner_id = (int)env('SHOPEE_PARTNER_ID');
                $partner_key = env('SHOPEE_PARTNER_KEY');
                $timestamp = time();
                $path = "/api/v2/product/update_stock";
                $salt = $partner_id.$path.$timestamp.$access_token.$shop_id;
                $sign = hash_hmac('sha256', $salt, $partner_key);
                // dd( $shop);
                if($product->is_variant == 1){
                    $getParent = DB::table('produk_marketplace')->where('id', $product->parent_produk_id)->first();
                    $item_id = $getParent->produk_id;
                    $model_id = $product->produk_id;
                }else{
                    $item_id = $product->produk_id;
                    $model_id = 0;
                }
                // dd($item_id, $model_id, $stock);
                 $data[0]['model_id']=(int)$model_id;
                $data[0]['normal_stock']=(int)$stock;
                $clientShopeeStock = new Client();
                $resShopeeStock = $clientShopeeStock->post("https://partner.shopeemobile.com/api/v2/product/update_stock?partner_id=$partner_id&timestamp=$timestamp&access_token=$access_token&shop_id=$shop_id&sign=$sign", ['headers'=>[
                    'Content-Type'=>'application/json',
                ],
                    'json'=>
                        [
                            'item_id'=>(int)$item_id,
                            'stock_list'=>$data
                        ]
                ])->getBody()->getContents();
                $responseShopeeStock = json_decode($resShopeeStock);
                // dd($responseShopeeStock, $data, $item_id);
            }elseif($product->platform == 'Blibli'){
                $shop = DB::table('Registered_store')->where('shop_id',$product->shop_id)->where('platform', 'Blibli')->first();
                $sellerApiKey = $shop->seller_api_key;
                $storeCode = $shop->shop_id;
                $client = new Client();
                $clientId = env('BLIBLI_CLIENT_ID');
                $clientKey = env('BLIBLI_CLIENT_KEY');
                $Authorization = base64_encode("{$clientId}:{$clientKey}");
                $blibliSku = $product->produk_id;
                // dd($Authorization, $sellerApiKey ,$storeCode, $blibliSku, $item);
                try{
                     $res = $client->put('https://api.blibli.com/v2/proxy/seller/v1/products/'.$blibliSku.'/stock?requestId=myCompany-f8127be2-1da4-4e65-9280-42137d0d40ed&storeCode='.$storeCode.'&channelId=PT. YOKESEN TEKNOLOGI&storeId=10001&username=daniel.yokesen@gmail.com', ['headers' => [
                        'Authorization'=> "Basic $Authorization",
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'Content-Length' => 0,
                        'User_Agent' => 'PostmanRuntime/7.17.1',
                        'Api-Seller-Key' => $sellerApiKey
                        ],
                            'json'=>
                                [
                                    'availableStock'=>$stock,
                                ]
                        ])->getBody();
                    $res = $res->getContents();
                    $response = json_decode($res);
                    $datas = $response->content;
                }catch (\GuzzleHttp\Exception\ClientException $ex) {
                    // $responseBody = $exception->getResponse()->getBody(true);
                    $response = json_decode($ex->getResponse()->getBody()->getContents(), true);
                    dd($response );
                }
            }elseif($product->platform == 'Warisan'){
                // dd($product->sku, $stock);
                $stockClient = new Client();
                $resStock = $stockClient->post("https://api.warisan.co.id/api/v1/product/stock", [
                  'json'=>[
                      'sku'=>$product->sku,
                      'stock'=>$stock
                  ]
                ])->getBody()->getContents();
                $responseStock = json_decode($resStock);
                // dd($stock, $product->sku, $resStock, $responseStock);
            }elseif($product->platform == 'JDID'){
                $jdidProduk = DB::table('produk_marketplace')->where('produk_id', $product->produk_id)->where('platform', 'JDID')->first();
                $shop = DB::table('Registered_store')->where('shop_id', $jdidProduk->shop_id)->first();
                $skuNo = $jdidProduk->produk_id;
                $skuId = (object)[
                    'skuId'=>$skuNo,
                ];
                $skuEncode = json_encode($skuId);
                // updateStock
                $realNum = (object)[
                    'realNum'=>$stock
                ];
                $c = new JdClient();
                $c->appKey = env('APP_KEY');
                $c->appSecret = env('APP_SECRET');
                $c->accessToken = $shop->access_token;
                $c->serverUrl = "https://open-api.jd.id/routerjson";
                $req = new EpistockUpdateEpiMerchantWareStockRequest();
                $req->setWareStockUpdateListStr( "[{'skuId':$skuId->skuId, 'realNum':$realNum->realNum}]" );
                $respUpdateStock = $c->execute($req, $c->accessToken);
            }elseif($product->platform == 'Sinde Home Delivery'){
                $shop = DB::table('Registered_store')->where('Platform', 'Sinde Home Delivery')->first();
                $access_token = $shop->access_token;
                $data[0]['product_id']=(string) $product->id;
                $data[0]['stock_value']=(int) $stock;
                $stockUpdateClient = new Client();
                $resUpdate = $stockUpdateClient->post('https://sindehomedelivery.com/api/stock/overwrite', ['headers'=>
                    ['Authorization' => "Bearer $access_token"],
                    'json'=>$data
                ])->getBody()->getContents();
                $responseLabel = json_decode($resLabel);
            }
        }
        // dd($getAllSindeProduct);
        echo 'sukses';
    }
    
    public function changeToko(){
        $getDatas = DB::table('statistik_discount')->where('id', '>', 2899)->get();
        foreach($getDatas as $data){
            $getStore = DB::table('Registered_store')->where('shop_id', $data->toko)->first();
            $update = DB::table('statistik_discount')->where('id', $data->id)->update([
                'toko'=>$getStore->store_name
            ]);
        }
        echo 'sukses';
    }
}
