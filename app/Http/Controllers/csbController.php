<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use Carbon\Carbon;
use DB;
class csbController extends Controller
{
    public function getOrder($startTime){
        $data = DB::table('bookingan')->where('bookingan.created_at', '>=',$startTime)->where('platform','!=','Blibli')->whereIn('toko', [223,233])->get();
        // $data = DB::table('order_from_marketplace')->where('order_from_marketplace.created_at', '>=',$startTime)->join('order_from_marketplace_detail', 'order_from_marketplace.id', 'order_from_marketplace_detail.orderId')->where('sku', 'CSB')->get();
        $countData = count($data);
        $tes = 'tes';
        return response()->json('tes');
        if($countData > 0){
            foreach($data as $key=>$value){
            // $getdetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->whereIn('sku', array('CSB', 'CSB-LECI', 'CSB-JAMBU', 'CSB-JERUK', 'STW', 'STW-LECI', 'STW-JAMBU', 'STW-JERUK'))->get();
            $getdetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->whereIn('sku', array('CSB', 'CSB-LECI', 'CSB-JAMBU', 'CSB-JERUK', 'BADAK-22364', 'BADAK-22312', 'PROMO-LECI', 'PROMO-JERUK','PROMO-JAMBU','STW', 'STW-LECI', 'STW-JAMBU', 'STW-JERUK','LPCB200ML', 'LPCB500ML'))->get();
            // $getdetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->where('item','LIKE', '%Gatotkaca%')->get();
                if(count($getdetail) > 0){
                    $data[$key]->order= $getdetail;
                }else{
                    // dd($value);
                    $data[$key]->order= [];
                }
            }
            $data = $data->filter(function ($item){
                return count($item->order) > 0;
            })->values();
        }
        
        return response()->json(compact('data'), 200);
    }
    
    public function getOrderStw(){
        $data = DB::table('bookingan')->whereDate('created_at', Carbon::today())->where('platform','!=','Blibli')->whereIn('toko', array(223,233))->get();
        $countData = count($data);
        if($countData > 0){
            foreach($data as $key=>$value){
                $getdetail = DB::table('order_from_marketplace_detail')->where('orderId', $value->id)->whereIn('sku', array('STW', 'STW-LECI', 'STW-JAMBU', 'STW-JERUK', 'WIB-LECI', 'WIB-JAMBU', 'WIB-JERUK'))->get();
                if(count($getdetail) > 0){
                    $data[$key]->order= $getdetail;
                }else{
                    $data[$key]->order= [];
                }
            }
            
            $data = $data->filter(function ($item){

                return count($item->order) > 0;
            })->values();
        }
        return response()->json(compact('data', 'dateNow'), 200);
    }
    
    public function getDataHarbolnas(){
        $clientGetDataHarbolnas = new Client();
        $resGetData = $clientGetDataHarbolnas->get("https://data.sobatbadak.club/api/getdata-harbolnas")->getBody();
        $responseGetData = json_decode($resGetData);
        // dd($responseGetData);
        foreach($responseGetData as $data){
            $check = DB::table('bookingan')->where('kode', $data->order_id)->count();
        echo $check.' '.$data->order_id.' <br/>';
        // echo $check;
            if($check < 1 ){
                $getStation = DB::table('station')->where('first_queue', 1)->first();
                $nextStation = $getStation->id + 1;
                if($nextStation > 2){
                    $nextStation = 1;
                    
                }
                $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                    'first_queue'=>0,
                ]);
                $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                    'first_queue'=>1,
                    'status'=>'empty',
                ]);
                $orderId = $data->id;
                $logistic_type = 'dropoff';
                $order_sn = $data->order_id;
                $pdfQr = new \PDF();
                $customPaperQr = array(0,0,270.28,270.64);
                $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                $nameQr = $orderId.'.pdf';
                \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                $content = $pdfQr->download($nameQr);
                $filePath = 'storage/sticker/'.$nameQr;
                
                // // label
                // $pdf = new \PDF();
                // // $customPaper = array(0,0,500.28,600.64);
                // $pdf = \PDF::loadView('admin.invoicePdf', compact('data'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
                // $name = 'Harbolnas-'.$orderId.'.pdf';
                // \Storage::put('public/label/'.$name, $pdf->output());
                // $path = "storage/label/$name";
                $insertData = DB::table('bookingan')->insertGetId([
                    'phone'=>$data->no_wa,
                    'nama_pemesan'=>$data->nama,
                    'jmlh_item'=>1,
                    'totalBayar'=>61000,
                    'kode'=>$data->order_id,
                    'platform'=>'harbolnas',
                    'kurir'=>'ID Express',
                    'alamat'=>$data->alamat,
                    'status'=>'logistic',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'logistic_type'=>'dropoff',
                    'status'=>'packing',
                    'station'=>$getStation->id,
                    'invoice_toko'=>$filePath,
                    // 'buktiBayar'=>$path
                ]);
                
                $insertItem = DB::table('order_from_marketplace_detail')->insert([
                    'orderId'=>$insertData,
                    'item'=>'Harbolnas LPCB bundle (isi 6) rasa random',
                    'qty'=>1,
                    'hargaSatuan'=>61000,
                    'hargaTotal'=>61000,
                    'sku'=>'HARBOLNAS-1',
                ]);
            }else{
                echo 'sudah ada <br/>';
            }

        }
        echo 'sukses';
    }
    
    public function orderHarbolnas(){
        $clientGetOrderHarbolnas = new Client();
        $resOrder = $clientGetOrderHarbolnas->get("https://data.sobatbadak.club/api/harbolnas-order")->getBody();
        $responseGetOrder = json_decode($resOrder);
        // dd($responseGetOrder, $resOrder);
        $orderTeratur = array();
        $orderCount = 0;
        foreach($responseGetOrder as $key=>$value){
            if($value->provinsi == 'jabodetabek' && $orderCount < 200){
                // dd($value->nama);
                // echo $value->order_id.'<br/>';
                $orderCount++;
                
                // qr
                $logistic_type = 'dropoff';
                $order_sn = $value->order_id;
                $pdfQr = new \PDF();
                $customPaperQr = array(0,0,270.28,270.64);
                $pdfQr = \PDF::loadView('admin.qr', compact('order_sn','logistic_type'))->setOptions(['defaultFont' => 'sans-serif'])->setPaper($customPaperQr);
                $nameQr = $orderId.'.pdf';
                \Storage::put('public/sticker/'.$nameQr, $pdfQr->output());
                $content = $pdfQr->download($nameQr);
                $filePath = 'storage/sticker/'.$nameQr;
                
                //label
                $pdf = new \PDF();
                $pdf = \PDF::loadView('admin.invoicePdf', compact('value'))->setOptions(['defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
                $name = 'Harbolnas-'.$id.'.pdf';;
                \Storage::put('public/label/'.$name, $pdf->output());
                $path = "storage/label/$name";
                
                // assign station
                $getStation = DB::table('station')->where('first_queue', 1)->first();
                $nextStation = $getStation->id + 1;
                if($nextStation > 2){
                    $nextStation = 1;
                    
                }
                
                $updateCurrentStation = DB::table('station')->where('id',$getStation->id)->update([
                    'first_queue'=>0,
                ]);
                $updateNextStation = DB::table('station')->where('id', $nextStation)->update([
                    'first_queue'=>1,
                    'status'=>'empty',
                ]);
                
                $insertOrder = DB::table('bookingan')->insertGetId([
                    'nama_pemesan'=>$value->nama,
                    'phone'=>$value->no_wa,
                    'jmlh_item'=>1,
                    'totalBayar'=>61000,
                    'kode'=>$value->order_id,
                    'platform'=>'harbolnas',
                    'kurir'=>'ID Express',
                    'toko'=>223,
                    'alamat'=>$value->alamat,
                    'buktiBayar'=>$path,
                    'invoice_toko'=>$filePath,
                    'status'=>'packing',
                    'created_at'=>date('Y-m-d H:i:s'),
                    'station'=>$getStation->id,
                    'logistic_type'=>$logistic_type,
                ]);
                
                $insertItem = DB::table('order_from_marketplace_detail')->insert([
                    'orderId'=>$insertOrder,
                    'item'=>'1 Bundle Larutan Penyegar Cap Badak',
                    'qty'=>1,
                    'harga_satuan'=>61000,
                    'harga_total'=>61000,
                    'sku'=>'HARBOLNAS-1',
                ]);
                array_push($orderTeratur, array_splice($responseGetOrder, $key, 1)[0]);
            //   dd('tes'); 
            }
        }
        dd($orderTeratur, $responseGetOrder);
    }
}
