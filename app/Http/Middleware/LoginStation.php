<?php

namespace App\Http\Middleware;

use Closure;

class LoginStation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(CRUDBooster::myId()){
            return $next($request);
        }else{
            // return redirect()->route('webPagesIndex');
            return \Redirect::to('https://teratur.warisangajahmada.com/admin/login');
        }
    }
}
