<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
   
      $schedule->call('App\Http\Controllers\tokopediaController@orderTokped')->everyMinute();
      $schedule->call('App\Http\Controllers\shopeeController@getShopeeOrder')->everyMinute();
      $schedule->call('App\Http\Controllers\SindeHomeDeliveryController@getListOrder')->everyMinute();
      $schedule->call('App\Http\Controllers\blibliController@getOrder')->everyMinute();
      $schedule->call('App\Http\Controllers\JdidController@jdidOrder')->everyMinute();
      
      
      $schedule->call('App\Http\Controllers\shopeeController@TeraturShopee')->everyMinute();

      $schedule->call('App\Http\Controllers\blibliController@getBlibliProduct')->dailyAt('00:00');
      $schedule->call('App\Http\Controllers\shopeeController@getShopeeProducts')->dailyAt('00:00');
      $schedule->call('App\Http\Controllers\tokopediaController@getProducts')->dailyAt('00:00');
      $schedule->call('App\Http\Controllers\JdidController@getProductsJDID')->dailyAt('00:00');
      $schedule->call('App\Http\Controllers\WarisanController@getProduct')->dailyAt('00:00');
      $schedule->call('App\Http\Controllers\SindeHomeDeliveryController@getProducts')->dailyAt('00:00');
      
    
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
